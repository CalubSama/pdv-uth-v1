﻿using Lib_pdv_uth_v1.clientes;
using Lib_pdv_uth_v1.usuarios;
using LibBD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib_pdv_uth_v1.creditos
{
    public class Credito: ICrud
    {
        public static string msgError;

        LibMySql bd;
        public int idCliente;
        public int id;
        string saldo;
        DateTime fechaApertura;
        string estado;
        DateTime fechaSaldado;

        public int IdCliente { get => idCliente; set => idCliente = value; }
        public int Id { get => id; set => id = value; }
        public string Saldo { get => saldo; set => saldo = value; }
        public DateTime FechaApertura { get => fechaApertura; set => fechaApertura = value; }
        public string Estado { get => estado; set => estado = value; }
        public DateTime FechaSaldado { get => fechaSaldado; set => fechaSaldado = value; }

        public Credito(string saldo, DateTime fechaApertura, int idCliente, string estado, DateTime fechaSaldado)
        {
            string fechaAperturaConv = this.FechaApertura.Year + "/" + this.FechaApertura.Month + "/" + this.FechaApertura.Day;
            string fechaSaldadoConv = this.FechaSaldado.Year + "/" + this.FechaSaldado.Month + "/" + this.FechaSaldado.Day;

            this.saldo = saldo;
            this.fechaApertura = fechaApertura;
            this.idCliente = idCliente;
            this.estado= estado;
            this.fechaSaldado = fechaSaldado;
            
            bd = new LibMySql("127.0.0.1", "root", "jctd", "pdv");
        }

        public Credito(int id)
        {
            bd = new LibMySql("127.0.0.1", "root", "jctd", "pdv");
        }

        public Credito()
        {

            bd = new LibMySql("127.0.0.1", "root", "jctd", "pdv");
        }

        public virtual  bool insertar(string saldo, string fechaAperturaConv, int idCliente, string estado, string fechaSaldadoConv)
        {
            bool res = false;
            //parametros convertidos a 0 checar getandsetters
            string valores = "'" + this.Saldo + "','"+ fechaAperturaConv + "','" + this.IdCliente+ "','" + this.Estado + "','" + fechaSaldadoConv+ "'";
            if (bd.insertar("creditos",
                            "saldo, fecha_apertura, cliente_id, estado, fecha_saldado",
                            valores))
            {
                res = true;
            }
            else
            {
                msgError = "Error al dar de alta nuevo Creditos. " + LibMySql.msgError;
            }
            return res;
        }

        public bool actualizar(string saldo, string fechaApertura, int idCliente, string estado, string fechaSaldado, string id)
        {

            bool res = false;
            string fechaAperturaConv = this.FechaApertura.Year + "/" + this.FechaApertura.Month + "/" + this.FechaApertura.Day;
            string fechaSaldadoConv = this.FechaSaldado.Year + "/" + this.FechaSaldado.Month + "/" + this.FechaSaldado.Day;

            if (bd.actualizar("creditos",
                              "saldo='" + fechaSaldadoConv + "',fecha_apertura='" + fechaAperturaConv +
                              "',cliente_id='" + idCliente + "',estado='" + estado + "'," +
                              " fecha_saldado='" + fechaSaldado + "'", " id=" + id)
                )
            { res = true; }
            else
            {
                msgError = "Error al actualizar Creditos. " + LibMySql.msgError;
            }
            return res;
        }



       /* public List<Credito> consultar(string saldo, DateTime fechaApertura, int idCliente, string estado)
        {
            string where = "saldo LIKE '%" + saldo + "%' AND ( fecha_apertura LIKE '%" + fechaApertura + "%' OR  cliente_id LIKE '%" + idCliente + "%') AND estado LIKE '%" + estado + "%'";
            List<object> listaTemp = bd.consultar("*", "creditos", where);
            List<Credito> listaCreditos = new List<Credito>();
            foreach (var registro in listaTemp)
            {
                for (int i = 0; i < listaTemp.Count; i++)
                {
*//*                    object[] arreglo = (object[])registro;
                    //creamos un objeto Creditos
                    Creditos temp = new Creditos
                    {
                        //poner todos los valores en el objeto Creditos temp
                        Id = int.Parse(arreglo[0].ToString()),
                        Saldo = arreglo[1].ToString(),
                        FechaApertura = DateTime.Parse(arreglo[2].ToString()),
                        IdCliente = int.Parse(arreglo[3].ToString()),
                        Estado = arreglo[4].ToString(),
                        FechaSalado = DateTime.Parse(arreglo[5].ToString())
                    };
                    //ponemos obj temp  en listaCreditos
                    listaCreditos.Add(temp);
*//*                }
            }
            return listaCreditos;
        }
*/
        public virtual bool alta()
        {
            //Parametros en 0 checar get and set
            //convertir a string
            string fechaAperturaConv = this.FechaApertura.Year + "/" + this.FechaApertura.Month + "/" + this.FechaApertura.Day;
            string fechaSaldadoConv = this.FechaSaldado.Year + "/" + this.FechaSaldado.Month + "/" + this.FechaSaldado.Day;
            return insertar(this.Saldo, fechaAperturaConv , this.IdCliente, this.Estado, fechaSaldadoConv);
        }


        public bool modificar(List<DatosParaActualizar> datos, int id)
        {
            //crear la losta de datos
            string camposValores = "";
            for (int i = 0; i < datos.Count; i++)
            {
                camposValores += " " + datos[i].campo + " = " + "'" + datos[i].valor + "'";
                if (i < datos.Count - 1) camposValores += ",";
            }
            //ejecuta el actualizar de BD con los datos
            return bd.actualizar("creditos", camposValores, "id=" + id);
            //regresar el res
        }

        public bool eliminar(int id)
        {
            return bd.eliminar("creditos", " id=" + id);
        }

        public List<object> consultar(List<CriteriosBusqueda> criteriosBusqueda)
        {
            List<object> res = new List<object>();
            string where = "";
            for (int i = 0; i < criteriosBusqueda.Count; i++)
            {
                string opIntermedio = "";
                switch (criteriosBusqueda[i].operadorIntermedio)
                {
                    case OperadorDeConsulta.IGUAL: opIntermedio = "="; break;
                    case OperadorDeConsulta.LIKE: opIntermedio = "LIKE"; break;
                    case OperadorDeConsulta.DIFERENTE: opIntermedio = "<>"; break;
                    case OperadorDeConsulta.NO_IGUAL: opIntermedio = "!="; break;
                    default: opIntermedio = ""; break;
                }
                string opFinal = "";
                switch (criteriosBusqueda[i].operadorIntermedio)
                {
                    case OperadorDeConsulta.AND: opFinal = "AND"; break;
                    case OperadorDeConsulta.OR: opFinal = "OR"; break;

                    default: opFinal = ""; break;
                }
                where += " " + criteriosBusqueda[i].campo + " " + opIntermedio + " " + criteriosBusqueda[i].valor + " " + opFinal + " ";
            }//for para hacer where
            //hacemos la consulta
            List<object> tmp = bd.consultar("*", "creditos", where);
            //mapeamos cada Object en un Creditos
            foreach (object[] cliTmp in tmp)
            {
                var cli = new
                {
                    Id = int.Parse(cliTmp[0].ToString()),
                    Saldo = cliTmp[1].ToString(),
                    FechaApertura = DateTime.Parse(cliTmp[2].ToString()),
                    IdCliente = int.Parse(cliTmp[3].ToString()),
                    Estado = cliTmp[4].ToString(),
                    FechaSalado = DateTime.Parse(cliTmp[5].ToString())
                };
                res.Add(cli);
            }
            //regresamos la lista de Creditos
            return res;
        }

        public List<Credito> consulta(List<CriteriosBusqueda> criteriosBusqueda)
        {
            //lista de object para resultado
            List<Credito> res = new List<Credito>();
            //hacemos el where
            string where = "";
            //insttterpretamos los operadores de cada condiciones del WHERE
            for (int i = 0; i < criteriosBusqueda.Count; i++)
                where += " " + criteriosBusqueda[i].campo + " " + criteriosBusqueda[i].opIntermedioSql() + " " + criteriosBusqueda[i].valor + " " + criteriosBusqueda[i].opFinalSql() + " ";
            //hacemos la consulta
            List<object> tmp = bd.consultar("*", "creditos", where);
            //mapeamos cada Object en un Creditos
            foreach (object[] cliTmp in tmp)
            {
                object cli = new
                {
                    Id = int.Parse(cliTmp[0].ToString()),
                    Saldo = cliTmp[1].ToString(),
                    FechaApertura = DateTime.Parse(cliTmp[2].ToString()),
                    IdCliente = int.Parse(cliTmp[3].ToString()),
                    Estado = cliTmp[4].ToString(),
                    FechaSalado = DateTime.Parse(cliTmp[5].ToString())
                };
                res.Add((Credito)cli);
            }
            //regresamos la lista de Creditos
            return res;
        }

    }
}
