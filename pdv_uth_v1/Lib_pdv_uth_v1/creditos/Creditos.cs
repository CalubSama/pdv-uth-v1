﻿using System;

namespace Lib_pdv_uth_v1.creditos
{
    internal class Creditos
    {
        public int Id { get; set; }
        public string Saldo { get; set; }
        public DateTime FechaApertura { get; set; }
        public int IdCliente { get; set; }
        public string Estado { get; set; }
        public DateTime FechaSalado { get; set; }
    }
}