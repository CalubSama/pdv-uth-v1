﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib_pdv_uth_v1.creditos
{
    public enum CreditoEstado
    {
        APERTURA,
        OPERANDO,
        CONGELADO,
        SALDADO
    }
}
