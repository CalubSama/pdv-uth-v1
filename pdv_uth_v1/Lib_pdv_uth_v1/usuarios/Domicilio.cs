public class Domicilio
{

    public Domicilio()
    {
    }

    public Domicilio(string calle, string numero, string colonia, string seccionFraccionamiento, string codigoPostal, string localidad, string municipio, object fotoComprobante)
    {
        this.calle = calle;
        this.numero = numero;
        this.colonia = colonia;
        this.seccionFraccionamiento = seccionFraccionamiento;
        this.codigoPostal = codigoPostal;
        this.localidad = localidad;
        this.municipio = municipio;
        this.fotoComprobante = fotoComprobante;
    }

    public string calle;

    public string numero;

    public string colonia;

    public string seccionFraccionamiento;

    public string codigoPostal;

    public string localidad;

    public string municipio;

    public object fotoComprobante;



}