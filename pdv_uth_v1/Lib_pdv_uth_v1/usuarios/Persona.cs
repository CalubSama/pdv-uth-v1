﻿using System;
namespace Lib_pdv_uth_v1.usuarios
{
    public class Persona
    {
        protected int id;
        protected string nombre;
        protected string apellidoPaterno;
        protected string apellidoMaterno;
        protected DateTime fechaNacimiento;
        protected string celular;
        protected string telefono;
        protected string correo;
        protected Domicilio domicilio;
        protected object comprobanteINE;
        protected string curp;
        protected object curpCompro;
        public Persona(int id, string nombre, string apellidoPaterno, DateTime fecha, string apellidoMaterno, string celular, string telefono, string correo, Domicilio domicilio, object comprobanteINE, string curp, object curpCompro)
        {
            this.Id = id;
            this.Nombre = nombre;
            this.ApellidoPaterno = apellidoPaterno;
            this.ApellidoMaterno = apellidoMaterno;
            this.fechaNacimiento = fecha;
            this.Celular = celular;
            this.Telefono = telefono;
            this.Correo = correo;
            this.Domicilio = domicilio;
            this.ComprobanteINE = comprobanteINE;
            this.Curp = curp;
            this.CurpCompro = curpCompro;
        }

        public int Id { get => id; set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string ApellidoPaterno { get => apellidoPaterno; set => apellidoPaterno = value; }
        public string ApellidoMaterno { get => apellidoMaterno; set => apellidoMaterno = value; }
        public DateTime FechaNacimiento { get => fechaNacimiento; set => fechaNacimiento = value; }
        public string Celular { get => celular; set => celular = value; }
        public string Telefono { get => telefono; set => telefono = value; }
        public string Correo { get => correo; set => correo = value; }
        public Domicilio Domicilio { get => domicilio; set => domicilio = value; }
        public object ComprobanteINE { get => comprobanteINE; set => comprobanteINE = value; }
        public string Curp { get => curp; set => curp = value; }
        public object CurpCompro { get => curpCompro; set => curpCompro = value; }
    }
}
