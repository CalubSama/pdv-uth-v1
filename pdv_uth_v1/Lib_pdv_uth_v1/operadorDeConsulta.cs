﻿namespace Lib_pdv_uth_v1
{
    public enum OperadorDeConsulta
    {
        AND,
        OR,
        LIKE,
        IGUAL,
        DIFERENTE,
        NO_IGUAL,
        NINGUNO
    }
}
