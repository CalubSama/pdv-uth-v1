﻿namespace pdv_uth_v1
{
    partial class FrmPersonaSecundaria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPersonaSecundaria));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelForm = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblImagenOriginal2 = new System.Windows.Forms.Label();
            this.lblImagenAGuardar2 = new System.Windows.Forms.Label();
            this.lblImagenAGuardar1 = new System.Windows.Forms.Label();
            this.lblImagenOriginal1 = new System.Windows.Forms.Label();
            this.lblImagenAGuardar = new System.Windows.Forms.Label();
            this.lblImagenOriginal = new System.Windows.Forms.Label();
            this.btnModificarPerAltern = new System.Windows.Forms.Button();
            this.btnGuardarPerAltern = new System.Windows.Forms.Button();
            this.btnEliminarPerAltern = new System.Windows.Forms.Button();
            this.btnApagar = new System.Windows.Forms.Button();
            this.panelFormulario = new System.Windows.Forms.Panel();
            this.dgPersonaSecundaria = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlImgs = new System.Windows.Forms.Panel();
            this.pnlImgINE = new System.Windows.Forms.PictureBox();
            this.pnlImgCURP = new System.Windows.Forms.PictureBox();
            this.picComprobantes = new System.Windows.Forms.PictureBox();
            this.pnlMostrarINE = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.btnImgComINE = new System.Windows.Forms.Button();
            this.txtComproINE = new System.Windows.Forms.TextBox();
            this.pnlMostrarCURP = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.btnImgCCurp = new System.Windows.Forms.Button();
            this.txtComprobanteCurp = new System.Windows.Forms.TextBox();
            this.pnlMostarPicDom = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.btnImgCDom = new System.Windows.Forms.Button();
            this.txtComprobanteDomicilio = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtLocalidad = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtCurp = new System.Windows.Forms.TextBox();
            this.lblCalle = new System.Windows.Forms.Label();
            this.txtCalle = new System.Windows.Forms.TextBox();
            this.dtpFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.txtCP = new System.Windows.Forms.TextBox();
            this.txtNumCasa = new System.Windows.Forms.TextBox();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.txtFraccionamiento = new System.Windows.Forms.TextBox();
            this.txtMunicipio = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.txtColonia = new System.Windows.Forms.TextBox();
            this.txtCorreo = new System.Windows.Forms.TextBox();
            this.txtCelular = new System.Windows.Forms.TextBox();
            this.txtApMat = new System.Windows.Forms.TextBox();
            this.txtApPat = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblidCliente = new System.Windows.Forms.Label();
            this.picUserImage = new System.Windows.Forms.PictureBox();
            this.lblNombrepersonaSecundaria = new System.Windows.Forms.Label();
            this.btnRetroceder = new System.Windows.Forms.Button();
            this.Cerrar = new System.Windows.Forms.Button();
            this.openFileDialogComprobantes = new System.Windows.Forms.OpenFileDialog();
            this.panelForm.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panelFormulario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPersonaSecundaria)).BeginInit();
            this.panel2.SuspendLayout();
            this.pnlImgs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlImgINE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlImgCURP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picComprobantes)).BeginInit();
            this.pnlMostrarINE.SuspendLayout();
            this.pnlMostrarCURP.SuspendLayout();
            this.pnlMostarPicDom.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUserImage)).BeginInit();
            this.SuspendLayout();
            // 
            // panelForm
            // 
            this.panelForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.panelForm.Controls.Add(this.panel1);
            this.panelForm.Controls.Add(this.btnApagar);
            this.panelForm.Controls.Add(this.panelFormulario);
            this.panelForm.Controls.Add(this.panel5);
            this.panelForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelForm.Location = new System.Drawing.Point(0, 0);
            this.panelForm.Margin = new System.Windows.Forms.Padding(2);
            this.panelForm.Name = "panelForm";
            this.panelForm.Size = new System.Drawing.Size(800, 593);
            this.panelForm.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 513);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 80);
            this.panel1.TabIndex = 43;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            this.panel4.Controls.Add(this.lblImagenOriginal2);
            this.panel4.Controls.Add(this.lblImagenAGuardar2);
            this.panel4.Controls.Add(this.lblImagenAGuardar1);
            this.panel4.Controls.Add(this.lblImagenOriginal1);
            this.panel4.Controls.Add(this.lblImagenAGuardar);
            this.panel4.Controls.Add(this.lblImagenOriginal);
            this.panel4.Controls.Add(this.btnModificarPerAltern);
            this.panel4.Controls.Add(this.btnGuardarPerAltern);
            this.panel4.Controls.Add(this.btnEliminarPerAltern);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(796, 76);
            this.panel4.TabIndex = 44;
            // 
            // lblImagenOriginal2
            // 
            this.lblImagenOriginal2.AutoSize = true;
            this.lblImagenOriginal2.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenOriginal2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenOriginal2.Location = new System.Drawing.Point(62, 43);
            this.lblImagenOriginal2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenOriginal2.Name = "lblImagenOriginal2";
            this.lblImagenOriginal2.Size = new System.Drawing.Size(113, 15);
            this.lblImagenOriginal2.TabIndex = 73;
            this.lblImagenOriginal2.Text = "Nombre Original";
            this.lblImagenOriginal2.Visible = false;
            // 
            // lblImagenAGuardar2
            // 
            this.lblImagenAGuardar2.AutoSize = true;
            this.lblImagenAGuardar2.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenAGuardar2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenAGuardar2.Location = new System.Drawing.Point(62, 66);
            this.lblImagenAGuardar2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenAGuardar2.Name = "lblImagenAGuardar2";
            this.lblImagenAGuardar2.Size = new System.Drawing.Size(113, 15);
            this.lblImagenAGuardar2.TabIndex = 72;
            this.lblImagenAGuardar2.Text = "Nombre Original";
            this.lblImagenAGuardar2.Visible = false;
            // 
            // lblImagenAGuardar1
            // 
            this.lblImagenAGuardar1.AutoSize = true;
            this.lblImagenAGuardar1.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenAGuardar1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenAGuardar1.Location = new System.Drawing.Point(-14, 20);
            this.lblImagenAGuardar1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenAGuardar1.Name = "lblImagenAGuardar1";
            this.lblImagenAGuardar1.Size = new System.Drawing.Size(113, 15);
            this.lblImagenAGuardar1.TabIndex = 71;
            this.lblImagenAGuardar1.Text = "Nombre Original";
            this.lblImagenAGuardar1.Visible = false;
            // 
            // lblImagenOriginal1
            // 
            this.lblImagenOriginal1.AutoSize = true;
            this.lblImagenOriginal1.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenOriginal1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenOriginal1.Location = new System.Drawing.Point(-14, 43);
            this.lblImagenOriginal1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenOriginal1.Name = "lblImagenOriginal1";
            this.lblImagenOriginal1.Size = new System.Drawing.Size(113, 15);
            this.lblImagenOriginal1.TabIndex = 70;
            this.lblImagenOriginal1.Text = "Nombre Original";
            this.lblImagenOriginal1.Visible = false;
            // 
            // lblImagenAGuardar
            // 
            this.lblImagenAGuardar.AutoSize = true;
            this.lblImagenAGuardar.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenAGuardar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenAGuardar.Location = new System.Drawing.Point(103, 23);
            this.lblImagenAGuardar.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenAGuardar.Name = "lblImagenAGuardar";
            this.lblImagenAGuardar.Size = new System.Drawing.Size(113, 15);
            this.lblImagenAGuardar.TabIndex = 69;
            this.lblImagenAGuardar.Text = "Nombre Original";
            this.lblImagenAGuardar.Visible = false;
            // 
            // lblImagenOriginal
            // 
            this.lblImagenOriginal.AutoSize = true;
            this.lblImagenOriginal.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenOriginal.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenOriginal.Location = new System.Drawing.Point(103, 46);
            this.lblImagenOriginal.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenOriginal.Name = "lblImagenOriginal";
            this.lblImagenOriginal.Size = new System.Drawing.Size(113, 15);
            this.lblImagenOriginal.TabIndex = 68;
            this.lblImagenOriginal.Text = "Nombre Original";
            this.lblImagenOriginal.Visible = false;
            // 
            // btnModificarPerAltern
            // 
            this.btnModificarPerAltern.FlatAppearance.BorderSize = 2;
            this.btnModificarPerAltern.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnModificarPerAltern.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnModificarPerAltern.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModificarPerAltern.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificarPerAltern.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnModificarPerAltern.Image = ((System.Drawing.Image)(resources.GetObject("btnModificarPerAltern.Image")));
            this.btnModificarPerAltern.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnModificarPerAltern.Location = new System.Drawing.Point(342, 13);
            this.btnModificarPerAltern.Name = "btnModificarPerAltern";
            this.btnModificarPerAltern.Size = new System.Drawing.Size(122, 57);
            this.btnModificarPerAltern.TabIndex = 42;
            this.btnModificarPerAltern.Text = "Modificar Persona";
            this.btnModificarPerAltern.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnModificarPerAltern.UseVisualStyleBackColor = true;
            this.btnModificarPerAltern.Click += new System.EventHandler(this.btnModificarPerAltern_Click);
            // 
            // btnGuardarPerAltern
            // 
            this.btnGuardarPerAltern.FlatAppearance.BorderSize = 2;
            this.btnGuardarPerAltern.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnGuardarPerAltern.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnGuardarPerAltern.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardarPerAltern.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardarPerAltern.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnGuardarPerAltern.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardarPerAltern.Image")));
            this.btnGuardarPerAltern.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardarPerAltern.Location = new System.Drawing.Point(573, 12);
            this.btnGuardarPerAltern.Name = "btnGuardarPerAltern";
            this.btnGuardarPerAltern.Size = new System.Drawing.Size(122, 57);
            this.btnGuardarPerAltern.TabIndex = 40;
            this.btnGuardarPerAltern.Text = "Guardar";
            this.btnGuardarPerAltern.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGuardarPerAltern.UseVisualStyleBackColor = true;
            this.btnGuardarPerAltern.Click += new System.EventHandler(this.btnGuardarPerAltern_Click);
            // 
            // btnEliminarPerAltern
            // 
            this.btnEliminarPerAltern.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEliminarPerAltern.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            this.btnEliminarPerAltern.FlatAppearance.BorderSize = 2;
            this.btnEliminarPerAltern.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnEliminarPerAltern.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnEliminarPerAltern.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarPerAltern.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarPerAltern.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnEliminarPerAltern.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminarPerAltern.Image")));
            this.btnEliminarPerAltern.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarPerAltern.Location = new System.Drawing.Point(116, 12);
            this.btnEliminarPerAltern.Name = "btnEliminarPerAltern";
            this.btnEliminarPerAltern.Size = new System.Drawing.Size(122, 57);
            this.btnEliminarPerAltern.TabIndex = 39;
            this.btnEliminarPerAltern.Text = "Eliminar   Persona";
            this.btnEliminarPerAltern.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEliminarPerAltern.UseVisualStyleBackColor = false;
            this.btnEliminarPerAltern.Click += new System.EventHandler(this.btnEliminarPerAltern_Click);
            // 
            // btnApagar
            // 
            this.btnApagar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnApagar.FlatAppearance.BorderSize = 0;
            this.btnApagar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnApagar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApagar.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApagar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnApagar.Image = ((System.Drawing.Image)(resources.GetObject("btnApagar.Image")));
            this.btnApagar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnApagar.Location = new System.Drawing.Point(485, -57);
            this.btnApagar.Name = "btnApagar";
            this.btnApagar.Size = new System.Drawing.Size(53, 54);
            this.btnApagar.TabIndex = 37;
            this.btnApagar.Text = "Salir";
            this.btnApagar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnApagar.UseVisualStyleBackColor = true;
            // 
            // panelFormulario
            // 
            this.panelFormulario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            this.panelFormulario.Controls.Add(this.dgPersonaSecundaria);
            this.panelFormulario.Controls.Add(this.panel2);
            this.panelFormulario.Controls.Add(this.pnlMostrarINE);
            this.panelFormulario.Controls.Add(this.pnlMostrarCURP);
            this.panelFormulario.Controls.Add(this.pnlMostarPicDom);
            this.panelFormulario.Controls.Add(this.label26);
            this.panelFormulario.Controls.Add(this.txtLocalidad);
            this.panelFormulario.Controls.Add(this.label27);
            this.panelFormulario.Controls.Add(this.txtCurp);
            this.panelFormulario.Controls.Add(this.lblCalle);
            this.panelFormulario.Controls.Add(this.txtCalle);
            this.panelFormulario.Controls.Add(this.dtpFechaNacimiento);
            this.panelFormulario.Controls.Add(this.label28);
            this.panelFormulario.Controls.Add(this.label29);
            this.panelFormulario.Controls.Add(this.label30);
            this.panelFormulario.Controls.Add(this.label31);
            this.panelFormulario.Controls.Add(this.label32);
            this.panelFormulario.Controls.Add(this.label33);
            this.panelFormulario.Controls.Add(this.txtCP);
            this.panelFormulario.Controls.Add(this.txtNumCasa);
            this.panelFormulario.Controls.Add(this.txtTelefono);
            this.panelFormulario.Controls.Add(this.txtFraccionamiento);
            this.panelFormulario.Controls.Add(this.txtMunicipio);
            this.panelFormulario.Controls.Add(this.label34);
            this.panelFormulario.Controls.Add(this.label35);
            this.panelFormulario.Controls.Add(this.label36);
            this.panelFormulario.Controls.Add(this.label37);
            this.panelFormulario.Controls.Add(this.label38);
            this.panelFormulario.Controls.Add(this.lblNombre);
            this.panelFormulario.Controls.Add(this.txtColonia);
            this.panelFormulario.Controls.Add(this.txtCorreo);
            this.panelFormulario.Controls.Add(this.txtCelular);
            this.panelFormulario.Controls.Add(this.txtApMat);
            this.panelFormulario.Controls.Add(this.txtApPat);
            this.panelFormulario.Controls.Add(this.txtNombre);
            this.panelFormulario.Location = new System.Drawing.Point(0, 78);
            this.panelFormulario.Name = "panelFormulario";
            this.panelFormulario.Size = new System.Drawing.Size(800, 443);
            this.panelFormulario.TabIndex = 40;
            // 
            // dgPersonaSecundaria
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.dgPersonaSecundaria.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgPersonaSecundaria.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            this.dgPersonaSecundaria.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgPersonaSecundaria.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgPersonaSecundaria.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.HotTrack;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPersonaSecundaria.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgPersonaSecundaria.ColumnHeadersHeight = 30;
            this.dgPersonaSecundaria.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgPersonaSecundaria.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgPersonaSecundaria.EnableHeadersVisualStyles = false;
            this.dgPersonaSecundaria.GridColor = System.Drawing.Color.SteelBlue;
            this.dgPersonaSecundaria.Location = new System.Drawing.Point(0, 250);
            this.dgPersonaSecundaria.Name = "dgPersonaSecundaria";
            this.dgPersonaSecundaria.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPersonaSecundaria.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            this.dgPersonaSecundaria.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgPersonaSecundaria.Size = new System.Drawing.Size(800, 185);
            this.dgPersonaSecundaria.TabIndex = 61;
            this.dgPersonaSecundaria.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgProductos_CellContentClick);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pnlImgs);
            this.panel2.Location = new System.Drawing.Point(4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(309, 249);
            this.panel2.TabIndex = 60;
            // 
            // pnlImgs
            // 
            this.pnlImgs.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlImgs.Controls.Add(this.pnlImgINE);
            this.pnlImgs.Controls.Add(this.pnlImgCURP);
            this.pnlImgs.Controls.Add(this.picComprobantes);
            this.pnlImgs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlImgs.Location = new System.Drawing.Point(0, 0);
            this.pnlImgs.Name = "pnlImgs";
            this.pnlImgs.Size = new System.Drawing.Size(309, 249);
            this.pnlImgs.TabIndex = 43;
            // 
            // pnlImgINE
            // 
            this.pnlImgINE.BackColor = System.Drawing.SystemColors.GrayText;
            this.pnlImgINE.Image = ((System.Drawing.Image)(resources.GetObject("pnlImgINE.Image")));
            this.pnlImgINE.Location = new System.Drawing.Point(38, 11);
            this.pnlImgINE.Name = "pnlImgINE";
            this.pnlImgINE.Size = new System.Drawing.Size(215, 225);
            this.pnlImgINE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pnlImgINE.TabIndex = 3;
            this.pnlImgINE.TabStop = false;
            this.pnlImgINE.Visible = false;
            // 
            // pnlImgCURP
            // 
            this.pnlImgCURP.BackColor = System.Drawing.SystemColors.GrayText;
            this.pnlImgCURP.Image = ((System.Drawing.Image)(resources.GetObject("pnlImgCURP.Image")));
            this.pnlImgCURP.Location = new System.Drawing.Point(38, 9);
            this.pnlImgCURP.Name = "pnlImgCURP";
            this.pnlImgCURP.Size = new System.Drawing.Size(215, 227);
            this.pnlImgCURP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pnlImgCURP.TabIndex = 2;
            this.pnlImgCURP.TabStop = false;
            this.pnlImgCURP.Visible = false;
            // 
            // picComprobantes
            // 
            this.picComprobantes.BackColor = System.Drawing.SystemColors.GrayText;
            this.picComprobantes.Image = ((System.Drawing.Image)(resources.GetObject("picComprobantes.Image")));
            this.picComprobantes.Location = new System.Drawing.Point(38, 11);
            this.picComprobantes.Name = "picComprobantes";
            this.picComprobantes.Size = new System.Drawing.Size(215, 225);
            this.picComprobantes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picComprobantes.TabIndex = 1;
            this.picComprobantes.TabStop = false;
            // 
            // pnlMostrarINE
            // 
            this.pnlMostrarINE.Controls.Add(this.label2);
            this.pnlMostrarINE.Controls.Add(this.btnImgComINE);
            this.pnlMostrarINE.Controls.Add(this.txtComproINE);
            this.pnlMostrarINE.Location = new System.Drawing.Point(654, 184);
            this.pnlMostrarINE.Name = "pnlMostrarINE";
            this.pnlMostrarINE.Size = new System.Drawing.Size(146, 69);
            this.pnlMostrarINE.TabIndex = 59;
            this.pnlMostrarINE.MouseEnter += new System.EventHandler(this.pnlMostrarINE_MouseEnter);
            this.pnlMostrarINE.MouseLeave += new System.EventHandler(this.pnlMostrarINE_MouseLeave);
            this.pnlMostrarINE.MouseHover += new System.EventHandler(this.pnlMostrarINE_MouseHover);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.GhostWhite;
            this.label2.Location = new System.Drawing.Point(2, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 14);
            this.label2.TabIndex = 52;
            this.label2.Text = "Comprobante INE";
            // 
            // btnImgComINE
            // 
            this.btnImgComINE.FlatAppearance.BorderSize = 0;
            this.btnImgComINE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImgComINE.Image = ((System.Drawing.Image)(resources.GetObject("btnImgComINE.Image")));
            this.btnImgComINE.Location = new System.Drawing.Point(114, 17);
            this.btnImgComINE.Name = "btnImgComINE";
            this.btnImgComINE.Size = new System.Drawing.Size(23, 27);
            this.btnImgComINE.TabIndex = 56;
            this.btnImgComINE.Text = "...";
            this.btnImgComINE.UseVisualStyleBackColor = true;
            this.btnImgComINE.Click += new System.EventHandler(this.btnImgComINE_Click);
            // 
            // txtComproINE
            // 
            this.txtComproINE.Location = new System.Drawing.Point(5, 22);
            this.txtComproINE.Name = "txtComproINE";
            this.txtComproINE.Size = new System.Drawing.Size(108, 20);
            this.txtComproINE.TabIndex = 15;
            this.txtComproINE.TextChanged += new System.EventHandler(this.txtComproINE_TextChanged);
            // 
            // pnlMostrarCURP
            // 
            this.pnlMostrarCURP.Controls.Add(this.label3);
            this.pnlMostrarCURP.Controls.Add(this.btnImgCCurp);
            this.pnlMostrarCURP.Controls.Add(this.txtComprobanteCurp);
            this.pnlMostrarCURP.Location = new System.Drawing.Point(316, 182);
            this.pnlMostrarCURP.Name = "pnlMostrarCURP";
            this.pnlMostrarCURP.Size = new System.Drawing.Size(162, 71);
            this.pnlMostrarCURP.TabIndex = 58;
            this.pnlMostrarCURP.MouseLeave += new System.EventHandler(this.pnlMostrarCURP_MouseLeave);
            this.pnlMostrarCURP.MouseHover += new System.EventHandler(this.pnlMostrarCURP_MouseHover);
            this.pnlMostrarCURP.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlMostrarCURP_MouseMove);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.GhostWhite;
            this.label3.Location = new System.Drawing.Point(3, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 14);
            this.label3.TabIndex = 54;
            this.label3.Text = "Comprobante CURP";
            // 
            // btnImgCCurp
            // 
            this.btnImgCCurp.FlatAppearance.BorderSize = 0;
            this.btnImgCCurp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImgCCurp.Image = ((System.Drawing.Image)(resources.GetObject("btnImgCCurp.Image")));
            this.btnImgCCurp.Location = new System.Drawing.Point(117, 15);
            this.btnImgCCurp.Name = "btnImgCCurp";
            this.btnImgCCurp.Size = new System.Drawing.Size(23, 27);
            this.btnImgCCurp.TabIndex = 57;
            this.btnImgCCurp.Text = "...";
            this.btnImgCCurp.UseVisualStyleBackColor = true;
            this.btnImgCCurp.Click += new System.EventHandler(this.btnImgCCurp_Click);
            // 
            // txtComprobanteCurp
            // 
            this.txtComprobanteCurp.Location = new System.Drawing.Point(7, 19);
            this.txtComprobanteCurp.Name = "txtComprobanteCurp";
            this.txtComprobanteCurp.Size = new System.Drawing.Size(108, 20);
            this.txtComprobanteCurp.TabIndex = 17;
            this.txtComprobanteCurp.TextChanged += new System.EventHandler(this.txtComprobanteCurp_TextChanged);
            // 
            // pnlMostarPicDom
            // 
            this.pnlMostarPicDom.Controls.Add(this.label4);
            this.pnlMostarPicDom.Controls.Add(this.btnImgCDom);
            this.pnlMostarPicDom.Controls.Add(this.txtComprobanteDomicilio);
            this.pnlMostarPicDom.Location = new System.Drawing.Point(484, 183);
            this.pnlMostarPicDom.Name = "pnlMostarPicDom";
            this.pnlMostarPicDom.Size = new System.Drawing.Size(165, 70);
            this.pnlMostarPicDom.TabIndex = 57;
            this.pnlMostarPicDom.MouseEnter += new System.EventHandler(this.pnlMostarPicDom_MouseEnter);
            this.pnlMostarPicDom.MouseLeave += new System.EventHandler(this.pnlMostarPicDom_MouseLeave);
            this.pnlMostarPicDom.MouseHover += new System.EventHandler(this.pnlMostarPicDom_MouseHover);
            this.pnlMostarPicDom.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlMostarPicDom_MouseMove);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.GhostWhite;
            this.label4.Location = new System.Drawing.Point(1, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 14);
            this.label4.TabIndex = 46;
            this.label4.Text = "Comprobante dom";
            // 
            // btnImgCDom
            // 
            this.btnImgCDom.FlatAppearance.BorderSize = 0;
            this.btnImgCDom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImgCDom.Image = ((System.Drawing.Image)(resources.GetObject("btnImgCDom.Image")));
            this.btnImgCDom.Location = new System.Drawing.Point(115, 12);
            this.btnImgCDom.Name = "btnImgCDom";
            this.btnImgCDom.Size = new System.Drawing.Size(23, 27);
            this.btnImgCDom.TabIndex = 55;
            this.btnImgCDom.UseVisualStyleBackColor = true;
            this.btnImgCDom.Click += new System.EventHandler(this.btnImgCDom_Click);
            // 
            // txtComprobanteDomicilio
            // 
            this.txtComprobanteDomicilio.Location = new System.Drawing.Point(6, 19);
            this.txtComprobanteDomicilio.Name = "txtComprobanteDomicilio";
            this.txtComprobanteDomicilio.Size = new System.Drawing.Size(108, 20);
            this.txtComprobanteDomicilio.TabIndex = 14;
            this.txtComprobanteDomicilio.TextChanged += new System.EventHandler(this.txtComprobanteDomicilio_TextChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.GhostWhite;
            this.label26.Location = new System.Drawing.Point(708, 146);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(60, 14);
            this.label26.TabIndex = 50;
            this.label26.Text = "Localidad";
            // 
            // txtLocalidad
            // 
            this.txtLocalidad.Location = new System.Drawing.Point(682, 164);
            this.txtLocalidad.Name = "txtLocalidad";
            this.txtLocalidad.Size = new System.Drawing.Size(108, 20);
            this.txtLocalidad.TabIndex = 7;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.GhostWhite;
            this.label27.Location = new System.Drawing.Point(717, 98);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(39, 14);
            this.label27.TabIndex = 48;
            this.label27.Text = "CURP";
            // 
            // txtCurp
            // 
            this.txtCurp.Location = new System.Drawing.Point(682, 117);
            this.txtCurp.Name = "txtCurp";
            this.txtCurp.Size = new System.Drawing.Size(108, 20);
            this.txtCurp.TabIndex = 8;
            // 
            // lblCalle
            // 
            this.lblCalle.AutoSize = true;
            this.lblCalle.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalle.ForeColor = System.Drawing.Color.GhostWhite;
            this.lblCalle.Location = new System.Drawing.Point(591, 98);
            this.lblCalle.Name = "lblCalle";
            this.lblCalle.Size = new System.Drawing.Size(36, 14);
            this.lblCalle.TabIndex = 44;
            this.lblCalle.Text = "Calle";
            // 
            // txtCalle
            // 
            this.txtCalle.Location = new System.Drawing.Point(555, 116);
            this.txtCalle.Name = "txtCalle";
            this.txtCalle.Size = new System.Drawing.Size(108, 20);
            this.txtCalle.TabIndex = 6;
            // 
            // dtpFechaNacimiento
            // 
            this.dtpFechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaNacimiento.Location = new System.Drawing.Point(437, 35);
            this.dtpFechaNacimiento.Name = "dtpFechaNacimiento";
            this.dtpFechaNacimiento.Size = new System.Drawing.Size(108, 20);
            this.dtpFechaNacimiento.TabIndex = 9;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.GhostWhite;
            this.label28.Location = new System.Drawing.Point(684, 60);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(84, 14);
            this.label28.TabIndex = 41;
            this.label28.Text = "Codigo Postal";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.GhostWhite;
            this.label29.Location = new System.Drawing.Point(677, 23);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(101, 14);
            this.label29.TabIndex = 40;
            this.label29.Text = "Numero de Casa";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.GhostWhite;
            this.label30.Location = new System.Drawing.Point(580, 141);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(58, 14);
            this.label30.TabIndex = 39;
            this.label30.Text = "Telefono";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.GhostWhite;
            this.label31.Location = new System.Drawing.Point(438, 101);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(101, 14);
            this.label31.TabIndex = 38;
            this.label31.Text = "Fraccionamiento";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.GhostWhite;
            this.label32.Location = new System.Drawing.Point(442, 56);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(60, 14);
            this.label32.TabIndex = 37;
            this.label32.Text = "Municipio";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.GhostWhite;
            this.label33.Location = new System.Drawing.Point(411, 18);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(126, 14);
            this.label33.TabIndex = 36;
            this.label33.Text = "Fecha De Nacimiento";
            // 
            // txtCP
            // 
            this.txtCP.Location = new System.Drawing.Point(679, 77);
            this.txtCP.Name = "txtCP";
            this.txtCP.Size = new System.Drawing.Size(108, 20);
            this.txtCP.TabIndex = 14;
            // 
            // txtNumCasa
            // 
            this.txtNumCasa.Location = new System.Drawing.Point(679, 37);
            this.txtNumCasa.Name = "txtNumCasa";
            this.txtNumCasa.Size = new System.Drawing.Size(108, 20);
            this.txtNumCasa.TabIndex = 13;
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(558, 159);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(108, 20);
            this.txtTelefono.TabIndex = 12;
            // 
            // txtFraccionamiento
            // 
            this.txtFraccionamiento.Location = new System.Drawing.Point(434, 118);
            this.txtFraccionamiento.Name = "txtFraccionamiento";
            this.txtFraccionamiento.Size = new System.Drawing.Size(108, 20);
            this.txtFraccionamiento.TabIndex = 11;
            // 
            // txtMunicipio
            // 
            this.txtMunicipio.Location = new System.Drawing.Point(437, 71);
            this.txtMunicipio.Name = "txtMunicipio";
            this.txtMunicipio.Size = new System.Drawing.Size(108, 20);
            this.txtMunicipio.TabIndex = 10;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.GhostWhite;
            this.label34.Location = new System.Drawing.Point(582, 59);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(50, 14);
            this.label34.TabIndex = 29;
            this.label34.Text = "Colonia";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.GhostWhite;
            this.label35.Location = new System.Drawing.Point(554, 23);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(112, 14);
            this.label35.TabIndex = 28;
            this.label35.Text = "Correo Electronico";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.GhostWhite;
            this.label36.Location = new System.Drawing.Point(457, 141);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(51, 14);
            this.label36.TabIndex = 27;
            this.label36.Text = "Celular ";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.GhostWhite;
            this.label37.Location = new System.Drawing.Point(323, 122);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(102, 14);
            this.label37.TabIndex = 26;
            this.label37.Text = "Apellido Materno";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.GhostWhite;
            this.label38.Location = new System.Drawing.Point(324, 84);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(99, 14);
            this.label38.TabIndex = 25;
            this.label38.Text = "Apellido Paterno";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.ForeColor = System.Drawing.Color.GhostWhite;
            this.lblNombre.Location = new System.Drawing.Point(324, 46);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(55, 14);
            this.lblNombre.TabIndex = 24;
            this.lblNombre.Text = "Nombre ";
            // 
            // txtColonia
            // 
            this.txtColonia.Location = new System.Drawing.Point(556, 77);
            this.txtColonia.Name = "txtColonia";
            this.txtColonia.Size = new System.Drawing.Size(108, 20);
            this.txtColonia.TabIndex = 5;
            // 
            // txtCorreo
            // 
            this.txtCorreo.Location = new System.Drawing.Point(556, 37);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Size = new System.Drawing.Size(108, 20);
            this.txtCorreo.TabIndex = 4;
            // 
            // txtCelular
            // 
            this.txtCelular.Location = new System.Drawing.Point(436, 155);
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(108, 20);
            this.txtCelular.TabIndex = 3;
            // 
            // txtApMat
            // 
            this.txtApMat.Location = new System.Drawing.Point(319, 139);
            this.txtApMat.Name = "txtApMat";
            this.txtApMat.Size = new System.Drawing.Size(108, 20);
            this.txtApMat.TabIndex = 2;
            // 
            // txtApPat
            // 
            this.txtApPat.Location = new System.Drawing.Point(319, 99);
            this.txtApPat.Name = "txtApPat";
            this.txtApPat.Size = new System.Drawing.Size(108, 20);
            this.txtApPat.TabIndex = 1;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(319, 63);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(108, 20);
            this.txtNombre.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Controls.Add(this.btnRetroceder);
            this.panel5.Controls.Add(this.Cerrar);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(800, 77);
            this.panel5.TabIndex = 42;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            this.panel6.Controls.Add(this.lblidCliente);
            this.panel6.Controls.Add(this.picUserImage);
            this.panel6.Controls.Add(this.lblNombrepersonaSecundaria);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(55, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(692, 77);
            this.panel6.TabIndex = 49;
            // 
            // lblidCliente
            // 
            this.lblidCliente.AutoSize = true;
            this.lblidCliente.ForeColor = System.Drawing.Color.White;
            this.lblidCliente.Location = new System.Drawing.Point(409, 44);
            this.lblidCliente.Name = "lblidCliente";
            this.lblidCliente.Size = new System.Drawing.Size(66, 13);
            this.lblidCliente.TabIndex = 24;
            this.lblidCliente.Text = "ID CLIENTE";
            this.lblidCliente.Visible = false;
            this.lblidCliente.Click += new System.EventHandler(this.lblidCliente_Click);
            // 
            // picUserImage
            // 
            this.picUserImage.Image = ((System.Drawing.Image)(resources.GetObject("picUserImage.Image")));
            this.picUserImage.Location = new System.Drawing.Point(260, 3);
            this.picUserImage.Name = "picUserImage";
            this.picUserImage.Size = new System.Drawing.Size(90, 91);
            this.picUserImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picUserImage.TabIndex = 22;
            this.picUserImage.TabStop = false;
            // 
            // lblNombrepersonaSecundaria
            // 
            this.lblNombrepersonaSecundaria.AutoSize = true;
            this.lblNombrepersonaSecundaria.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombrepersonaSecundaria.ForeColor = System.Drawing.Color.GhostWhite;
            this.lblNombrepersonaSecundaria.Location = new System.Drawing.Point(204, 97);
            this.lblNombrepersonaSecundaria.Name = "lblNombrepersonaSecundaria";
            this.lblNombrepersonaSecundaria.Size = new System.Drawing.Size(192, 14);
            this.lblNombrepersonaSecundaria.TabIndex = 23;
            this.lblNombrepersonaSecundaria.Text = "Nonmbre  La Persona Secundaria";
            // 
            // btnRetroceder
            // 
            this.btnRetroceder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            this.btnRetroceder.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnRetroceder.FlatAppearance.BorderSize = 0;
            this.btnRetroceder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetroceder.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetroceder.Image = ((System.Drawing.Image)(resources.GetObject("btnRetroceder.Image")));
            this.btnRetroceder.Location = new System.Drawing.Point(0, 0);
            this.btnRetroceder.Name = "btnRetroceder";
            this.btnRetroceder.Size = new System.Drawing.Size(55, 77);
            this.btnRetroceder.TabIndex = 48;
            this.btnRetroceder.UseVisualStyleBackColor = false;
            this.btnRetroceder.Click += new System.EventHandler(this.btnRetroceder_Click);
            // 
            // Cerrar
            // 
            this.Cerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            this.Cerrar.Dock = System.Windows.Forms.DockStyle.Right;
            this.Cerrar.FlatAppearance.BorderSize = 0;
            this.Cerrar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.Cerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cerrar.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cerrar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Cerrar.Image = ((System.Drawing.Image)(resources.GetObject("Cerrar.Image")));
            this.Cerrar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Cerrar.Location = new System.Drawing.Point(747, 0);
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(53, 77);
            this.Cerrar.TabIndex = 47;
            this.Cerrar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Cerrar.UseVisualStyleBackColor = false;
            this.Cerrar.Click += new System.EventHandler(this.Cerrar_Click);
            // 
            // openFileDialogComprobantes
            // 
            this.openFileDialogComprobantes.FileName = "openFileDialog1";
            // 
            // FrmPersonaSecundaria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 593);
            this.Controls.Add(this.panelForm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmPersonaSecundaria";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PersonaSecundaria";
            this.Load += new System.EventHandler(this.FrmPersonaSecundaria_Load);
            this.panelForm.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panelFormulario.ResumeLayout(false);
            this.panelFormulario.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPersonaSecundaria)).EndInit();
            this.panel2.ResumeLayout(false);
            this.pnlImgs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlImgINE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlImgCURP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picComprobantes)).EndInit();
            this.pnlMostrarINE.ResumeLayout(false);
            this.pnlMostrarINE.PerformLayout();
            this.pnlMostrarCURP.ResumeLayout(false);
            this.pnlMostrarCURP.PerformLayout();
            this.pnlMostarPicDom.ResumeLayout(false);
            this.pnlMostarPicDom.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUserImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelForm;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnEliminarPerAltern;
        private System.Windows.Forms.Button btnModificarPerAltern;
        private System.Windows.Forms.Button btnGuardarPerAltern;
        private System.Windows.Forms.Button btnApagar;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblNombrepersonaSecundaria;
        private System.Windows.Forms.PictureBox picUserImage;
        private System.Windows.Forms.Panel panelFormulario;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtLocalidad;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtCurp;
        private System.Windows.Forms.Label lblCalle;
        private System.Windows.Forms.TextBox txtCalle;
        private System.Windows.Forms.DateTimePicker dtpFechaNacimiento;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtCP;
        private System.Windows.Forms.TextBox txtNumCasa;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.TextBox txtFraccionamiento;
        private System.Windows.Forms.TextBox txtMunicipio;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox txtColonia;
        private System.Windows.Forms.TextBox txtCorreo;
        private System.Windows.Forms.TextBox txtCelular;
        private System.Windows.Forms.TextBox txtApMat;
        private System.Windows.Forms.TextBox txtApPat;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Button Cerrar;
        private System.Windows.Forms.Button btnRetroceder;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlMostrarINE;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnImgComINE;
        private System.Windows.Forms.TextBox txtComproINE;
        private System.Windows.Forms.Panel pnlMostrarCURP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnImgCCurp;
        private System.Windows.Forms.TextBox txtComprobanteCurp;
        private System.Windows.Forms.Panel pnlMostarPicDom;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnImgCDom;
        private System.Windows.Forms.TextBox txtComprobanteDomicilio;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel pnlImgs;
        private System.Windows.Forms.PictureBox picComprobantes;
        private System.Windows.Forms.PictureBox pnlImgCURP;
        private System.Windows.Forms.PictureBox pnlImgINE;
        private System.Windows.Forms.Label lblImagenOriginal2;
        private System.Windows.Forms.Label lblImagenAGuardar2;
        private System.Windows.Forms.Label lblImagenAGuardar1;
        private System.Windows.Forms.Label lblImagenOriginal1;
        private System.Windows.Forms.Label lblImagenAGuardar;
        private System.Windows.Forms.Label lblImagenOriginal;
        private System.Windows.Forms.OpenFileDialog openFileDialogComprobantes;
        private System.Windows.Forms.Label lblidCliente;
        private System.Windows.Forms.DataGridView dgPersonaSecundaria;
    }
}