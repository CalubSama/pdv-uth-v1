﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Lib_pdv_uth_v1;
using Lib_pdv_uth_v1.cajas;
using Lib_pdv_uth_v1.clientes;
using Lib_pdv_uth_v1.creditos;
namespace pdv_uth_v1
{
    public partial class FrmCreditos : Form
    {

        Credito credito;
        public static int id;
        public static int idClienteParaCredito;
        public static double Saldo;
        public static bool banCompletarventa = false;
        FrmCatalogoCliente frm = new FrmCatalogoCliente();
        public FrmCreditos()
        {
            InitializeComponent();
        }


        private void Cerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Usted desea cerrar la ventana de ventas?",
                            "Cerrar",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }
      
        private void btnRetroceder_Click(object sender, EventArgs e)
        {
            
            this.Close(); 
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        public void mostrarRegistrosEnDG()
        {
            Cliente cli = new Cliente();
            dgClientes.DataSource = null;
            //borrar todos los ren del DG 
            dgClientes.Rows.Clear();
            //se crea lista de criterios de busqueda
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = " 1 ";
            criterio.operadorIntermedio = OperadorDeConsulta.IGUAL;
            criterio.valor = "1";
            criterio.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dgClientes.DataSource = cli.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dgClientes.Refresh();
        }
        public void consultarCreditos()
        {
            credito = new Credito();
            dgCreditosClientes.DataSource = null;
            //borrar todos los ren del DG 
            dgCreditosClientes.Rows.Clear();
            //se crea lista de criterios de busqueda
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = " 1 ";
            criterio.operadorIntermedio = OperadorDeConsulta.IGUAL;
            criterio.valor = "1";
            criterio.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dgCreditosClientes.DataSource = credito.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dgCreditosClientes.Refresh();
        }
        public void consultarCliente()
        {
            Cliente cli = new Cliente();
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = "nombre ";
            criterio.operadorIntermedio = OperadorDeConsulta.LIKE;
            criterio.valor = "'%" + txtBusqueda.Text + "%'";
            criterio.operadorFinal = OperadorDeConsulta.OR;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            CriteriosBusqueda criterio2 = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio2.campo = "apellido_paterno";
            criterio2.operadorIntermedio = OperadorDeConsulta.LIKE;
            criterio2.valor = "'%" + txtBusqueda.Text + "%'";
            criterio2.operadorFinal = OperadorDeConsulta.OR;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio2);
            CriteriosBusqueda criterio3 = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio3.campo = "apellido_materno";
            criterio3.operadorIntermedio = OperadorDeConsulta.LIKE;
            criterio3.valor = "'%" + txtBusqueda.Text + "%'";
            criterio3.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio3);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dgClientes.DataSource = cli.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dgClientes.Update();
            dgClientes.Refresh();
        }



        private void FrmCreditos_Load(object sender, EventArgs e)
        {
          
            txtTotal.Text =  FrmCaja.total.ToString();
            
            if (FrmCaja.banderaCreditos == true)
            {
                
                pnlCreditoInfo.Size = new Size(257, 302);
                PanelVenta.Visible = true;
            }
            else
            {
                pnlCreditoInfo.Size = new Size(257, 430);
                PanelVenta.Visible = false;
            }

            consultarCreditos();
            mostrarRegistrosEnDG();
        }

        private void PicBuscar_Click(object sender, EventArgs e)
        {
            if (txtBusqueda.Text == "")
                mostrarRegistrosEnDG();

            else
                consultarCliente();
        }

        private void txtBusqueda_Enter(object sender, EventArgs e)
        {
            txtBusqueda.Text = "";
        }

        private void btnGuardarCliente_Click(object sender, EventArgs e)
        {
            credito = new Credito(txtSaldo.Text, dTFechaApertura.Value, idClienteParaCredito, cbEstadoCuenta.SelectedItem.ToString(), dTFechaASaldar.Value);
            if (credito.alta())
            {
                MessageBox.Show("registro correcto");
            }
            else
                MessageBox.Show("Errror al guardar crédito. " + Credito.msgError);
        }

        private void dgCreditosClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                id = int.Parse(dgCreditosClientes.Rows[e.RowIndex].Cells[0].Value.ToString());
                txtSaldo.Text= dgCreditosClientes.Rows[e.RowIndex].Cells[1].Value.ToString();
                dTFechaApertura.Value= DateTime.Parse(dgCreditosClientes.Rows[e.RowIndex].Cells[2].Value.ToString());
                cbEstadoCuenta.Text = dgCreditosClientes.Rows[e.RowIndex].Cells[4].Value.ToString();
                idClienteParaCredito= int.Parse(dgCreditosClientes.Rows[e.RowIndex].Cells[3].Value.ToString());
                dTFechaASaldar.Value = DateTime.Parse(dgCreditosClientes.Rows[e.RowIndex].Cells[5].Value.ToString());
                Saldo = double.Parse(txtSaldo.Text);
            }
            txtSaldo.Enabled = false;
            dTFechaApertura.Enabled = false;
            dTFechaASaldar.Enabled = false;
            cbEstadoCuenta.Enabled = false;

            Saldo = double.Parse(txtSaldo.Text);
            double nuevoSaldo = double.Parse(txtSaldo.Text) - double.Parse(txtTotal.Text);
            txtSaldoParaVenta.Text = nuevoSaldo.ToString();

        }

        private void picUserImage_Click(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dgClientes_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                idClienteParaCredito = int.Parse(dgClientes.Rows[e.RowIndex].Cells[0].Value.ToString());
                txtNombre.Text = dgClientes.Rows[e.RowIndex].Cells[1].Value.ToString();
                lblInformacion.Text = "Datos de " + dgClientes.Rows[e.RowIndex].Cells[1].Value.ToString();
                txtApPat.Text = dgClientes.Rows[e.RowIndex].Cells[2].Value.ToString();
                txtApMat.Text = dgClientes.Rows[e.RowIndex].Cells[3].Value.ToString();
                dtpFechaNacimiento.Value = DateTime.Parse(dgClientes.Rows[e.RowIndex].Cells[4].Value.ToString());
                txtCorreo.Text = dgClientes.Rows[e.RowIndex].Cells[7].Value.ToString();
                txtCelular.Text = dgClientes.Rows[e.RowIndex].Cells[5].Value.ToString();
                txtTelefono.Text = dgClientes.Rows[e.RowIndex].Cells[6].Value.ToString();
                txtCalle.Text = dgClientes.Rows[e.RowIndex].Cells[8].Value.ToString();
                txtColonia.Text = dgClientes.Rows[e.RowIndex].Cells[11].Value.ToString();
                txtLocalidad.Text = dgClientes.Rows[e.RowIndex].Cells[13].Value.ToString();
                txtMunicipio.Text = dgClientes.Rows[e.RowIndex].Cells[14].Value.ToString();
                dgClientes.Rows[e.RowIndex].Cells[15].Value.ToString();
                dgClientes.Rows[e.RowIndex].Cells[16].Value.ToString();
                txtCurp.Text = dgClientes.Rows[e.RowIndex].Cells[17].Value.ToString();
                dgClientes.Rows[e.RowIndex].Cells[18].Value.ToString();
            }
            lblInformacion.Text = "Datos de " + txtNombre.Text + txtApPat.Text + txtApMat.Text;
            credito = new Credito();
            dgCreditosClientes.DataSource = null;
            //borrar todos los ren del DG 
            dgCreditosClientes.Rows.Clear();
            //se crea lista de criterios de busqueda
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = "cliente_id";
            criterio.operadorIntermedio = OperadorDeConsulta.IGUAL;
            criterio.valor = "'"+idClienteParaCredito+"'";
            criterio.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dgCreditosClientes.DataSource = credito.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dgCreditosClientes.Refresh();
        }
        private void lblElejirCliente_Click(object sender, EventArgs e)
        {
            if (true)
            {
                mostrarRegistrosEnDG();
            }
            dgClientes.Show();
        }

        private void txtBusqueda_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txtBusqueda.Text == " ")
                    mostrarRegistrosEnDG();
                else
                    consultarCliente();
            }
            
        }

        private void btnModificarCliente_Click(object sender, EventArgs e)
        {
            Credito paraModif = new Credito();
            List<DatosParaActualizar> datos = new List<DatosParaActualizar>();
            datos.Add(new DatosParaActualizar("saldo", txtSaldo.Text));
            datos.Add(new DatosParaActualizar("cliente_id", idClienteParaCredito.ToString()));
            datos.Add(new DatosParaActualizar("estado",cbEstadoCuenta.SelectedItem.ToString()));
            datos.Add(new DatosParaActualizar("fecha_saldado", dTFechaASaldar.Value.Year + "-" + dTFechaASaldar.Value.Month + "-" + dTFechaASaldar.Value.Day));
            
            if (paraModif.modificar(datos, id))
            {
                MessageBox.Show("Crédito modificado");
            }
            else MessageBox.Show("Error Crédito no modificado");
            consultarCreditos();

        }

        private void btnAñadirClientes_Click(object sender, EventArgs e)
        {
            txtSaldo.Enabled = true;
            dTFechaApertura.Enabled = true;
            dTFechaASaldar.Enabled = true;
            cbEstadoCuenta.Enabled = true;
        }

        private void dgClientes_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnAñadirVenta_Click(object sender, EventArgs e)
        {
            Caja cajaCredito = new Caja();
            
            if (cajaCredito.venderCredito(FrmLogin.us.Id, Saldo, double.Parse(txtTotal.Text)))
            {
                Credito paraModif = new Credito();
                List<DatosParaActualizar> datos = new List<DatosParaActualizar>();
                datos.Add(new DatosParaActualizar("saldo", txtSaldoParaVenta.Text));
                datos.Add(new DatosParaActualizar("cliente_id", idClienteParaCredito.ToString()));
                datos.Add(new DatosParaActualizar("estado", cbEstadoCuenta.SelectedItem.ToString()));
                datos.Add(new DatosParaActualizar("fecha_saldado", dTFechaASaldar.Value.Year + "-" + dTFechaASaldar.Value.Month + "-" + dTFechaASaldar.Value.Day));

                if (paraModif.modificar(datos, id))
                {
                    MessageBox.Show("El crédito se agregó exitosamente");
                }
                else MessageBox.Show("Error al agregar el crédito en la venta");
            }
            else MessageBox.Show("Error al vender con crédito");
           
            consultarCreditos();
            txtSaldo.BackColor = Color.Green;
        }
    }
}
