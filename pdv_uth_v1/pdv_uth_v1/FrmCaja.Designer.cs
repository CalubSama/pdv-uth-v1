﻿namespace pdv_uth_v1
{
    partial class FrmCaja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCaja));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgListaProductos = new System.Windows.Forms.DataGridView();
            this.ColID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCodBarras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDescripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPrecio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColSubTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelDatosCredito = new System.Windows.Forms.Panel();
            this.lblFechaProxPago = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblCreditoDisponible = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblClienteCredito = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.txtIva = new System.Windows.Forms.TextBox();
            this.lblIva = new System.Windows.Forms.Label();
            this.txtSubTotal = new System.Windows.Forms.TextBox();
            this.lblSubtotal = new System.Windows.Forms.Label();
            this.numericCantidad = new System.Windows.Forms.NumericUpDown();
            this.lblCantidad = new System.Windows.Forms.Label();
            this.txtCodBarras = new System.Windows.Forms.TextBox();
            this.lblCodBarras = new System.Windows.Forms.Label();
            this.txtMetodoEfectivo = new System.Windows.Forms.TextBox();
            this.pbEfectivo = new System.Windows.Forms.PictureBox();
            this.txtMetodoCredito = new System.Windows.Forms.TextBox();
            this.pbCredito = new System.Windows.Forms.PictureBox();
            this.txtMetodoTarjeta = new System.Windows.Forms.TextBox();
            this.pbTarjeta = new System.Windows.Forms.PictureBox();
            this.btnPagar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pnlVentaInfo = new System.Windows.Forms.Panel();
            this.btnApagar = new System.Windows.Forms.Button();
            this.cboCamera = new System.Windows.Forms.ComboBox();
            this.lblNombreProducto = new System.Windows.Forms.Label();
            this.pibWebCam = new System.Windows.Forms.PictureBox();
            this.picProductoAVender = new System.Windows.Forms.PictureBox();
            this.lblCambio = new System.Windows.Forms.Label();
            this.txtCambio = new System.Windows.Forms.TextBox();
            this.btnCancelarVenta = new System.Windows.Forms.Button();
            this.btnAgregarAVenta = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlMetodoTarjeta = new System.Windows.Forms.Panel();
            this.pnlCreditoCliente = new System.Windows.Forms.Panel();
            this.pnlMetodoEfectivo = new System.Windows.Forms.Panel();
            this.pnlNavbar = new System.Windows.Forms.Panel();
            this.PnlInfo = new System.Windows.Forms.Panel();
            this.picInfoCaja = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.lblCajaTitle = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.picBuscarProduc = new System.Windows.Forms.PictureBox();
            this.panelTimer = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.lblReloj = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnCompras = new System.Windows.Forms.Button();
            this.btnCreditos = new System.Windows.Forms.Button();
            this.btnVentas = new System.Windows.Forms.Button();
            this.btnProductos = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.pnlBarraTareas = new System.Windows.Forms.Panel();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.btnExpandirMn = new System.Windows.Forms.Button();
            this.txtConsultaProducto = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pnlInfoProductos = new System.Windows.Forms.Panel();
            this.dgProductos = new System.Windows.Forms.DataGridView();
            this.pnlInfoAbrirCaja = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.btnCerrarCaja = new System.Windows.Forms.Button();
            this.btnAbrirCaja = new System.Windows.Forms.Button();
            this.txtMontoEnCaja = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtVentasEnTarjeta = new System.Windows.Forms.TextBox();
            this.txtVentasEnEfectivo = new System.Windows.Forms.TextBox();
            this.txtVentasEnCredito = new System.Windows.Forms.TextBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtVentaDeTurno = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numMontoInicial = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.lblCerrarInfoCaja = new System.Windows.Forms.Label();
            this.lblNumberCaja = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.lblTitleInfoCaja = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.TTCaja = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgListaProductos)).BeginInit();
            this.panelDatosCredito.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericCantidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEfectivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCredito)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTarjeta)).BeginInit();
            this.panel5.SuspendLayout();
            this.pnlVentaInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pibWebCam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picProductoAVender)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlMetodoTarjeta.SuspendLayout();
            this.pnlCreditoCliente.SuspendLayout();
            this.pnlMetodoEfectivo.SuspendLayout();
            this.pnlNavbar.SuspendLayout();
            this.PnlInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picInfoCaja)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBuscarProduc)).BeginInit();
            this.panelTimer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel4.SuspendLayout();
            this.pnlBarraTareas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.panel6.SuspendLayout();
            this.pnlInfoProductos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProductos)).BeginInit();
            this.pnlInfoAbrirCaja.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMontoInicial)).BeginInit();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel12.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgListaProductos
            // 
            this.dgListaProductos.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            this.dgListaProductos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgListaProductos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgListaProductos.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgListaProductos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgListaProductos.ColumnHeadersHeight = 30;
            this.dgListaProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgListaProductos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColID,
            this.ColCodBarras,
            this.ColNombre,
            this.ColDescripcion,
            this.ColCantidad,
            this.ColPrecio,
            this.ColSubTotal});
            this.dgListaProductos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListaProductos.EnableHeadersVisualStyles = false;
            this.dgListaProductos.GridColor = System.Drawing.Color.SteelBlue;
            this.dgListaProductos.Location = new System.Drawing.Point(0, 0);
            this.dgListaProductos.Margin = new System.Windows.Forms.Padding(2);
            this.dgListaProductos.Name = "dgListaProductos";
            this.dgListaProductos.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgListaProductos.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgListaProductos.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            this.dgListaProductos.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgListaProductos.RowTemplate.Height = 24;
            this.dgListaProductos.Size = new System.Drawing.Size(796, 677);
            this.dgListaProductos.TabIndex = 0;
            this.dgListaProductos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgListaProductos_CellContentClick);
            this.dgListaProductos.MouseEnter += new System.EventHandler(this.dgListaProductos_MouseEnter);
            // 
            // ColID
            // 
            this.ColID.HeaderText = "ID";
            this.ColID.Name = "ColID";
            this.ColID.ReadOnly = true;
            this.ColID.Width = 70;
            // 
            // ColCodBarras
            // 
            this.ColCodBarras.HeaderText = "Cod. Barras";
            this.ColCodBarras.MinimumWidth = 13;
            this.ColCodBarras.Name = "ColCodBarras";
            this.ColCodBarras.ReadOnly = true;
            this.ColCodBarras.Width = 150;
            // 
            // ColNombre
            // 
            this.ColNombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColNombre.HeaderText = "Producto";
            this.ColNombre.Name = "ColNombre";
            this.ColNombre.ReadOnly = true;
            this.ColNombre.Width = 89;
            // 
            // ColDescripcion
            // 
            this.ColDescripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColDescripcion.HeaderText = "Descripción";
            this.ColDescripcion.Name = "ColDescripcion";
            this.ColDescripcion.ReadOnly = true;
            // 
            // ColCantidad
            // 
            this.ColCantidad.HeaderText = "Cantidad";
            this.ColCantidad.Name = "ColCantidad";
            this.ColCantidad.ReadOnly = true;
            this.ColCantidad.Width = 78;
            // 
            // ColPrecio
            // 
            this.ColPrecio.HeaderText = "Precio";
            this.ColPrecio.Name = "ColPrecio";
            this.ColPrecio.ReadOnly = true;
            this.ColPrecio.Width = 75;
            // 
            // ColSubTotal
            // 
            this.ColSubTotal.HeaderText = "SubTotal";
            this.ColSubTotal.Name = "ColSubTotal";
            this.ColSubTotal.ReadOnly = true;
            this.ColSubTotal.Width = 88;
            // 
            // panelDatosCredito
            // 
            this.panelDatosCredito.Controls.Add(this.lblFechaProxPago);
            this.panelDatosCredito.Controls.Add(this.label8);
            this.panelDatosCredito.Controls.Add(this.lblCreditoDisponible);
            this.panelDatosCredito.Controls.Add(this.label6);
            this.panelDatosCredito.Controls.Add(this.lblClienteCredito);
            this.panelDatosCredito.Controls.Add(this.label3);
            this.panelDatosCredito.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelDatosCredito.Location = new System.Drawing.Point(0, 0);
            this.panelDatosCredito.Margin = new System.Windows.Forms.Padding(2);
            this.panelDatosCredito.Name = "panelDatosCredito";
            this.panelDatosCredito.Size = new System.Drawing.Size(343, 179);
            this.panelDatosCredito.TabIndex = 2;
            this.panelDatosCredito.Visible = false;
            // 
            // lblFechaProxPago
            // 
            this.lblFechaProxPago.AutoSize = true;
            this.lblFechaProxPago.Font = new System.Drawing.Font("Lucida Sans", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaProxPago.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblFechaProxPago.Location = new System.Drawing.Point(87, 153);
            this.lblFechaProxPago.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFechaProxPago.Name = "lblFechaProxPago";
            this.lblFechaProxPago.Size = new System.Drawing.Size(136, 23);
            this.lblFechaProxPago.TabIndex = 5;
            this.lblFechaProxPago.Text = "15/04/2020";
            this.lblFechaProxPago.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Lucida Sans", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label8.Location = new System.Drawing.Point(6, 125);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(121, 22);
            this.label8.TabIndex = 4;
            this.label8.Text = "Prox. Pago:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lblCreditoDisponible
            // 
            this.lblCreditoDisponible.AutoSize = true;
            this.lblCreditoDisponible.Font = new System.Drawing.Font("Lucida Sans", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreditoDisponible.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblCreditoDisponible.Location = new System.Drawing.Point(114, 98);
            this.lblCreditoDisponible.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCreditoDisponible.Name = "lblCreditoDisponible";
            this.lblCreditoDisponible.Size = new System.Drawing.Size(94, 23);
            this.lblCreditoDisponible.TabIndex = 3;
            this.lblCreditoDisponible.Text = "$100.00";
            this.lblCreditoDisponible.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Lucida Sans", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(5, 71);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(203, 22);
            this.label6.TabIndex = 2;
            this.label6.Text = "Crédito disponible:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblClienteCredito
            // 
            this.lblClienteCredito.AutoSize = true;
            this.lblClienteCredito.Font = new System.Drawing.Font("Lucida Sans", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClienteCredito.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblClienteCredito.Location = new System.Drawing.Point(26, 42);
            this.lblClienteCredito.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblClienteCredito.Name = "lblClienteCredito";
            this.lblClienteCredito.Size = new System.Drawing.Size(167, 23);
            this.lblClienteCredito.TabIndex = 1;
            this.lblClienteCredito.Text = "Nombre Cliente";
            this.lblClienteCredito.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Lucida Sans", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(4, 15);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 22);
            this.label3.TabIndex = 0;
            this.label3.Text = "Cliente:";
            // 
            // txtTotal
            // 
            this.txtTotal.Font = new System.Drawing.Font("Lucida Bright", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.Location = new System.Drawing.Point(5, 710);
            this.txtTotal.Margin = new System.Windows.Forms.Padding(2);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(147, 32);
            this.txtTotal.TabIndex = 14;
            this.txtTotal.Text = "000,000\r\n";
            this.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTotal.Location = new System.Drawing.Point(52, 690);
            this.lblTotal.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(58, 18);
            this.lblTotal.TabIndex = 13;
            this.lblTotal.Text = "TOTAL";
            // 
            // txtIva
            // 
            this.txtIva.Font = new System.Drawing.Font("Lucida Bright", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIva.Location = new System.Drawing.Point(4, 652);
            this.txtIva.Margin = new System.Windows.Forms.Padding(2);
            this.txtIva.Name = "txtIva";
            this.txtIva.Size = new System.Drawing.Size(147, 32);
            this.txtIva.TabIndex = 12;
            this.txtIva.Text = "000,000\r\n";
            this.txtIva.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblIva
            // 
            this.lblIva.AutoSize = true;
            this.lblIva.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIva.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblIva.Location = new System.Drawing.Point(57, 630);
            this.lblIva.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIva.Name = "lblIva";
            this.lblIva.Size = new System.Drawing.Size(35, 18);
            this.lblIva.TabIndex = 11;
            this.lblIva.Text = "IVA";
            // 
            // txtSubTotal
            // 
            this.txtSubTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSubTotal.Font = new System.Drawing.Font("Lucida Bright", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubTotal.Location = new System.Drawing.Point(4, 603);
            this.txtSubTotal.Margin = new System.Windows.Forms.Padding(2);
            this.txtSubTotal.Name = "txtSubTotal";
            this.txtSubTotal.Size = new System.Drawing.Size(147, 25);
            this.txtSubTotal.TabIndex = 10;
            this.txtSubTotal.Text = "000,000\r\n";
            this.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblSubtotal
            // 
            this.lblSubtotal.AutoSize = true;
            this.lblSubtotal.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubtotal.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblSubtotal.Location = new System.Drawing.Point(36, 581);
            this.lblSubtotal.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSubtotal.Name = "lblSubtotal";
            this.lblSubtotal.Size = new System.Drawing.Size(89, 18);
            this.lblSubtotal.TabIndex = 9;
            this.lblSubtotal.Text = "SUBTOTAL";
            // 
            // numericCantidad
            // 
            this.numericCantidad.DecimalPlaces = 3;
            this.numericCantidad.Font = new System.Drawing.Font("Lucida Bright", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericCantidad.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.numericCantidad.Location = new System.Drawing.Point(7, 541);
            this.numericCantidad.Margin = new System.Windows.Forms.Padding(2);
            this.numericCantidad.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            196608});
            this.numericCantidad.Name = "numericCantidad";
            this.numericCantidad.Size = new System.Drawing.Size(103, 30);
            this.numericCantidad.TabIndex = 10;
            this.numericCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericCantidad.ThousandsSeparator = true;
            this.numericCantidad.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblCantidad
            // 
            this.lblCantidad.AutoSize = true;
            this.lblCantidad.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCantidad.Location = new System.Drawing.Point(4, 516);
            this.lblCantidad.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCantidad.Name = "lblCantidad";
            this.lblCantidad.Size = new System.Drawing.Size(78, 18);
            this.lblCantidad.TabIndex = 9;
            this.lblCantidad.Text = "Cantidad";
            // 
            // txtCodBarras
            // 
            this.txtCodBarras.Font = new System.Drawing.Font("Trebuchet MS", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodBarras.Location = new System.Drawing.Point(4, 470);
            this.txtCodBarras.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodBarras.MaxLength = 13;
            this.txtCodBarras.Name = "txtCodBarras";
            this.txtCodBarras.Size = new System.Drawing.Size(168, 33);
            this.txtCodBarras.TabIndex = 8;
            this.txtCodBarras.Text = "\r\n";
            this.txtCodBarras.TextChanged += new System.EventHandler(this.txtCodBarras_TextChanged);
            this.txtCodBarras.Enter += new System.EventHandler(this.txtCodBarras_Enter);
            this.txtCodBarras.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodBarras_KeyPress);
            // 
            // lblCodBarras
            // 
            this.lblCodBarras.AutoSize = true;
            this.lblCodBarras.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodBarras.Location = new System.Drawing.Point(4, 441);
            this.lblCodBarras.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCodBarras.Name = "lblCodBarras";
            this.lblCodBarras.Size = new System.Drawing.Size(141, 18);
            this.lblCodBarras.TabIndex = 7;
            this.lblCodBarras.Text = "Código de Barras";
            // 
            // txtMetodoEfectivo
            // 
            this.txtMetodoEfectivo.Font = new System.Drawing.Font("Lucida Bright", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMetodoEfectivo.Location = new System.Drawing.Point(4, 91);
            this.txtMetodoEfectivo.Margin = new System.Windows.Forms.Padding(2);
            this.txtMetodoEfectivo.Name = "txtMetodoEfectivo";
            this.txtMetodoEfectivo.Size = new System.Drawing.Size(90, 32);
            this.txtMetodoEfectivo.TabIndex = 1;
            this.txtMetodoEfectivo.Text = "000.00";
            this.txtMetodoEfectivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtMetodoEfectivo.TextChanged += new System.EventHandler(this.txtMetodoEfectivo_TextChanged);
            this.txtMetodoEfectivo.MouseLeave += new System.EventHandler(this.txtMetodoEfectivo_MouseLeave);
            // 
            // pbEfectivo
            // 
            this.pbEfectivo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbEfectivo.Image = ((System.Drawing.Image)(resources.GetObject("pbEfectivo.Image")));
            this.pbEfectivo.ImageLocation = "";
            this.pbEfectivo.Location = new System.Drawing.Point(7, 15);
            this.pbEfectivo.Margin = new System.Windows.Forms.Padding(2);
            this.pbEfectivo.Name = "pbEfectivo";
            this.pbEfectivo.Size = new System.Drawing.Size(86, 73);
            this.pbEfectivo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbEfectivo.TabIndex = 0;
            this.pbEfectivo.TabStop = false;
            // 
            // txtMetodoCredito
            // 
            this.txtMetodoCredito.Font = new System.Drawing.Font("Lucida Bright", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMetodoCredito.Location = new System.Drawing.Point(1, 92);
            this.txtMetodoCredito.Margin = new System.Windows.Forms.Padding(2);
            this.txtMetodoCredito.Name = "txtMetodoCredito";
            this.txtMetodoCredito.Size = new System.Drawing.Size(87, 32);
            this.txtMetodoCredito.TabIndex = 1;
            this.txtMetodoCredito.Text = "000.00";
            this.txtMetodoCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pbCredito
            // 
            this.pbCredito.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbCredito.Image = ((System.Drawing.Image)(resources.GetObject("pbCredito.Image")));
            this.pbCredito.ImageLocation = "";
            this.pbCredito.Location = new System.Drawing.Point(9, 11);
            this.pbCredito.Margin = new System.Windows.Forms.Padding(2);
            this.pbCredito.Name = "pbCredito";
            this.pbCredito.Size = new System.Drawing.Size(68, 77);
            this.pbCredito.TabIndex = 0;
            this.pbCredito.TabStop = false;
            this.pbCredito.Click += new System.EventHandler(this.pbCredito_Click);
            // 
            // txtMetodoTarjeta
            // 
            this.txtMetodoTarjeta.Font = new System.Drawing.Font("Lucida Bright", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMetodoTarjeta.Location = new System.Drawing.Point(2, 92);
            this.txtMetodoTarjeta.Margin = new System.Windows.Forms.Padding(2);
            this.txtMetodoTarjeta.Name = "txtMetodoTarjeta";
            this.txtMetodoTarjeta.Size = new System.Drawing.Size(88, 32);
            this.txtMetodoTarjeta.TabIndex = 1;
            this.txtMetodoTarjeta.Text = "000.00";
            this.txtMetodoTarjeta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pbTarjeta
            // 
            this.pbTarjeta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbTarjeta.Image = ((System.Drawing.Image)(resources.GetObject("pbTarjeta.Image")));
            this.pbTarjeta.ImageLocation = "";
            this.pbTarjeta.Location = new System.Drawing.Point(10, 17);
            this.pbTarjeta.Margin = new System.Windows.Forms.Padding(2);
            this.pbTarjeta.Name = "pbTarjeta";
            this.pbTarjeta.Size = new System.Drawing.Size(72, 64);
            this.pbTarjeta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbTarjeta.TabIndex = 0;
            this.pbTarjeta.TabStop = false;
            this.pbTarjeta.Click += new System.EventHandler(this.pbTarjeta_Click);
            // 
            // btnPagar
            // 
            this.btnPagar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnPagar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnPagar.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnPagar.FlatAppearance.BorderSize = 2;
            this.btnPagar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPagar.Font = new System.Drawing.Font("Trebuchet MS", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPagar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnPagar.Image = ((System.Drawing.Image)(resources.GetObject("btnPagar.Image")));
            this.btnPagar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPagar.Location = new System.Drawing.Point(14, 17);
            this.btnPagar.Margin = new System.Windows.Forms.Padding(2);
            this.btnPagar.Name = "btnPagar";
            this.btnPagar.Size = new System.Drawing.Size(127, 72);
            this.btnPagar.TabIndex = 4;
            this.btnPagar.Text = "Pagar";
            this.btnPagar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPagar.UseVisualStyleBackColor = false;
            this.btnPagar.Click += new System.EventHandler(this.btnPagar_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 2;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Trebuchet MS", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(14, 98);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(127, 72);
            this.button1.TabIndex = 5;
            this.button1.Text = "Ticket";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.panel5.Controls.Add(this.pnlVentaInfo);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Font = new System.Drawing.Font("Lucida Sans", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel5.Location = new System.Drawing.Point(1065, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(287, 744);
            this.panel5.TabIndex = 15;
            // 
            // pnlVentaInfo
            // 
            this.pnlVentaInfo.Controls.Add(this.btnApagar);
            this.pnlVentaInfo.Controls.Add(this.cboCamera);
            this.pnlVentaInfo.Controls.Add(this.lblNombreProducto);
            this.pnlVentaInfo.Controls.Add(this.pibWebCam);
            this.pnlVentaInfo.Controls.Add(this.picProductoAVender);
            this.pnlVentaInfo.Controls.Add(this.lblCambio);
            this.pnlVentaInfo.Controls.Add(this.txtCambio);
            this.pnlVentaInfo.Controls.Add(this.btnCancelarVenta);
            this.pnlVentaInfo.Controls.Add(this.btnAgregarAVenta);
            this.pnlVentaInfo.Controls.Add(this.btnStart);
            this.pnlVentaInfo.Controls.Add(this.lblCodBarras);
            this.pnlVentaInfo.Controls.Add(this.txtIva);
            this.pnlVentaInfo.Controls.Add(this.txtCodBarras);
            this.pnlVentaInfo.Controls.Add(this.lblIva);
            this.pnlVentaInfo.Controls.Add(this.lblTotal);
            this.pnlVentaInfo.Controls.Add(this.numericCantidad);
            this.pnlVentaInfo.Controls.Add(this.txtSubTotal);
            this.pnlVentaInfo.Controls.Add(this.lblSubtotal);
            this.pnlVentaInfo.Controls.Add(this.lblCantidad);
            this.pnlVentaInfo.Controls.Add(this.txtTotal);
            this.pnlVentaInfo.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlVentaInfo.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlVentaInfo.Location = new System.Drawing.Point(3, 0);
            this.pnlVentaInfo.Name = "pnlVentaInfo";
            this.pnlVentaInfo.Size = new System.Drawing.Size(284, 744);
            this.pnlVentaInfo.TabIndex = 16;
            this.pnlVentaInfo.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlVentaInfo_Paint);
            // 
            // btnApagar
            // 
            this.btnApagar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnApagar.FlatAppearance.BorderSize = 0;
            this.btnApagar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnApagar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApagar.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApagar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnApagar.Image = ((System.Drawing.Image)(resources.GetObject("btnApagar.Image")));
            this.btnApagar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnApagar.Location = new System.Drawing.Point(228, 2);
            this.btnApagar.Name = "btnApagar";
            this.btnApagar.Size = new System.Drawing.Size(53, 57);
            this.btnApagar.TabIndex = 47;
            this.btnApagar.Text = "Salir";
            this.btnApagar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnApagar.UseVisualStyleBackColor = true;
            this.btnApagar.Click += new System.EventHandler(this.btnApagar_Click);
            // 
            // cboCamera
            // 
            this.cboCamera.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboCamera.FormattingEnabled = true;
            this.cboCamera.Location = new System.Drawing.Point(19, 407);
            this.cboCamera.Name = "cboCamera";
            this.cboCamera.Size = new System.Drawing.Size(234, 26);
            this.cboCamera.TabIndex = 16;
            // 
            // lblNombreProducto
            // 
            this.lblNombreProducto.AutoSize = true;
            this.lblNombreProducto.Font = new System.Drawing.Font("Lucida Bright", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreProducto.ForeColor = System.Drawing.Color.GhostWhite;
            this.lblNombreProducto.Location = new System.Drawing.Point(36, 383);
            this.lblNombreProducto.Name = "lblNombreProducto";
            this.lblNombreProducto.Size = new System.Drawing.Size(218, 22);
            this.lblNombreProducto.TabIndex = 4;
            this.lblNombreProducto.Text = "NOMBRE  PRODUCTO";
            // 
            // pibWebCam
            // 
            this.pibWebCam.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pibWebCam.Location = new System.Drawing.Point(0, 156);
            this.pibWebCam.Name = "pibWebCam";
            this.pibWebCam.Size = new System.Drawing.Size(281, 227);
            this.pibWebCam.TabIndex = 15;
            this.pibWebCam.TabStop = false;
            this.pibWebCam.Visible = false;
            this.pibWebCam.Click += new System.EventHandler(this.pibWebCam_Click);
            // 
            // picProductoAVender
            // 
            this.picProductoAVender.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picProductoAVender.Image = ((System.Drawing.Image)(resources.GetObject("picProductoAVender.Image")));
            this.picProductoAVender.Location = new System.Drawing.Point(19, 77);
            this.picProductoAVender.Name = "picProductoAVender";
            this.picProductoAVender.Size = new System.Drawing.Size(238, 302);
            this.picProductoAVender.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picProductoAVender.TabIndex = 0;
            this.picProductoAVender.TabStop = false;
            // 
            // lblCambio
            // 
            this.lblCambio.AutoSize = true;
            this.lblCambio.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCambio.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblCambio.Location = new System.Drawing.Point(163, 684);
            this.lblCambio.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCambio.Name = "lblCambio";
            this.lblCambio.Size = new System.Drawing.Size(70, 18);
            this.lblCambio.TabIndex = 17;
            this.lblCambio.Text = "CAMBIO";
            // 
            // txtCambio
            // 
            this.txtCambio.Font = new System.Drawing.Font("Lucida Bright", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCambio.Location = new System.Drawing.Point(160, 709);
            this.txtCambio.Margin = new System.Windows.Forms.Padding(2);
            this.txtCambio.Name = "txtCambio";
            this.txtCambio.Size = new System.Drawing.Size(115, 32);
            this.txtCambio.TabIndex = 18;
            this.txtCambio.Text = "000,000\r\n";
            this.txtCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnCancelarVenta
            // 
            this.btnCancelarVenta.FlatAppearance.BorderSize = 2;
            this.btnCancelarVenta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelarVenta.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelarVenta.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCancelarVenta.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelarVenta.Image")));
            this.btnCancelarVenta.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelarVenta.Location = new System.Drawing.Point(157, 605);
            this.btnCancelarVenta.Name = "btnCancelarVenta";
            this.btnCancelarVenta.Size = new System.Drawing.Size(124, 57);
            this.btnCancelarVenta.TabIndex = 15;
            this.btnCancelarVenta.Text = "Cancelar ";
            this.btnCancelarVenta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelarVenta.UseVisualStyleBackColor = true;
            this.btnCancelarVenta.Click += new System.EventHandler(this.btnCancelarVenta_Click);
            // 
            // btnAgregarAVenta
            // 
            this.btnAgregarAVenta.FlatAppearance.BorderSize = 2;
            this.btnAgregarAVenta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarAVenta.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregarAVenta.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAgregarAVenta.Image = ((System.Drawing.Image)(resources.GetObject("btnAgregarAVenta.Image")));
            this.btnAgregarAVenta.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgregarAVenta.Location = new System.Drawing.Point(156, 534);
            this.btnAgregarAVenta.Name = "btnAgregarAVenta";
            this.btnAgregarAVenta.Size = new System.Drawing.Size(124, 57);
            this.btnAgregarAVenta.TabIndex = 14;
            this.btnAgregarAVenta.Text = "Agregar";
            this.btnAgregarAVenta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAgregarAVenta.UseVisualStyleBackColor = true;
            this.btnAgregarAVenta.Click += new System.EventHandler(this.btnAgregarAVenta_Click);
            // 
            // btnStart
            // 
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.Image = ((System.Drawing.Image)(resources.GetObject("btnStart.Image")));
            this.btnStart.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnStart.Location = new System.Drawing.Point(190, 436);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(81, 92);
            this.btnStart.TabIndex = 16;
            this.btnStart.Text = "ScannerCam\r\n";
            this.btnStart.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.panelDatosCredito);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(796, 179);
            this.panel1.TabIndex = 16;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.btnPagar);
            this.panel8.Controls.Add(this.button1);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(646, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(150, 179);
            this.panel8.TabIndex = 18;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.pnlMetodoTarjeta);
            this.panel2.Controls.Add(this.pnlCreditoCliente);
            this.panel2.Controls.Add(this.pnlMetodoEfectivo);
            this.panel2.Location = new System.Drawing.Point(343, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(309, 179);
            this.panel2.TabIndex = 17;
            // 
            // pnlMetodoTarjeta
            // 
            this.pnlMetodoTarjeta.Controls.Add(this.pbTarjeta);
            this.pnlMetodoTarjeta.Controls.Add(this.txtMetodoTarjeta);
            this.pnlMetodoTarjeta.Location = new System.Drawing.Point(107, 2);
            this.pnlMetodoTarjeta.Name = "pnlMetodoTarjeta";
            this.pnlMetodoTarjeta.Size = new System.Drawing.Size(93, 177);
            this.pnlMetodoTarjeta.TabIndex = 4;
            this.pnlMetodoTarjeta.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlMetodoTarjeta_MouseClick);
            // 
            // pnlCreditoCliente
            // 
            this.pnlCreditoCliente.Controls.Add(this.pbCredito);
            this.pnlCreditoCliente.Controls.Add(this.txtMetodoCredito);
            this.pnlCreditoCliente.Location = new System.Drawing.Point(201, 3);
            this.pnlCreditoCliente.Name = "pnlCreditoCliente";
            this.pnlCreditoCliente.Size = new System.Drawing.Size(93, 174);
            this.pnlCreditoCliente.TabIndex = 3;
            this.pnlCreditoCliente.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlCreditoCliente_MouseClick);
            // 
            // pnlMetodoEfectivo
            // 
            this.pnlMetodoEfectivo.Controls.Add(this.pbEfectivo);
            this.pnlMetodoEfectivo.Controls.Add(this.txtMetodoEfectivo);
            this.pnlMetodoEfectivo.Location = new System.Drawing.Point(8, 2);
            this.pnlMetodoEfectivo.Name = "pnlMetodoEfectivo";
            this.pnlMetodoEfectivo.Size = new System.Drawing.Size(98, 177);
            this.pnlMetodoEfectivo.TabIndex = 2;
            // 
            // pnlNavbar
            // 
            this.pnlNavbar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.pnlNavbar.Controls.Add(this.PnlInfo);
            this.pnlNavbar.Controls.Add(this.panelTimer);
            this.pnlNavbar.Controls.Add(this.panel4);
            this.pnlNavbar.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlNavbar.Location = new System.Drawing.Point(0, 0);
            this.pnlNavbar.Name = "pnlNavbar";
            this.pnlNavbar.Size = new System.Drawing.Size(269, 744);
            this.pnlNavbar.TabIndex = 17;
            // 
            // PnlInfo
            // 
            this.PnlInfo.Controls.Add(this.picInfoCaja);
            this.PnlInfo.Controls.Add(this.panel3);
            this.PnlInfo.Controls.Add(this.pictureBox14);
            this.PnlInfo.Controls.Add(this.lblCajaTitle);
            this.PnlInfo.Controls.Add(this.lblUserName);
            this.PnlInfo.Controls.Add(this.picBuscarProduc);
            this.PnlInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.PnlInfo.Location = new System.Drawing.Point(0, 0);
            this.PnlInfo.Name = "PnlInfo";
            this.PnlInfo.Size = new System.Drawing.Size(269, 291);
            this.PnlInfo.TabIndex = 44;
            // 
            // picInfoCaja
            // 
            this.picInfoCaja.Image = global::pdv_uth_v1.Properties.Resources.ic_info_outline_white_48dp;
            this.picInfoCaja.Location = new System.Drawing.Point(215, 4);
            this.picInfoCaja.Name = "picInfoCaja";
            this.picInfoCaja.Size = new System.Drawing.Size(46, 41);
            this.picInfoCaja.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picInfoCaja.TabIndex = 49;
            this.picInfoCaja.TabStop = false;
            this.picInfoCaja.Click += new System.EventHandler(this.pictureBox8_Click);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(268, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(600, 42);
            this.panel3.TabIndex = 48;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.Location = new System.Drawing.Point(10, 1);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(54, 58);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox14.TabIndex = 47;
            this.pictureBox14.TabStop = false;
            // 
            // lblCajaTitle
            // 
            this.lblCajaTitle.AutoSize = true;
            this.lblCajaTitle.Font = new System.Drawing.Font("Lucida Bright", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCajaTitle.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblCajaTitle.Location = new System.Drawing.Point(93, 12);
            this.lblCajaTitle.Name = "lblCajaTitle";
            this.lblCajaTitle.Size = new System.Drawing.Size(92, 33);
            this.lblCajaTitle.TabIndex = 46;
            this.lblCajaTitle.Text = "Cajas";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Lucida Bright", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblUserName.Location = new System.Drawing.Point(12, 242);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(238, 22);
            this.lblUserName.TabIndex = 45;
            this.lblUserName.Text = "NOMBRE DEL USUARIO ";
            this.lblUserName.UseWaitCursor = true;
            // 
            // picBuscarProduc
            // 
            this.picBuscarProduc.Image = ((System.Drawing.Image)(resources.GetObject("picBuscarProduc.Image")));
            this.picBuscarProduc.Location = new System.Drawing.Point(37, 65);
            this.picBuscarProduc.Name = "picBuscarProduc";
            this.picBuscarProduc.Size = new System.Drawing.Size(175, 168);
            this.picBuscarProduc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBuscarProduc.TabIndex = 43;
            this.picBuscarProduc.TabStop = false;
            this.picBuscarProduc.UseWaitCursor = true;
            // 
            // panelTimer
            // 
            this.panelTimer.Controls.Add(this.pictureBox3);
            this.panelTimer.Controls.Add(this.lblReloj);
            this.panelTimer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTimer.Location = new System.Drawing.Point(0, 629);
            this.panelTimer.Name = "panelTimer";
            this.panelTimer.Size = new System.Drawing.Size(269, 115);
            this.panelTimer.TabIndex = 43;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(0, 29);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(70, 83);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // lblReloj
            // 
            this.lblReloj.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblReloj.Font = new System.Drawing.Font("Lucida Bright", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReloj.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblReloj.Location = new System.Drawing.Point(70, 54);
            this.lblReloj.Name = "lblReloj";
            this.lblReloj.Size = new System.Drawing.Size(194, 52);
            this.lblReloj.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnCompras);
            this.panel4.Controls.Add(this.btnCreditos);
            this.panel4.Controls.Add(this.btnVentas);
            this.panel4.Controls.Add(this.btnProductos);
            this.panel4.Controls.Add(this.btnHome);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.label21);
            this.panel4.Controls.Add(this.label22);
            this.panel4.Location = new System.Drawing.Point(2, 291);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(264, 340);
            this.panel4.TabIndex = 42;
            // 
            // btnCompras
            // 
            this.btnCompras.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnCompras.FlatAppearance.BorderSize = 2;
            this.btnCompras.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCompras.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnCompras.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCompras.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCompras.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCompras.Image = ((System.Drawing.Image)(resources.GetObject("btnCompras.Image")));
            this.btnCompras.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCompras.Location = new System.Drawing.Point(7, 274);
            this.btnCompras.Name = "btnCompras";
            this.btnCompras.Size = new System.Drawing.Size(249, 47);
            this.btnCompras.TabIndex = 44;
            this.btnCompras.Text = " Persona";
            this.btnCompras.UseVisualStyleBackColor = false;
            this.btnCompras.Click += new System.EventHandler(this.btnCompras_Click);
            // 
            // btnCreditos
            // 
            this.btnCreditos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnCreditos.FlatAppearance.BorderSize = 2;
            this.btnCreditos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCreditos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnCreditos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreditos.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreditos.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCreditos.Image = ((System.Drawing.Image)(resources.GetObject("btnCreditos.Image")));
            this.btnCreditos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCreditos.Location = new System.Drawing.Point(6, 212);
            this.btnCreditos.Name = "btnCreditos";
            this.btnCreditos.Size = new System.Drawing.Size(249, 47);
            this.btnCreditos.TabIndex = 43;
            this.btnCreditos.Text = "Créditos";
            this.btnCreditos.UseVisualStyleBackColor = false;
            this.btnCreditos.Click += new System.EventHandler(this.btnCreditos_Click);
            // 
            // btnVentas
            // 
            this.btnVentas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnVentas.FlatAppearance.BorderSize = 2;
            this.btnVentas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnVentas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnVentas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVentas.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVentas.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnVentas.Image = ((System.Drawing.Image)(resources.GetObject("btnVentas.Image")));
            this.btnVentas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVentas.Location = new System.Drawing.Point(6, 85);
            this.btnVentas.Name = "btnVentas";
            this.btnVentas.Size = new System.Drawing.Size(249, 47);
            this.btnVentas.TabIndex = 42;
            this.btnVentas.Text = "Clientes";
            this.btnVentas.UseVisualStyleBackColor = false;
            this.btnVentas.Click += new System.EventHandler(this.btnVentas_Click);
            // 
            // btnProductos
            // 
            this.btnProductos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnProductos.FlatAppearance.BorderSize = 2;
            this.btnProductos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnProductos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnProductos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProductos.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProductos.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnProductos.Image = ((System.Drawing.Image)(resources.GetObject("btnProductos.Image")));
            this.btnProductos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProductos.Location = new System.Drawing.Point(6, 148);
            this.btnProductos.Name = "btnProductos";
            this.btnProductos.Size = new System.Drawing.Size(249, 47);
            this.btnProductos.TabIndex = 41;
            this.btnProductos.Text = "   Productos";
            this.btnProductos.UseVisualStyleBackColor = false;
            this.btnProductos.Click += new System.EventHandler(this.btnProductos_Click);
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnHome.FlatAppearance.BorderSize = 2;
            this.btnHome.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnHome.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHome.Image = ((System.Drawing.Image)(resources.GetObject("btnHome.Image")));
            this.btnHome.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHome.Location = new System.Drawing.Point(6, 24);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(249, 47);
            this.btnHome.TabIndex = 40;
            this.btnHome.Text = "Home";
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // label17
            // 
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label17.Location = new System.Drawing.Point(5, 132);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(250, 3);
            this.label17.TabIndex = 39;
            this.label17.Text = "label17";
            this.label17.UseWaitCursor = true;
            // 
            // label18
            // 
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label18.Location = new System.Drawing.Point(6, 71);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(250, 3);
            this.label18.TabIndex = 38;
            this.label18.Text = "label18";
            this.label18.UseWaitCursor = true;
            // 
            // label20
            // 
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label20.Location = new System.Drawing.Point(7, 259);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(250, 3);
            this.label20.TabIndex = 37;
            this.label20.Text = "label20";
            this.label20.UseWaitCursor = true;
            // 
            // label21
            // 
            this.label21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label21.Location = new System.Drawing.Point(7, 321);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(250, 3);
            this.label21.TabIndex = 36;
            this.label21.Text = "label21";
            this.label21.UseWaitCursor = true;
            // 
            // label22
            // 
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label22.Location = new System.Drawing.Point(6, 195);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(250, 3);
            this.label22.TabIndex = 35;
            this.label22.Text = "label22";
            this.label22.UseWaitCursor = true;
            // 
            // pnlBarraTareas
            // 
            this.pnlBarraTareas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.pnlBarraTareas.Controls.Add(this.pictureBox9);
            this.pnlBarraTareas.Controls.Add(this.btnExpandirMn);
            this.pnlBarraTareas.Controls.Add(this.txtConsultaProducto);
            this.pnlBarraTareas.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBarraTareas.Location = new System.Drawing.Point(269, 0);
            this.pnlBarraTareas.Name = "pnlBarraTareas";
            this.pnlBarraTareas.Size = new System.Drawing.Size(796, 67);
            this.pnlBarraTareas.TabIndex = 18;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(731, 0);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(65, 67);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox9.TabIndex = 46;
            this.pictureBox9.TabStop = false;
            this.pictureBox9.Click += new System.EventHandler(this.pictureBox9_Click);
            // 
            // btnExpandirMn
            // 
            this.btnExpandirMn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnExpandirMn.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnExpandirMn.FlatAppearance.BorderSize = 0;
            this.btnExpandirMn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnExpandirMn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnExpandirMn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExpandirMn.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExpandirMn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnExpandirMn.Image = ((System.Drawing.Image)(resources.GetObject("btnExpandirMn.Image")));
            this.btnExpandirMn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExpandirMn.Location = new System.Drawing.Point(0, 0);
            this.btnExpandirMn.Name = "btnExpandirMn";
            this.btnExpandirMn.Size = new System.Drawing.Size(62, 67);
            this.btnExpandirMn.TabIndex = 45;
            this.btnExpandirMn.UseVisualStyleBackColor = false;
            this.btnExpandirMn.Click += new System.EventHandler(this.btnExpandirMn_Click_1);
            // 
            // txtConsultaProducto
            // 
            this.txtConsultaProducto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConsultaProducto.Location = new System.Drawing.Point(94, 32);
            this.txtConsultaProducto.Name = "txtConsultaProducto";
            this.txtConsultaProducto.Size = new System.Drawing.Size(578, 20);
            this.txtConsultaProducto.TabIndex = 41;
            this.txtConsultaProducto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtConsultaProducto_KeyPress);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.pnlInfoProductos);
            this.panel6.Controls.Add(this.pnlInfoAbrirCaja);
            this.panel6.Controls.Add(this.dgListaProductos);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(269, 67);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(796, 677);
            this.panel6.TabIndex = 19;
            // 
            // pnlInfoProductos
            // 
            this.pnlInfoProductos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            this.pnlInfoProductos.Controls.Add(this.dgProductos);
            this.pnlInfoProductos.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlInfoProductos.Location = new System.Drawing.Point(0, 0);
            this.pnlInfoProductos.Name = "pnlInfoProductos";
            this.pnlInfoProductos.Size = new System.Drawing.Size(796, 460);
            this.pnlInfoProductos.TabIndex = 46;
            this.pnlInfoProductos.Visible = false;
            // 
            // dgProductos
            // 
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            this.dgProductos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgProductos.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            this.dgProductos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgProductos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgProductos.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.HotTrack;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProductos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgProductos.ColumnHeadersHeight = 30;
            this.dgProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgProductos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgProductos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgProductos.EnableHeadersVisualStyles = false;
            this.dgProductos.GridColor = System.Drawing.Color.SteelBlue;
            this.dgProductos.Location = new System.Drawing.Point(0, 0);
            this.dgProductos.Name = "dgProductos";
            this.dgProductos.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProductos.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.White;
            this.dgProductos.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgProductos.Size = new System.Drawing.Size(796, 460);
            this.dgProductos.TabIndex = 1;
            this.dgProductos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgProductos_CellContentClick);
            this.dgProductos.MouseLeave += new System.EventHandler(this.dgProductos_MouseLeave);
            // 
            // pnlInfoAbrirCaja
            // 
            this.pnlInfoAbrirCaja.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            this.pnlInfoAbrirCaja.Controls.Add(this.pictureBox6);
            this.pnlInfoAbrirCaja.Controls.Add(this.btnCerrarCaja);
            this.pnlInfoAbrirCaja.Controls.Add(this.btnAbrirCaja);
            this.pnlInfoAbrirCaja.Controls.Add(this.txtMontoEnCaja);
            this.pnlInfoAbrirCaja.Controls.Add(this.label4);
            this.pnlInfoAbrirCaja.Controls.Add(this.label9);
            this.pnlInfoAbrirCaja.Controls.Add(this.txtVentasEnTarjeta);
            this.pnlInfoAbrirCaja.Controls.Add(this.txtVentasEnEfectivo);
            this.pnlInfoAbrirCaja.Controls.Add(this.txtVentasEnCredito);
            this.pnlInfoAbrirCaja.Controls.Add(this.pictureBox7);
            this.pnlInfoAbrirCaja.Controls.Add(this.label14);
            this.pnlInfoAbrirCaja.Controls.Add(this.label12);
            this.pnlInfoAbrirCaja.Controls.Add(this.label11);
            this.pnlInfoAbrirCaja.Controls.Add(this.pictureBox5);
            this.pnlInfoAbrirCaja.Controls.Add(this.label7);
            this.pnlInfoAbrirCaja.Controls.Add(this.txtVentaDeTurno);
            this.pnlInfoAbrirCaja.Controls.Add(this.label1);
            this.pnlInfoAbrirCaja.Controls.Add(this.numMontoInicial);
            this.pnlInfoAbrirCaja.Controls.Add(this.label5);
            this.pnlInfoAbrirCaja.Controls.Add(this.panel14);
            this.pnlInfoAbrirCaja.Location = new System.Drawing.Point(-2, -1);
            this.pnlInfoAbrirCaja.Name = "pnlInfoAbrirCaja";
            this.pnlInfoAbrirCaja.Size = new System.Drawing.Size(805, 424);
            this.pnlInfoAbrirCaja.TabIndex = 1;
            this.pnlInfoAbrirCaja.Visible = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.ImageLocation = "";
            this.pictureBox6.Location = new System.Drawing.Point(345, 251);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(166, 104);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 83;
            this.pictureBox6.TabStop = false;
            // 
            // btnCerrarCaja
            // 
            this.btnCerrarCaja.FlatAppearance.BorderSize = 2;
            this.btnCerrarCaja.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarCaja.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrarCaja.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCerrarCaja.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrarCaja.Image")));
            this.btnCerrarCaja.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCerrarCaja.Location = new System.Drawing.Point(672, 107);
            this.btnCerrarCaja.Name = "btnCerrarCaja";
            this.btnCerrarCaja.Size = new System.Drawing.Size(130, 40);
            this.btnCerrarCaja.TabIndex = 82;
            this.btnCerrarCaja.Text = "Cerrar Caja";
            this.btnCerrarCaja.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCerrarCaja.UseVisualStyleBackColor = true;
            this.btnCerrarCaja.Click += new System.EventHandler(this.btnCerrarCaja_Click);
            // 
            // btnAbrirCaja
            // 
            this.btnAbrirCaja.FlatAppearance.BorderSize = 2;
            this.btnAbrirCaja.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAbrirCaja.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAbrirCaja.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAbrirCaja.Image = ((System.Drawing.Image)(resources.GetObject("btnAbrirCaja.Image")));
            this.btnAbrirCaja.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAbrirCaja.Location = new System.Drawing.Point(672, 55);
            this.btnAbrirCaja.Name = "btnAbrirCaja";
            this.btnAbrirCaja.Size = new System.Drawing.Size(130, 40);
            this.btnAbrirCaja.TabIndex = 81;
            this.btnAbrirCaja.Text = "Abrir Caja";
            this.btnAbrirCaja.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAbrirCaja.UseVisualStyleBackColor = true;
            this.btnAbrirCaja.Click += new System.EventHandler(this.btnAbrirCaja_Click_1);
            // 
            // txtMontoEnCaja
            // 
            this.txtMontoEnCaja.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMontoEnCaja.Location = new System.Drawing.Point(485, 112);
            this.txtMontoEnCaja.Name = "txtMontoEnCaja";
            this.txtMontoEnCaja.Size = new System.Drawing.Size(169, 39);
            this.txtMontoEnCaja.TabIndex = 79;
            this.txtMontoEnCaja.Text = "0.000";
            this.txtMontoEnCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Lucida Bright", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label4.Location = new System.Drawing.Point(459, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(215, 33);
            this.label4.TabIndex = 78;
            this.label4.Text = "Monto en caja";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Lucida Bright", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label9.Location = new System.Drawing.Point(350, 162);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(148, 40);
            this.label9.TabIndex = 51;
            this.label9.Text = "Montos";
            // 
            // txtVentasEnTarjeta
            // 
            this.txtVentasEnTarjeta.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVentasEnTarjeta.Location = new System.Drawing.Point(582, 361);
            this.txtVentasEnTarjeta.Name = "txtVentasEnTarjeta";
            this.txtVentasEnTarjeta.Size = new System.Drawing.Size(169, 39);
            this.txtVentasEnTarjeta.TabIndex = 77;
            this.txtVentasEnTarjeta.Text = "0.000";
            this.txtVentasEnTarjeta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtVentasEnEfectivo
            // 
            this.txtVentasEnEfectivo.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVentasEnEfectivo.Location = new System.Drawing.Point(342, 361);
            this.txtVentasEnEfectivo.Name = "txtVentasEnEfectivo";
            this.txtVentasEnEfectivo.Size = new System.Drawing.Size(169, 39);
            this.txtVentasEnEfectivo.TabIndex = 76;
            this.txtVentasEnEfectivo.Text = "0.000";
            this.txtVentasEnEfectivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtVentasEnCredito
            // 
            this.txtVentasEnCredito.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVentasEnCredito.Location = new System.Drawing.Point(96, 361);
            this.txtVentasEnCredito.Name = "txtVentasEnCredito";
            this.txtVentasEnCredito.Size = new System.Drawing.Size(169, 39);
            this.txtVentasEnCredito.TabIndex = 75;
            this.txtVentasEnCredito.Text = "0.000";
            this.txtVentasEnCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.ImageLocation = "";
            this.pictureBox7.Location = new System.Drawing.Point(603, 257);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(114, 98);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 74;
            this.pictureBox7.TabStop = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Lucida Bright", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label14.Location = new System.Drawing.Point(599, 219);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(118, 33);
            this.label14.TabIndex = 73;
            this.label14.Text = "Tarjeta";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Lucida Bright", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label12.Location = new System.Drawing.Point(359, 219);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(132, 33);
            this.label12.TabIndex = 72;
            this.label12.Text = "Efectivo";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Lucida Bright", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label11.Location = new System.Drawing.Point(113, 215);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 33);
            this.label11.TabIndex = 71;
            this.label11.Text = "Crédito";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(111, 251);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(114, 98);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 68;
            this.pictureBox5.TabStop = false;
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Location = new System.Drawing.Point(-3, 159);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(805, 3);
            this.label7.TabIndex = 65;
            this.label7.Text = "label7";
            this.label7.UseWaitCursor = true;
            // 
            // txtVentaDeTurno
            // 
            this.txtVentaDeTurno.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVentaDeTurno.Location = new System.Drawing.Point(269, 110);
            this.txtVentaDeTurno.Name = "txtVentaDeTurno";
            this.txtVentaDeTurno.Size = new System.Drawing.Size(169, 39);
            this.txtVentaDeTurno.TabIndex = 53;
            this.txtVentaDeTurno.Text = "0.000";
            this.txtVentaDeTurno.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Bright", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(5, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(201, 33);
            this.label1.TabIndex = 50;
            this.label1.Text = "Monto Inicial";
            // 
            // numMontoInicial
            // 
            this.numMontoInicial.DecimalPlaces = 1;
            this.numMontoInicial.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numMontoInicial.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.numMontoInicial.Location = new System.Drawing.Point(6, 111);
            this.numMontoInicial.Margin = new System.Windows.Forms.Padding(2);
            this.numMontoInicial.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numMontoInicial.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            196608});
            this.numMontoInicial.Name = "numMontoInicial";
            this.numMontoInicial.Size = new System.Drawing.Size(189, 39);
            this.numMontoInicial.TabIndex = 49;
            this.numMontoInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numMontoInicial.ThousandsSeparator = true;
            this.numMontoInicial.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Lucida Bright", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label5.Location = new System.Drawing.Point(218, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(239, 33);
            this.label5.TabIndex = 52;
            this.label5.Text = "Venta del turno";
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.label10);
            this.panel14.Controls.Add(this.lblCerrarInfoCaja);
            this.panel14.Controls.Add(this.lblNumberCaja);
            this.panel14.Controls.Add(this.pictureBox4);
            this.panel14.Controls.Add(this.lblTitleInfoCaja);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(805, 53);
            this.panel14.TabIndex = 52;
            // 
            // label10
            // 
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Location = new System.Drawing.Point(-3, 49);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(805, 3);
            this.label10.TabIndex = 78;
            this.label10.Text = "label10";
            this.label10.UseWaitCursor = true;
            // 
            // lblCerrarInfoCaja
            // 
            this.lblCerrarInfoCaja.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCerrarInfoCaja.AutoSize = true;
            this.lblCerrarInfoCaja.Font = new System.Drawing.Font("Lucida Bright", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCerrarInfoCaja.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblCerrarInfoCaja.Location = new System.Drawing.Point(758, 6);
            this.lblCerrarInfoCaja.Name = "lblCerrarInfoCaja";
            this.lblCerrarInfoCaja.Size = new System.Drawing.Size(42, 40);
            this.lblCerrarInfoCaja.TabIndex = 50;
            this.lblCerrarInfoCaja.Text = "X";
            this.lblCerrarInfoCaja.Click += new System.EventHandler(this.lblCerrarInfoCaja_Click);
            // 
            // lblNumberCaja
            // 
            this.lblNumberCaja.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblNumberCaja.AutoSize = true;
            this.lblNumberCaja.Font = new System.Drawing.Font("Lucida Bright", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumberCaja.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblNumberCaja.Location = new System.Drawing.Point(6, 11);
            this.lblNumberCaja.Name = "lblNumberCaja";
            this.lblNumberCaja.Size = new System.Drawing.Size(62, 40);
            this.lblNumberCaja.TabIndex = 49;
            this.lblNumberCaja.Text = "#1";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::pdv_uth_v1.Properties.Resources.ic_info_outline_white_48dp;
            this.pictureBox4.Location = new System.Drawing.Point(572, 6);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(51, 43);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 48;
            this.pictureBox4.TabStop = false;
            // 
            // lblTitleInfoCaja
            // 
            this.lblTitleInfoCaja.AutoSize = true;
            this.lblTitleInfoCaja.Font = new System.Drawing.Font("Lucida Bright", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitleInfoCaja.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblTitleInfoCaja.Location = new System.Drawing.Point(243, 9);
            this.lblTitleInfoCaja.Name = "lblTitleInfoCaja";
            this.lblTitleInfoCaja.Size = new System.Drawing.Size(323, 40);
            this.lblTitleInfoCaja.TabIndex = 47;
            this.lblTitleInfoCaja.Text = "Información Caja\r\n";
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.panel1);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel12.Location = new System.Drawing.Point(269, 565);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(796, 179);
            this.panel12.TabIndex = 21;
            // 
            // TTCaja
            // 
            this.TTCaja.ShowAlways = true;
            // 
            // FrmCaja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1352, 744);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.pnlBarraTareas);
            this.Controls.Add(this.pnlNavbar);
            this.Controls.Add(this.panel5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmCaja";
            this.Text = "FrmCaja";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmCaja_FormClosing);
            this.Load += new System.EventHandler(this.FrmCaja_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgListaProductos)).EndInit();
            this.panelDatosCredito.ResumeLayout(false);
            this.panelDatosCredito.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericCantidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEfectivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCredito)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTarjeta)).EndInit();
            this.panel5.ResumeLayout(false);
            this.pnlVentaInfo.ResumeLayout(false);
            this.pnlVentaInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pibWebCam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picProductoAVender)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.pnlMetodoTarjeta.ResumeLayout(false);
            this.pnlMetodoTarjeta.PerformLayout();
            this.pnlCreditoCliente.ResumeLayout(false);
            this.pnlCreditoCliente.PerformLayout();
            this.pnlMetodoEfectivo.ResumeLayout(false);
            this.pnlMetodoEfectivo.PerformLayout();
            this.pnlNavbar.ResumeLayout(false);
            this.PnlInfo.ResumeLayout(false);
            this.PnlInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picInfoCaja)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBuscarProduc)).EndInit();
            this.panelTimer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel4.ResumeLayout(false);
            this.pnlBarraTareas.ResumeLayout(false);
            this.pnlBarraTareas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.panel6.ResumeLayout(false);
            this.pnlInfoProductos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgProductos)).EndInit();
            this.pnlInfoAbrirCaja.ResumeLayout(false);
            this.pnlInfoAbrirCaja.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMontoInicial)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel12.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.NumericUpDown numericCantidad;
        private System.Windows.Forms.Label lblCantidad;
        private System.Windows.Forms.TextBox txtCodBarras;
        private System.Windows.Forms.Label lblCodBarras;
        private System.Windows.Forms.DataGridView dgListaProductos;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.TextBox txtIva;
        private System.Windows.Forms.Label lblIva;
        private System.Windows.Forms.TextBox txtSubTotal;
        private System.Windows.Forms.Label lblSubtotal;
        private System.Windows.Forms.Panel panelDatosCredito;
        private System.Windows.Forms.Label lblFechaProxPago;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblCreditoDisponible;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblClienteCredito;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtMetodoEfectivo;
        private System.Windows.Forms.PictureBox pbEfectivo;
        private System.Windows.Forms.TextBox txtMetodoCredito;
        private System.Windows.Forms.PictureBox pbCredito;
        private System.Windows.Forms.TextBox txtMetodoTarjeta;
        private System.Windows.Forms.PictureBox pbTarjeta;
        private System.Windows.Forms.Button btnPagar;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnCancelarVenta;
        private System.Windows.Forms.Button btnAgregarAVenta;
        private System.Windows.Forms.Label lblNombreProducto;
        private System.Windows.Forms.PictureBox picProductoAVender;
        private System.Windows.Forms.Panel pnlVentaInfo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlNavbar;
        private System.Windows.Forms.Panel panelTimer;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnCompras;
        private System.Windows.Forms.Button btnCreditos;
        private System.Windows.Forms.Button btnVentas;
        private System.Windows.Forms.Button btnProductos;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel pnlBarraTareas;
        private System.Windows.Forms.Button btnExpandirMn;
        private System.Windows.Forms.TextBox txtConsultaProducto;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel pnlCreditoCliente;
        private System.Windows.Forms.Panel pnlMetodoEfectivo;
        private System.Windows.Forms.Panel pnlMetodoTarjeta;
        private System.Windows.Forms.PictureBox pibWebCam;
        private System.Windows.Forms.ComboBox cboCamera;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Button btnApagar;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label lblReloj;
        private System.Windows.Forms.Panel PnlInfo;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.Label lblCajaTitle;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.PictureBox picBuscarProduc;
        private System.Windows.Forms.Label lblCambio;
        private System.Windows.Forms.TextBox txtCambio;
        private System.Windows.Forms.Panel pnlInfoAbrirCaja;
        private System.Windows.Forms.TextBox txtVentaDeTurno;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numMontoInicial;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label lblTitleInfoCaja;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtVentasEnTarjeta;
        private System.Windows.Forms.TextBox txtVentasEnEfectivo;
        private System.Windows.Forms.TextBox txtVentasEnCredito;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblCerrarInfoCaja;
        private System.Windows.Forms.Label lblNumberCaja;
        private System.Windows.Forms.PictureBox picInfoCaja;
        private System.Windows.Forms.TextBox txtMontoEnCaja;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnCerrarCaja;
        private System.Windows.Forms.Button btnAbrirCaja;
        private System.Windows.Forms.Panel pnlInfoProductos;
        private System.Windows.Forms.DataGridView dgProductos;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.ToolTip TTCaja;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCodBarras;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDescripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPrecio;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColSubTotal;
    }
}