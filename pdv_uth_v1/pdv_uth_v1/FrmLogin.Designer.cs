﻿namespace pdv_uth_v1
{
    partial class FrmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLogin));
            this.panelLogin1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelLogin2 = new System.Windows.Forms.Panel();
            this.pnlInfoLogin = new System.Windows.Forms.Panel();
            this.lblInfoLogin = new System.Windows.Forms.Label();
            this.lblMacAddress = new System.Windows.Forms.LinkLabel();
            this.picMostrarInfo = new System.Windows.Forms.PictureBox();
            this.btnSalir = new System.Windows.Forms.Button();
            this.lblForgetPass = new System.Windows.Forms.LinkLabel();
            this.btnIngresar = new System.Windows.Forms.Button();
            this.txtContraseña = new System.Windows.Forms.TextBox();
            this.txtCorreo = new System.Windows.Forms.TextBox();
            this.loginTitle = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panelLogin1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelLogin2.SuspendLayout();
            this.pnlInfoLogin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMostrarInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelLogin1
            // 
            this.panelLogin1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(30)))), ((int)(((byte)(98)))));
            this.panelLogin1.Controls.Add(this.pictureBox1);
            this.panelLogin1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLogin1.Location = new System.Drawing.Point(0, 0);
            this.panelLogin1.Name = "panelLogin1";
            this.panelLogin1.Size = new System.Drawing.Size(276, 385);
            this.panelLogin1.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(53, 64);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(168, 266);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panelLogin2
            // 
            this.panelLogin2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.panelLogin2.Controls.Add(this.pictureBox2);
            this.panelLogin2.Controls.Add(this.pnlInfoLogin);
            this.panelLogin2.Controls.Add(this.picMostrarInfo);
            this.panelLogin2.Controls.Add(this.btnSalir);
            this.panelLogin2.Controls.Add(this.lblForgetPass);
            this.panelLogin2.Controls.Add(this.btnIngresar);
            this.panelLogin2.Controls.Add(this.txtContraseña);
            this.panelLogin2.Controls.Add(this.txtCorreo);
            this.panelLogin2.Controls.Add(this.loginTitle);
            this.panelLogin2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLogin2.Location = new System.Drawing.Point(276, 0);
            this.panelLogin2.Name = "panelLogin2";
            this.panelLogin2.Size = new System.Drawing.Size(660, 385);
            this.panelLogin2.TabIndex = 3;
            // 
            // pnlInfoLogin
            // 
            this.pnlInfoLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlInfoLogin.Controls.Add(this.lblInfoLogin);
            this.pnlInfoLogin.Controls.Add(this.lblMacAddress);
            this.pnlInfoLogin.Location = new System.Drawing.Point(404, 304);
            this.pnlInfoLogin.Name = "pnlInfoLogin";
            this.pnlInfoLogin.Size = new System.Drawing.Size(253, 51);
            this.pnlInfoLogin.TabIndex = 100;
            this.pnlInfoLogin.Visible = false;
            // 
            // lblInfoLogin
            // 
            this.lblInfoLogin.AutoSize = true;
            this.lblInfoLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.lblInfoLogin.Font = new System.Drawing.Font("Lucida Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfoLogin.ForeColor = System.Drawing.Color.DarkGray;
            this.lblInfoLogin.Location = new System.Drawing.Point(11, 4);
            this.lblInfoLogin.Name = "lblInfoLogin";
            this.lblInfoLogin.Size = new System.Drawing.Size(233, 14);
            this.lblInfoLogin.TabIndex = 0;
            this.lblInfoLogin.Text = "La caja esta asociada a su Ip Mac address:";
            // 
            // lblMacAddress
            // 
            this.lblMacAddress.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.lblMacAddress.AutoSize = true;
            this.lblMacAddress.LinkColor = System.Drawing.Color.DimGray;
            this.lblMacAddress.Location = new System.Drawing.Point(90, 27);
            this.lblMacAddress.Name = "lblMacAddress";
            this.lblMacAddress.Size = new System.Drawing.Size(80, 13);
            this.lblMacAddress.TabIndex = 11;
            this.lblMacAddress.TabStop = true;
            this.lblMacAddress.Text = "IP mac address";
            // 
            // picMostrarInfo
            // 
            this.picMostrarInfo.Image = global::pdv_uth_v1.Properties.Resources.ic_add_alert_white_48dp;
            this.picMostrarInfo.Location = new System.Drawing.Point(634, 359);
            this.picMostrarInfo.Name = "picMostrarInfo";
            this.picMostrarInfo.Size = new System.Drawing.Size(24, 23);
            this.picMostrarInfo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picMostrarInfo.TabIndex = 12;
            this.picMostrarInfo.TabStop = false;
            this.picMostrarInfo.Click += new System.EventHandler(this.picMostrarInfo_Click);
            this.picMostrarInfo.MouseEnter += new System.EventHandler(this.pictureBox2_MouseEnter);
            this.picMostrarInfo.MouseLeave += new System.EventHandler(this.picMostrarInfo_MouseLeave);
            this.picMostrarInfo.MouseHover += new System.EventHandler(this.picMostrarInfo_MouseHover);
            // 
            // btnSalir
            // 
            this.btnSalir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.ForeColor = System.Drawing.Color.DarkGray;
            this.btnSalir.Location = new System.Drawing.Point(635, 3);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(23, 22);
            this.btnSalir.TabIndex = 3;
            this.btnSalir.Text = "X";
            this.btnSalir.UseVisualStyleBackColor = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lblForgetPass
            // 
            this.lblForgetPass.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.lblForgetPass.AutoSize = true;
            this.lblForgetPass.LinkColor = System.Drawing.Color.DimGray;
            this.lblForgetPass.Location = new System.Drawing.Point(293, 348);
            this.lblForgetPass.Name = "lblForgetPass";
            this.lblForgetPass.Size = new System.Drawing.Size(146, 13);
            this.lblForgetPass.TabIndex = 10;
            this.lblForgetPass.TabStop = true;
            this.lblForgetPass.Text = "¿Ha olvidado su contraseña?";
            // 
            // btnIngresar
            // 
            this.btnIngresar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnIngresar.FlatAppearance.BorderSize = 0;
            this.btnIngresar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.btnIngresar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnIngresar.Font = new System.Drawing.Font("Lucida Sans", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIngresar.ForeColor = System.Drawing.Color.DarkGray;
            this.btnIngresar.Location = new System.Drawing.Point(200, 280);
            this.btnIngresar.Name = "btnIngresar";
            this.btnIngresar.Size = new System.Drawing.Size(320, 50);
            this.btnIngresar.TabIndex = 9;
            this.btnIngresar.Text = "Ingresar";
            this.btnIngresar.UseVisualStyleBackColor = false;
            this.btnIngresar.Click += new System.EventHandler(this.btnIngresar_Click_1);
            // 
            // txtContraseña
            // 
            this.txtContraseña.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.txtContraseña.Font = new System.Drawing.Font("Lucida Sans", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContraseña.ForeColor = System.Drawing.Color.DarkGray;
            this.txtContraseña.Location = new System.Drawing.Point(58, 199);
            this.txtContraseña.Name = "txtContraseña";
            this.txtContraseña.Size = new System.Drawing.Size(547, 32);
            this.txtContraseña.TabIndex = 8;
            this.txtContraseña.Text = "Contraseña";
            this.txtContraseña.TextChanged += new System.EventHandler(this.txtContraseña_TextChanged);
            this.txtContraseña.Enter += new System.EventHandler(this.txtContraseña_Enter);
            this.txtContraseña.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtContraseña_KeyPress);
            // 
            // txtCorreo
            // 
            this.txtCorreo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.txtCorreo.Font = new System.Drawing.Font("Lucida Sans", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCorreo.ForeColor = System.Drawing.Color.DarkGray;
            this.txtCorreo.Location = new System.Drawing.Point(58, 91);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Size = new System.Drawing.Size(547, 32);
            this.txtCorreo.TabIndex = 7;
            this.txtCorreo.Text = "Usuario";
            this.txtCorreo.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtCorreo_MouseClick);
            // 
            // loginTitle
            // 
            this.loginTitle.AutoSize = true;
            this.loginTitle.Font = new System.Drawing.Font("Lucida Sans", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loginTitle.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.loginTitle.Location = new System.Drawing.Point(270, 7);
            this.loginTitle.Name = "loginTitle";
            this.loginTitle.Size = new System.Drawing.Size(136, 42);
            this.loginTitle.TabIndex = 6;
            this.loginTitle.Text = "LOGIN";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(570, 203);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(30, 23);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 101;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseEnter += new System.EventHandler(this.pictureBox2_MouseEnter_1);
            this.pictureBox2.MouseLeave += new System.EventHandler(this.pictureBox2_MouseLeave);
            this.pictureBox2.MouseHover += new System.EventHandler(this.pictureBox2_MouseHover);
            // 
            // FrmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 385);
            this.ControlBox = false;
            this.Controls.Add(this.panelLogin2);
            this.Controls.Add(this.panelLogin1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmLogin";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.FrmLogin_Load);
            this.panelLogin1.ResumeLayout(false);
            this.panelLogin1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelLogin2.ResumeLayout(false);
            this.panelLogin2.PerformLayout();
            this.pnlInfoLogin.ResumeLayout(false);
            this.pnlInfoLogin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMostrarInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelLogin1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panelLogin2;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.LinkLabel lblForgetPass;
        private System.Windows.Forms.Button btnIngresar;
        private System.Windows.Forms.TextBox txtContraseña;
        private System.Windows.Forms.TextBox txtCorreo;
        private System.Windows.Forms.Label loginTitle;
        private System.Windows.Forms.LinkLabel lblMacAddress;
        private System.Windows.Forms.Panel pnlInfoLogin;
        private System.Windows.Forms.Label lblInfoLogin;
        private System.Windows.Forms.PictureBox picMostrarInfo;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}