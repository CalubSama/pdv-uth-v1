﻿using Lib_pdv_uth_v1;
using Lib_pdv_uth_v1.usuarios;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace pdv_uth_v1
{
    //agregar referencias lib_pdv usuarios/personas
    public partial class FrmUsuarios : Form
    {
        int idUsuario = 0;
        Usuario usuario;
        public FrmUsuarios()
        {
            InitializeComponent();
        }

        private void panelFormulario_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnApagar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Usted desea cerrar la ventana de usuarios?",
                            "Cerrar",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        
        private void btnRetroceder_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmMenuPpal frm = new FrmMenuPpal();
            frm.Show();
        }

        private void btnAñadirClientes_Click(object sender, EventArgs e)
        {
            habilitarFrom();
        }

        private void dgUsuarios_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            idUsuario = int.Parse(dgUsuarios.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtNombre.Text = dgUsuarios.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtApPat.Text = dgUsuarios.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtApMat.Text = dgUsuarios.Rows[e.RowIndex].Cells[3].Value.ToString();
            dtpFechaNacimiento.Value = DateTime.Parse(dgUsuarios.Rows[e.RowIndex].Cells[4].Value.ToString());
            txtCorreo.Text = dgUsuarios.Rows[e.RowIndex].Cells[7].Value.ToString();
            txtCelular.Text = dgUsuarios.Rows[e.RowIndex].Cells[6].Value.ToString();
            txtTelefono.Text = dgUsuarios.Rows[e.RowIndex].Cells[5].Value.ToString();
            txtCalle.Text = dgUsuarios.Rows[e.RowIndex].Cells[8].Value.ToString();
            txtNumCasa.Text = dgUsuarios.Rows[e.RowIndex].Cells[9].Value.ToString();
            txtCP.Text = dgUsuarios.Rows[e.RowIndex].Cells[10].Value.ToString();
            txtColonia.Text = dgUsuarios.Rows[e.RowIndex].Cells[11].Value.ToString();
            txtFraccionamiento.Text = dgUsuarios.Rows[e.RowIndex].Cells[12].Value.ToString();
            txtLocalidad.Text = dgUsuarios.Rows[e.RowIndex].Cells[13].Value.ToString();
            txtMunicipio.Text = dgUsuarios.Rows[e.RowIndex].Cells[14].Value.ToString();
            txtComprobanteDomicilio.Text = dgUsuarios.Rows[e.RowIndex].Cells[15].Value.ToString();
            txtComproINE.Text = dgUsuarios.Rows[e.RowIndex].Cells[16].Value.ToString();
            txtCurp.Text = dgUsuarios.Rows[e.RowIndex].Cells[17].Value.ToString();
            txtComprobanteCurp.Text = dgUsuarios.Rows[e.RowIndex].Cells[18].Value.ToString();
            txtActaDeNac.Text = dgUsuarios.Rows[e.RowIndex].Cells[19].Value.ToString();
            txtCertEstudios.Text = dgUsuarios.Rows[e.RowIndex].Cells[20].Value.ToString();
            txtNumSS.Text = dgUsuarios.Rows[e.RowIndex].Cells[21].Value.ToString();
            cbTipoUser.Text = dgUsuarios.Rows[e.RowIndex].Cells[22].Value.ToString();
            txtUsuarioContra.Text = dgUsuarios.Rows[e.RowIndex].Cells[23].Value.ToString();
        }

        private void btnModificarCliente_Click(object sender, EventArgs e)
        {
             Usuario paraModif = new Usuario();
            List<DatosParaActualizar> datos = new List<DatosParaActualizar>();

            datos.Add(new DatosParaActualizar("nombre", txtNombre.Text));
            datos.Add(new DatosParaActualizar("apellido_paterno", txtApPat.Text));
            datos.Add(new DatosParaActualizar("apellido_materno", txtApMat.Text));
            datos.Add(new DatosParaActualizar("fecha_de_nacimiento", dtpFechaNacimiento.Value.Year + "-" + dtpFechaNacimiento.Value.Month + "-" + dtpFechaNacimiento.Value.Day));
            datos.Add(new DatosParaActualizar("correo", txtCorreo.Text));
            datos.Add(new DatosParaActualizar("telefono", txtTelefono.Text));
            datos.Add(new DatosParaActualizar("celular", txtCelular.Text));
            datos.Add(new DatosParaActualizar("calle", txtCalle.Text));
            datos.Add(new DatosParaActualizar("numero_casa", txtNumCasa.Text));
            datos.Add(new DatosParaActualizar("codigo_postal", txtCP.Text));
            datos.Add(new DatosParaActualizar("colonia", txtColonia.Text));
            datos.Add(new DatosParaActualizar("fraccionamiento", txtFraccionamiento.Text));
            datos.Add(new DatosParaActualizar("localidad", txtLocalidad.Text));
            datos.Add(new DatosParaActualizar("municipio", txtMunicipio.Text));
            datos.Add(new DatosParaActualizar("img_comprobante_domicilio", lblImagenAGuardar.Text));
            datos.Add(new DatosParaActualizar("ine_comprobante", lblImagenAGuardar2.Text));
            datos.Add(new DatosParaActualizar("curp", txtCurp.Text));
            datos.Add(new DatosParaActualizar("curp_comprobante", lblImagenAGuardar1.Text));
            datos.Add(new DatosParaActualizar("acta_nacimiento", txtActaDeNac.Text));
            datos.Add(new DatosParaActualizar("certificado_estudios", lblImagenAGaurdarCert.Text));
            datos.Add(new DatosParaActualizar("numero_seguro_social",txtNumSS.Text));
            datos.Add(new DatosParaActualizar("tipo_usuario", cbTipoUser.Text));
            datos.Add(new DatosParaActualizar("pwd", txtUsuarioContra.Text));
            if (paraModif.modificar(datos, idUsuario))
            {
                MessageBox.Show("cliente modificado");
                mostrarRegistrosEnDG();
            }
            else MessageBox.Show("Error cliente no modificado");
            limpiarForm();
        }
        public void mostrarRegistrosEnDG()
        {
            Usuario cli = new Usuario();
            dgUsuarios.DataSource = null;
            //borrar todos los ren del DG 
            dgUsuarios.Rows.Clear();
            //se crea lista de criterios de busqueda
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = " 1 ";
            criterio.operadorIntermedio = OperadorDeConsulta.IGUAL;
            criterio.valor = "1";
            criterio.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dgUsuarios.DataSource = cli.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dgUsuarios.Refresh();
        }

        private void btnEliminarclientes_Click(object sender, EventArgs e)
        {
            Usuario cli = new Usuario();
            if (MessageBox.Show("¿Realmente desea eliminar este cliente?", "Borrar cliente", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (cli.eliminar(idUsuario))
                {
                    mostrarRegistrosEnDG();
                    MessageBox.Show("Cliente eliminado ");
                }
                else MessageBox.Show("Error, Cliente no fue eliminado ");
            }

        }

        private void FrmUsuarios_Load(object sender, EventArgs e)
        {
            limpiarForm();
            mostrarRegistrosEnDG();
        }

        private void btnGuardarCliente_Click(object sender, EventArgs e)
        {
            Usuario paraAlta = new Usuario();
            paraAlta.Nombre = txtNombre.Text;
            paraAlta.ApellidoPaterno = txtApPat.Text;
            paraAlta.ApellidoMaterno = txtApMat.Text;
            paraAlta.Celular = txtCelular.Text;
            paraAlta.Telefono = txtTelefono.Text;
            paraAlta.Correo = txtCorreo.Text;
            paraAlta.FechaNacimiento = dtpFechaNacimiento.Value;
            paraAlta.Domicilio.calle = txtCalle.Text;
            paraAlta.Domicilio.numero = txtNumCasa.Text;
            paraAlta.Domicilio.colonia = txtColonia.Text;
            paraAlta.Domicilio.seccionFraccionamiento = txtFraccionamiento.Text;
            paraAlta.Domicilio.codigoPostal = txtCP.Text;
            paraAlta.Domicilio.localidad = txtLocalidad.Text;
            paraAlta.Domicilio.municipio = txtMunicipio.Text;
            paraAlta.Domicilio.fotoComprobante = lblImagenAGuardar.Text;
            paraAlta.ComprobanteINE = lblImagenAGuardar2.Text;
            paraAlta.Curp = txtCurp.Text;
            paraAlta.CurpCompro = lblImagenAGuardar1.Text;
            paraAlta.ActaNacimientoComprobante = lblImagenAGuardarActa.Text;
            paraAlta.CertificadoEscolar = lblImagenAGuardarEstudios.Text;
            paraAlta.NumeroSeguroSocial = txtNumSS.Text;
            paraAlta.TipoUsuario = (TipoUsuario)Enum.Parse(typeof(TipoUsuario), cbTipoUser.SelectedItem.ToString());
            paraAlta.Contraseña = txtUsuarioContra.Text;
            if (paraAlta.alta())
            {
                /*                // obtener el dir de la app
                                string bin = System.IO.Path.GetDirectoryName(Application.StartupPath);
                                //ir a un dir arriba
                                string dir = System.IO.Path.GetDirectoryName(bin);
                                //agregar el path para guardar la imagen
                                dir += "\\Imagenes\\Clientes\\";
                                //guardar la imagen
                *//*                picComprobantes.Image.Save(dir + lblImagenAGuardar.Text);
                                pnlImgCURP.Image.Save(dir + lblImagenAGuardar2.Text);
                                PnlImgINE.Image.Save(dir + lblImagenAGuardar1.Text);
                /*                guardarImg(txtComprobanteCurp.Text, PictureBoxDomicilio)*/
                MessageBox.Show("registro correcto");
                mostrarRegistrosEnDG();
                limpiarForm();

            }
            else
            {
                MessageBox.Show("Errror al guardar cliente. " + Usuario.msgError);
            }
        }
        public void habilitarFrom()
        {
            //habilitar forms
            txtNombre.Enabled = txtApPat.Enabled = txtApMat.Enabled = dtpFechaNacimiento.Enabled = txtCorreo.Enabled =
            txtCelular.Enabled = txtTelefono.Enabled = txtCalle.Enabled = txtNumCasa.Enabled =
            txtCP.Enabled = txtColonia.Enabled = txtFraccionamiento.Enabled = txtLocalidad.Enabled = txtMunicipio.Enabled =
            txtComprobanteDomicilio.Enabled = txtComproINE.Enabled = txtCurp.Enabled = txtComprobanteCurp.Enabled =
             txtActaDeNac.Enabled = txtCertEstudios.Enabled = txtNumSS.Enabled= txtUsuarioContra.Enabled= cbTipoUser.Enabled = true;
            //change colors txt
            txtNombre.BackColor = txtApPat.BackColor = txtApMat.BackColor = txtCorreo.BackColor = txtCelular.BackColor =
            txtTelefono.BackColor = txtCalle.BackColor = txtNumCasa.BackColor = txtCP.BackColor = txtColonia.BackColor =
            txtFraccionamiento.BackColor = txtLocalidad.BackColor = txtMunicipio.BackColor = txtComprobanteDomicilio.BackColor =
            txtComproINE.BackColor = txtCurp.BackColor = txtComprobanteCurp.BackColor =
            txtActaDeNac.BackColor = txtCertEstudios.BackColor = txtNumSS.BackColor = txtUsuarioContra.BackColor = cbTipoUser.BackColor = 
            Color.White;
        }
        public void limpiarForm()
        {
            //desabilitamos el form
            txtNombre.Enabled = txtApPat.Enabled = txtApMat.Enabled = dtpFechaNacimiento.Enabled = txtCorreo.Enabled =
            txtCelular.Enabled = txtTelefono.Enabled = txtCalle.Enabled = txtNumCasa.Enabled =
            txtCP.Enabled = txtColonia.Enabled = txtFraccionamiento.Enabled = txtLocalidad.Enabled = txtMunicipio.Enabled =
            txtComprobanteDomicilio.Enabled = txtComproINE.Enabled = txtCurp.Enabled = txtComprobanteCurp.Enabled =
             txtActaDeNac.Enabled = txtCertEstudios.Enabled = txtNumSS.Enabled = txtUsuarioContra.Enabled = cbTipoUser.Enabled = 
            false;
            //limpiamos el form
            txtNombre.Text = txtApPat.Text = txtApMat.Text = txtCorreo.Text = txtCelular.Text =
            txtTelefono.Text = txtCalle.Text = txtNumCasa.Text = txtCP.Text = txtColonia.Text =
            txtFraccionamiento.Text = txtLocalidad.Text = txtMunicipio.Text = txtComprobanteDomicilio.Text =
            txtComproINE.Text = txtCurp.Text = txtComprobanteCurp.Text = txtActaDeNac.Text = txtCertEstudios.Text = txtNumSS.Text = cbTipoUser.Text=txtUsuarioContra.Text ="";
            // cambiar el color de fondo
            txtNombre.BackColor = txtApPat.BackColor = txtApMat.BackColor = txtCorreo.BackColor = txtCelular.BackColor =
            txtTelefono.BackColor = txtCalle.BackColor = txtNumCasa.BackColor = txtCP.BackColor = txtColonia.BackColor =
            txtFraccionamiento.BackColor = txtLocalidad.BackColor = txtMunicipio.BackColor = txtComprobanteDomicilio.BackColor =
            txtComproINE.BackColor = txtCurp.BackColor = txtComprobanteCurp.BackColor =
            txtActaDeNac.BackColor = txtCertEstudios.BackColor = txtNumSS.BackColor = txtUsuarioContra.BackColor = cbTipoUser.BackColor =
            Color.DarkGray;

        }

        private void PicBuscar_Click(object sender, EventArgs e)
        {
            if (txtBusqueda.Text == "")
                mostrarRegistrosEnDG();

            else
                consultarUsuario();
        }

        private void txtBusqueda_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txtBusqueda.Text == " ")
                    mostrarRegistrosEnDG();
                else
                    consultarUsuario();
            }

        }

        private void consultarUsuario()
        {
            Usuario cli = new Usuario();
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = "nombre ";
            criterio.operadorIntermedio = OperadorDeConsulta.LIKE;
            criterio.valor = "'%" + txtBusqueda.Text + "%' OR";
            criterio.operadorFinal = OperadorDeConsulta.OR;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            CriteriosBusqueda criterio2 = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio2.campo = "apellido_paterno";
            criterio2.operadorIntermedio = OperadorDeConsulta.LIKE;
            criterio2.valor = "'%" + txtBusqueda.Text + "%' OR";
            criterio2.operadorFinal = OperadorDeConsulta.OR;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio2);
            CriteriosBusqueda criterio3 = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio3.campo = "apellido_materno";
            criterio3.operadorIntermedio = OperadorDeConsulta.LIKE;
            criterio3.valor = "'%" + txtBusqueda.Text + "%'";
            criterio3.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio3);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dgUsuarios.DataSource = cli.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dgUsuarios.Update();
            dgUsuarios.Refresh();
        }

        private void dgUsuarios_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            idUsuario = int.Parse(dgUsuarios.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtNombre.Text = dgUsuarios.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtApPat.Text = dgUsuarios.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtApMat.Text = dgUsuarios.Rows[e.RowIndex].Cells[3].Value.ToString();
            dtpFechaNacimiento.Value = DateTime.Parse(dgUsuarios.Rows[e.RowIndex].Cells[4].Value.ToString());
            txtCorreo.Text = dgUsuarios.Rows[e.RowIndex].Cells[7].Value.ToString();
            txtCelular.Text = dgUsuarios.Rows[e.RowIndex].Cells[6].Value.ToString();
            txtTelefono.Text = dgUsuarios.Rows[e.RowIndex].Cells[5].Value.ToString();
            txtCalle.Text = dgUsuarios.Rows[e.RowIndex].Cells[8].Value.ToString();
            txtNumCasa.Text = dgUsuarios.Rows[e.RowIndex].Cells[9].Value.ToString();
            txtCP.Text = dgUsuarios.Rows[e.RowIndex].Cells[10].Value.ToString();
            txtColonia.Text = dgUsuarios.Rows[e.RowIndex].Cells[11].Value.ToString();
            txtFraccionamiento.Text = dgUsuarios.Rows[e.RowIndex].Cells[12].Value.ToString();
            txtLocalidad.Text = dgUsuarios.Rows[e.RowIndex].Cells[13].Value.ToString();
            txtMunicipio.Text = dgUsuarios.Rows[e.RowIndex].Cells[14].Value.ToString();
            txtComprobanteDomicilio.Text = dgUsuarios.Rows[e.RowIndex].Cells[15].Value.ToString();
            txtComproINE.Text = dgUsuarios.Rows[e.RowIndex].Cells[16].Value.ToString();
            txtCurp.Text = dgUsuarios.Rows[e.RowIndex].Cells[17].Value.ToString();
            txtComprobanteCurp.Text = dgUsuarios.Rows[e.RowIndex].Cells[18].Value.ToString();
            txtActaDeNac.Text = dgUsuarios.Rows[e.RowIndex].Cells[19].Value.ToString();
            txtCertEstudios.Text = dgUsuarios.Rows[e.RowIndex].Cells[20].Value.ToString();
            txtNumSS.Text = dgUsuarios.Rows[e.RowIndex].Cells[21].Value.ToString();
            cbTipoUser.Text = dgUsuarios.Rows[e.RowIndex].Cells[22].Value.ToString();
            txtUsuarioContra.Text = dgUsuarios.Rows[e.RowIndex].Cells[23].Value.ToString();
        }

        private void btnImgActaNac_MouseEnter(object sender, EventArgs e)
        {
            picActaNacc.Visible = true;
        }

        private void btnCerEst_MouseEnter(object sender, EventArgs e)
        {
            picCertEst.Visible = true;
        }

        private void btnComproDom_MouseEnter(object sender, EventArgs e)
        {
            picComproDom.Visible = true;
        }

        private void btnComproINE_MouseEnter(object sender, EventArgs e)
        {
            picINE.Visible = true;

        }

        private void btnComproCurp_MouseEnter(object sender, EventArgs e)
        {
            picCURP.Visible = true;
        }

        private void btnImgActaNac_MouseHover(object sender, EventArgs e)
        {
            picActaNacc.Visible = true;
        }

        private void btnCerEst_MouseHover(object sender, EventArgs e)
        {
            picCertEst.Visible = true;
        }

        private void btnComproDom_MouseHover(object sender, EventArgs e)
        {
            picComproDom.Visible = true;
        }

        private void btnComproINE_MouseHover(object sender, EventArgs e)
        {
            picINE.Visible = true;
        }

        private void btnComproCurp_MouseHover(object sender, EventArgs e)
        {
            picCURP.Visible = true;
        }

        private void btnImgActaNac_Click(object sender, EventArgs e)
        {
            if (openFileDialogComprobantes.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = openFileDialogComprobantes.FileName;
                //se carga archivo por su nombre al picBox
                picActaNacc.Image = Image.FromFile(openFileDialogComprobantes.FileName);
                //se toma el nmbre original
                lblImagenOriginalActa.Text = openFileDialogComprobantes.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension = lblImagenOriginalActa.Text.Substring(lblImagenOriginalActa.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblImagenAGuardarActa.Text = string.Format(@"Acta_{0}." + extension, DateTime.Now.Ticks);
            }
            txtActaDeNac.Text = lblImagenAGuardarActa.Text;
        }

        private void btnCerEst_Click(object sender, EventArgs e)
        {
            if (openFileDialogComprobantes.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = openFileDialogComprobantes.FileName;
                //se carga archivo por su nombre al picBox
                picCertEst.Image = Image.FromFile(openFileDialogComprobantes.FileName);
                //se toma el nmbre original
                lblImagenOriginalEstudios.Text = openFileDialogComprobantes.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension = lblImagenOriginalEstudios.Text.Substring(lblImagenOriginalEstudios.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblImagenAGuardarEstudios.Text = string.Format(@"Certificado_{0}." + extension, DateTime.Now.Ticks);
            }
            txtCertEstudios.Text = lblImagenAGuardarEstudios.Text;
        }

        private void btnComproDom_Click(object sender, EventArgs e)
        {
            if (openFileDialogComprobantes.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = openFileDialogComprobantes.FileName;
                //se carga archivo por su nombre al picBox
                picComproDom.Image = Image.FromFile(openFileDialogComprobantes.FileName);
                //se toma el nmbre original
                lblImagenOriginal.Text = openFileDialogComprobantes.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension = lblImagenOriginal.Text.Substring(lblImagenOriginal.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblImagenAGuardar.Text = string.Format(@"Dom_{0}."  + extension, DateTime.Now.Ticks);
            }
            txtComprobanteDomicilio.Text = lblImagenAGuardar.Text;
        }

        private void btnComproINE_Click(object sender, EventArgs e)
        {
            if (openFileDialogComprobantes.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = openFileDialogComprobantes.FileName;
                //se carga archivo por su nombre al picBox
                picINE.Image = Image.FromFile(openFileDialogComprobantes.FileName);
                //se toma el nmbre original
                lblImagenOriginal2.Text = openFileDialogComprobantes.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension1 = lblImagenOriginal2.Text.Substring(lblImagenOriginal2.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension1 = extension1.ToLower() == "peg" ? "jpeg" : extension1.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblImagenAGuardar2.Text = string.Format(txtNombre.Text + @"INE_{0}." + extension1, DateTime.Now.Ticks);
            }
            txtComproINE.Text = lblImagenAGuardar2.Text;
        }

        private void btnComproCurp_Click(object sender, EventArgs e)
        {
            if (openFileDialogComprobantes.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = openFileDialogComprobantes.FileName;
                //se carga archivo por su nombre al picBox
                picCURP.Image = Image.FromFile(openFileDialogComprobantes.FileName);
                //se toma el nmbre original
                lblImagenOriginal1.Text = openFileDialogComprobantes.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension2 = lblImagenOriginal1.Text.Substring(lblImagenOriginal1.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension2 = extension2.ToLower() == "peg" ? "jpeg" : extension2.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblImagenAGuardar1.Text = string.Format(@"CURP_{0}." + extension2, DateTime.Now.Ticks);
            }
            txtComprobanteCurp.Text = lblImagenAGuardar1.Text;
        }

        private void btnImgActaNac_MouseLeave(object sender, EventArgs e)
        {
            picActaNacc.Visible = false;
        }

        private void btnCerEst_MouseLeave(object sender, EventArgs e)
        {
            picCertEst.Visible = false;
        }

        private void btnComproDom_MouseLeave(object sender, EventArgs e)
        {
            picComproDom.Visible = false;
        }

        private void btnComproINE_MouseLeave(object sender, EventArgs e)
        {
            picINE.Visible = false;
        }

        private void btnComproCurp_MouseLeave(object sender, EventArgs e)
        {
            picCURP.Visible = false;
        }

        private void txtConfirmarContra_TextChanged(object sender, EventArgs e)
        {
            txtConfirmarContra.PasswordChar = '*';

            if (txtConfirmarContra.Text ==  txtUsuarioContra.Text)
            {
                txtConfirmarContra.BackColor = Color.Green;
            }
            else
            {
                txtConfirmarContra.BackColor = Color.Red;
            }
        }
    }
}
