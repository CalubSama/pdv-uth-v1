﻿using CrearTicketVenta;
using Lib_pdv_uth_v1.cajas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pdv_uth_v1
{
    public partial class TicketDeVenta : Form
    {
        Caja caja = new Caja();
        public TicketDeVenta()
        {
            InitializeComponent();
        }
        public void generarTicket()
        {
            //Creamos una instancia d ela clase CrearTicket
            CrearTicket ticket = new CrearTicket();
            //Ya podemos usar todos sus metodos
            ticket.AbreCajon();//Para abrir el cajon de dinero.

            //De aqui en adelante pueden formar su ticket a su gusto... Les muestro un ejemplo

            //Datos de la cabecera del Ticket.
            ticket.TextoCentro("ABARROTES SUSY");
            ticket.TextoIzquierda("EXPEDIDO EN: LOCAL PRINCIPAL");
            ticket.TextoIzquierda("DIREC: BOULEBARD LIVERTAD COL. OLIVOS");
            ticket.TextoIzquierda("TELEF: 666666666");
            ticket.TextoIzquierda("R.F.C: ABRSUSY22588");
            ticket.TextoIzquierda("EMAIL: DESARROLLADOR@GMAIL.COM");//Es el mio por si me quieren contactar ...
            ticket.TextoIzquierda("");
            ticket.TextoExtremos("Caja # 1", "Ticket # 002-0000006");
            ticket.lineasAsteriscos();

            //Sub cabecera.
            ticket.TextoIzquierda("");
            ticket.TextoIzquierda("ATENDIÓ:" + FrmLogin.personName);
            ticket.TextoIzquierda("CLIENTE: PUBLICO EN GENERAL");
            ticket.TextoIzquierda("");
            ticket.TextoExtremos("FECHA: " + DateTime.Now.ToShortDateString(), "HORA: " + DateTime.Now.ToShortTimeString());
            ticket.lineasAsteriscos();

            //Articulos a vender.
            ticket.EncabezadoVenta();//NOMBRE DEL ARTICULO, CANT, PRECIO, IMPORTE
            ticket.lineasAsteriscos();
            //Si tiene una DataGridView donde estan sus articulos a vender pueden usar esta manera para agregarlos al ticket.
            //foreach (DataGridViewRow fila in dgvLista.Rows)//dgvLista es el nombre del datagridview
            //{
            //ticket.AgregaArticulo(fila.Cells[2].Value.ToString(), int.Parse(fila.Cells[5].Value.ToString()),
            //double.Parse(fila.Cells[4].Value.ToString()), decimal.Parse(fila.Cells[6].Value.ToString()));
            //}
           object temp = new object();
            for (int i = 0; i < caja.ListaProductos.Count; i++)
            {
                    
            }
            ticket.AgregaArticulo("Cheto Flaming", 1, 9, 9);
            ticket.AgregaArticulo("Cigarros Económicos", 1, 20, 20);
            ticket.AgregaArticulo("Harina Diluvio", 1, 12.50, 12.50);
            ticket.lineasIgual();

            //Resumen de la venta. Sólo son ejemplos
            double totalVenta = FrmCaja.total;
            ticket.AgregarTotales("         SUBTOTAL......$",totalVenta * 0.84);
            ticket.AgregarTotales("         IVA...........$", totalVenta*0.16);//La M indica que es un double en C#
            ticket.AgregarTotales("         TOTAL.........$", totalVenta);
            ticket.TextoIzquierda("");
            ticket.AgregarTotales("         EFECTIVO......$", FrmCaja.montoRecibido);
            ticket.AgregarTotales("         CAMBIO........$", totalVenta - FrmCaja.montoRecibido);

            //Texto final del Ticket.
            ticket.TextoIzquierda("");
            
            ticket.TextoIzquierda("ARTÍCULOS VENDIDOS: "+caja.ListaProductos.Count);
            ticket.TextoIzquierda("");
            ticket.TextoCentro("¡GRACIAS POR SU COMPRA!");
            ticket.CortaTicket();
            ticket.ImprimirTicket("Microsoft XPS Document Writer");//Nombre de la impresora ticketera

        }
        private void button1_Click(object sender, EventArgs e)
        {
            generarTicket();
        }
    }
}
