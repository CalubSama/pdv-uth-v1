﻿namespace pdv_uth_v1
{
    partial class FrmCatalogoCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCatalogoCliente));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelForm = new System.Windows.Forms.Panel();
            this.lblImagenOriginal2 = new System.Windows.Forms.Label();
            this.lblImagenAGuardar2 = new System.Windows.Forms.Label();
            this.lblImagenAGuardar1 = new System.Windows.Forms.Label();
            this.lblImagenOriginal1 = new System.Windows.Forms.Label();
            this.lblImagenAGuardar = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnEliminarclientes = new System.Windows.Forms.Button();
            this.btnModificarCliente = new System.Windows.Forms.Button();
            this.btnPersonasAlternativas = new System.Windows.Forms.Button();
            this.btnGuardarCliente = new System.Windows.Forms.Button();
            this.btnApagar = new System.Windows.Forms.Button();
            this.lblImagenOriginal = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnAñadirClientes = new System.Windows.Forms.Button();
            this.lblNombreCliente = new System.Windows.Forms.Label();
            this.picUserImage = new System.Windows.Forms.PictureBox();
            this.panelFormulario = new System.Windows.Forms.Panel();
            this.pnlMostrarINE = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnImgComINE = new System.Windows.Forms.Button();
            this.txtComproINE = new System.Windows.Forms.TextBox();
            this.pnlMostrarCURP = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.btnImgCCurp = new System.Windows.Forms.Button();
            this.txtComprobanteCurp = new System.Windows.Forms.TextBox();
            this.pnlMostarPicDom = new System.Windows.Forms.Panel();
            this.txtComprobanteDom = new System.Windows.Forms.Label();
            this.btnImgCDom = new System.Windows.Forms.Button();
            this.txtComprobanteDomicilio = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtLocalidad = new System.Windows.Forms.TextBox();
            this.txtCurp = new System.Windows.Forms.TextBox();
            this.lblCalle = new System.Windows.Forms.Label();
            this.txtCalle = new System.Windows.Forms.TextBox();
            this.dtpFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.txtCP = new System.Windows.Forms.TextBox();
            this.txtNumCasa = new System.Windows.Forms.TextBox();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.txtFraccionamiento = new System.Windows.Forms.TextBox();
            this.txtMunicipio = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.txtColonia = new System.Windows.Forms.TextBox();
            this.txtCorreo = new System.Windows.Forms.TextBox();
            this.txtCelular = new System.Windows.Forms.TextBox();
            this.txtApMat = new System.Windows.Forms.TextBox();
            this.txtApPat = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.pnlNavbar = new System.Windows.Forms.Panel();
            this.panelTimer = new System.Windows.Forms.Panel();
            this.picReloj = new System.Windows.Forms.PictureBox();
            this.lblReloj = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnCajas = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCreditos = new System.Windows.Forms.Button();
            this.btnProductos = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.PnlInfo = new System.Windows.Forms.Panel();
            this.lblNombreUser = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblUserName = new System.Windows.Forms.Label();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.lblCajaTitle = new System.Windows.Forms.Label();
            this.ipbLogo = new System.Windows.Forms.PictureBox();
            this.txtBusqueda = new System.Windows.Forms.TextBox();
            this.PicBuscar = new System.Windows.Forms.PictureBox();
            this.PanelData = new System.Windows.Forms.Panel();
            this.pnlBarraTareas = new System.Windows.Forms.Panel();
            this.btnExpandirMn = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnDgClientes = new System.Windows.Forms.Panel();
            this.picComprobantes = new System.Windows.Forms.PictureBox();
            this.dgClientes = new System.Windows.Forms.DataGridView();
            this.PnlImgINE = new System.Windows.Forms.PictureBox();
            this.pnlImgCURP = new System.Windows.Forms.PictureBox();
            this.openFileDialogComprobantes = new System.Windows.Forms.OpenFileDialog();
            this.btnPersonaAlt = new System.Windows.Forms.Button();
            this.panelForm.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUserImage)).BeginInit();
            this.panelFormulario.SuspendLayout();
            this.pnlMostrarINE.SuspendLayout();
            this.pnlMostrarCURP.SuspendLayout();
            this.pnlMostarPicDom.SuspendLayout();
            this.pnlNavbar.SuspendLayout();
            this.panelTimer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picReloj)).BeginInit();
            this.panel4.SuspendLayout();
            this.PnlInfo.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ipbLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBuscar)).BeginInit();
            this.PanelData.SuspendLayout();
            this.pnlBarraTareas.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnDgClientes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picComprobantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgClientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PnlImgINE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlImgCURP)).BeginInit();
            this.SuspendLayout();
            // 
            // panelForm
            // 
            this.panelForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.panelForm.Controls.Add(this.lblImagenOriginal2);
            this.panelForm.Controls.Add(this.lblImagenAGuardar2);
            this.panelForm.Controls.Add(this.lblImagenAGuardar1);
            this.panelForm.Controls.Add(this.lblImagenOriginal1);
            this.panelForm.Controls.Add(this.lblImagenAGuardar);
            this.panelForm.Controls.Add(this.panel1);
            this.panelForm.Controls.Add(this.btnApagar);
            this.panelForm.Controls.Add(this.lblImagenOriginal);
            this.panelForm.Controls.Add(this.panel5);
            this.panelForm.Controls.Add(this.panelFormulario);
            this.panelForm.Controls.Add(this.btnLimpiar);
            this.panelForm.Controls.Add(this.btnBorrar);
            this.panelForm.Controls.Add(this.btnModificar);
            this.panelForm.Controls.Add(this.btnGuardar);
            this.panelForm.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelForm.Location = new System.Drawing.Point(816, 0);
            this.panelForm.Margin = new System.Windows.Forms.Padding(2);
            this.panelForm.Name = "panelForm";
            this.panelForm.Size = new System.Drawing.Size(275, 706);
            this.panelForm.TabIndex = 0;
            // 
            // lblImagenOriginal2
            // 
            this.lblImagenOriginal2.AutoSize = true;
            this.lblImagenOriginal2.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenOriginal2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenOriginal2.Location = new System.Drawing.Point(46, 24);
            this.lblImagenOriginal2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenOriginal2.Name = "lblImagenOriginal2";
            this.lblImagenOriginal2.Size = new System.Drawing.Size(113, 15);
            this.lblImagenOriginal2.TabIndex = 67;
            this.lblImagenOriginal2.Text = "Nombre Original";
            this.lblImagenOriginal2.Visible = false;
            // 
            // lblImagenAGuardar2
            // 
            this.lblImagenAGuardar2.AutoSize = true;
            this.lblImagenAGuardar2.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenAGuardar2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenAGuardar2.Location = new System.Drawing.Point(46, 47);
            this.lblImagenAGuardar2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenAGuardar2.Name = "lblImagenAGuardar2";
            this.lblImagenAGuardar2.Size = new System.Drawing.Size(113, 15);
            this.lblImagenAGuardar2.TabIndex = 66;
            this.lblImagenAGuardar2.Text = "Nombre Original";
            this.lblImagenAGuardar2.Visible = false;
            // 
            // lblImagenAGuardar1
            // 
            this.lblImagenAGuardar1.AutoSize = true;
            this.lblImagenAGuardar1.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenAGuardar1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenAGuardar1.Location = new System.Drawing.Point(-30, 1);
            this.lblImagenAGuardar1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenAGuardar1.Name = "lblImagenAGuardar1";
            this.lblImagenAGuardar1.Size = new System.Drawing.Size(113, 15);
            this.lblImagenAGuardar1.TabIndex = 65;
            this.lblImagenAGuardar1.Text = "Nombre Original";
            this.lblImagenAGuardar1.Visible = false;
            // 
            // lblImagenOriginal1
            // 
            this.lblImagenOriginal1.AutoSize = true;
            this.lblImagenOriginal1.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenOriginal1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenOriginal1.Location = new System.Drawing.Point(-30, 24);
            this.lblImagenOriginal1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenOriginal1.Name = "lblImagenOriginal1";
            this.lblImagenOriginal1.Size = new System.Drawing.Size(113, 15);
            this.lblImagenOriginal1.TabIndex = 64;
            this.lblImagenOriginal1.Text = "Nombre Original";
            this.lblImagenOriginal1.Visible = false;
            // 
            // lblImagenAGuardar
            // 
            this.lblImagenAGuardar.AutoSize = true;
            this.lblImagenAGuardar.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenAGuardar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenAGuardar.Location = new System.Drawing.Point(87, 4);
            this.lblImagenAGuardar.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenAGuardar.Name = "lblImagenAGuardar";
            this.lblImagenAGuardar.Size = new System.Drawing.Size(113, 15);
            this.lblImagenAGuardar.TabIndex = 63;
            this.lblImagenAGuardar.Text = "Nombre Original";
            this.lblImagenAGuardar.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.btnEliminarclientes);
            this.panel1.Controls.Add(this.btnModificarCliente);
            this.panel1.Controls.Add(this.btnPersonasAlternativas);
            this.panel1.Controls.Add(this.btnGuardarCliente);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 565);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(275, 141);
            this.panel1.TabIndex = 43;
            // 
            // btnEliminarclientes
            // 
            this.btnEliminarclientes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEliminarclientes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnEliminarclientes.FlatAppearance.BorderSize = 2;
            this.btnEliminarclientes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnEliminarclientes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnEliminarclientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarclientes.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarclientes.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnEliminarclientes.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminarclientes.Image")));
            this.btnEliminarclientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarclientes.Location = new System.Drawing.Point(7, 75);
            this.btnEliminarclientes.Name = "btnEliminarclientes";
            this.btnEliminarclientes.Size = new System.Drawing.Size(122, 57);
            this.btnEliminarclientes.TabIndex = 39;
            this.btnEliminarclientes.Text = "Eliminar     Clientes";
            this.btnEliminarclientes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEliminarclientes.UseVisualStyleBackColor = false;
            this.btnEliminarclientes.Click += new System.EventHandler(this.btnEliminarclientes_Click);
            // 
            // btnModificarCliente
            // 
            this.btnModificarCliente.FlatAppearance.BorderSize = 2;
            this.btnModificarCliente.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnModificarCliente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnModificarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModificarCliente.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificarCliente.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnModificarCliente.Image = ((System.Drawing.Image)(resources.GetObject("btnModificarCliente.Image")));
            this.btnModificarCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnModificarCliente.Location = new System.Drawing.Point(141, 2);
            this.btnModificarCliente.Name = "btnModificarCliente";
            this.btnModificarCliente.Size = new System.Drawing.Size(122, 57);
            this.btnModificarCliente.TabIndex = 42;
            this.btnModificarCliente.Text = "Modificar Cliente";
            this.btnModificarCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnModificarCliente.UseVisualStyleBackColor = true;
            this.btnModificarCliente.Click += new System.EventHandler(this.btnModificarCliente_Click);
            // 
            // btnPersonasAlternativas
            // 
            this.btnPersonasAlternativas.FlatAppearance.BorderSize = 2;
            this.btnPersonasAlternativas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnPersonasAlternativas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnPersonasAlternativas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPersonasAlternativas.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPersonasAlternativas.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnPersonasAlternativas.Image = ((System.Drawing.Image)(resources.GetObject("btnPersonasAlternativas.Image")));
            this.btnPersonasAlternativas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPersonasAlternativas.Location = new System.Drawing.Point(7, 2);
            this.btnPersonasAlternativas.Name = "btnPersonasAlternativas";
            this.btnPersonasAlternativas.Size = new System.Drawing.Size(122, 57);
            this.btnPersonasAlternativas.TabIndex = 41;
            this.btnPersonasAlternativas.Text = " Persona Secundaria";
            this.btnPersonasAlternativas.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPersonasAlternativas.UseVisualStyleBackColor = true;
            this.btnPersonasAlternativas.Click += new System.EventHandler(this.btnPersonasAlternativas_Click);
            // 
            // btnGuardarCliente
            // 
            this.btnGuardarCliente.FlatAppearance.BorderSize = 2;
            this.btnGuardarCliente.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnGuardarCliente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnGuardarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardarCliente.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardarCliente.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnGuardarCliente.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardarCliente.Image")));
            this.btnGuardarCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardarCliente.Location = new System.Drawing.Point(141, 75);
            this.btnGuardarCliente.Name = "btnGuardarCliente";
            this.btnGuardarCliente.Size = new System.Drawing.Size(122, 56);
            this.btnGuardarCliente.TabIndex = 40;
            this.btnGuardarCliente.Text = "         Guardar      Cliente";
            this.btnGuardarCliente.UseVisualStyleBackColor = true;
            this.btnGuardarCliente.Click += new System.EventHandler(this.btnGuardarCliente_Click);
            // 
            // btnApagar
            // 
            this.btnApagar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnApagar.FlatAppearance.BorderSize = 0;
            this.btnApagar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnApagar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApagar.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApagar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnApagar.Image = ((System.Drawing.Image)(resources.GetObject("btnApagar.Image")));
            this.btnApagar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnApagar.Location = new System.Drawing.Point(222, 0);
            this.btnApagar.Name = "btnApagar";
            this.btnApagar.Size = new System.Drawing.Size(53, 54);
            this.btnApagar.TabIndex = 37;
            this.btnApagar.Text = "Salir";
            this.btnApagar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnApagar.UseVisualStyleBackColor = true;
            this.btnApagar.Click += new System.EventHandler(this.btnApagar_Click);
            // 
            // lblImagenOriginal
            // 
            this.lblImagenOriginal.AutoSize = true;
            this.lblImagenOriginal.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenOriginal.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenOriginal.Location = new System.Drawing.Point(87, 27);
            this.lblImagenOriginal.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenOriginal.Name = "lblImagenOriginal";
            this.lblImagenOriginal.Size = new System.Drawing.Size(113, 15);
            this.lblImagenOriginal.TabIndex = 62;
            this.lblImagenOriginal.Text = "Nombre Original";
            this.lblImagenOriginal.Visible = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnAñadirClientes);
            this.panel5.Controls.Add(this.lblNombreCliente);
            this.panel5.Controls.Add(this.picUserImage);
            this.panel5.Location = new System.Drawing.Point(2, 67);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(273, 111);
            this.panel5.TabIndex = 42;
            // 
            // btnAñadirClientes
            // 
            this.btnAñadirClientes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAñadirClientes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnAñadirClientes.FlatAppearance.BorderSize = 2;
            this.btnAñadirClientes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnAñadirClientes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnAñadirClientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAñadirClientes.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAñadirClientes.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAñadirClientes.Image = ((System.Drawing.Image)(resources.GetObject("btnAñadirClientes.Image")));
            this.btnAñadirClientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAñadirClientes.Location = new System.Drawing.Point(1, 64);
            this.btnAñadirClientes.Name = "btnAñadirClientes";
            this.btnAñadirClientes.Size = new System.Drawing.Size(116, 47);
            this.btnAñadirClientes.TabIndex = 38;
            this.btnAñadirClientes.Text = "Añadir Clientes";
            this.btnAñadirClientes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAñadirClientes.UseVisualStyleBackColor = false;
            this.btnAñadirClientes.Click += new System.EventHandler(this.btnAñadirClientes_Click);
            // 
            // lblNombreCliente
            // 
            this.lblNombreCliente.AutoSize = true;
            this.lblNombreCliente.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreCliente.ForeColor = System.Drawing.Color.GhostWhite;
            this.lblNombreCliente.Location = new System.Drawing.Point(131, 97);
            this.lblNombreCliente.Name = "lblNombreCliente";
            this.lblNombreCliente.Size = new System.Drawing.Size(127, 14);
            this.lblNombreCliente.TabIndex = 23;
            this.lblNombreCliente.Text = "Nonmbre  Del Cliente";
            // 
            // picUserImage
            // 
            this.picUserImage.Image = ((System.Drawing.Image)(resources.GetObject("picUserImage.Image")));
            this.picUserImage.Location = new System.Drawing.Point(152, 3);
            this.picUserImage.Name = "picUserImage";
            this.picUserImage.Size = new System.Drawing.Size(90, 91);
            this.picUserImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picUserImage.TabIndex = 22;
            this.picUserImage.TabStop = false;
            // 
            // panelFormulario
            // 
            this.panelFormulario.Controls.Add(this.pnlMostrarINE);
            this.panelFormulario.Controls.Add(this.pnlMostrarCURP);
            this.panelFormulario.Controls.Add(this.pnlMostarPicDom);
            this.panelFormulario.Controls.Add(this.label27);
            this.panelFormulario.Controls.Add(this.label26);
            this.panelFormulario.Controls.Add(this.txtLocalidad);
            this.panelFormulario.Controls.Add(this.txtCurp);
            this.panelFormulario.Controls.Add(this.lblCalle);
            this.panelFormulario.Controls.Add(this.txtCalle);
            this.panelFormulario.Controls.Add(this.dtpFechaNacimiento);
            this.panelFormulario.Controls.Add(this.label28);
            this.panelFormulario.Controls.Add(this.label29);
            this.panelFormulario.Controls.Add(this.label30);
            this.panelFormulario.Controls.Add(this.label31);
            this.panelFormulario.Controls.Add(this.label32);
            this.panelFormulario.Controls.Add(this.label33);
            this.panelFormulario.Controls.Add(this.txtCP);
            this.panelFormulario.Controls.Add(this.txtNumCasa);
            this.panelFormulario.Controls.Add(this.txtTelefono);
            this.panelFormulario.Controls.Add(this.txtFraccionamiento);
            this.panelFormulario.Controls.Add(this.txtMunicipio);
            this.panelFormulario.Controls.Add(this.label34);
            this.panelFormulario.Controls.Add(this.label35);
            this.panelFormulario.Controls.Add(this.label36);
            this.panelFormulario.Controls.Add(this.label37);
            this.panelFormulario.Controls.Add(this.label38);
            this.panelFormulario.Controls.Add(this.lblNombre);
            this.panelFormulario.Controls.Add(this.txtColonia);
            this.panelFormulario.Controls.Add(this.txtCorreo);
            this.panelFormulario.Controls.Add(this.txtCelular);
            this.panelFormulario.Controls.Add(this.txtApMat);
            this.panelFormulario.Controls.Add(this.txtApPat);
            this.panelFormulario.Controls.Add(this.txtNombre);
            this.panelFormulario.Location = new System.Drawing.Point(3, 181);
            this.panelFormulario.Name = "panelFormulario";
            this.panelFormulario.Size = new System.Drawing.Size(272, 378);
            this.panelFormulario.TabIndex = 40;
            // 
            // pnlMostrarINE
            // 
            this.pnlMostrarINE.Controls.Add(this.label1);
            this.pnlMostrarINE.Controls.Add(this.btnImgComINE);
            this.pnlMostrarINE.Controls.Add(this.txtComproINE);
            this.pnlMostrarINE.Location = new System.Drawing.Point(128, 256);
            this.pnlMostrarINE.Name = "pnlMostrarINE";
            this.pnlMostrarINE.Size = new System.Drawing.Size(142, 44);
            this.pnlMostrarINE.TabIndex = 53;
            this.pnlMostrarINE.MouseLeave += new System.EventHandler(this.pnlMostrarINE_MouseLeave);
            this.pnlMostrarINE.MouseHover += new System.EventHandler(this.pnlMostrarINE_MouseHover);
            this.pnlMostrarINE.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlMostrarINE_MouseMove);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.GhostWhite;
            this.label1.Location = new System.Drawing.Point(2, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 14);
            this.label1.TabIndex = 52;
            this.label1.Text = "Comprobante INE";
            // 
            // btnImgComINE
            // 
            this.btnImgComINE.FlatAppearance.BorderSize = 0;
            this.btnImgComINE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImgComINE.Image = ((System.Drawing.Image)(resources.GetObject("btnImgComINE.Image")));
            this.btnImgComINE.Location = new System.Drawing.Point(114, 17);
            this.btnImgComINE.Name = "btnImgComINE";
            this.btnImgComINE.Size = new System.Drawing.Size(23, 27);
            this.btnImgComINE.TabIndex = 56;
            this.btnImgComINE.Text = "...";
            this.btnImgComINE.UseVisualStyleBackColor = true;
            this.btnImgComINE.Click += new System.EventHandler(this.btnImgComINE_Click);
            // 
            // txtComproINE
            // 
            this.txtComproINE.Location = new System.Drawing.Point(5, 22);
            this.txtComproINE.Name = "txtComproINE";
            this.txtComproINE.Size = new System.Drawing.Size(108, 20);
            this.txtComproINE.TabIndex = 15;
            // 
            // pnlMostrarCURP
            // 
            this.pnlMostrarCURP.Controls.Add(this.label25);
            this.pnlMostrarCURP.Controls.Add(this.btnImgCCurp);
            this.pnlMostrarCURP.Controls.Add(this.txtComprobanteCurp);
            this.pnlMostrarCURP.Location = new System.Drawing.Point(126, 332);
            this.pnlMostrarCURP.Name = "pnlMostrarCURP";
            this.pnlMostrarCURP.Size = new System.Drawing.Size(144, 43);
            this.pnlMostrarCURP.TabIndex = 52;
            this.pnlMostrarCURP.MouseLeave += new System.EventHandler(this.pnlMostrarCURP_MouseLeave);
            this.pnlMostrarCURP.MouseHover += new System.EventHandler(this.pnlMostrarCURP_MouseHover);
            this.pnlMostrarCURP.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlMostrarCURP_MouseMove);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.GhostWhite;
            this.label25.Location = new System.Drawing.Point(3, 2);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(120, 14);
            this.label25.TabIndex = 54;
            this.label25.Text = "Comprobante CURP";
            // 
            // btnImgCCurp
            // 
            this.btnImgCCurp.FlatAppearance.BorderSize = 0;
            this.btnImgCCurp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImgCCurp.Image = ((System.Drawing.Image)(resources.GetObject("btnImgCCurp.Image")));
            this.btnImgCCurp.Location = new System.Drawing.Point(117, 15);
            this.btnImgCCurp.Name = "btnImgCCurp";
            this.btnImgCCurp.Size = new System.Drawing.Size(23, 27);
            this.btnImgCCurp.TabIndex = 57;
            this.btnImgCCurp.Text = "...";
            this.btnImgCCurp.UseVisualStyleBackColor = true;
            this.btnImgCCurp.Click += new System.EventHandler(this.btnImgCCurp_Click);
            // 
            // txtComprobanteCurp
            // 
            this.txtComprobanteCurp.Location = new System.Drawing.Point(7, 19);
            this.txtComprobanteCurp.Name = "txtComprobanteCurp";
            this.txtComprobanteCurp.Size = new System.Drawing.Size(108, 20);
            this.txtComprobanteCurp.TabIndex = 17;
            // 
            // pnlMostarPicDom
            // 
            this.pnlMostarPicDom.Controls.Add(this.txtComprobanteDom);
            this.pnlMostarPicDom.Controls.Add(this.btnImgCDom);
            this.pnlMostarPicDom.Controls.Add(this.txtComprobanteDomicilio);
            this.pnlMostarPicDom.Location = new System.Drawing.Point(128, 207);
            this.pnlMostarPicDom.Name = "pnlMostarPicDom";
            this.pnlMostarPicDom.Size = new System.Drawing.Size(142, 45);
            this.pnlMostarPicDom.TabIndex = 51;
            this.pnlMostarPicDom.MouseLeave += new System.EventHandler(this.pnlMostarPicDom_MouseLeave);
            this.pnlMostarPicDom.MouseHover += new System.EventHandler(this.pnlMostarPicDom_MouseHover);
            this.pnlMostarPicDom.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlMostarPicDom_MouseMove);
            // 
            // txtComprobanteDom
            // 
            this.txtComprobanteDom.AutoSize = true;
            this.txtComprobanteDom.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComprobanteDom.ForeColor = System.Drawing.Color.GhostWhite;
            this.txtComprobanteDom.Location = new System.Drawing.Point(1, 0);
            this.txtComprobanteDom.Name = "txtComprobanteDom";
            this.txtComprobanteDom.Size = new System.Drawing.Size(113, 14);
            this.txtComprobanteDom.TabIndex = 46;
            this.txtComprobanteDom.Text = "Comprobante dom";
            // 
            // btnImgCDom
            // 
            this.btnImgCDom.FlatAppearance.BorderSize = 0;
            this.btnImgCDom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImgCDom.Image = ((System.Drawing.Image)(resources.GetObject("btnImgCDom.Image")));
            this.btnImgCDom.Location = new System.Drawing.Point(115, 12);
            this.btnImgCDom.Name = "btnImgCDom";
            this.btnImgCDom.Size = new System.Drawing.Size(23, 27);
            this.btnImgCDom.TabIndex = 55;
            this.btnImgCDom.UseVisualStyleBackColor = true;
            this.btnImgCDom.Click += new System.EventHandler(this.btnImgCDom_Click);
            // 
            // txtComprobanteDomicilio
            // 
            this.txtComprobanteDomicilio.Location = new System.Drawing.Point(6, 19);
            this.txtComprobanteDomicilio.Name = "txtComprobanteDomicilio";
            this.txtComprobanteDomicilio.Size = new System.Drawing.Size(108, 20);
            this.txtComprobanteDomicilio.TabIndex = 14;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.GhostWhite;
            this.label27.Location = new System.Drawing.Point(210, 299);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(39, 14);
            this.label27.TabIndex = 48;
            this.label27.Text = "CURP";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.GhostWhite;
            this.label26.Location = new System.Drawing.Point(188, 128);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(60, 14);
            this.label26.TabIndex = 50;
            this.label26.Text = "Localidad";
            // 
            // txtLocalidad
            // 
            this.txtLocalidad.Location = new System.Drawing.Point(140, 146);
            this.txtLocalidad.Name = "txtLocalidad";
            this.txtLocalidad.Size = new System.Drawing.Size(108, 20);
            this.txtLocalidad.TabIndex = 12;
            // 
            // txtCurp
            // 
            this.txtCurp.Location = new System.Drawing.Point(141, 313);
            this.txtCurp.Name = "txtCurp";
            this.txtCurp.Size = new System.Drawing.Size(108, 20);
            this.txtCurp.TabIndex = 16;
            // 
            // lblCalle
            // 
            this.lblCalle.AutoSize = true;
            this.lblCalle.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalle.ForeColor = System.Drawing.Color.GhostWhite;
            this.lblCalle.Location = new System.Drawing.Point(8, 290);
            this.lblCalle.Name = "lblCalle";
            this.lblCalle.Size = new System.Drawing.Size(36, 14);
            this.lblCalle.TabIndex = 44;
            this.lblCalle.Text = "Calle";
            // 
            // txtCalle
            // 
            this.txtCalle.Location = new System.Drawing.Point(8, 308);
            this.txtCalle.Name = "txtCalle";
            this.txtCalle.Size = new System.Drawing.Size(108, 20);
            this.txtCalle.TabIndex = 7;
            // 
            // dtpFechaNacimiento
            // 
            this.dtpFechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFechaNacimiento.Location = new System.Drawing.Point(8, 141);
            this.dtpFechaNacimiento.Name = "dtpFechaNacimiento";
            this.dtpFechaNacimiento.Size = new System.Drawing.Size(108, 20);
            this.dtpFechaNacimiento.TabIndex = 3;
            this.dtpFechaNacimiento.Value = new System.DateTime(2020, 4, 22, 9, 11, 0, 0);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.GhostWhite;
            this.label28.Location = new System.Drawing.Point(164, 10);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(84, 14);
            this.label28.TabIndex = 41;
            this.label28.Text = "Codigo Postal";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.GhostWhite;
            this.label29.Location = new System.Drawing.Point(8, 330);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(101, 14);
            this.label29.TabIndex = 40;
            this.label29.Text = "Numero de Casa";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.GhostWhite;
            this.label30.Location = new System.Drawing.Point(8, 206);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(58, 14);
            this.label30.TabIndex = 39;
            this.label30.Text = "Telefono";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.GhostWhite;
            this.label31.Location = new System.Drawing.Point(147, 88);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(101, 14);
            this.label31.TabIndex = 38;
            this.label31.Text = "Fraccionamiento";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.GhostWhite;
            this.label32.Location = new System.Drawing.Point(188, 169);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(60, 14);
            this.label32.TabIndex = 37;
            this.label32.Text = "Municipio";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.GhostWhite;
            this.label33.Location = new System.Drawing.Point(8, 126);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(126, 14);
            this.label33.TabIndex = 36;
            this.label33.Text = "Fecha De Nacimiento";
            // 
            // txtCP
            // 
            this.txtCP.Location = new System.Drawing.Point(140, 27);
            this.txtCP.Name = "txtCP";
            this.txtCP.Size = new System.Drawing.Size(108, 20);
            this.txtCP.TabIndex = 9;
            // 
            // txtNumCasa
            // 
            this.txtNumCasa.Location = new System.Drawing.Point(8, 348);
            this.txtNumCasa.Name = "txtNumCasa";
            this.txtNumCasa.Size = new System.Drawing.Size(108, 20);
            this.txtNumCasa.TabIndex = 8;
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(8, 224);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(108, 20);
            this.txtTelefono.TabIndex = 5;
            // 
            // txtFraccionamiento
            // 
            this.txtFraccionamiento.Location = new System.Drawing.Point(140, 105);
            this.txtFraccionamiento.Name = "txtFraccionamiento";
            this.txtFraccionamiento.Size = new System.Drawing.Size(108, 20);
            this.txtFraccionamiento.TabIndex = 11;
            // 
            // txtMunicipio
            // 
            this.txtMunicipio.Location = new System.Drawing.Point(140, 184);
            this.txtMunicipio.Name = "txtMunicipio";
            this.txtMunicipio.Size = new System.Drawing.Size(108, 20);
            this.txtMunicipio.TabIndex = 13;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.GhostWhite;
            this.label34.Location = new System.Drawing.Point(198, 47);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(50, 14);
            this.label34.TabIndex = 29;
            this.label34.Text = "Colonia";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.GhostWhite;
            this.label35.Location = new System.Drawing.Point(8, 251);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(112, 14);
            this.label35.TabIndex = 28;
            this.label35.Text = "Correo Electronico";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.GhostWhite;
            this.label36.Location = new System.Drawing.Point(8, 165);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(51, 14);
            this.label36.TabIndex = 27;
            this.label36.Text = "Celular ";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.GhostWhite;
            this.label37.Location = new System.Drawing.Point(8, 86);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(102, 14);
            this.label37.TabIndex = 26;
            this.label37.Text = "Apellido Materno";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.GhostWhite;
            this.label38.Location = new System.Drawing.Point(8, 48);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(99, 14);
            this.label38.TabIndex = 25;
            this.label38.Text = "Apellido Paterno";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.ForeColor = System.Drawing.Color.GhostWhite;
            this.lblNombre.Location = new System.Drawing.Point(8, 12);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(55, 14);
            this.lblNombre.TabIndex = 24;
            this.lblNombre.Text = "Nombre ";
            // 
            // txtColonia
            // 
            this.txtColonia.Location = new System.Drawing.Point(140, 65);
            this.txtColonia.Name = "txtColonia";
            this.txtColonia.Size = new System.Drawing.Size(108, 20);
            this.txtColonia.TabIndex = 10;
            // 
            // txtCorreo
            // 
            this.txtCorreo.Location = new System.Drawing.Point(8, 268);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Size = new System.Drawing.Size(108, 20);
            this.txtCorreo.TabIndex = 6;
            // 
            // txtCelular
            // 
            this.txtCelular.Location = new System.Drawing.Point(8, 183);
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(108, 20);
            this.txtCelular.TabIndex = 4;
            // 
            // txtApMat
            // 
            this.txtApMat.Location = new System.Drawing.Point(8, 103);
            this.txtApMat.Name = "txtApMat";
            this.txtApMat.Size = new System.Drawing.Size(108, 20);
            this.txtApMat.TabIndex = 2;
            // 
            // txtApPat
            // 
            this.txtApPat.Location = new System.Drawing.Point(8, 63);
            this.txtApPat.Name = "txtApPat";
            this.txtApPat.Size = new System.Drawing.Size(108, 20);
            this.txtApPat.TabIndex = 1;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(8, 27);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(108, 20);
            this.txtNombre.TabIndex = 0;
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(658, 195);
            this.btnLimpiar.Margin = new System.Windows.Forms.Padding(2);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(56, 19);
            this.btnLimpiar.TabIndex = 31;
            this.btnLimpiar.Text = "LIMPIAR";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            // 
            // btnBorrar
            // 
            this.btnBorrar.Location = new System.Drawing.Point(658, 146);
            this.btnBorrar.Margin = new System.Windows.Forms.Padding(2);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(56, 19);
            this.btnBorrar.TabIndex = 30;
            this.btnBorrar.Text = "BORRAR";
            this.btnBorrar.UseVisualStyleBackColor = true;
            // 
            // btnModificar
            // 
            this.btnModificar.Location = new System.Drawing.Point(658, 98);
            this.btnModificar.Margin = new System.Windows.Forms.Padding(2);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(56, 19);
            this.btnModificar.TabIndex = 29;
            this.btnModificar.Text = "MODIFICAR";
            this.btnModificar.UseVisualStyleBackColor = true;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(658, 50);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(2);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(56, 19);
            this.btnGuardar.TabIndex = 28;
            this.btnGuardar.Text = "ALTA";
            this.btnGuardar.UseVisualStyleBackColor = true;
            // 
            // pnlNavbar
            // 
            this.pnlNavbar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.pnlNavbar.Controls.Add(this.panelTimer);
            this.pnlNavbar.Controls.Add(this.panel4);
            this.pnlNavbar.Controls.Add(this.PnlInfo);
            this.pnlNavbar.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlNavbar.Location = new System.Drawing.Point(0, 0);
            this.pnlNavbar.Name = "pnlNavbar";
            this.pnlNavbar.Size = new System.Drawing.Size(269, 706);
            this.pnlNavbar.TabIndex = 3;
            // 
            // panelTimer
            // 
            this.panelTimer.Controls.Add(this.picReloj);
            this.panelTimer.Controls.Add(this.lblReloj);
            this.panelTimer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTimer.Location = new System.Drawing.Point(0, 641);
            this.panelTimer.Name = "panelTimer";
            this.panelTimer.Size = new System.Drawing.Size(269, 65);
            this.panelTimer.TabIndex = 43;
            // 
            // picReloj
            // 
            this.picReloj.Image = ((System.Drawing.Image)(resources.GetObject("picReloj.Image")));
            this.picReloj.Location = new System.Drawing.Point(0, 5);
            this.picReloj.Name = "picReloj";
            this.picReloj.Size = new System.Drawing.Size(73, 52);
            this.picReloj.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picReloj.TabIndex = 1;
            this.picReloj.TabStop = false;
            // 
            // lblReloj
            // 
            this.lblReloj.Font = new System.Drawing.Font("Lucida Bright", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReloj.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblReloj.Location = new System.Drawing.Point(74, 25);
            this.lblReloj.Name = "lblReloj";
            this.lblReloj.Size = new System.Drawing.Size(183, 32);
            this.lblReloj.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnPersonaAlt);
            this.panel4.Controls.Add(this.btnCajas);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.btnCreditos);
            this.panel4.Controls.Add(this.btnProductos);
            this.panel4.Controls.Add(this.btnHome);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.label22);
            this.panel4.Location = new System.Drawing.Point(2, 291);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(261, 344);
            this.panel4.TabIndex = 42;
            // 
            // btnCajas
            // 
            this.btnCajas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnCajas.FlatAppearance.BorderSize = 2;
            this.btnCajas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCajas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnCajas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCajas.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCajas.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCajas.Image = ((System.Drawing.Image)(resources.GetObject("btnCajas.Image")));
            this.btnCajas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCajas.Location = new System.Drawing.Point(6, 279);
            this.btnCajas.Name = "btnCajas";
            this.btnCajas.Size = new System.Drawing.Size(249, 47);
            this.btnCajas.TabIndex = 46;
            this.btnCajas.Text = "Cajas";
            this.btnCajas.UseVisualStyleBackColor = false;
            this.btnCajas.Click += new System.EventHandler(this.btnCajas_Click);
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(3, 329);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(250, 3);
            this.label2.TabIndex = 45;
            this.label2.Text = "label2";
            this.label2.UseWaitCursor = true;
            // 
            // btnCreditos
            // 
            this.btnCreditos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnCreditos.FlatAppearance.BorderSize = 2;
            this.btnCreditos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCreditos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnCreditos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreditos.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreditos.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCreditos.Image = ((System.Drawing.Image)(resources.GetObject("btnCreditos.Image")));
            this.btnCreditos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCreditos.Location = new System.Drawing.Point(6, 214);
            this.btnCreditos.Name = "btnCreditos";
            this.btnCreditos.Size = new System.Drawing.Size(249, 47);
            this.btnCreditos.TabIndex = 43;
            this.btnCreditos.Text = "Créditos";
            this.btnCreditos.UseVisualStyleBackColor = false;
            this.btnCreditos.Click += new System.EventHandler(this.btnCreditos_Click);
            // 
            // btnProductos
            // 
            this.btnProductos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnProductos.FlatAppearance.BorderSize = 2;
            this.btnProductos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnProductos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnProductos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProductos.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProductos.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnProductos.Image = ((System.Drawing.Image)(resources.GetObject("btnProductos.Image")));
            this.btnProductos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProductos.Location = new System.Drawing.Point(6, 149);
            this.btnProductos.Name = "btnProductos";
            this.btnProductos.Size = new System.Drawing.Size(249, 47);
            this.btnProductos.TabIndex = 41;
            this.btnProductos.Text = "   Productos";
            this.btnProductos.UseVisualStyleBackColor = false;
            this.btnProductos.Click += new System.EventHandler(this.btnProductos_Click);
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnHome.FlatAppearance.BorderSize = 2;
            this.btnHome.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnHome.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHome.Image = ((System.Drawing.Image)(resources.GetObject("btnHome.Image")));
            this.btnHome.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHome.Location = new System.Drawing.Point(6, 21);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(249, 47);
            this.btnHome.TabIndex = 40;
            this.btnHome.Text = "Home";
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // label17
            // 
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label17.Location = new System.Drawing.Point(5, 134);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(250, 3);
            this.label17.TabIndex = 39;
            this.label17.Text = "label17";
            this.label17.UseWaitCursor = true;
            // 
            // label18
            // 
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label18.Location = new System.Drawing.Point(6, 68);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(250, 3);
            this.label18.TabIndex = 38;
            this.label18.Text = "label18";
            this.label18.UseWaitCursor = true;
            // 
            // label20
            // 
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label20.Location = new System.Drawing.Point(6, 262);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(250, 3);
            this.label20.TabIndex = 37;
            this.label20.Text = "label20";
            this.label20.UseWaitCursor = true;
            // 
            // label22
            // 
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label22.Location = new System.Drawing.Point(6, 197);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(250, 3);
            this.label22.TabIndex = 35;
            this.label22.Text = "label22";
            this.label22.UseWaitCursor = true;
            // 
            // PnlInfo
            // 
            this.PnlInfo.Controls.Add(this.lblNombreUser);
            this.PnlInfo.Controls.Add(this.panel3);
            this.PnlInfo.Controls.Add(this.pictureBox14);
            this.PnlInfo.Controls.Add(this.lblCajaTitle);
            this.PnlInfo.Controls.Add(this.ipbLogo);
            this.PnlInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.PnlInfo.Location = new System.Drawing.Point(0, 0);
            this.PnlInfo.Name = "PnlInfo";
            this.PnlInfo.Size = new System.Drawing.Size(269, 291);
            this.PnlInfo.TabIndex = 41;
            // 
            // lblNombreUser
            // 
            this.lblNombreUser.AutoSize = true;
            this.lblNombreUser.Font = new System.Drawing.Font("Lucida Bright", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreUser.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblNombreUser.Location = new System.Drawing.Point(12, 244);
            this.lblNombreUser.Name = "lblNombreUser";
            this.lblNombreUser.Size = new System.Drawing.Size(238, 22);
            this.lblNombreUser.TabIndex = 49;
            this.lblNombreUser.Text = "NOMBRE DEL USUARIO ";
            this.lblNombreUser.UseWaitCursor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblUserName);
            this.panel3.Location = new System.Drawing.Point(268, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(600, 42);
            this.panel3.TabIndex = 48;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Lucida Bright", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblUserName.Location = new System.Drawing.Point(181, 10);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(238, 22);
            this.lblUserName.TabIndex = 46;
            this.lblUserName.Text = "NOMBRE DEL USUARIO ";
            this.lblUserName.UseWaitCursor = true;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.Location = new System.Drawing.Point(10, 1);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(54, 58);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox14.TabIndex = 47;
            this.pictureBox14.TabStop = false;
            // 
            // lblCajaTitle
            // 
            this.lblCajaTitle.AutoSize = true;
            this.lblCajaTitle.Font = new System.Drawing.Font("Lucida Bright", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCajaTitle.ForeColor = System.Drawing.SystemColors.Info;
            this.lblCajaTitle.Location = new System.Drawing.Point(93, 12);
            this.lblCajaTitle.Name = "lblCajaTitle";
            this.lblCajaTitle.Size = new System.Drawing.Size(129, 33);
            this.lblCajaTitle.TabIndex = 46;
            this.lblCajaTitle.Text = "Clientes";
            // 
            // ipbLogo
            // 
            this.ipbLogo.Image = ((System.Drawing.Image)(resources.GetObject("ipbLogo.Image")));
            this.ipbLogo.Location = new System.Drawing.Point(37, 65);
            this.ipbLogo.Name = "ipbLogo";
            this.ipbLogo.Size = new System.Drawing.Size(175, 168);
            this.ipbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ipbLogo.TabIndex = 43;
            this.ipbLogo.TabStop = false;
            this.ipbLogo.UseWaitCursor = true;
            // 
            // txtBusqueda
            // 
            this.txtBusqueda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBusqueda.Location = new System.Drawing.Point(94, 32);
            this.txtBusqueda.Name = "txtBusqueda";
            this.txtBusqueda.Size = new System.Drawing.Size(325, 20);
            this.txtBusqueda.TabIndex = 41;
            this.txtBusqueda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBusqueda_KeyPress);
            // 
            // PicBuscar
            // 
            this.PicBuscar.Dock = System.Windows.Forms.DockStyle.Right;
            this.PicBuscar.Image = ((System.Drawing.Image)(resources.GetObject("PicBuscar.Image")));
            this.PicBuscar.Location = new System.Drawing.Point(487, 0);
            this.PicBuscar.Name = "PicBuscar";
            this.PicBuscar.Size = new System.Drawing.Size(56, 67);
            this.PicBuscar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PicBuscar.TabIndex = 36;
            this.PicBuscar.TabStop = false;
            this.PicBuscar.Click += new System.EventHandler(this.PicBuscar_Click);
            // 
            // PanelData
            // 
            this.PanelData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.PanelData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PanelData.Controls.Add(this.pnlBarraTareas);
            this.PanelData.Controls.Add(this.panel2);
            this.PanelData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelData.Location = new System.Drawing.Point(269, 0);
            this.PanelData.Name = "PanelData";
            this.PanelData.Size = new System.Drawing.Size(547, 706);
            this.PanelData.TabIndex = 6;
            // 
            // pnlBarraTareas
            // 
            this.pnlBarraTareas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.pnlBarraTareas.Controls.Add(this.btnExpandirMn);
            this.pnlBarraTareas.Controls.Add(this.txtBusqueda);
            this.pnlBarraTareas.Controls.Add(this.PicBuscar);
            this.pnlBarraTareas.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBarraTareas.Location = new System.Drawing.Point(0, 0);
            this.pnlBarraTareas.Name = "pnlBarraTareas";
            this.pnlBarraTareas.Size = new System.Drawing.Size(543, 67);
            this.pnlBarraTareas.TabIndex = 1;
            // 
            // btnExpandirMn
            // 
            this.btnExpandirMn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnExpandirMn.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnExpandirMn.FlatAppearance.BorderSize = 0;
            this.btnExpandirMn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnExpandirMn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnExpandirMn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExpandirMn.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExpandirMn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnExpandirMn.Image = ((System.Drawing.Image)(resources.GetObject("btnExpandirMn.Image")));
            this.btnExpandirMn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExpandirMn.Location = new System.Drawing.Point(0, 0);
            this.btnExpandirMn.Name = "btnExpandirMn";
            this.btnExpandirMn.Size = new System.Drawing.Size(62, 67);
            this.btnExpandirMn.TabIndex = 45;
            this.btnExpandirMn.UseVisualStyleBackColor = false;
            this.btnExpandirMn.Click += new System.EventHandler(this.btnExpandirMn_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pnDgClientes);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(543, 702);
            this.panel2.TabIndex = 1;
            // 
            // pnDgClientes
            // 
            this.pnDgClientes.Controls.Add(this.picComprobantes);
            this.pnDgClientes.Controls.Add(this.dgClientes);
            this.pnDgClientes.Controls.Add(this.PnlImgINE);
            this.pnDgClientes.Controls.Add(this.pnlImgCURP);
            this.pnDgClientes.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnDgClientes.Location = new System.Drawing.Point(0, 68);
            this.pnDgClientes.Name = "pnDgClientes";
            this.pnDgClientes.Size = new System.Drawing.Size(543, 634);
            this.pnDgClientes.TabIndex = 60;
            // 
            // picComprobantes
            // 
            this.picComprobantes.BackColor = System.Drawing.SystemColors.GrayText;
            this.picComprobantes.Image = ((System.Drawing.Image)(resources.GetObject("picComprobantes.Image")));
            this.picComprobantes.Location = new System.Drawing.Point(75, 106);
            this.picComprobantes.Name = "picComprobantes";
            this.picComprobantes.Size = new System.Drawing.Size(411, 335);
            this.picComprobantes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picComprobantes.TabIndex = 0;
            this.picComprobantes.TabStop = false;
            this.picComprobantes.Visible = false;
            this.picComprobantes.Click += new System.EventHandler(this.picComprobantes_Click);
            // 
            // dgClientes
            // 
            this.dgClientes.AllowUserToOrderColumns = true;
            this.dgClientes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgClientes.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgClientes.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            this.dgClientes.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgClientes.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.HotTrack;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgClientes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgClientes.ColumnHeadersHeight = 30;
            this.dgClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgClientes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgClientes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgClientes.EnableHeadersVisualStyles = false;
            this.dgClientes.GridColor = System.Drawing.Color.SteelBlue;
            this.dgClientes.ImeMode = System.Windows.Forms.ImeMode.On;
            this.dgClientes.Location = new System.Drawing.Point(0, 0);
            this.dgClientes.Name = "dgClientes";
            this.dgClientes.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgClientes.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            this.dgClientes.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgClientes.Size = new System.Drawing.Size(543, 634);
            this.dgClientes.TabIndex = 0;
            this.dgClientes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgClientes_CellContentClick_1);
            // 
            // PnlImgINE
            // 
            this.PnlImgINE.BackColor = System.Drawing.SystemColors.GrayText;
            this.PnlImgINE.Image = ((System.Drawing.Image)(resources.GetObject("PnlImgINE.Image")));
            this.PnlImgINE.Location = new System.Drawing.Point(75, 106);
            this.PnlImgINE.Name = "PnlImgINE";
            this.PnlImgINE.Size = new System.Drawing.Size(410, 330);
            this.PnlImgINE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.PnlImgINE.TabIndex = 59;
            this.PnlImgINE.TabStop = false;
            this.PnlImgINE.Visible = false;
            // 
            // pnlImgCURP
            // 
            this.pnlImgCURP.BackColor = System.Drawing.SystemColors.GrayText;
            this.pnlImgCURP.Image = ((System.Drawing.Image)(resources.GetObject("pnlImgCURP.Image")));
            this.pnlImgCURP.Location = new System.Drawing.Point(75, 106);
            this.pnlImgCURP.Name = "pnlImgCURP";
            this.pnlImgCURP.Size = new System.Drawing.Size(411, 338);
            this.pnlImgCURP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pnlImgCURP.TabIndex = 59;
            this.pnlImgCURP.TabStop = false;
            this.pnlImgCURP.Visible = false;
            // 
            // openFileDialogComprobantes
            // 
            this.openFileDialogComprobantes.FileName = "openFileDialog1";
            // 
            // btnPersonaAlt
            // 
            this.btnPersonaAlt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnPersonaAlt.FlatAppearance.BorderSize = 2;
            this.btnPersonaAlt.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnPersonaAlt.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnPersonaAlt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPersonaAlt.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPersonaAlt.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnPersonaAlt.Image = ((System.Drawing.Image)(resources.GetObject("btnPersonaAlt.Image")));
            this.btnPersonaAlt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPersonaAlt.Location = new System.Drawing.Point(5, 86);
            this.btnPersonaAlt.Name = "btnPersonaAlt";
            this.btnPersonaAlt.Size = new System.Drawing.Size(249, 47);
            this.btnPersonaAlt.TabIndex = 47;
            this.btnPersonaAlt.Text = " Persona";
            this.btnPersonaAlt.UseVisualStyleBackColor = false;
            this.btnPersonaAlt.Click += new System.EventHandler(this.btnPersonaAlt_Click);
            // 
            // FrmCatalogoCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1091, 706);
            this.Controls.Add(this.PanelData);
            this.Controls.Add(this.pnlNavbar);
            this.Controls.Add(this.panelForm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmCatalogoCliente";
            this.Text = "FrmCatalogoCliente";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmCatalogoCliente_Load);
            this.panelForm.ResumeLayout(false);
            this.panelForm.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUserImage)).EndInit();
            this.panelFormulario.ResumeLayout(false);
            this.panelFormulario.PerformLayout();
            this.pnlMostrarINE.ResumeLayout(false);
            this.pnlMostrarINE.PerformLayout();
            this.pnlMostrarCURP.ResumeLayout(false);
            this.pnlMostrarCURP.PerformLayout();
            this.pnlMostarPicDom.ResumeLayout(false);
            this.pnlMostarPicDom.PerformLayout();
            this.pnlNavbar.ResumeLayout(false);
            this.panelTimer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picReloj)).EndInit();
            this.panel4.ResumeLayout(false);
            this.PnlInfo.ResumeLayout(false);
            this.PnlInfo.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ipbLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBuscar)).EndInit();
            this.PanelData.ResumeLayout(false);
            this.pnlBarraTareas.ResumeLayout(false);
            this.pnlBarraTareas.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.pnDgClientes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picComprobantes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgClientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PnlImgINE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlImgCURP)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelForm;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Panel pnlNavbar;
        private System.Windows.Forms.Panel panelFormulario;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtComprobanteCurp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtComproINE;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtLocalidad;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtCurp;
        private System.Windows.Forms.Label txtComprobanteDom;
        private System.Windows.Forms.TextBox txtComprobanteDomicilio;
        private System.Windows.Forms.Label lblCalle;
        private System.Windows.Forms.TextBox txtCalle;
        private System.Windows.Forms.DateTimePicker dtpFechaNacimiento;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtCP;
        private System.Windows.Forms.TextBox txtNumCasa;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.TextBox txtFraccionamiento;
        private System.Windows.Forms.TextBox txtMunicipio;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox txtColonia;
        private System.Windows.Forms.TextBox txtCorreo;
        private System.Windows.Forms.TextBox txtCelular;
        private System.Windows.Forms.TextBox txtApMat;
        private System.Windows.Forms.TextBox txtApPat;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblNombreCliente;
        private System.Windows.Forms.PictureBox picUserImage;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnModificarCliente;
        private System.Windows.Forms.Button btnPersonasAlternativas;
        private System.Windows.Forms.Button btnGuardarCliente;
        private System.Windows.Forms.Button btnEliminarclientes;
        private System.Windows.Forms.Button btnAñadirClientes;
        private System.Windows.Forms.Button btnApagar;
        private System.Windows.Forms.PictureBox PicBuscar;
        private System.Windows.Forms.Panel PanelData;
        private System.Windows.Forms.DataGridView dgClientes;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnCreditos;
        private System.Windows.Forms.Button btnProductos;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtBusqueda;
        private System.Windows.Forms.Panel panelTimer;
        private System.Windows.Forms.PictureBox picReloj;
        private System.Windows.Forms.Label lblReloj;
        private System.Windows.Forms.Panel PnlInfo;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.Label lblCajaTitle;
        private System.Windows.Forms.PictureBox ipbLogo;
        private System.Windows.Forms.Panel pnlBarraTareas;
        private System.Windows.Forms.Button btnExpandirMn;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnCajas;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnImgCCurp;
        private System.Windows.Forms.Button btnImgComINE;
        private System.Windows.Forms.Button btnImgCDom;
        private System.Windows.Forms.OpenFileDialog openFileDialogComprobantes;
        private System.Windows.Forms.PictureBox PnlImgINE;
        private System.Windows.Forms.Label lblImagenAGuardar;
        private System.Windows.Forms.Label lblImagenOriginal;
        private System.Windows.Forms.Panel pnDgClientes;
        private System.Windows.Forms.PictureBox picComprobantes;
        private System.Windows.Forms.PictureBox pnlImgCURP;
        private System.Windows.Forms.Panel pnlMostrarINE;
        private System.Windows.Forms.Panel pnlMostrarCURP;
        private System.Windows.Forms.Panel pnlMostarPicDom;
        private System.Windows.Forms.Label lblImagenOriginal2;
        private System.Windows.Forms.Label lblImagenAGuardar2;
        private System.Windows.Forms.Label lblImagenAGuardar1;
        private System.Windows.Forms.Label lblImagenOriginal1;
        private System.Windows.Forms.Label lblNombreUser;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Button btnPersonaAlt;
    }
}