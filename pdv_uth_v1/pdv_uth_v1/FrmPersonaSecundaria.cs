﻿using Lib_pdv_uth_v1;
using Lib_pdv_uth_v1.clientes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace pdv_uth_v1
{
    public partial class FrmPersonaSecundaria : Form
    {

        //importar metodo tipos de imagenes
        FrmCatalogoCliente frm = new FrmCatalogoCliente();
        //Vars para datos del cliente
        public static string txtAGuardarDom;
        public static string txtAGuardarIne;
        public static string txtAGuardarCurp;

        string idPersonaSecundaria = FrmCatalogoCliente.idParaPersona.ToString();
        //vars para acciones CRUD

        PersonaSecundaria PerAlternativa = new PersonaSecundaria();
        int idPerAlternativa = 0;
        public FrmPersonaSecundaria()
        {
            InitializeComponent();
        }


        private void Cerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Usted desea cerrar la ventana de personas secundarias?",
                            "Cerrar",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnRetroceder_Click(object sender, EventArgs e)
        {
            //esconder el form
            this.Hide();
        }

        private void btnEliminarPerAltern_Click(object sender, EventArgs e)
        {
            PersonaSecundaria persona = new PersonaSecundaria();
            if (MessageBox.Show("¿Realmente desea eliminar este cliente?", "Borrar cliente", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (persona.eliminar(idPerAlternativa))
                {
                    MessageBox.Show("Persona Secundaria eliminada ");

                }
                else MessageBox.Show("Error, Persona Secundaria no fue eliminada ");
            }

        }

        private void btnGuardarPerAltern_Click(object sender, EventArgs e)
        {
            ;
            PersonaSecundaria paraAlta = new PersonaSecundaria();
            paraAlta.Nombre = txtNombre.Text;
            paraAlta.ApellidoPaterno = txtApPat.Text;
            paraAlta.ApellidoMaterno = txtApMat.Text;
            paraAlta.Celular = txtCelular.Text;
            paraAlta.Telefono = txtTelefono.Text;
            paraAlta.Correo = txtCorreo.Text;
            paraAlta.FechaNacimiento = dtpFechaNacimiento.Value;
            paraAlta.Domicilio.calle = txtCalle.Text;
            paraAlta.Domicilio.numero = txtNumCasa.Text;
            paraAlta.Domicilio.colonia = txtColonia.Text;
            paraAlta.Domicilio.seccionFraccionamiento = txtFraccionamiento.Text;
            paraAlta.Domicilio.codigoPostal = txtCP.Text;
            paraAlta.Domicilio.localidad = txtLocalidad.Text;
            paraAlta.Domicilio.municipio = txtMunicipio.Text;
            paraAlta.Domicilio.fotoComprobante = txtComprobanteDomicilio.Text;
            paraAlta.ComprobanteINE = txtComproINE.Text;
            paraAlta.Curp = txtCurp.Text;
            paraAlta.CurpCompro = txtComprobanteCurp.Text;
            paraAlta.idCliente = int.Parse(idPersonaSecundaria);
            if (paraAlta.alta())
            {
                // obtener el dir de la app
                string bin = System.IO.Path.GetDirectoryName(Application.StartupPath);
                //ir a un dir arriba
                string dir = System.IO.Path.GetDirectoryName(bin);
                //agregar el path para guardar la imagen
                dir += "\\Imagenes\\PersonasSecundarias\\";
                //guardar la imagen
                picComprobantes.Image.Save(dir + lblImagenAGuardar.Text);
                pnlImgCURP.Image.Save(dir + lblImagenAGuardar1.Text);
                pnlImgINE.Image.Save(dir + lblImagenAGuardar2.Text);
                MessageBox.Show("registro correcto");
                /*                guardarImg(txtComprobanteCurp.Text, PictureBoxDomicilio)*/

            }
            else
            {
                MessageBox.Show("Errror al guardar cliente. " + Cliente.msgError);
            }

        }

        private void btnModificarPerAltern_Click(object sender, EventArgs e)
        {
            //Datos Esta tomando el ultimo dato y lo sustituye en el sig add.
            PersonaSecundaria paraModif = new PersonaSecundaria();
            List<DatosParaActualizar> datos = new List<DatosParaActualizar>();

            datos.Add(new DatosParaActualizar("nombre", txtNombre.Text));
            datos.Add(new DatosParaActualizar("apellido_paterno", txtApPat.Text));
            datos.Add(new DatosParaActualizar("apellido_materno", txtApMat.Text));
            datos.Add(new DatosParaActualizar("fecha_de_nacimiento", dtpFechaNacimiento.Value.Year + "-" + dtpFechaNacimiento.Value.Month + "-" + dtpFechaNacimiento.Value.Day));
            datos.Add(new DatosParaActualizar("celular", txtCelular.Text));
            datos.Add(new DatosParaActualizar("telefono", txtTelefono.Text));
            datos.Add(new DatosParaActualizar("correo", txtCorreo.Text));
            datos.Add(new DatosParaActualizar("calle", txtCalle.Text));
            datos.Add(new DatosParaActualizar("numero_casa", txtNumCasa.Text));
            datos.Add(new DatosParaActualizar("codigo_postal", txtCP.Text));
            datos.Add(new DatosParaActualizar("colonia", txtColonia.Text));
            datos.Add(new DatosParaActualizar("fraccionamiento", txtFraccionamiento.Text));
            datos.Add(new DatosParaActualizar("localidad", txtLocalidad.Text));
            datos.Add(new DatosParaActualizar("municipio", txtMunicipio.Text));
            datos.Add(new DatosParaActualizar("img_comprobante_domicilio", lblImagenAGuardar.Text));
            datos.Add(new DatosParaActualizar("ine_comprobante", lblImagenAGuardar2.Text));
            datos.Add(new DatosParaActualizar("curp", txtCurp.Text));
            datos.Add(new DatosParaActualizar("curp_comprobante", lblImagenAGuardar1.Text));

            if (paraModif.modificar(datos, idPerAlternativa))
            {
                MessageBox.Show("cliente modificado");
            }
            else MessageBox.Show("Error cliente no modificado");
            mostrarRegistrosEnDG();

        }
        public void mostrarRegistrosEnDG()
        {
            PersonaSecundaria cli = new PersonaSecundaria();
            dgPersonaSecundaria.DataSource = null;
            //borrar todos los ren del DG 
            dgPersonaSecundaria.Rows.Clear();
            //se crea lista de criterios de busqueda
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = " 1 ";
            criterio.operadorIntermedio = OperadorDeConsulta.IGUAL;
            criterio.valor = "1";
            criterio.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dgPersonaSecundaria.DataSource = cli.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dgPersonaSecundaria.Refresh();
        }


        private void pnlMostrarCURP_MouseMove(object sender, MouseEventArgs e)
        {
            pnlImgCURP.Visible = true;
            pnlImgCURP.Show();
        }

        private void pnlMostrarCURP_MouseHover(object sender, EventArgs e)
        {
            pnlImgCURP.Visible = true;
            pnlImgCURP.Show();
        }

        private void pnlMostrarCURP_MouseLeave(object sender, EventArgs e)
        {
            pnlImgCURP.Visible = false;
            pnlImgCURP.Hide();
            txtComprobanteCurp.Text = lblImagenAGuardar1.Text;
        }

        //Ejemplo didactico pal simio del apodaca
        private void button1_Click(object sender, EventArgs e)
        {
            //Asignar El contenido de los textbox a la variable
            txtAGuardarDom = txtComprobanteDomicilio.Text;
            txtAGuardarCurp = txtComprobanteCurp.Text;
            txtAGuardarIne = txtComproINE.Text;
        }

        private void FrmPersonaSecundaria_Load(object sender, EventArgs e)
        {
            mostrarRegistrosEnDG();
            //cargar el formulario al que se le va enviar
            FrmCatalogoCliente Form2 = new FrmCatalogoCliente();
            Form2.Activate();
        }

        private void lblidCliente_Click(object sender, EventArgs e)
        {
            lblidCliente.Text = FrmCatalogoCliente.idParaPersona.ToString();
        }

        private void btnImgCCurp_Click(object sender, EventArgs e)
        {
            frm.tiposDeImagen();
            //si se abre imagen, se toma archivo
            if (openFileDialogComprobantes.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = openFileDialogComprobantes.FileName;
                //se carga archivo por su nombre al picBox
                pnlImgCURP.Image = Image.FromFile(openFileDialogComprobantes.FileName);
                //se toma el nmbre original
                lblImagenOriginal1.Text = openFileDialogComprobantes.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension2 = lblImagenOriginal1.Text.Substring(lblImagenOriginal1.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension2 = extension2.ToLower() == "peg" ? "jpeg" : extension2.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblImagenAGuardar1.Text = string.Format(@"CURP_{0}."+ extension2, DateTime.Now.Ticks);
            }

        }

        private void btnImgCDom_Click(object sender, EventArgs e)
        {
            frm.tiposDeImagen();
            //si se abre imagen, se toma archivo
            if (openFileDialogComprobantes.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = openFileDialogComprobantes.FileName;
                //se carga archivo por su nombre al picBox
                picComprobantes.Image = Image.FromFile(openFileDialogComprobantes.FileName);
                //se toma el nmbre original
                lblImagenOriginal.Text = openFileDialogComprobantes.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension = lblImagenOriginal.Text.Substring(lblImagenOriginal.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblImagenAGuardar.Text = string.Format(@"Dom_{0}."  + extension, DateTime.Now.Ticks);
            }
        }

        private void btnImgComINE_Click(object sender, EventArgs e)
        {
            frm.tiposDeImagen();
            //si se abre imagen, se toma archivo
            if (openFileDialogComprobantes.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = openFileDialogComprobantes.FileName;
                //se carga archivo por su nombre al picBox
                pnlImgINE.Image = Image.FromFile(openFileDialogComprobantes.FileName);
                //se toma el nmbre original
                lblImagenOriginal2.Text = openFileDialogComprobantes.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension1 = lblImagenOriginal2.Text.Substring(lblImagenOriginal2.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension1 = extension1.ToLower() == "peg" ? "jpeg" : extension1.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblImagenAGuardar2.Text = string.Format(txtNombre.Text + @"INE_{0}." + extension1, DateTime.Now.Ticks);
            }

        }

        private void pnlMostrarINE_MouseEnter(object sender, EventArgs e)
        {
            pnlImgINE.Visible = true;
            pnlImgINE.Show();
        }

        private void pnlMostrarINE_MouseHover(object sender, EventArgs e)
        {
            pnlImgINE.Visible = true;
            pnlImgINE.Show();
        }

        private void pnlMostrarINE_MouseLeave(object sender, EventArgs e)
        {
            pnlImgINE.Visible = false;
            pnlImgINE.Hide();
            txtComproINE.Text = lblImagenAGuardar2.Text;
        }

        private void pnlMostarPicDom_MouseEnter(object sender, EventArgs e)
        {
            picComprobantes.Visible = true;
            picComprobantes.Show();

        }

        private void pnlMostarPicDom_MouseHover(object sender, EventArgs e)
        {
            picComprobantes.Visible = true;
            picComprobantes.Show();

        }

        private void pnlMostarPicDom_MouseLeave(object sender, EventArgs e)
        {
            picComprobantes.Visible = false;
            picComprobantes.Hide();
            txtComprobanteDomicilio.Text = lblImagenAGuardar.Text;
        }

        private void pnlMostarPicDom_MouseMove(object sender, MouseEventArgs e)
        {
            picComprobantes.Visible = true;
            picComprobantes.Show();

        }

        private void txtComprobanteCurp_TextChanged(object sender, EventArgs e)
        {
            if (txtComprobanteCurp.Text != "")
                txtComprobanteCurp.BackColor = Color.ForestGreen;
        }

        private void txtComprobanteDomicilio_TextChanged(object sender, EventArgs e)
        {
            if (txtComprobanteDomicilio.Text != "")
                txtComprobanteDomicilio.BackColor = Color.ForestGreen;
        }

        private void txtComproINE_TextChanged(object sender, EventArgs e)
        {
            if (txtComproINE.Text != "")
                txtComproINE.BackColor = Color.ForestGreen;
        }

        private void dgProductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            idPerAlternativa = int.Parse(dgPersonaSecundaria.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtNombre.Text = dgPersonaSecundaria.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtApPat.Text = dgPersonaSecundaria.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtApMat.Text = dgPersonaSecundaria.Rows[e.RowIndex].Cells[3].Value.ToString();
            dtpFechaNacimiento.Value = DateTime.Parse(dgPersonaSecundaria.Rows[e.RowIndex].Cells[4].Value.ToString());
            txtCorreo.Text = dgPersonaSecundaria.Rows[e.RowIndex].Cells[7].Value.ToString();
            txtCelular.Text = dgPersonaSecundaria.Rows[e.RowIndex].Cells[5].Value.ToString();
            txtTelefono.Text = dgPersonaSecundaria.Rows[e.RowIndex].Cells[6].Value.ToString();
            txtCalle.Text = dgPersonaSecundaria.Rows[e.RowIndex].Cells[8].Value.ToString();
            txtNumCasa.Text = dgPersonaSecundaria.Rows[e.RowIndex].Cells[9].Value.ToString();
            txtCP.Text = dgPersonaSecundaria.Rows[e.RowIndex].Cells[10].Value.ToString();
            txtColonia.Text = dgPersonaSecundaria.Rows[e.RowIndex].Cells[11].Value.ToString();
            txtFraccionamiento.Text = dgPersonaSecundaria.Rows[e.RowIndex].Cells[12].Value.ToString();
            txtLocalidad.Text = dgPersonaSecundaria.Rows[e.RowIndex].Cells[13].Value.ToString();
            txtMunicipio.Text = dgPersonaSecundaria.Rows[e.RowIndex].Cells[14].Value.ToString();
            lblImagenAGuardar.Text = dgPersonaSecundaria.Rows[e.RowIndex].Cells[15].Value.ToString();
            lblImagenAGuardar2.Text = dgPersonaSecundaria.Rows[e.RowIndex].Cells[16].Value.ToString();
            txtCurp.Text = dgPersonaSecundaria.Rows[e.RowIndex].Cells[17].Value.ToString();
            lblImagenAGuardar1.Text = dgPersonaSecundaria.Rows[e.RowIndex].Cells[18].Value.ToString();
            /*lblidCliente.Text= dgPersonaSecundaria.Rows[e.RowIndex].Cells[19].Value.ToString()*/;
        }
    }
}