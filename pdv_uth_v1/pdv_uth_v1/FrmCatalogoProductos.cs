﻿using Lib_pdv_uth_v1;
using Lib_pdv_uth_v1.productos;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace pdv_uth_v1
{
    public partial class FrmCatalogoProductos : Form
    {

        //var de producto
        private Timer ti;


        private void eventTimer(object ob, EventArgs e)
        {
            DateTime today = DateTime.Now;
            lblReloj.Text = today.ToString("hh:mm:ss tt");
        }

        Producto prod = new Producto();
        int idProducto = 0;
        public FrmCatalogoProductos()
        {
            ti = new Timer();
            ti.Tick += new EventHandler(eventTimer);
            ti.Enabled = true;
            InitializeComponent();
            mostrarRegistrosEnDG();
        }



        private void iconBtnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void mostrarRegistrosEnDG()
        {
            //borrar todos los ren del DG
            dgProductos.DataSource = null;
            //se crea lista de criterios de busqueda
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = " 1 ";
            criterio.operadorIntermedio = OperadorDeConsulta.IGUAL;
            criterio.valor = "1";
            criterio.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dgProductos.DataSource = prod.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dgProductos.Update();
            dgProductos.Refresh();
        }

        private void FrmCatalogoProductos_Load(object sender, EventArgs e)
        {
            if (FrmLogin.usuario.ToString() == "CAJERO")
            {
                label2.Visible = false;
                btnCajas.Visible = false;
            }
            else
            {
                label2.Visible = true;
                btnCajas.Visible = true;
            }
            //set del combo
            lblUserName.Text = FrmLogin.personName;
            comboUnidadDeMedida.SelectedItem = comboUnidadDeMedida.Items[0];
        }


        private void icoBtnCargarImagen_Click(object sender, EventArgs e)
        {
            //Filtros para abrir imagen
            openFileDialogCargarImagen.Filter = "Image Files|*.png;";
            openFileDialogCargarImagen.Filter = "Bitmap Image (.bmp)|*.bmp|Gif Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg|JPG Image (.jpg)|*.jpg|Png Image (.png)|*.png|Tiff Image (.tiff)|*.tiff";
            openFileDialogCargarImagen.Title = "Abrir Imagen para Producto";
            openFileDialogCargarImagen.DefaultExt = ".png";
            //si se abre imagen, se toma archivo
            if (openFileDialogCargarImagen.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = openFileDialogCargarImagen.FileName;
                //se carga archivo por su nombre al picBox
                picBoxProducto.Image = Image.FromFile(openFileDialogCargarImagen.FileName);
                //se toma el nmbre original
                lblImagenOriginal.Text = openFileDialogCargarImagen.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension = lblImagenOriginal.Text.Substring(lblImagenOriginal.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblImagenAGuardar.Text = string.Format(@"prod_{0}." + extension, DateTime.Now.Ticks);
            }
        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtEAN13.Text = txtCodBarras.Text;
            if (e.KeyChar == 13)
            {
                string codBarras = txtCodBarras.Text.Substring(0, txtCodBarras.Text.Length - 1);
                Zen.Barcode.CodeEan13BarcodeDraw codeEan13 = Zen.Barcode.BarcodeDrawFactory.CodeEan13WithChecksum; //convertir el txt a ean13
                var barcodeImage = codeEan13.Draw(codBarras, 100);//altura de la imagen a mostrar
                var resultImage = new Bitmap(barcodeImage.Width + 65, barcodeImage.Height + 40); // Dimensiones de la imagen

                using (var graphics = Graphics.FromImage(resultImage))
                using (var font = new Font("Lucida Sans", 12))//Tipografia 
                using (var brush = new SolidBrush(Color.Black))//color del texto
                //Alinear el texto
                using (var format = new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Far })
                {
                    graphics.Clear(Color.White);
                    graphics.DrawImage(barcodeImage, 0, 0);
                    graphics.DrawString(txtCodBarras.Text, font, brush, resultImage.Width / 2, resultImage.Height, format);
                }
                codBarraTry.Image = resultImage;
            }

        }
        //notificaciones
        private void notificacion(string msg, FrmNotificaciones.enmType type)
        {
            FrmNotificaciones frm = new FrmNotificaciones();
            frm.notificacion(msg, type);
            frm.Show();
            /*            frm.ShowDialog();*/

        }
        //FilterInfo
        /*FilterInfoCollection */
        private void txtCodBarras_TextChanged(object sender, EventArgs e)
        {

        }
        private void btnApagar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Usted desea salir del programa?",
                               "Cerrando programa",
                               MessageBoxButtons.YesNo,
                               MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnExpandirMn_Click(object sender, EventArgs e)
        {
            if (pnlNavbar.Width == 269)
            {
                //Quitar bordes al contraer menu
                pnlNavbar.Width = 69;
                btnHome.FlatAppearance.BorderSize = 0;
                btnCreditos.FlatAppearance.BorderSize = 0;
                btnCompras.FlatAppearance.BorderSize = 0;
                ipbLogo.Visible = false;
            }
            //activar bordes al expandir menu
            else
            {
                pnlNavbar.Width = 269;
                btnHome.FlatAppearance.BorderSize = 2;
                btnCreditos.FlatAppearance.BorderSize = 2;
                btnCompras.FlatAppearance.BorderSize = 2;
                ipbLogo.Visible = true;
            }
        }


        private void btnGuardarCliente_Click(object sender, EventArgs e)
        {
            //cargar todos los campos en producto
            prod = new Producto();
            prod.Nombre = txtNombre.Text;
            prod.Descripcion = txtDescripcion.Text;
            prod.Precio = double.Parse(txtPrecio.Text);
            prod.CodigoDeBarras = txtCodBarras.Text;
            prod.Imagen = lblImagenAGuardar.Text;
            prod.UnidadDeMedida = (UnidadDeMedida)Enum.Parse(typeof(UnidadDeMedida), comboUnidadDeMedida.SelectedItem.ToString());
            prod.EsPerecedero = checkBoxEsPereceredo.Checked;
            //hacer el insert con metodo alta()
            if (prod.alta())
            {
                // obtener el dir de la app
                string bin = System.IO.Path.GetDirectoryName(Application.StartupPath);
                //ir a un dir arriba
                string dir = System.IO.Path.GetDirectoryName(bin);
                //agregar el path para guardar la imagen
                dir += "\\Imagenes\\Productos\\";
                //guardar la imagen
                if (File.Exists(dir) == true)
                {
                    picBoxProducto.Image.Save(dir + lblImagenAGuardar.Text);
                }

                MessageBox.Show("EL producto <" + txtNombre.Text + "> se ha almacenado.", "Nuevo Producto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                limpiarForm();
                mostrarRegistrosEnDG();
            }
            else
                MessageBox.Show("Error, no se almacenó producto. " + Producto.msgError);
        }

        private void btnEliminarclientes_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Realmente desea eliminar este Producto?", "Borrar cliente", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (prod.eliminar(idProducto))
                {
                    MessageBox.Show("Producto eliminado ");
                }
                else MessageBox.Show("Error, El Producto no fue eliminado ");
            }
            dgProductos.Refresh();
        }

        private void dgProductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                idProducto = int.Parse(dgProductos.Rows[e.RowIndex].Cells[0].Value.ToString());
                txtCodBarras.Text = dgProductos.Rows[e.RowIndex].Cells[4].Value.ToString();
                txtNombre.Text = dgProductos.Rows[e.RowIndex].Cells[1].Value.ToString();
                txtPrecio.Text = dgProductos.Rows[e.RowIndex].Cells[3].Value.ToString();
                picBoxProducto.AccessibleName = dgProductos.Rows[e.RowIndex].Cells[5].Value.ToString();
                comboUnidadDeMedida.Text = dgProductos.Rows[e.RowIndex].Cells[6].Value.ToString();
                txtDescripcion.Text = dgProductos.Rows[e.RowIndex].Cells[2].Value.ToString();
                checkBoxEsPereceredo.Checked = bool.Parse(dgProductos.Rows[e.RowIndex].Cells[7].Value.ToString());
            }

        }
        private void btnAgregarProducto_Click(object sender, EventArgs e)
        {
            habilitarFrom();
        }
        public void habilitarFrom()
        {
            //habilitar forms
            txtCodBarras.Enabled = txtNombre.Enabled = txtPrecio.Enabled = txtDescripcion.Enabled = comboUnidadDeMedida.Enabled =icoBtnCargarImagen.Enabled = true;
            //change colors txt
            txtCodBarras.BackColor = txtNombre.BackColor = txtPrecio.BackColor = txtDescripcion.BackColor = comboUnidadDeMedida.BackColor =Color.White;
        }
        public void limpiarForm()
        {
            //desabilitamos el form
            txtNombre.Enabled = txtPrecio.Enabled = txtDescripcion.Enabled = comboUnidadDeMedida.Enabled = icoBtnCargarImagen.Enabled = false;
            codBarraTry.Image.Dispose();
            //limpiamos el form "";
            txtCodBarras.Text = txtNombre.Text = txtPrecio.Text = txtDescripcion.Text = "";
            // cambiar el color de fondo
            txtCodBarras.BackColor = txtNombre.BackColor = txtPrecio.BackColor = txtDescripcion.BackColor = comboUnidadDeMedida.BackColor = Color.DarkGray;
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            if (FrmLogin.usuario.TipoUsuario.ToString() == "CAJERO")
            {
                this.Hide();
                FrmCaja frm = new FrmCaja();
                frm.Show();
            }
            else
            {

                this.Hide();
                FrmMenuPpal frm = new FrmMenuPpal();
                frm.Show();
            }
        }

        private void btnVentas_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmVentas frm = new FrmVentas();
            frm.Show();
        }

        private void btnProductos_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmCatalogoProductos frm = new FrmCatalogoProductos();
            frm.Show();
        }

        private void btnCreditos_Click(object sender, EventArgs e)
        {
            
            FrmCreditos frm = new FrmCreditos();
            frm.Show();
        }

        private void btnCompras_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmCompras frm = new FrmCompras();
            frm.Show();
        }

        private void btnCajas_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmCaja frm = new FrmCaja();
            frm.Show();
        }

        private void btnModificarProducto_Click(object sender, EventArgs e)
        {
            habilitarFrom();
            Producto paraModif = new Producto();
            List<DatosParaActualizar> datos = new List<DatosParaActualizar>();

            datos.Add(new DatosParaActualizar("nombre", txtNombre.Text));
            datos.Add(new DatosParaActualizar("descripcion", txtDescripcion.Text));
            datos.Add(new DatosParaActualizar("precio", txtPrecio.Text));
            datos.Add(new DatosParaActualizar("codigo_barras", txtCodBarras.Text));
            datos.Add(new DatosParaActualizar("imagen_producto", lblImagenAGuardar.Text));
            datos.Add(new DatosParaActualizar("unidad_medida", comboUnidadDeMedida.Text));
            datos.Add(new DatosParaActualizar("es_perecedero", checkBoxEsPereceredo.Checked ? "1" : "0"));
            habilitarFrom();
            mostrarRegistrosEnDG();
        }
        private void mostrarResConsulta()
        {
           //se crea lista de criterios de busqueda
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = "nombre";
            criterio.operadorIntermedio = OperadorDeConsulta.LIKE;
            criterio.valor = "'%" + txtConsultaProducto.Text + "%'";
            criterio.operadorFinal = OperadorDeConsulta.OR;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            CriteriosBusqueda criterio2 = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio2.campo = "descripcion";
            criterio2.operadorIntermedio = OperadorDeConsulta.LIKE;
            criterio2.valor = "'%" + txtConsultaProducto.Text + "%'";
            criterio2.operadorFinal = OperadorDeConsulta.OR;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio2);
            CriteriosBusqueda criterio3 = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio3.campo = "precio";
            criterio3.operadorIntermedio = OperadorDeConsulta.LIKE;
            criterio3.valor = "'%" + txtConsultaProducto.Text + "%'";
            criterio3.operadorFinal = OperadorDeConsulta.OR;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio3);
            CriteriosBusqueda criterio4 = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio4.campo = "codigo_barras";
            criterio4.operadorIntermedio = OperadorDeConsulta.LIKE;
            criterio4.valor = "'%" + txtConsultaProducto.Text + "%'";
            criterio4.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio4);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dgProductos.DataSource = prod.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dgProductos.Update();
            dgProductos.Refresh();
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            if (txtConsultaProducto.Text == " ")
            {
                mostrarRegistrosEnDG();
            }
            else mostrarResConsulta();
        }

        private void txtConsultaProducto_Enter(object sender, EventArgs e)
        {
           
              
        }

        private void btnCompras_Click_1(object sender, EventArgs e)
        {
            FrmPersonaSecundaria frm = new FrmPersonaSecundaria();
            frm.ShowDialog();
        }

        private void txtConsultaProducto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txtConsultaProducto.Text == " ")
                {
                    mostrarRegistrosEnDG();
                    
                }
                else
                mostrarResConsulta();
            }
        }
    }
}