﻿using System;
using System.Windows.Forms;

namespace pdv_uth_v1
{
    public partial class FrmMenuPpal : Form
    {
        public FrmMenuPpal()
        {
            InitializeComponent();
        }

        private void FrmMenuPpal_Load(object sender, EventArgs e)
        {

        }

        private void bntClientes_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmCatalogoCliente frm = new FrmCatalogoCliente();
            frm.Show();
        }

        private void btnUsuarios_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmUsuarios frm = new FrmUsuarios();
            frm.Show();
        }

        private void btnProductos_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmCatalogoProductos frm = new FrmCatalogoProductos();
            frm.Show();
        }

        private void btnLogs_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmLog frm = new FrmLog();
            frm.Show();
        }

        private void btnVentas_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmCaja frm = new FrmCaja();
            frm.Show();
        }

        private void btnCreditos_Click(object sender, EventArgs e)
        {            
            FrmCreditos frm = new FrmCreditos();
            frm.Show();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro que desea salir?", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }
    }
}
