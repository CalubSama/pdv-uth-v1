﻿using Lib_pdv_uth_v1;
using Lib_pdv_uth_v1.clientes;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace pdv_uth_v1
{
    public partial class FrmMetodoDePago : Form
    {
        public static int idClienteCredito;
        public static string nombreCliente;
        public static Cliente cli = new Cliente();
        public FrmMetodoDePago()
        {
            InitializeComponent();
        }
        private void btnRetroceder_Click(object sender, EventArgs e)
        {
            
            this.Close();
        }
        private void Cerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Usted desea cerrar la ventana de personas secundarias?",
                            "Cerrar",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void FrmMetodoDePago_Load(object sender, EventArgs e)
        {
            
        }
    }
}
