﻿using AForge.Video;
using AForge.Video.DirectShow;
using Lib_pdv_uth_v1;
using Lib_pdv_uth_v1.cajas;
using Lib_pdv_uth_v1.productos;
using Lib_pdv_uth_v1.usuarios;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using ZXing;

namespace pdv_uth_v1
{
    public partial class FrmCaja : Form
    {
        private Timer ti;
        static double ventaDia= 0;
        static string valueTemp;
        public static bool banderaMetodo = false;
        public static bool banderaCreditos= false;
        public static double total= 0.0;
        public static double montoRecibido= 0.0;

        Producto prod = new Producto();
 

        private void eventTimer(object ob, EventArgs e)
        {
            DateTime today = DateTime.Now;
            lblReloj.Text = today.ToString("hh:mm:ss tt");

        }
        Caja caja = new Caja();
        public FrmCaja()
        {
            ti = new Timer();
            ti.Tick += new EventHandler(eventTimer);
            ti.Enabled = true;

            InitializeComponent();
        }
        //CREATE A  WEBCAM SCAN CODEBAR
        //instanciar de Aforge.video.directShow
        FilterInfoCollection filterInfoCollection;
        VideoCaptureDevice videoCaptureDevice;
        //instanciar el reader
        BarcodeReader reader;
        private void FrmCaja_Load(object sender, EventArgs e)
        {
            TTCaja.SetToolTip(btnAbrirCaja, "Guardar el monto inicial de la caja \n el valor de inicio de caja no podrá ser accesible \n una vez se haya ingresado el monto y guardado.");
            TTCaja.SetToolTip(btnCerrarCaja, "Se guardará el monto total de las ventas del turno \n sin tomar en cuenta el monto de inicio de caja \n Se limpiarán los formularios para poder ingresar una nueva venta.");
            TTCaja.SetToolTip(btnExpandirMn, "Comprime y Expande la barra de navegación para tener más espacio..");
            TTCaja.SetToolTip(btnPagar, "Recibe el monto del método de pago \n y realiza la venta de los productos \n se limpiarán los formularios para una venta nueva.");
            TTCaja.SetToolTip(picBuscarProduc, "Muestra el resultado de la búsqueda del texto.");
            TTCaja.SetToolTip(picInfoCaja, "Muestra el formulario de información de la caja \n abrir caja, cerrar caja, montos de efectivo.");
            TTCaja.SetToolTip(btnAgregarAVenta, "Agrega a la venta el producto con el código de barras ingresado.");
            TTCaja.SetToolTip(pnlMetodoEfectivo, "Recibe el monto a pagar en efectivo \n al ingresar el monto se obtendrá la cantidad a devolver al cliente \n puede utilizar varios métodos a la vez.");
            TTCaja.SetToolTip(pnlMetodoTarjeta, "Recibe el monto a pagar en tarjeta \n puede utilizar varios métodos a la vez.");
            TTCaja.SetToolTip(pnlCreditoCliente, "Recibe el monto a pagar en crédito o fiado \n Desglosa una listado de clientes para agregar el total a su crédito.");
            TTCaja.SetToolTip(btnCancelarVenta, "Cancela la venta para iniciar una nueva \n limpiar los formularios de venta.");
            TTCaja.SetToolTip(btnStart, "Inicia la cámara lectora de códigos de barras\n Al volver a hacer clic desactiva la cámara\n vuelven los componentes a su estado inicial.");
            TTCaja.SetToolTip(btnHome, "Abre la ventana Menú Principal.");
            TTCaja.SetToolTip(btnProductos, "Abre la ventana de productos.");
            TTCaja.SetToolTip(btnCreditos, "Abre la ventana de creditos.");
            /*TTCaja.SetToolTip(btnCliente, "Recibe el monto del método de pago \n y realiza la venta de los productos \n se limpiarán los formularios para una venta nueva.");
            TTCaja.SetToolTip(btnPagar, "Recibe el monto del método de pago \n y realiza la venta de los productos \n se limpiarán los formularios para una venta nueva.");
*/
            btnPagar.Enabled = false;
            lblUserName.Text = FrmLogin.personName;
            filterInfoCollection = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo device in filterInfoCollection)
            {
                cboCamera.Items.Add(device.Name);
                cboCamera.SelectedIndex = 0;
            }

        }
        private void agregarProducto()
        {
            if (txtCodBarras.TextLength >= 13)
            {
                Producto prod = new Producto();
                prod = prod.consultarPorCodigoDeBarras(txtCodBarras.Text);
                //asignar el nombre del producto al label
                lblNombreProducto.Text = prod.Nombre;
                if (prod.CodigoDeBarras != null)
                {
                    //Agregar la imagen al formulario
                    // obtener el dir de la app
                    string bin = Path.GetDirectoryName(Application.StartupPath);
                    //ir a un dir arriba
                    string ruta = Path.GetDirectoryName(bin);
                    ruta += "\\Imagenes\\Productos\\" + prod.Imagen.ToString();
                    //validar que no sea necesario el poner la imagen
                    if (File.Exists(ruta) == true)
                    {
                        //Mostrar la imagen en el formulario
                        //File exists 
                        picProductoAVender.Image = Image.FromFile(ruta);
                        //pasar producto obj a obj[]
                    }
                        //agregar el producto al DataGrid
                        dgListaProductos.Rows.Add(new object[] { prod.Id, prod.CodigoDeBarras, prod.Nombre, prod.Descripcion, numericCantidad.Value, prod.Precio, (prod.Precio * double.Parse(numericCantidad.Value.ToString())) });
                        //agregar el produ a caja.ListaProductos
                        caja.ListaProductos.Add(new ProductosAVender(prod.Id, double.Parse(numericCantidad.Value.ToString()), prod.CodigoDeBarras));
                    //limpiamos los text
                    txtCodBarras.Clear();
                    numericCantidad.Value = 1;
                }
                else
                {
                    //producto NO EXISTE
                    this.notificacion("El producto con el Código de Barras" + "\n" + "<" + txtCodBarras.Text + ">, \n no Existe. ", FrmNotificaciones.enmType.Info);
                    //cursor a CodBarras y select all
                    txtCodBarras.Focus();
                    txtCodBarras.SelectAll();
                }
                double temp = 0;
                for (int i = 0; i < dgListaProductos.RowCount - 1; i++)
                {
                    temp = temp +
                        double.Parse(dgListaProductos.Rows[i].Cells[dgListaProductos.ColumnCount - 1].Value.ToString());
                }
                txtTotal.Text = temp.ToString();
                txtSubTotal.Text = (temp * 0.84).ToString();
                txtIva.Text = (temp * 0.16).ToString();

            }
            else
            {
                this.notificacion("El Código de barras \n debe contener 13 caracteres", FrmNotificaciones.enmType.Info);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            total = double.Parse(txtTotal.Text);
            montoRecibido = double.Parse(txtMetodoEfectivo.Text);
            TicketDeVenta ticket = new TicketDeVenta();
            ticket.generarTicket();
            panelDatosCredito.Visible = true;

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea cerrar la caja?",
                                "Salir",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnPagar_Click(object sender, EventArgs e)
        {
            
            Usuario usr = new Usuario();
            ventaDia += double.Parse(txtTotal.Text.ToString());
            //meter variables
            //Validar que no haya cantidades nulas o menores a cero
            if (double.Parse(txtMetodoEfectivo.Text)> 0.0 || double.Parse(txtMetodoCredito.Text)> 0.0 || double.Parse(txtMetodoTarjeta.Text) > 0.0)
            {
                //valida que el metodo de pago sea en efectivo
                if (double.Parse(txtMetodoCredito.Text) <= 0.0 && double.Parse(txtMetodoTarjeta.Text) <= 0.0)
                {
                    if (caja.vender(FrmLogin.us.Id, double.Parse(txtMetodoEfectivo.Text), double.Parse(txtTotal.Text)))
                    {
                        this.notificacion("la venta ha sido registrada", FrmNotificaciones.enmType.Success);
                        txtTotal.Text = "";
                    }
                    else
                    {
                        this.notificacion("Error en el registro de la venta. " + Caja.msgError, FrmNotificaciones.enmType.Error);
                    }
                }
                //Validar que el metodo de pago sea en  tarjeta
                else if (double.Parse(txtMetodoCredito.Text) <= 0.0 && double.Parse(txtMetodoEfectivo.Text) <= 0.0)
                {
                    if (caja.vender(FrmLogin.us.Id, double.Parse(txtMetodoTarjeta.Text), double.Parse(txtTotal.Text)))
                    {
                        this.notificacion("la venta ha sido registrada", FrmNotificaciones.enmType.Success);
                        txtTotal.Text = "";
                    }
                    else
                    {
                        this.notificacion("Error en el registro de la venta. " + Caja.msgError, FrmNotificaciones.enmType.Error);
                    }
                }
                //Validar que el metodo de pago sea en  credito
                else if (double.Parse(txtMetodoTarjeta.Text) <= 0.0 && double.Parse(txtMetodoEfectivo.Text) <= 0.0)
                {
                    
                    if (caja.vender(FrmLogin.us.Id, double.Parse(txtMetodoCredito.Text), double.Parse(txtTotal.Text)))
                    {
                        this.notificacion("la venta ha sido registrada", FrmNotificaciones.enmType.Success);
                        txtTotal.Text = "";
                    }
                    else
                    {
                        this.notificacion("Error en el registro de la venta. " + Caja.msgError, FrmNotificaciones.enmType.Error);
                    }
                }
                //Validar que el metodo de pago sea efectivo y credito
                else if (double.Parse(txtMetodoTarjeta.Text) <= 0.0 && double.Parse(txtMetodoEfectivo.Text)+ double.Parse(txtMetodoCredito.Text) > double.Parse(txtTotal.Text))
                {
                    if (caja.vender(FrmLogin.us.Id, double.Parse(txtMetodoCredito.Text)+ double.Parse(txtMetodoEfectivo.Text), double.Parse(txtTotal.Text)))
                    {
                        this.notificacion("la venta ha sido registrada", FrmNotificaciones.enmType.Success);
                        txtTotal.Text = "";
                    }
                    else
                    {
                        this.notificacion("Error en el registro de la venta. " + Caja.msgError, FrmNotificaciones.enmType.Error);
                    }
                }
                //Validar que el metodo de pago sea efectivo y tarjeta
                else if (double.Parse(txtMetodoCredito.Text) <= 0.0 && double.Parse(txtMetodoEfectivo.Text) + double.Parse(txtMetodoTarjeta.Text) > double.Parse(txtTotal.Text))
                {
                    if (caja.vender(FrmLogin.us.Id, double.Parse(txtMetodoTarjeta.Text)+ double.Parse(txtMetodoEfectivo.Text), double.Parse(txtTotal.Text)))
                    {
                        this.notificacion("la venta ha sido registrada", FrmNotificaciones.enmType.Success);
                        txtTotal.Text = "";
                    }
                    else
                    {
                        this.notificacion("Error en el registro de la venta. " + Caja.msgError, FrmNotificaciones.enmType.Error);
                    }
                }
                //Validar que el metodo de pago sea Tarjeta y credito
                else if (double.Parse(txtMetodoEfectivo.Text) <= 0.0 && double.Parse(txtMetodoTarjeta.Text) + double.Parse(txtMetodoCredito.Text) > double.Parse(txtTotal.Text))
                {
                    if (caja.vender(FrmLogin.us.Id, double.Parse(txtMetodoTarjeta.Text) + double.Parse(txtMetodoCredito.Text), double.Parse(txtTotal.Text)))
                    {
                        this.notificacion("la venta ha sido registrada", FrmNotificaciones.enmType.Success);
                        txtTotal.Text = "";
                    }
                    else
                    {
                        this.notificacion("Error en el registro de la venta. " + Caja.msgError, FrmNotificaciones.enmType.Error);
                    }
                }
                //Notificar error
                else
                {
                    this.notificacion("Asegurese de que los campos de pago esten correctos", FrmNotificaciones.enmType.Info);
                }

            }
            //notificar error
            else
            {
                notificacion("El monto de pago debe ser mayor", FrmNotificaciones.enmType.Warning);
            }
            numericCantidad.Value = 1;
            txtMetodoEfectivo.Text = txtMetodoTarjeta.Text = txtMetodoCredito.Text = txtSubTotal.Text = txtIva.Text = txtTotal.Text = "000.00";
            dgListaProductos.Rows.Clear();
        }
    
    private void notificacion(string msg, FrmNotificaciones.enmType type)
        {
            FrmNotificaciones frm = new FrmNotificaciones();
            frm.notificacion(msg, type);
            frm.Show();
            /*            frm.ShowDialog();*/

        }
        private void btnAgregarAVenta_Click(object sender, EventArgs e)
        {
            agregarProducto();
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            if (FrmLogin.usuario.TipoUsuario.ToString() == "CAJERO")
            {
                MessageBox.Show("Estas en el Home de cajero");
            }
            else
            {
                this.Close();
                FrmMenuPpal frm = new FrmMenuPpal();
                frm.Show();
            }
;
        }
        private void btnVentas_Click(object sender, EventArgs e)
        {
            this.Close();
            FrmCatalogoCliente frm = new FrmCatalogoCliente();
            frm.Show();
        }
        private void btnProductos_Click(object sender, EventArgs e)
        {
            this.Close();
            FrmCatalogoProductos frm = new FrmCatalogoProductos();
            frm.Show();
        }
        private void btnCreditos_Click(object sender, EventArgs e)
        {            
            FrmCreditos frm = new FrmCreditos();
            frm.Show();

        }
        private void btnCompras_Click(object sender, EventArgs e)
        {
            FrmPersonaSecundaria frm = new FrmPersonaSecundaria();
            frm.Show();
        }

        private void btnCajas_Click(object sender, EventArgs e)
        {
            this.Close();
            FrmCaja frm = new FrmCaja();
            frm.Show();

        }
        private void btnExpandirMn_Click(object sender, EventArgs e)
        {
            if (pnlNavbar.Width == 269)
            {
                //Quitar bordes al contraer menu
                pnlNavbar.Width = 69;
                btnHome.FlatAppearance.BorderSize = 0;
                btnCompras.FlatAppearance.BorderSize = 0;
                btnProductos.FlatAppearance.BorderSize = 0;
                btnCreditos.FlatAppearance.BorderSize = 0;
                btnVentas.FlatAppearance.BorderSize = 0;
                picBuscarProduc.Visible = false;
            }
            //activar bordes al expandir menu
            else
            {
                pnlNavbar.Width = 269;
                btnHome.FlatAppearance.BorderSize = 2;
                btnCompras.FlatAppearance.BorderSize = 2;
                btnProductos.FlatAppearance.BorderSize = 2;
                btnCreditos.FlatAppearance.BorderSize = 2;
                btnVentas.FlatAppearance.BorderSize = 2;
                picBuscarProduc.Visible = true;
            }
        }

        private void dgListaProductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            /*            dgListaProductos.DataSource = .consultar () ;*/
        }

        private void pnlVentaInfo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            
            
            if (picProductoAVender.Width == 238)
            {
                lblNombreProducto.Location = new Point(4, 158);
                picProductoAVender.Location = new Point(3, 4);
                picProductoAVender.Size = new Size(186, 146);
                pibWebCam.Location = new Point(0, 177);
                pibWebCam.Visible = true;
                //filtrar los dispositivos desde el combo box
                videoCaptureDevice = new VideoCaptureDevice(filterInfoCollection[cboCamera.SelectedIndex].MonikerString);
                videoCaptureDevice.NewFrame += VideoCaptureDevice_NewFrame;
                videoCaptureDevice.Start();
            }
            //activar bordes al expandir menu
            else
            {
                pibWebCam.Visible = false;
                lblNombreProducto.Location = new Point(35, 382);
                picProductoAVender.Location = new Point(19, 77);
                picProductoAVender.Size = new Size(238, 302);
                videoCaptureDevice.Stop();
            }
            
        }

        private void VideoCaptureDevice_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            //leer del bitmap
            Bitmap bitmap = (Bitmap)eventArgs.Frame.Clone();
            BarcodeReader reader = new BarcodeReader();
            //decodificar el resultado
            var result = reader.Decode(bitmap);
            if (result != null)
            {
                txtCodBarras.Invoke(new MethodInvoker(delegate ()
                {
                    //crear una cadena con el texto que nos da el reader
                    txtCodBarras.Text = result.ToString();
                }));
            }
            //igualar la imagen al bitmap que tenemos
            pibWebCam.Image = bitmap;
        }

        private void FrmCaja_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Cerrar la webcam cuando se esta cerrando el form
            if (videoCaptureDevice != null)
            {
                if (videoCaptureDevice.IsRunning)
                    videoCaptureDevice.Stop();
            }
        }

        private void pibWebCam_Click(object sender, EventArgs e)
        {

        }

        private void btnExpandirMn_Click_1(object sender, EventArgs e)
        {
            if (pnlNavbar.Width == 269)
            {
                //Quitar bordes al contraer menu
                pnlNavbar.Width = 69;
                btnHome.FlatAppearance.BorderSize = 0;
                btnCompras.FlatAppearance.BorderSize = 0;
                btnProductos.FlatAppearance.BorderSize = 0;
                btnCreditos.FlatAppearance.BorderSize = 0;
                btnVentas.FlatAppearance.BorderSize = 0;
                picBuscarProduc.Visible = false;
            }
            //activar bordes al expandir menu
            else
            {
                pnlNavbar.Width = 269;
                btnHome.FlatAppearance.BorderSize = 2;
                btnCompras.FlatAppearance.BorderSize = 2;
                btnProductos.FlatAppearance.BorderSize = 2;
                btnCreditos.FlatAppearance.BorderSize = 2;
                btnVentas.FlatAppearance.BorderSize = 2;
                picBuscarProduc.Visible = true;
            }
        }

        private void btnApagar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Usted desea salir del programa?",
                               "Cerrando programa",
                               MessageBoxButtons.YesNo,
                               MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void txtCodBarras_Enter(object sender, EventArgs e)
        {
            agregarProducto();
        }

        private void btnCancelarVenta_Click(object sender, EventArgs e)
        {
            dgListaProductos.Columns.Clear();
        }

        private void lblCerrarInfoCaja_Click(object sender, EventArgs e)
        {
            pnlInfoAbrirCaja.Visible = false;
            
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            
            double ventaEfectivo =ventaDia - double.Parse(numMontoInicial.Value.ToString());
            double montoEnCaja = ventaDia + double.Parse(numMontoInicial.Value.ToString());
            //ventas del día 
            txtVentaDeTurno.Text = ventaDia.ToString();
            //Monto de efectivo en la caja
            txtVentasEnEfectivo.Text = ventaEfectivo.ToString();
            pnlInfoAbrirCaja.Visible = true;
            //Mostrar monto de efectivo en caja
            txtMontoEnCaja.Text = montoEnCaja.ToString();
             
        }

        private void txtMetodoEfectivo_MouseLeave(object sender, EventArgs e)
        {
            double cambio = double.Parse(txtMetodoEfectivo.Text.ToString()) - double.Parse(txtTotal.Text.ToString());
            txtCambio.Text = cambio.ToString();
        }
        
        private void btnCerrarCaja_Click(object sender, EventArgs e)
        {
            Caja cerrarCaja = new Caja();
            //verificamos que se haya agregado un monto inicial
            if (numMontoInicial.Enabled == false)
            {
                cerrarCaja.corteDeCaja(FrmLogin.usuario.Id, double.Parse(txtVentaDeTurno.Text));
                notificacion("Monto de cierre :" + txtVentaDeTurno.Text, FrmNotificaciones.enmType.Success);
            }
            else
                notificacion("Agrege un monto de apertura mayor", FrmNotificaciones.enmType.Info);
        }

        private void btnAbrirCaja_Click_1(object sender, EventArgs e)
        {

            Caja abrircaja = new Caja();
            //validamos que haya un monto en monto inicial
            if (double.Parse(numMontoInicial.Value.ToString()) > 1)
            {
                abrircaja.abrirCaja(FrmLogin.usuario.Id, double.Parse(numMontoInicial.Value.ToString()));
                notificacion("Monto inicial de: " + numMontoInicial.Value.ToString()+"Guardado", FrmNotificaciones.enmType.Success);
                numMontoInicial.Enabled = false;
            }
            else
                notificacion("Agrege un monto de apertura mayor", FrmNotificaciones.enmType.Info);
        }
        //datagridProductos
        private void mostrarRegistrosEnDG()
        {
            //se crea lista de criterios de busqueda
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = " 1 ";
            criterio.operadorIntermedio = OperadorDeConsulta.IGUAL;
            criterio.valor = "1";
            criterio.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dgProductos.DataSource = prod.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dgProductos.Update();
            dgProductos.Refresh();
            dgProductos.Visible = true;
        }
        private void dgProductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                int.Parse(dgProductos.Rows[e.RowIndex].Cells[0].Value.ToString());
                txtCodBarras.Text= dgProductos.Rows[e.RowIndex].Cells[4].Value.ToString();
                dgProductos.Rows[e.RowIndex].Cells[1].Value.ToString();
                dgProductos.Rows[e.RowIndex].Cells[3].Value.ToString();
                dgProductos.Rows[e.RowIndex].Cells[5].Value.ToString();
                dgProductos.Rows[e.RowIndex].Cells[6].Value.ToString();
                dgProductos.Rows[e.RowIndex].Cells[2].Value.ToString();
                bool.Parse(dgProductos.Rows[e.RowIndex].Cells[7].Value.ToString());
            }

        }
        private void mostrarResConsulta()
        {

            //se crea lista de criterios de busqueda
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = "nombre ";
            criterio.operadorIntermedio = OperadorDeConsulta.LIKE;
            criterio.valor = "'%"+ txtConsultaProducto.Text + "%'";
            criterio.operadorFinal = OperadorDeConsulta.OR;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            CriteriosBusqueda criterio2 = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio2.campo = "descripcion";
            criterio2.operadorIntermedio = OperadorDeConsulta.LIKE;
            criterio2.valor = "'%" + txtConsultaProducto.Text + "%'";
            criterio2.operadorFinal = OperadorDeConsulta.OR;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio2);
            CriteriosBusqueda criterio3 = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio3.campo = "precio";
            criterio3.operadorIntermedio = OperadorDeConsulta.LIKE;
            criterio3.valor = "'%" + txtConsultaProducto.Text + "%'";
            criterio3.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio3);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dgProductos.DataSource = prod.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dgProductos.Update();
            dgProductos.Refresh();
            dgProductos.Visible = true;
        }
        private void pictureBox9_Click(object sender, EventArgs e)       
        {
            if (txtConsultaProducto.Text != " ")
                mostrarResConsulta();
            else mostrarRegistrosEnDG();

            pnlInfoProductos.Visible = true;
        }
        private void dgProductos_MouseLeave(object sender, EventArgs e)
        {
            pnlInfoProductos.Visible = false;
        }
        private void dgListaProductos_MouseEnter(object sender, EventArgs e)
        {
            lblClienteCredito.Text = FrmMetodoDePago.nombreCliente;

        }
        private void pnlCreditoCliente_MouseClick(object sender, MouseEventArgs e)
        {
            total = double.Parse(txtTotal.Text);
            banderaCreditos = true;
            FrmCreditos frm1 = new FrmCreditos();
            frm1.ShowDialog();
        }

        private void pnlMetodoTarjeta_MouseClick(object sender, MouseEventArgs e)
        {
            banderaMetodo = true;
            FrmMetodoDePago frm1 = new FrmMetodoDePago();
            frm1.ShowDialog();
        }
        private void pbTarjeta_Click(object sender, EventArgs e)
        {
            
            FrmMetodoDePago frm1 = new FrmMetodoDePago();
            frm1.ShowDialog();
            banderaMetodo = true;
        }
        private void pbCredito_Click(object sender, EventArgs e)        
        {
            txtMetodoCredito.Text = txtTotal.Text;

            total = double.Parse(txtTotal.Text);
            banderaCreditos = true;
            FrmCreditos frm1 = new FrmCreditos();
            frm1.ShowDialog();
            btnPagar.Enabled = true;
            
        }
        private void txtConsultaProducto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {

                if (txtConsultaProducto.Text != " ")
                    mostrarResConsulta();
                
                else mostrarRegistrosEnDG();

                pnlInfoProductos.Visible = true;
            }
        }
        private void txtCodBarras_TextChanged(object sender, EventArgs e)
        {
            if (txtCodBarras.Text.Length == 13)
            {
                txtCodBarras.BackColor = Color.Green;
                btnAgregarAVenta.Enabled = true;
            }
            else
            {
                btnAgregarAVenta.Enabled = false;
                txtCodBarras.BackColor = Color.Red;

            }

        }

        private void txtMetodoEfectivo_TextChanged(object sender, EventArgs e)
        {
            if (double.Parse(txtMetodoEfectivo.Text)> double.Parse(txtTotal.Text))
            {
                btnPagar.Enabled = true;
                double cambio = double.Parse(txtMetodoEfectivo.Text.ToString()) - double.Parse(txtTotal.Text.ToString());
                txtCambio.Text = cambio.ToString();
            }
            
           
        }
    }
}
