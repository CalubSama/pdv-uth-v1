﻿namespace pdv_uth_v1
{
    partial class FrmUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUsuarios));
            this.panelForm = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgUsuarios = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtBusqueda = new System.Windows.Forms.TextBox();
            this.PicBuscar = new System.Windows.Forms.PictureBox();
            this.btnRetroceder = new System.Windows.Forms.Button();
            this.btnApagar = new System.Windows.Forms.Button();
            this.lblImagenOriginal2 = new System.Windows.Forms.Label();
            this.lblImagenAGuardar2 = new System.Windows.Forms.Label();
            this.lblImagenAGuardar1 = new System.Windows.Forms.Label();
            this.lblImagenOriginal1 = new System.Windows.Forms.Label();
            this.lblImagenAGuardar = new System.Windows.Forms.Label();
            this.lblImagenOriginal = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblImagenAGuardarActa = new System.Windows.Forms.Label();
            this.lblImagenAGuardarEstudios = new System.Windows.Forms.Label();
            this.lblImagenOriginalActa = new System.Windows.Forms.Label();
            this.lblImagenAGaurdarCert = new System.Windows.Forms.Label();
            this.btnEliminarclientes = new System.Windows.Forms.Button();
            this.lblImagenOriginalEstudios = new System.Windows.Forms.Label();
            this.btnModificarCliente = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnGuardarCliente = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnAñadirClientes = new System.Windows.Forms.Button();
            this.lblNombreCliente = new System.Windows.Forms.Label();
            this.picUserImage = new System.Windows.Forms.PictureBox();
            this.panelFormulario = new System.Windows.Forms.Panel();
            this.lblConfir = new System.Windows.Forms.Label();
            this.txtConfirmarContra = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picCURP = new System.Windows.Forms.PictureBox();
            this.picCertEst = new System.Windows.Forms.PictureBox();
            this.picINE = new System.Windows.Forms.PictureBox();
            this.picComproDom = new System.Windows.Forms.PictureBox();
            this.picActaNacc = new System.Windows.Forms.PictureBox();
            this.btnComproCurp = new System.Windows.Forms.Button();
            this.btnComproINE = new System.Windows.Forms.Button();
            this.btnComproDom = new System.Windows.Forms.Button();
            this.btnCerEst = new System.Windows.Forms.Button();
            this.btnImgActaNac = new System.Windows.Forms.Button();
            this.cbTipoUser = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtUsuarioContra = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNumSS = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCertEstudios = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtActaDeNac = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtComprobanteCurp = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtComproINE = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtLocalidad = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtCurp = new System.Windows.Forms.TextBox();
            this.txtComprobanteDom = new System.Windows.Forms.Label();
            this.txtComprobanteDomicilio = new System.Windows.Forms.TextBox();
            this.lblCalle = new System.Windows.Forms.Label();
            this.txtCalle = new System.Windows.Forms.TextBox();
            this.dtpFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.txtCP = new System.Windows.Forms.TextBox();
            this.txtNumCasa = new System.Windows.Forms.TextBox();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.txtFraccionamiento = new System.Windows.Forms.TextBox();
            this.txtMunicipio = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.txtColonia = new System.Windows.Forms.TextBox();
            this.txtCorreo = new System.Windows.Forms.TextBox();
            this.txtCelular = new System.Windows.Forms.TextBox();
            this.txtApMat = new System.Windows.Forms.TextBox();
            this.txtApPat = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.openFileDialogComprobantes = new System.Windows.Forms.OpenFileDialog();
            this.panelForm.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUsuarios)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBuscar)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUserImage)).BeginInit();
            this.panelFormulario.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCURP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCertEst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picINE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picComproDom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picActaNacc)).BeginInit();
            this.SuspendLayout();
            // 
            // panelForm
            // 
            this.panelForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.panelForm.Controls.Add(this.panel3);
            this.panelForm.Controls.Add(this.panel2);
            this.panelForm.Controls.Add(this.lblImagenOriginal2);
            this.panelForm.Controls.Add(this.lblImagenAGuardar2);
            this.panelForm.Controls.Add(this.lblImagenAGuardar1);
            this.panelForm.Controls.Add(this.lblImagenOriginal1);
            this.panelForm.Controls.Add(this.lblImagenAGuardar);
            this.panelForm.Controls.Add(this.lblImagenOriginal);
            this.panelForm.Controls.Add(this.panel1);
            this.panelForm.Controls.Add(this.panel5);
            this.panelForm.Controls.Add(this.panelFormulario);
            this.panelForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelForm.Location = new System.Drawing.Point(0, 0);
            this.panelForm.Margin = new System.Windows.Forms.Padding(2);
            this.panelForm.Name = "panelForm";
            this.panelForm.Size = new System.Drawing.Size(997, 656);
            this.panelForm.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgUsuarios);
            this.panel3.Location = new System.Drawing.Point(388, 73);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(608, 494);
            this.panel3.TabIndex = 75;
            // 
            // dgUsuarios
            // 
            this.dgUsuarios.AllowUserToOrderColumns = true;
            this.dgUsuarios.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgUsuarios.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgUsuarios.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            this.dgUsuarios.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgUsuarios.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.HotTrack;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgUsuarios.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgUsuarios.ColumnHeadersHeight = 30;
            this.dgUsuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgUsuarios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgUsuarios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgUsuarios.EnableHeadersVisualStyles = false;
            this.dgUsuarios.GridColor = System.Drawing.Color.SteelBlue;
            this.dgUsuarios.ImeMode = System.Windows.Forms.ImeMode.On;
            this.dgUsuarios.Location = new System.Drawing.Point(0, 0);
            this.dgUsuarios.Name = "dgUsuarios";
            this.dgUsuarios.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgUsuarios.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            this.dgUsuarios.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgUsuarios.Size = new System.Drawing.Size(608, 494);
            this.dgUsuarios.TabIndex = 75;
            this.dgUsuarios.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgUsuarios_CellClick);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtBusqueda);
            this.panel2.Controls.Add(this.PicBuscar);
            this.panel2.Controls.Add(this.btnRetroceder);
            this.panel2.Controls.Add(this.btnApagar);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(997, 67);
            this.panel2.TabIndex = 74;
            // 
            // txtBusqueda
            // 
            this.txtBusqueda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBusqueda.Font = new System.Drawing.Font("Lucida Sans", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBusqueda.Location = new System.Drawing.Point(209, 30);
            this.txtBusqueda.Name = "txtBusqueda";
            this.txtBusqueda.Size = new System.Drawing.Size(684, 32);
            this.txtBusqueda.TabIndex = 51;
            this.txtBusqueda.Tag = "Buscar usuario";
            this.txtBusqueda.Text = "Buscar Usuario";
            this.txtBusqueda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBusqueda_KeyPress);
            // 
            // PicBuscar
            // 
            this.PicBuscar.Dock = System.Windows.Forms.DockStyle.Right;
            this.PicBuscar.Image = ((System.Drawing.Image)(resources.GetObject("PicBuscar.Image")));
            this.PicBuscar.Location = new System.Drawing.Point(899, 0);
            this.PicBuscar.Name = "PicBuscar";
            this.PicBuscar.Size = new System.Drawing.Size(45, 67);
            this.PicBuscar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PicBuscar.TabIndex = 50;
            this.PicBuscar.TabStop = false;
            this.PicBuscar.Click += new System.EventHandler(this.PicBuscar_Click);
            // 
            // btnRetroceder
            // 
            this.btnRetroceder.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnRetroceder.FlatAppearance.BorderSize = 0;
            this.btnRetroceder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetroceder.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetroceder.Image = ((System.Drawing.Image)(resources.GetObject("btnRetroceder.Image")));
            this.btnRetroceder.Location = new System.Drawing.Point(0, 0);
            this.btnRetroceder.Name = "btnRetroceder";
            this.btnRetroceder.Size = new System.Drawing.Size(121, 67);
            this.btnRetroceder.TabIndex = 49;
            this.btnRetroceder.UseVisualStyleBackColor = true;
            this.btnRetroceder.Click += new System.EventHandler(this.btnRetroceder_Click);
            // 
            // btnApagar
            // 
            this.btnApagar.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnApagar.FlatAppearance.BorderSize = 0;
            this.btnApagar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnApagar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApagar.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApagar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnApagar.Image = ((System.Drawing.Image)(resources.GetObject("btnApagar.Image")));
            this.btnApagar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnApagar.Location = new System.Drawing.Point(944, 0);
            this.btnApagar.Name = "btnApagar";
            this.btnApagar.Size = new System.Drawing.Size(53, 67);
            this.btnApagar.TabIndex = 37;
            this.btnApagar.Text = "Salir";
            this.btnApagar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnApagar.UseVisualStyleBackColor = true;
            this.btnApagar.Click += new System.EventHandler(this.btnApagar_Click);
            // 
            // lblImagenOriginal2
            // 
            this.lblImagenOriginal2.AutoSize = true;
            this.lblImagenOriginal2.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenOriginal2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenOriginal2.Location = new System.Drawing.Point(356, 32);
            this.lblImagenOriginal2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenOriginal2.Name = "lblImagenOriginal2";
            this.lblImagenOriginal2.Size = new System.Drawing.Size(113, 15);
            this.lblImagenOriginal2.TabIndex = 73;
            this.lblImagenOriginal2.Text = "Nombre Original";
            this.lblImagenOriginal2.Visible = false;
            // 
            // lblImagenAGuardar2
            // 
            this.lblImagenAGuardar2.AutoSize = true;
            this.lblImagenAGuardar2.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenAGuardar2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenAGuardar2.Location = new System.Drawing.Point(356, 55);
            this.lblImagenAGuardar2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenAGuardar2.Name = "lblImagenAGuardar2";
            this.lblImagenAGuardar2.Size = new System.Drawing.Size(113, 15);
            this.lblImagenAGuardar2.TabIndex = 72;
            this.lblImagenAGuardar2.Text = "Nombre Original";
            this.lblImagenAGuardar2.Visible = false;
            // 
            // lblImagenAGuardar1
            // 
            this.lblImagenAGuardar1.AutoSize = true;
            this.lblImagenAGuardar1.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenAGuardar1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenAGuardar1.Location = new System.Drawing.Point(280, 9);
            this.lblImagenAGuardar1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenAGuardar1.Name = "lblImagenAGuardar1";
            this.lblImagenAGuardar1.Size = new System.Drawing.Size(113, 15);
            this.lblImagenAGuardar1.TabIndex = 71;
            this.lblImagenAGuardar1.Text = "Nombre Original";
            this.lblImagenAGuardar1.Visible = false;
            // 
            // lblImagenOriginal1
            // 
            this.lblImagenOriginal1.AutoSize = true;
            this.lblImagenOriginal1.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenOriginal1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenOriginal1.Location = new System.Drawing.Point(280, 32);
            this.lblImagenOriginal1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenOriginal1.Name = "lblImagenOriginal1";
            this.lblImagenOriginal1.Size = new System.Drawing.Size(113, 15);
            this.lblImagenOriginal1.TabIndex = 70;
            this.lblImagenOriginal1.Text = "Nombre Original";
            this.lblImagenOriginal1.Visible = false;
            // 
            // lblImagenAGuardar
            // 
            this.lblImagenAGuardar.AutoSize = true;
            this.lblImagenAGuardar.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenAGuardar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenAGuardar.Location = new System.Drawing.Point(397, 12);
            this.lblImagenAGuardar.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenAGuardar.Name = "lblImagenAGuardar";
            this.lblImagenAGuardar.Size = new System.Drawing.Size(113, 15);
            this.lblImagenAGuardar.TabIndex = 69;
            this.lblImagenAGuardar.Text = "Nombre Original";
            this.lblImagenAGuardar.Visible = false;
            // 
            // lblImagenOriginal
            // 
            this.lblImagenOriginal.AutoSize = true;
            this.lblImagenOriginal.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenOriginal.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenOriginal.Location = new System.Drawing.Point(397, 35);
            this.lblImagenOriginal.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenOriginal.Name = "lblImagenOriginal";
            this.lblImagenOriginal.Size = new System.Drawing.Size(113, 15);
            this.lblImagenOriginal.TabIndex = 68;
            this.lblImagenOriginal.Text = "Nombre Original";
            this.lblImagenOriginal.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblImagenAGuardarActa);
            this.panel1.Controls.Add(this.lblImagenAGuardarEstudios);
            this.panel1.Controls.Add(this.lblImagenOriginalActa);
            this.panel1.Controls.Add(this.lblImagenAGaurdarCert);
            this.panel1.Controls.Add(this.btnEliminarclientes);
            this.panel1.Controls.Add(this.lblImagenOriginalEstudios);
            this.panel1.Controls.Add(this.btnModificarCliente);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.btnGuardarCliente);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 581);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(997, 75);
            this.panel1.TabIndex = 43;
            // 
            // lblImagenAGuardarActa
            // 
            this.lblImagenAGuardarActa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblImagenAGuardarActa.AutoSize = true;
            this.lblImagenAGuardarActa.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenAGuardarActa.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenAGuardarActa.Location = new System.Drawing.Point(273, 53);
            this.lblImagenAGuardarActa.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenAGuardarActa.Name = "lblImagenAGuardarActa";
            this.lblImagenAGuardarActa.Size = new System.Drawing.Size(113, 15);
            this.lblImagenAGuardarActa.TabIndex = 75;
            this.lblImagenAGuardarActa.Text = "Nombre Original";
            this.lblImagenAGuardarActa.Visible = false;
            // 
            // lblImagenAGuardarEstudios
            // 
            this.lblImagenAGuardarEstudios.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblImagenAGuardarEstudios.AutoSize = true;
            this.lblImagenAGuardarEstudios.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenAGuardarEstudios.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenAGuardarEstudios.Location = new System.Drawing.Point(513, 54);
            this.lblImagenAGuardarEstudios.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenAGuardarEstudios.Name = "lblImagenAGuardarEstudios";
            this.lblImagenAGuardarEstudios.Size = new System.Drawing.Size(113, 15);
            this.lblImagenAGuardarEstudios.TabIndex = 74;
            this.lblImagenAGuardarEstudios.Text = "Nombre Original";
            this.lblImagenAGuardarEstudios.Visible = false;
            // 
            // lblImagenOriginalActa
            // 
            this.lblImagenOriginalActa.AutoSize = true;
            this.lblImagenOriginalActa.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenOriginalActa.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenOriginalActa.Location = new System.Drawing.Point(268, 37);
            this.lblImagenOriginalActa.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenOriginalActa.Name = "lblImagenOriginalActa";
            this.lblImagenOriginalActa.Size = new System.Drawing.Size(113, 15);
            this.lblImagenOriginalActa.TabIndex = 68;
            this.lblImagenOriginalActa.Text = "Nombre Original";
            this.lblImagenOriginalActa.Visible = false;
            // 
            // lblImagenAGaurdarCert
            // 
            this.lblImagenAGaurdarCert.AutoSize = true;
            this.lblImagenAGaurdarCert.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenAGaurdarCert.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenAGaurdarCert.Location = new System.Drawing.Point(688, 0);
            this.lblImagenAGaurdarCert.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenAGaurdarCert.Name = "lblImagenAGaurdarCert";
            this.lblImagenAGaurdarCert.Size = new System.Drawing.Size(113, 15);
            this.lblImagenAGaurdarCert.TabIndex = 73;
            this.lblImagenAGaurdarCert.Text = "Nombre Original";
            this.lblImagenAGaurdarCert.Visible = false;
            // 
            // btnEliminarclientes
            // 
            this.btnEliminarclientes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEliminarclientes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnEliminarclientes.FlatAppearance.BorderSize = 2;
            this.btnEliminarclientes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnEliminarclientes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnEliminarclientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarclientes.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarclientes.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnEliminarclientes.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminarclientes.Image")));
            this.btnEliminarclientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarclientes.Location = new System.Drawing.Point(631, 7);
            this.btnEliminarclientes.Name = "btnEliminarclientes";
            this.btnEliminarclientes.Size = new System.Drawing.Size(122, 57);
            this.btnEliminarclientes.TabIndex = 39;
            this.btnEliminarclientes.Text = "Eliminar    Usuarios";
            this.btnEliminarclientes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEliminarclientes.UseVisualStyleBackColor = false;
            this.btnEliminarclientes.Click += new System.EventHandler(this.btnEliminarclientes_Click);
            // 
            // lblImagenOriginalEstudios
            // 
            this.lblImagenOriginalEstudios.AutoSize = true;
            this.lblImagenOriginalEstudios.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenOriginalEstudios.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenOriginalEstudios.Location = new System.Drawing.Point(513, 44);
            this.lblImagenOriginalEstudios.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenOriginalEstudios.Name = "lblImagenOriginalEstudios";
            this.lblImagenOriginalEstudios.Size = new System.Drawing.Size(113, 15);
            this.lblImagenOriginalEstudios.TabIndex = 72;
            this.lblImagenOriginalEstudios.Text = "Nombre Original";
            this.lblImagenOriginalEstudios.Visible = false;
            // 
            // btnModificarCliente
            // 
            this.btnModificarCliente.FlatAppearance.BorderSize = 2;
            this.btnModificarCliente.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnModificarCliente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnModificarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModificarCliente.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificarCliente.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnModificarCliente.Image = ((System.Drawing.Image)(resources.GetObject("btnModificarCliente.Image")));
            this.btnModificarCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnModificarCliente.Location = new System.Drawing.Point(121, 8);
            this.btnModificarCliente.Name = "btnModificarCliente";
            this.btnModificarCliente.Size = new System.Drawing.Size(122, 57);
            this.btnModificarCliente.TabIndex = 42;
            this.btnModificarCliente.Text = "Modificar Usuario";
            this.btnModificarCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnModificarCliente.UseVisualStyleBackColor = true;
            this.btnModificarCliente.Click += new System.EventHandler(this.btnModificarCliente_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label9.Location = new System.Drawing.Point(6, 37);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 15);
            this.label9.TabIndex = 71;
            this.label9.Text = "Nombre Original";
            this.label9.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label10.Location = new System.Drawing.Point(3, 58);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(113, 15);
            this.label10.TabIndex = 70;
            this.label10.Text = "Nombre Original";
            this.label10.Visible = false;
            // 
            // btnGuardarCliente
            // 
            this.btnGuardarCliente.FlatAppearance.BorderSize = 2;
            this.btnGuardarCliente.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnGuardarCliente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnGuardarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardarCliente.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardarCliente.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnGuardarCliente.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardarCliente.Image")));
            this.btnGuardarCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardarCliente.Location = new System.Drawing.Point(386, 8);
            this.btnGuardarCliente.Name = "btnGuardarCliente";
            this.btnGuardarCliente.Size = new System.Drawing.Size(122, 57);
            this.btnGuardarCliente.TabIndex = 40;
            this.btnGuardarCliente.Text = "Guardar  Usuario";
            this.btnGuardarCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGuardarCliente.UseVisualStyleBackColor = true;
            this.btnGuardarCliente.Click += new System.EventHandler(this.btnGuardarCliente_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label11.Location = new System.Drawing.Point(100, 3);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(113, 15);
            this.label11.TabIndex = 69;
            this.label11.Text = "Nombre Original";
            this.label11.Visible = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnAñadirClientes);
            this.panel5.Controls.Add(this.lblNombreCliente);
            this.panel5.Controls.Add(this.picUserImage);
            this.panel5.Location = new System.Drawing.Point(2, 67);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(387, 111);
            this.panel5.TabIndex = 42;
            // 
            // btnAñadirClientes
            // 
            this.btnAñadirClientes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAñadirClientes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnAñadirClientes.FlatAppearance.BorderSize = 2;
            this.btnAñadirClientes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnAñadirClientes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnAñadirClientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAñadirClientes.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAñadirClientes.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAñadirClientes.Image = ((System.Drawing.Image)(resources.GetObject("btnAñadirClientes.Image")));
            this.btnAñadirClientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAñadirClientes.Location = new System.Drawing.Point(7, 50);
            this.btnAñadirClientes.Name = "btnAñadirClientes";
            this.btnAñadirClientes.Size = new System.Drawing.Size(122, 57);
            this.btnAñadirClientes.TabIndex = 38;
            this.btnAñadirClientes.Text = "Añadir        Usuario";
            this.btnAñadirClientes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAñadirClientes.UseVisualStyleBackColor = false;
            this.btnAñadirClientes.Click += new System.EventHandler(this.btnAñadirClientes_Click);
            // 
            // lblNombreCliente
            // 
            this.lblNombreCliente.AutoSize = true;
            this.lblNombreCliente.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreCliente.ForeColor = System.Drawing.Color.GhostWhite;
            this.lblNombreCliente.Location = new System.Drawing.Point(142, 94);
            this.lblNombreCliente.Name = "lblNombreCliente";
            this.lblNombreCliente.Size = new System.Drawing.Size(127, 14);
            this.lblNombreCliente.TabIndex = 23;
            this.lblNombreCliente.Text = "Nonmbre  Del Cliente";
            // 
            // picUserImage
            // 
            this.picUserImage.Image = ((System.Drawing.Image)(resources.GetObject("picUserImage.Image")));
            this.picUserImage.Location = new System.Drawing.Point(162, 0);
            this.picUserImage.Name = "picUserImage";
            this.picUserImage.Size = new System.Drawing.Size(90, 91);
            this.picUserImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picUserImage.TabIndex = 22;
            this.picUserImage.TabStop = false;
            // 
            // panelFormulario
            // 
            this.panelFormulario.Controls.Add(this.lblConfir);
            this.panelFormulario.Controls.Add(this.txtConfirmarContra);
            this.panelFormulario.Controls.Add(this.panel4);
            this.panelFormulario.Controls.Add(this.btnComproCurp);
            this.panelFormulario.Controls.Add(this.btnComproINE);
            this.panelFormulario.Controls.Add(this.btnComproDom);
            this.panelFormulario.Controls.Add(this.btnCerEst);
            this.panelFormulario.Controls.Add(this.btnImgActaNac);
            this.panelFormulario.Controls.Add(this.cbTipoUser);
            this.panelFormulario.Controls.Add(this.label7);
            this.panelFormulario.Controls.Add(this.txtUsuarioContra);
            this.panelFormulario.Controls.Add(this.label4);
            this.panelFormulario.Controls.Add(this.label5);
            this.panelFormulario.Controls.Add(this.txtNumSS);
            this.panelFormulario.Controls.Add(this.label3);
            this.panelFormulario.Controls.Add(this.txtCertEstudios);
            this.panelFormulario.Controls.Add(this.label2);
            this.panelFormulario.Controls.Add(this.txtActaDeNac);
            this.panelFormulario.Controls.Add(this.label25);
            this.panelFormulario.Controls.Add(this.txtComprobanteCurp);
            this.panelFormulario.Controls.Add(this.label1);
            this.panelFormulario.Controls.Add(this.txtComproINE);
            this.panelFormulario.Controls.Add(this.label26);
            this.panelFormulario.Controls.Add(this.txtLocalidad);
            this.panelFormulario.Controls.Add(this.label27);
            this.panelFormulario.Controls.Add(this.txtCurp);
            this.panelFormulario.Controls.Add(this.txtComprobanteDom);
            this.panelFormulario.Controls.Add(this.txtComprobanteDomicilio);
            this.panelFormulario.Controls.Add(this.lblCalle);
            this.panelFormulario.Controls.Add(this.txtCalle);
            this.panelFormulario.Controls.Add(this.dtpFechaNacimiento);
            this.panelFormulario.Controls.Add(this.label28);
            this.panelFormulario.Controls.Add(this.label29);
            this.panelFormulario.Controls.Add(this.label30);
            this.panelFormulario.Controls.Add(this.label31);
            this.panelFormulario.Controls.Add(this.label32);
            this.panelFormulario.Controls.Add(this.label33);
            this.panelFormulario.Controls.Add(this.txtCP);
            this.panelFormulario.Controls.Add(this.txtNumCasa);
            this.panelFormulario.Controls.Add(this.txtTelefono);
            this.panelFormulario.Controls.Add(this.txtFraccionamiento);
            this.panelFormulario.Controls.Add(this.txtMunicipio);
            this.panelFormulario.Controls.Add(this.label34);
            this.panelFormulario.Controls.Add(this.label35);
            this.panelFormulario.Controls.Add(this.label36);
            this.panelFormulario.Controls.Add(this.label37);
            this.panelFormulario.Controls.Add(this.label38);
            this.panelFormulario.Controls.Add(this.lblNombre);
            this.panelFormulario.Controls.Add(this.txtColonia);
            this.panelFormulario.Controls.Add(this.txtCorreo);
            this.panelFormulario.Controls.Add(this.txtCelular);
            this.panelFormulario.Controls.Add(this.txtApMat);
            this.panelFormulario.Controls.Add(this.txtApPat);
            this.panelFormulario.Controls.Add(this.txtNombre);
            this.panelFormulario.Location = new System.Drawing.Point(3, 181);
            this.panelFormulario.Name = "panelFormulario";
            this.panelFormulario.Size = new System.Drawing.Size(386, 402);
            this.panelFormulario.TabIndex = 40;
            this.panelFormulario.Paint += new System.Windows.Forms.PaintEventHandler(this.panelFormulario_Paint);
            // 
            // lblConfir
            // 
            this.lblConfir.AutoSize = true;
            this.lblConfir.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConfir.ForeColor = System.Drawing.Color.GhostWhite;
            this.lblConfir.Location = new System.Drawing.Point(6, 378);
            this.lblConfir.Name = "lblConfir";
            this.lblConfir.Size = new System.Drawing.Size(135, 14);
            this.lblConfir.TabIndex = 111;
            this.lblConfir.Text = "Confirmar Contraseña";
            // 
            // txtConfirmarContra
            // 
            this.txtConfirmarContra.Location = new System.Drawing.Point(147, 374);
            this.txtConfirmarContra.Name = "txtConfirmarContra";
            this.txtConfirmarContra.Size = new System.Drawing.Size(174, 20);
            this.txtConfirmarContra.TabIndex = 110;
            this.txtConfirmarContra.TextChanged += new System.EventHandler(this.txtConfirmarContra_TextChanged);
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.pictureBox1);
            this.panel4.Controls.Add(this.picCURP);
            this.panel4.Controls.Add(this.picCertEst);
            this.panel4.Controls.Add(this.picINE);
            this.panel4.Controls.Add(this.picComproDom);
            this.panel4.Controls.Add(this.picActaNacc);
            this.panel4.Location = new System.Drawing.Point(234, 10);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(150, 157);
            this.panel4.TabIndex = 109;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.GrayText;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(143, 150);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 109;
            this.pictureBox1.TabStop = false;
            // 
            // picCURP
            // 
            this.picCURP.BackColor = System.Drawing.SystemColors.GrayText;
            this.picCURP.Image = ((System.Drawing.Image)(resources.GetObject("picCURP.Image")));
            this.picCURP.Location = new System.Drawing.Point(1, 1);
            this.picCURP.Name = "picCURP";
            this.picCURP.Size = new System.Drawing.Size(143, 150);
            this.picCURP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picCURP.TabIndex = 108;
            this.picCURP.TabStop = false;
            this.picCURP.Visible = false;
            // 
            // picCertEst
            // 
            this.picCertEst.BackColor = System.Drawing.SystemColors.GrayText;
            this.picCertEst.Image = ((System.Drawing.Image)(resources.GetObject("picCertEst.Image")));
            this.picCertEst.Location = new System.Drawing.Point(1, 2);
            this.picCertEst.Name = "picCertEst";
            this.picCertEst.Size = new System.Drawing.Size(143, 150);
            this.picCertEst.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picCertEst.TabIndex = 106;
            this.picCertEst.TabStop = false;
            this.picCertEst.Visible = false;
            // 
            // picINE
            // 
            this.picINE.BackColor = System.Drawing.SystemColors.GrayText;
            this.picINE.Image = ((System.Drawing.Image)(resources.GetObject("picINE.Image")));
            this.picINE.Location = new System.Drawing.Point(1, 2);
            this.picINE.Name = "picINE";
            this.picINE.Size = new System.Drawing.Size(143, 150);
            this.picINE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picINE.TabIndex = 107;
            this.picINE.TabStop = false;
            this.picINE.Visible = false;
            // 
            // picComproDom
            // 
            this.picComproDom.BackColor = System.Drawing.SystemColors.GrayText;
            this.picComproDom.Image = ((System.Drawing.Image)(resources.GetObject("picComproDom.Image")));
            this.picComproDom.Location = new System.Drawing.Point(0, 1);
            this.picComproDom.Name = "picComproDom";
            this.picComproDom.Size = new System.Drawing.Size(143, 150);
            this.picComproDom.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picComproDom.TabIndex = 66;
            this.picComproDom.TabStop = false;
            this.picComproDom.Visible = false;
            // 
            // picActaNacc
            // 
            this.picActaNacc.BackColor = System.Drawing.SystemColors.GrayText;
            this.picActaNacc.Image = ((System.Drawing.Image)(resources.GetObject("picActaNacc.Image")));
            this.picActaNacc.Location = new System.Drawing.Point(1, 1);
            this.picActaNacc.Name = "picActaNacc";
            this.picActaNacc.Size = new System.Drawing.Size(143, 150);
            this.picActaNacc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picActaNacc.TabIndex = 105;
            this.picActaNacc.TabStop = false;
            this.picActaNacc.Visible = false;
            // 
            // btnComproCurp
            // 
            this.btnComproCurp.FlatAppearance.BorderSize = 0;
            this.btnComproCurp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnComproCurp.Image = ((System.Drawing.Image)(resources.GetObject("btnComproCurp.Image")));
            this.btnComproCurp.Location = new System.Drawing.Point(352, 345);
            this.btnComproCurp.Name = "btnComproCurp";
            this.btnComproCurp.Size = new System.Drawing.Size(33, 26);
            this.btnComproCurp.TabIndex = 71;
            this.btnComproCurp.UseVisualStyleBackColor = true;
            this.btnComproCurp.Click += new System.EventHandler(this.btnComproCurp_Click);
            this.btnComproCurp.MouseEnter += new System.EventHandler(this.btnComproCurp_MouseEnter);
            this.btnComproCurp.MouseLeave += new System.EventHandler(this.btnComproCurp_MouseLeave);
            this.btnComproCurp.MouseHover += new System.EventHandler(this.btnComproCurp_MouseHover);
            // 
            // btnComproINE
            // 
            this.btnComproINE.FlatAppearance.BorderSize = 0;
            this.btnComproINE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnComproINE.Image = ((System.Drawing.Image)(resources.GetObject("btnComproINE.Image")));
            this.btnComproINE.Location = new System.Drawing.Point(353, 304);
            this.btnComproINE.Name = "btnComproINE";
            this.btnComproINE.Size = new System.Drawing.Size(33, 26);
            this.btnComproINE.TabIndex = 70;
            this.btnComproINE.UseVisualStyleBackColor = true;
            this.btnComproINE.Click += new System.EventHandler(this.btnComproINE_Click);
            this.btnComproINE.MouseEnter += new System.EventHandler(this.btnComproINE_MouseEnter);
            this.btnComproINE.MouseLeave += new System.EventHandler(this.btnComproINE_MouseLeave);
            this.btnComproINE.MouseHover += new System.EventHandler(this.btnComproINE_MouseHover);
            // 
            // btnComproDom
            // 
            this.btnComproDom.FlatAppearance.BorderSize = 0;
            this.btnComproDom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnComproDom.Image = ((System.Drawing.Image)(resources.GetObject("btnComproDom.Image")));
            this.btnComproDom.Location = new System.Drawing.Point(352, 259);
            this.btnComproDom.Name = "btnComproDom";
            this.btnComproDom.Size = new System.Drawing.Size(33, 26);
            this.btnComproDom.TabIndex = 69;
            this.btnComproDom.UseVisualStyleBackColor = true;
            this.btnComproDom.Click += new System.EventHandler(this.btnComproDom_Click);
            this.btnComproDom.MouseEnter += new System.EventHandler(this.btnComproDom_MouseEnter);
            this.btnComproDom.MouseLeave += new System.EventHandler(this.btnComproDom_MouseLeave);
            this.btnComproDom.MouseHover += new System.EventHandler(this.btnComproDom_MouseHover);
            // 
            // btnCerEst
            // 
            this.btnCerEst.FlatAppearance.BorderSize = 0;
            this.btnCerEst.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerEst.Image = ((System.Drawing.Image)(resources.GetObject("btnCerEst.Image")));
            this.btnCerEst.Location = new System.Drawing.Point(352, 219);
            this.btnCerEst.Name = "btnCerEst";
            this.btnCerEst.Size = new System.Drawing.Size(33, 26);
            this.btnCerEst.TabIndex = 68;
            this.btnCerEst.UseVisualStyleBackColor = true;
            this.btnCerEst.Click += new System.EventHandler(this.btnCerEst_Click);
            this.btnCerEst.MouseEnter += new System.EventHandler(this.btnCerEst_MouseEnter);
            this.btnCerEst.MouseLeave += new System.EventHandler(this.btnCerEst_MouseLeave);
            this.btnCerEst.MouseHover += new System.EventHandler(this.btnCerEst_MouseHover);
            // 
            // btnImgActaNac
            // 
            this.btnImgActaNac.FlatAppearance.BorderSize = 0;
            this.btnImgActaNac.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImgActaNac.Image = ((System.Drawing.Image)(resources.GetObject("btnImgActaNac.Image")));
            this.btnImgActaNac.Location = new System.Drawing.Point(352, 179);
            this.btnImgActaNac.Name = "btnImgActaNac";
            this.btnImgActaNac.Size = new System.Drawing.Size(33, 26);
            this.btnImgActaNac.TabIndex = 67;
            this.btnImgActaNac.UseVisualStyleBackColor = true;
            this.btnImgActaNac.Click += new System.EventHandler(this.btnImgActaNac_Click);
            this.btnImgActaNac.MouseEnter += new System.EventHandler(this.btnImgActaNac_MouseEnter);
            this.btnImgActaNac.MouseLeave += new System.EventHandler(this.btnImgActaNac_MouseLeave);
            this.btnImgActaNac.MouseHover += new System.EventHandler(this.btnImgActaNac_MouseHover);
            // 
            // cbTipoUser
            // 
            this.cbTipoUser.FormattingEnabled = true;
            this.cbTipoUser.Items.AddRange(new object[] {
            "ADMINISTRADOR",
            "CAJERO",
            "ERROR"});
            this.cbTipoUser.Location = new System.Drawing.Point(121, 310);
            this.cbTipoUser.Name = "cbTipoUser";
            this.cbTipoUser.Size = new System.Drawing.Size(110, 21);
            this.cbTipoUser.TabIndex = 65;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.GhostWhite;
            this.label7.Location = new System.Drawing.Point(120, 330);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 14);
            this.label7.TabIndex = 64;
            this.label7.Text = "Contraseña";
            // 
            // txtUsuarioContra
            // 
            this.txtUsuarioContra.Location = new System.Drawing.Point(123, 347);
            this.txtUsuarioContra.Name = "txtUsuarioContra";
            this.txtUsuarioContra.Size = new System.Drawing.Size(108, 20);
            this.txtUsuarioContra.TabIndex = 63;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.GhostWhite;
            this.label4.Location = new System.Drawing.Point(120, 290);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 14);
            this.label4.TabIndex = 62;
            this.label4.Text = "Tipo de Usuario";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.GhostWhite;
            this.label5.Location = new System.Drawing.Point(120, 250);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 14);
            this.label5.TabIndex = 60;
            this.label5.Text = "Num Seguro  Social";
            // 
            // txtNumSS
            // 
            this.txtNumSS.Location = new System.Drawing.Point(123, 267);
            this.txtNumSS.Name = "txtNumSS";
            this.txtNumSS.Size = new System.Drawing.Size(108, 20);
            this.txtNumSS.TabIndex = 59;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.GhostWhite;
            this.label3.Location = new System.Drawing.Point(237, 208);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 14);
            this.label3.TabIndex = 58;
            this.label3.Text = "Certificado De Estudios";
            // 
            // txtCertEstudios
            // 
            this.txtCertEstudios.Location = new System.Drawing.Point(247, 225);
            this.txtCertEstudios.Name = "txtCertEstudios";
            this.txtCertEstudios.Size = new System.Drawing.Size(108, 20);
            this.txtCertEstudios.TabIndex = 103;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.GhostWhite;
            this.label2.Location = new System.Drawing.Point(244, 168);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 14);
            this.label2.TabIndex = 56;
            this.label2.Text = "Acta de  Nacimiento";
            // 
            // txtActaDeNac
            // 
            this.txtActaDeNac.Location = new System.Drawing.Point(247, 185);
            this.txtActaDeNac.Name = "txtActaDeNac";
            this.txtActaDeNac.Size = new System.Drawing.Size(108, 20);
            this.txtActaDeNac.TabIndex = 104;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.GhostWhite;
            this.label25.Location = new System.Drawing.Point(243, 332);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(120, 14);
            this.label25.TabIndex = 54;
            this.label25.Text = "Comprobante CURP";
            // 
            // txtComprobanteCurp
            // 
            this.txtComprobanteCurp.Location = new System.Drawing.Point(246, 349);
            this.txtComprobanteCurp.Name = "txtComprobanteCurp";
            this.txtComprobanteCurp.Size = new System.Drawing.Size(108, 20);
            this.txtComprobanteCurp.TabIndex = 100;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.GhostWhite;
            this.label1.Location = new System.Drawing.Point(244, 291);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 14);
            this.label1.TabIndex = 52;
            this.label1.Text = "Comprobante INE";
            // 
            // txtComproINE
            // 
            this.txtComproINE.Location = new System.Drawing.Point(245, 310);
            this.txtComproINE.Name = "txtComproINE";
            this.txtComproINE.Size = new System.Drawing.Size(108, 20);
            this.txtComproINE.TabIndex = 101;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.GhostWhite;
            this.label26.Location = new System.Drawing.Point(27, 293);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(60, 14);
            this.label26.TabIndex = 50;
            this.label26.Text = "Localidad";
            // 
            // txtLocalidad
            // 
            this.txtLocalidad.Location = new System.Drawing.Point(1, 311);
            this.txtLocalidad.Name = "txtLocalidad";
            this.txtLocalidad.Size = new System.Drawing.Size(108, 20);
            this.txtLocalidad.TabIndex = 7;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.GhostWhite;
            this.label27.Location = new System.Drawing.Point(36, 334);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(39, 14);
            this.label27.TabIndex = 48;
            this.label27.Text = "CURP";
            // 
            // txtCurp
            // 
            this.txtCurp.Location = new System.Drawing.Point(2, 348);
            this.txtCurp.Name = "txtCurp";
            this.txtCurp.Size = new System.Drawing.Size(108, 20);
            this.txtCurp.TabIndex = 8;
            // 
            // txtComprobanteDom
            // 
            this.txtComprobanteDom.AutoSize = true;
            this.txtComprobanteDom.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComprobanteDom.ForeColor = System.Drawing.Color.GhostWhite;
            this.txtComprobanteDom.Location = new System.Drawing.Point(241, 249);
            this.txtComprobanteDom.Name = "txtComprobanteDom";
            this.txtComprobanteDom.Size = new System.Drawing.Size(113, 14);
            this.txtComprobanteDom.TabIndex = 46;
            this.txtComprobanteDom.Text = "Comprobante dom";
            // 
            // txtComprobanteDomicilio
            // 
            this.txtComprobanteDomicilio.Location = new System.Drawing.Point(245, 266);
            this.txtComprobanteDomicilio.Name = "txtComprobanteDomicilio";
            this.txtComprobanteDomicilio.Size = new System.Drawing.Size(108, 20);
            this.txtComprobanteDomicilio.TabIndex = 102;
            // 
            // lblCalle
            // 
            this.lblCalle.AutoSize = true;
            this.lblCalle.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalle.ForeColor = System.Drawing.Color.GhostWhite;
            this.lblCalle.Location = new System.Drawing.Point(38, 246);
            this.lblCalle.Name = "lblCalle";
            this.lblCalle.Size = new System.Drawing.Size(36, 14);
            this.lblCalle.TabIndex = 44;
            this.lblCalle.Text = "Calle";
            // 
            // txtCalle
            // 
            this.txtCalle.Location = new System.Drawing.Point(2, 264);
            this.txtCalle.Name = "txtCalle";
            this.txtCalle.Size = new System.Drawing.Size(108, 20);
            this.txtCalle.TabIndex = 6;
            // 
            // dtpFechaNacimiento
            // 
            this.dtpFechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaNacimiento.Location = new System.Drawing.Point(121, 29);
            this.dtpFechaNacimiento.Name = "dtpFechaNacimiento";
            this.dtpFechaNacimiento.Size = new System.Drawing.Size(108, 20);
            this.dtpFechaNacimiento.TabIndex = 9;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.GhostWhite;
            this.label28.Location = new System.Drawing.Point(127, 209);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(84, 14);
            this.label28.TabIndex = 41;
            this.label28.Text = "Codigo Postal";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.GhostWhite;
            this.label29.Location = new System.Drawing.Point(120, 168);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(101, 14);
            this.label29.TabIndex = 40;
            this.label29.Text = "Numero de Casa";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.GhostWhite;
            this.label30.Location = new System.Drawing.Point(144, 127);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(58, 14);
            this.label30.TabIndex = 39;
            this.label30.Text = "Telefono";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.GhostWhite;
            this.label31.Location = new System.Drawing.Point(125, 88);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(101, 14);
            this.label31.TabIndex = 38;
            this.label31.Text = "Fraccionamiento";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.GhostWhite;
            this.label32.Location = new System.Drawing.Point(126, 50);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(60, 14);
            this.label32.TabIndex = 37;
            this.label32.Text = "Municipio";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.GhostWhite;
            this.label33.Location = new System.Drawing.Point(112, 14);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(126, 14);
            this.label33.TabIndex = 36;
            this.label33.Text = "Fecha De Nacimiento";
            // 
            // txtCP
            // 
            this.txtCP.Location = new System.Drawing.Point(122, 226);
            this.txtCP.Name = "txtCP";
            this.txtCP.Size = new System.Drawing.Size(108, 20);
            this.txtCP.TabIndex = 14;
            // 
            // txtNumCasa
            // 
            this.txtNumCasa.Location = new System.Drawing.Point(122, 186);
            this.txtNumCasa.Name = "txtNumCasa";
            this.txtNumCasa.Size = new System.Drawing.Size(108, 20);
            this.txtNumCasa.TabIndex = 13;
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(122, 145);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(108, 20);
            this.txtTelefono.TabIndex = 12;
            // 
            // txtFraccionamiento
            // 
            this.txtFraccionamiento.Location = new System.Drawing.Point(121, 105);
            this.txtFraccionamiento.Name = "txtFraccionamiento";
            this.txtFraccionamiento.Size = new System.Drawing.Size(108, 20);
            this.txtFraccionamiento.TabIndex = 11;
            // 
            // txtMunicipio
            // 
            this.txtMunicipio.Location = new System.Drawing.Point(121, 65);
            this.txtMunicipio.Name = "txtMunicipio";
            this.txtMunicipio.Size = new System.Drawing.Size(108, 20);
            this.txtMunicipio.TabIndex = 10;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.GhostWhite;
            this.label34.Location = new System.Drawing.Point(29, 207);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(50, 14);
            this.label34.TabIndex = 29;
            this.label34.Text = "Colonia";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.GhostWhite;
            this.label35.Location = new System.Drawing.Point(1, 167);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(112, 14);
            this.label35.TabIndex = 28;
            this.label35.Text = "Correo Electronico";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.GhostWhite;
            this.label36.Location = new System.Drawing.Point(25, 126);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(51, 14);
            this.label36.TabIndex = 27;
            this.label36.Text = "Celular ";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.GhostWhite;
            this.label37.Location = new System.Drawing.Point(7, 88);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(102, 14);
            this.label37.TabIndex = 26;
            this.label37.Text = "Apellido Materno";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.GhostWhite;
            this.label38.Location = new System.Drawing.Point(8, 50);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(99, 14);
            this.label38.TabIndex = 25;
            this.label38.Text = "Apellido Paterno";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.ForeColor = System.Drawing.Color.GhostWhite;
            this.lblNombre.Location = new System.Drawing.Point(25, 14);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(55, 14);
            this.lblNombre.TabIndex = 24;
            this.lblNombre.Text = "Nombre ";
            // 
            // txtColonia
            // 
            this.txtColonia.Location = new System.Drawing.Point(3, 225);
            this.txtColonia.Name = "txtColonia";
            this.txtColonia.Size = new System.Drawing.Size(108, 20);
            this.txtColonia.TabIndex = 5;
            // 
            // txtCorreo
            // 
            this.txtCorreo.Location = new System.Drawing.Point(3, 185);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Size = new System.Drawing.Size(108, 20);
            this.txtCorreo.TabIndex = 4;
            // 
            // txtCelular
            // 
            this.txtCelular.Location = new System.Drawing.Point(3, 144);
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(108, 20);
            this.txtCelular.TabIndex = 3;
            // 
            // txtApMat
            // 
            this.txtApMat.Location = new System.Drawing.Point(3, 105);
            this.txtApMat.Name = "txtApMat";
            this.txtApMat.Size = new System.Drawing.Size(108, 20);
            this.txtApMat.TabIndex = 2;
            // 
            // txtApPat
            // 
            this.txtApPat.Location = new System.Drawing.Point(3, 65);
            this.txtApPat.Name = "txtApPat";
            this.txtApPat.Size = new System.Drawing.Size(108, 20);
            this.txtApPat.TabIndex = 1;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(3, 29);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(108, 20);
            this.txtNombre.TabIndex = 0;
            // 
            // openFileDialogComprobantes
            // 
            this.openFileDialogComprobantes.FileName = "openFileDialog1";
            // 
            // FrmUsuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(997, 656);
            this.Controls.Add(this.panelForm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmUsuarios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmUsuarios";
            this.Load += new System.EventHandler(this.FrmUsuarios_Load);
            this.panelForm.ResumeLayout(false);
            this.panelForm.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgUsuarios)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBuscar)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUserImage)).EndInit();
            this.panelFormulario.ResumeLayout(false);
            this.panelFormulario.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCURP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCertEst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picINE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picComproDom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picActaNacc)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelForm;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnEliminarclientes;
        private System.Windows.Forms.Button btnModificarCliente;
        private System.Windows.Forms.Button btnGuardarCliente;
        private System.Windows.Forms.Button btnApagar;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnAñadirClientes;
        private System.Windows.Forms.Label lblNombreCliente;
        private System.Windows.Forms.PictureBox picUserImage;
        private System.Windows.Forms.Panel panelFormulario;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtComprobanteCurp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtComproINE;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtLocalidad;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtCurp;
        private System.Windows.Forms.Label txtComprobanteDom;
        private System.Windows.Forms.TextBox txtComprobanteDomicilio;
        private System.Windows.Forms.Label lblCalle;
        private System.Windows.Forms.TextBox txtCalle;
        private System.Windows.Forms.DateTimePicker dtpFechaNacimiento;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtCP;
        private System.Windows.Forms.TextBox txtNumCasa;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.TextBox txtFraccionamiento;
        private System.Windows.Forms.TextBox txtMunicipio;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox txtColonia;
        private System.Windows.Forms.TextBox txtCorreo;
        private System.Windows.Forms.TextBox txtCelular;
        private System.Windows.Forms.TextBox txtApMat;
        private System.Windows.Forms.TextBox txtApPat;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtUsuarioContra;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNumSS;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCertEstudios;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtActaDeNac;
        private System.Windows.Forms.ComboBox cbTipoUser;
        private System.Windows.Forms.Button btnRetroceder;
        private System.Windows.Forms.Label lblImagenOriginal2;
        private System.Windows.Forms.Label lblImagenAGuardar2;
        private System.Windows.Forms.Label lblImagenAGuardar1;
        private System.Windows.Forms.Label lblImagenOriginal1;
        private System.Windows.Forms.Label lblImagenAGuardar;
        private System.Windows.Forms.Label lblImagenOriginal;
        private System.Windows.Forms.Label lblImagenAGaurdarCert;
        private System.Windows.Forms.Label lblImagenOriginalEstudios;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblImagenOriginalActa;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dgUsuarios;
        private System.Windows.Forms.TextBox txtBusqueda;
        private System.Windows.Forms.PictureBox PicBuscar;
        private System.Windows.Forms.PictureBox picComproDom;
        private System.Windows.Forms.Button btnComproCurp;
        private System.Windows.Forms.Button btnComproINE;
        private System.Windows.Forms.Button btnComproDom;
        private System.Windows.Forms.Button btnCerEst;
        private System.Windows.Forms.Button btnImgActaNac;
        private System.Windows.Forms.PictureBox picCURP;
        private System.Windows.Forms.PictureBox picINE;
        private System.Windows.Forms.PictureBox picCertEst;
        private System.Windows.Forms.PictureBox picActaNacc;
        private System.Windows.Forms.Label lblImagenAGuardarActa;
        private System.Windows.Forms.Label lblImagenAGuardarEstudios;
        private System.Windows.Forms.OpenFileDialog openFileDialogComprobantes;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblConfir;
        private System.Windows.Forms.TextBox txtConfirmarContra;
    }
}