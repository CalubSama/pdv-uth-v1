﻿using Lib_pdv_uth_v1.usuarios;
using System;
using System.ComponentModel;
using System.Linq;
using System.Net.NetworkInformation;
using System.Windows.Forms;

namespace pdv_uth_v1
{
    public partial class FrmLogin : Form
    {
        //instancia de usuario
        public static Usuario us = new Usuario();
        public static Usuario usuario = new Usuario();
        //vars
        public static string personName;
        public static string mostrarPass;

        public FrmLogin()
        {
            InitializeComponent();
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            //cerrar la forma
            if (MessageBox.Show("¿Esta seguro que desea salir?", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }
        public void logearse()
        {  
            FrmLogin.usuario = FrmLogin.usuario == null ? new Usuario() : FrmLogin.usuario;
            FrmLogin.usuario = FrmLogin.usuario.loginUsuario(txtCorreo.Text, txtContraseña.Text);
            personName = FrmLogin.usuario.Nombre.ToString()+" " + FrmLogin.usuario.ApellidoMaterno.ToString()+" " + FrmLogin.usuario.ApellidoPaterno.ToString();
            //login
            if (FrmLogin.usuario.TipoUsuario == TipoUsuario.ADMINISTRADOR)
            {
                FrmMenuPpal frmMenuPpal = new FrmMenuPpal();
                //escondemos el login
                this.Hide();
                //mostramos la forma de DIALOG para que este sobre todas enfrente
                frmMenuPpal.ShowDialog();
               
            }
            else if (FrmLogin.usuario.TipoUsuario == TipoUsuario.CAJERO)
            {
                //abrimos el Caja
                FrmCaja frmcaja = new FrmCaja();
                //escondemos el l0gin
                this.Hide();
                //mostramos la forma de DIALOG para que este sobre todas enfrente
                frmcaja.ShowDialog();
                
            }
            else
            {
                //ERROR
                notificacion("Usuario no existe, revisa tus entradas",FrmNotificaciones.enmType.Warning);
            }
        }
        private void notificacion(string msg, FrmNotificaciones.enmType type)
        {
            FrmNotificaciones frm = new FrmNotificaciones();
            frm.notificacion(msg, type);
            frm.Show();
            /*            frm.ShowDialog();*/

        }

        private void btnIngresar_Click_1(object sender, EventArgs e)
        {
            logearse();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Esta seguro que desea salir?", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }
        private void txtContraseña_TextChanged(object sender, EventArgs e)
        {
            mostrarPass = txtContraseña.Text;
            // The password character is an asterisk.
            txtContraseña.UseSystemPasswordChar = true;

            // The control will allow no more than 14 characters.
            txtContraseña.MaxLength = 30;
        }

        private void txtContraseña_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                logearse();
            }
        }

        private void txtCorreo_MouseClick(object sender, MouseEventArgs e)
        {
            txtCorreo.Text = "";
        }

        private void txtContraseña_Enter(object sender, EventArgs e)
        {
            /*txtContraseña.Text = "";*/
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            var macAddr =(from nic in NetworkInterface.GetAllNetworkInterfaces()
        where nic.OperationalStatus == OperationalStatus.Up
        select nic.GetPhysicalAddress().ToString()).FirstOrDefault();
            lblMacAddress.Text = macAddr; 
        }

        private void pictureBox2_MouseEnter(object sender, EventArgs e)
        {
            pnlInfoLogin.Visible = true;
        }

        private void picMostrarInfo_MouseHover(object sender, EventArgs e)
        {
            pnlInfoLogin.Visible = true;
        }

        private void picMostrarInfo_Click(object sender, EventArgs e)
        {
            pnlInfoLogin.Visible = true;
        }

        private void picMostrarInfo_MouseLeave(object sender, EventArgs e)
        {
            pnlInfoLogin.Visible = false;
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {

            txtContraseña.UseSystemPasswordChar = true;
            txtContraseña.Text = mostrarPass;
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {

            txtContraseña.UseSystemPasswordChar = false;
            txtContraseña.Text = mostrarPass;

        }

        private void pictureBox2_MouseEnter_1(object sender, EventArgs e)
        {

            txtContraseña.UseSystemPasswordChar = false;
            txtContraseña.Text = mostrarPass;

        }
    }
}
