﻿namespace pdv_uth_v1
{
    partial class FrmCatalogoProductos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCatalogoProductos));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelFormulario = new System.Windows.Forms.Panel();
            this.PnlContainerPrin = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnModificarProducto = new System.Windows.Forms.Button();
            this.btnEliminarclientes = new System.Windows.Forms.Button();
            this.btnGuardarCliente = new System.Windows.Forms.Button();
            this.panelFrm = new System.Windows.Forms.Panel();
            this.txtEAN13 = new System.Windows.Forms.TextBox();
            this.btnAgregarProducto = new System.Windows.Forms.Button();
            this.codBarraTry = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBoxEsPereceredo = new System.Windows.Forms.CheckBox();
            this.comboUnidadDeMedida = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPrecio = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCodBarras = new System.Windows.Forms.TextBox();
            this.lblCodBarras = new System.Windows.Forms.Label();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.lblNombre = new System.Windows.Forms.Label();
            this.panelImgProducto = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.icoBtnCargarImagen = new FontAwesome.Sharp.IconButton();
            this.label3 = new System.Windows.Forms.Label();
            this.lblImagenAGuardar = new System.Windows.Forms.Label();
            this.lblImagenOriginal = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.picBoxProducto = new System.Windows.Forms.PictureBox();
            this.pnlBarraTareas = new System.Windows.Forms.Panel();
            this.pbBuscar = new System.Windows.Forms.PictureBox();
            this.txtConsultaProducto = new System.Windows.Forms.TextBox();
            this.btnApagar = new System.Windows.Forms.Button();
            this.btnExpandirMn = new System.Windows.Forms.Button();
            this.PanelData = new System.Windows.Forms.Panel();
            this.dgProductos = new System.Windows.Forms.DataGridView();
            this.pnlNavbar = new System.Windows.Forms.Panel();
            this.panelTimer = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblReloj = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.btnCompras = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.btnCajas = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.btnCreditos = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.PnlInfo = new System.Windows.Forms.Panel();
            this.lblUserName = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblNombreUser = new System.Windows.Forms.Label();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.lblCajaTitle = new System.Windows.Forms.Label();
            this.ipbLogo = new System.Windows.Forms.PictureBox();
            this.openFileDialogCargarImagen = new System.Windows.Forms.OpenFileDialog();
            this.panelFormulario.SuspendLayout();
            this.PnlContainerPrin.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelFrm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.codBarraTry)).BeginInit();
            this.panelImgProducto.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxProducto)).BeginInit();
            this.pnlBarraTareas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBuscar)).BeginInit();
            this.PanelData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProductos)).BeginInit();
            this.pnlNavbar.SuspendLayout();
            this.panelTimer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel4.SuspendLayout();
            this.PnlInfo.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ipbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // panelFormulario
            // 
            this.panelFormulario.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panelFormulario.Controls.Add(this.PnlContainerPrin);
            this.panelFormulario.Controls.Add(this.pnlBarraTareas);
            this.panelFormulario.Controls.Add(this.PanelData);
            this.panelFormulario.Controls.Add(this.pnlNavbar);
            this.panelFormulario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFormulario.Location = new System.Drawing.Point(0, 0);
            this.panelFormulario.Margin = new System.Windows.Forms.Padding(2);
            this.panelFormulario.Name = "panelFormulario";
            this.panelFormulario.Size = new System.Drawing.Size(1208, 724);
            this.panelFormulario.TabIndex = 2;
            // 
            // PnlContainerPrin
            // 
            this.PnlContainerPrin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.PnlContainerPrin.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PnlContainerPrin.Controls.Add(this.panel1);
            this.PnlContainerPrin.Controls.Add(this.panelFrm);
            this.PnlContainerPrin.Controls.Add(this.panelImgProducto);
            this.PnlContainerPrin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PnlContainerPrin.ForeColor = System.Drawing.Color.White;
            this.PnlContainerPrin.Location = new System.Drawing.Point(269, 61);
            this.PnlContainerPrin.Name = "PnlContainerPrin";
            this.PnlContainerPrin.Size = new System.Drawing.Size(939, 472);
            this.PnlContainerPrin.TabIndex = 22;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.btnModificarProducto);
            this.panel1.Controls.Add(this.btnEliminarclientes);
            this.panel1.Controls.Add(this.btnGuardarCliente);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(792, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(143, 468);
            this.panel1.TabIndex = 20;
            // 
            // btnModificarProducto
            // 
            this.btnModificarProducto.FlatAppearance.BorderSize = 2;
            this.btnModificarProducto.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnModificarProducto.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnModificarProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModificarProducto.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificarProducto.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnModificarProducto.Image = ((System.Drawing.Image)(resources.GetObject("btnModificarProducto.Image")));
            this.btnModificarProducto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnModificarProducto.Location = new System.Drawing.Point(3, 173);
            this.btnModificarProducto.Name = "btnModificarProducto";
            this.btnModificarProducto.Size = new System.Drawing.Size(131, 57);
            this.btnModificarProducto.TabIndex = 43;
            this.btnModificarProducto.Text = "             Modificar            Producto";
            this.btnModificarProducto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnModificarProducto.UseVisualStyleBackColor = true;
            this.btnModificarProducto.Click += new System.EventHandler(this.btnModificarProducto_Click);
            // 
            // btnEliminarclientes
            // 
            this.btnEliminarclientes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEliminarclientes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnEliminarclientes.FlatAppearance.BorderSize = 2;
            this.btnEliminarclientes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnEliminarclientes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnEliminarclientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarclientes.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarclientes.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnEliminarclientes.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminarclientes.Image")));
            this.btnEliminarclientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarclientes.Location = new System.Drawing.Point(2, 334);
            this.btnEliminarclientes.Name = "btnEliminarclientes";
            this.btnEliminarclientes.Size = new System.Drawing.Size(132, 57);
            this.btnEliminarclientes.TabIndex = 41;
            this.btnEliminarclientes.Text = "Eliminar     Producto";
            this.btnEliminarclientes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEliminarclientes.UseVisualStyleBackColor = false;
            this.btnEliminarclientes.Click += new System.EventHandler(this.btnEliminarclientes_Click);
            // 
            // btnGuardarCliente
            // 
            this.btnGuardarCliente.FlatAppearance.BorderSize = 2;
            this.btnGuardarCliente.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnGuardarCliente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnGuardarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardarCliente.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardarCliente.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnGuardarCliente.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardarCliente.Image")));
            this.btnGuardarCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardarCliente.Location = new System.Drawing.Point(3, 5);
            this.btnGuardarCliente.Name = "btnGuardarCliente";
            this.btnGuardarCliente.Size = new System.Drawing.Size(131, 57);
            this.btnGuardarCliente.TabIndex = 42;
            this.btnGuardarCliente.Text = "Guardar Producto";
            this.btnGuardarCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGuardarCliente.UseVisualStyleBackColor = true;
            this.btnGuardarCliente.Click += new System.EventHandler(this.btnGuardarCliente_Click);
            // 
            // panelFrm
            // 
            this.panelFrm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.panelFrm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelFrm.Controls.Add(this.txtEAN13);
            this.panelFrm.Controls.Add(this.btnAgregarProducto);
            this.panelFrm.Controls.Add(this.codBarraTry);
            this.panelFrm.Controls.Add(this.label5);
            this.panelFrm.Controls.Add(this.checkBoxEsPereceredo);
            this.panelFrm.Controls.Add(this.comboUnidadDeMedida);
            this.panelFrm.Controls.Add(this.label4);
            this.panelFrm.Controls.Add(this.txtPrecio);
            this.panelFrm.Controls.Add(this.label2);
            this.panelFrm.Controls.Add(this.txtCodBarras);
            this.panelFrm.Controls.Add(this.lblCodBarras);
            this.panelFrm.Controls.Add(this.txtDescripcion);
            this.panelFrm.Controls.Add(this.label1);
            this.panelFrm.Controls.Add(this.txtNombre);
            this.panelFrm.Controls.Add(this.lblNombre);
            this.panelFrm.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelFrm.Location = new System.Drawing.Point(0, 0);
            this.panelFrm.Margin = new System.Windows.Forms.Padding(2);
            this.panelFrm.Name = "panelFrm";
            this.panelFrm.Size = new System.Drawing.Size(362, 468);
            this.panelFrm.TabIndex = 19;
            // 
            // txtEAN13
            // 
            this.txtEAN13.Font = new System.Drawing.Font("EAN-13", 71.99999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEAN13.Location = new System.Drawing.Point(174, 254);
            this.txtEAN13.Multiline = true;
            this.txtEAN13.Name = "txtEAN13";
            this.txtEAN13.Size = new System.Drawing.Size(180, 166);
            this.txtEAN13.TabIndex = 24;
            // 
            // btnAgregarProducto
            // 
            this.btnAgregarProducto.FlatAppearance.BorderSize = 2;
            this.btnAgregarProducto.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnAgregarProducto.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnAgregarProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarProducto.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregarProducto.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAgregarProducto.Image = ((System.Drawing.Image)(resources.GetObject("btnAgregarProducto.Image")));
            this.btnAgregarProducto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgregarProducto.Location = new System.Drawing.Point(142, 7);
            this.btnAgregarProducto.Name = "btnAgregarProducto";
            this.btnAgregarProducto.Size = new System.Drawing.Size(122, 57);
            this.btnAgregarProducto.TabIndex = 48;
            this.btnAgregarProducto.Text = "Agregar Producto";
            this.btnAgregarProducto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAgregarProducto.UseVisualStyleBackColor = true;
            this.btnAgregarProducto.Click += new System.EventHandler(this.btnAgregarProducto_Click);
            // 
            // codBarraTry
            // 
            this.codBarraTry.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.codBarraTry.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.codBarraTry.Location = new System.Drawing.Point(4, 260);
            this.codBarraTry.Margin = new System.Windows.Forms.Padding(2);
            this.codBarraTry.Name = "codBarraTry";
            this.codBarraTry.Size = new System.Drawing.Size(165, 150);
            this.codBarraTry.TabIndex = 30;
            this.codBarraTry.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(12, 243);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(169, 15);
            this.label5.TabIndex = 29;
            this.label5.Text = "Imagen Codigo de Barras";
            // 
            // checkBoxEsPereceredo
            // 
            this.checkBoxEsPereceredo.AutoSize = true;
            this.checkBoxEsPereceredo.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxEsPereceredo.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.checkBoxEsPereceredo.Location = new System.Drawing.Point(222, 437);
            this.checkBoxEsPereceredo.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxEsPereceredo.Name = "checkBoxEsPereceredo";
            this.checkBoxEsPereceredo.Size = new System.Drawing.Size(132, 19);
            this.checkBoxEsPereceredo.TabIndex = 4;
            this.checkBoxEsPereceredo.Text = "¿Es Perecedero?";
            this.checkBoxEsPereceredo.UseVisualStyleBackColor = true;
            // 
            // comboUnidadDeMedida
            // 
            this.comboUnidadDeMedida.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboUnidadDeMedida.FormattingEnabled = true;
            this.comboUnidadDeMedida.ItemHeight = 13;
            this.comboUnidadDeMedida.Items.AddRange(new object[] {
            "UNIDAD",
            "LITRO",
            "KILO",
            "CAJA",
            "PAQUETE"});
            this.comboUnidadDeMedida.Location = new System.Drawing.Point(205, 228);
            this.comboUnidadDeMedida.Margin = new System.Windows.Forms.Padding(2);
            this.comboUnidadDeMedida.Name = "comboUnidadDeMedida";
            this.comboUnidadDeMedida.Size = new System.Drawing.Size(150, 21);
            this.comboUnidadDeMedida.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(202, 204);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 15);
            this.label4.TabIndex = 25;
            this.label4.Text = "Unidad de Medida";
            // 
            // txtPrecio
            // 
            this.txtPrecio.Location = new System.Drawing.Point(9, 437);
            this.txtPrecio.Margin = new System.Windows.Forms.Padding(2);
            this.txtPrecio.Name = "txtPrecio";
            this.txtPrecio.Size = new System.Drawing.Size(200, 20);
            this.txtPrecio.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(4, 412);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 15);
            this.label2.TabIndex = 23;
            this.label2.Text = "Precio";
            // 
            // txtCodBarras
            // 
            this.txtCodBarras.Location = new System.Drawing.Point(14, 204);
            this.txtCodBarras.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodBarras.Name = "txtCodBarras";
            this.txtCodBarras.Size = new System.Drawing.Size(159, 20);
            this.txtCodBarras.TabIndex = 2;
            this.txtCodBarras.TextChanged += new System.EventHandler(this.txtCodBarras_TextChanged);
            this.txtCodBarras.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodBarras_KeyPress);
            // 
            // lblCodBarras
            // 
            this.lblCodBarras.AutoSize = true;
            this.lblCodBarras.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodBarras.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblCodBarras.Location = new System.Drawing.Point(11, 187);
            this.lblCodBarras.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCodBarras.Name = "lblCodBarras";
            this.lblCodBarras.Size = new System.Drawing.Size(83, 15);
            this.lblCodBarras.TabIndex = 21;
            this.lblCodBarras.Text = "Cod. Barras";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(10, 110);
            this.txtDescripcion.Margin = new System.Windows.Forms.Padding(2);
            this.txtDescripcion.Multiline = true;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(321, 63);
            this.txtDescripcion.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(7, 93);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 15);
            this.label1.TabIndex = 19;
            this.label1.Text = "Descripción";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(10, 71);
            this.txtNombre.Margin = new System.Windows.Forms.Padding(2);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(200, 20);
            this.txtNombre.TabIndex = 0;
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblNombre.Location = new System.Drawing.Point(12, 54);
            this.lblNombre.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(59, 15);
            this.lblNombre.TabIndex = 17;
            this.lblNombre.Text = "Nombre";
            // 
            // panelImgProducto
            // 
            this.panelImgProducto.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelImgProducto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.panelImgProducto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelImgProducto.Controls.Add(this.panel5);
            this.panelImgProducto.Controls.Add(this.panel2);
            this.panelImgProducto.Location = new System.Drawing.Point(360, -2);
            this.panelImgProducto.Margin = new System.Windows.Forms.Padding(2);
            this.panelImgProducto.Name = "panelImgProducto";
            this.panelImgProducto.Size = new System.Drawing.Size(432, 470);
            this.panelImgProducto.TabIndex = 18;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.icoBtnCargarImagen);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.lblImagenAGuardar);
            this.panel5.Controls.Add(this.lblImagenOriginal);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 282);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(428, 184);
            this.panel5.TabIndex = 25;
            // 
            // icoBtnCargarImagen
            // 
            this.icoBtnCargarImagen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.icoBtnCargarImagen.FlatAppearance.BorderSize = 2;
            this.icoBtnCargarImagen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.icoBtnCargarImagen.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.icoBtnCargarImagen.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.icoBtnCargarImagen.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.icoBtnCargarImagen.IconChar = FontAwesome.Sharp.IconChar.Image;
            this.icoBtnCargarImagen.IconColor = System.Drawing.Color.MediumAquamarine;
            this.icoBtnCargarImagen.IconSize = 66;
            this.icoBtnCargarImagen.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.icoBtnCargarImagen.Location = new System.Drawing.Point(124, 71);
            this.icoBtnCargarImagen.Margin = new System.Windows.Forms.Padding(2);
            this.icoBtnCargarImagen.Name = "icoBtnCargarImagen";
            this.icoBtnCargarImagen.Rotation = 0D;
            this.icoBtnCargarImagen.Size = new System.Drawing.Size(201, 53);
            this.icoBtnCargarImagen.TabIndex = 20;
            this.icoBtnCargarImagen.Text = "Cargar Imagen";
            this.icoBtnCargarImagen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.icoBtnCargarImagen.UseVisualStyleBackColor = true;
            this.icoBtnCargarImagen.Click += new System.EventHandler(this.icoBtnCargarImagen_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(244, 21);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(163, 15);
            this.label3.TabIndex = 8;
            this.label3.Text = "Nombre Original Imagen";
            // 
            // lblImagenAGuardar
            // 
            this.lblImagenAGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblImagenAGuardar.AutoSize = true;
            this.lblImagenAGuardar.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenAGuardar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenAGuardar.Location = new System.Drawing.Point(34, 41);
            this.lblImagenAGuardar.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenAGuardar.Name = "lblImagenAGuardar";
            this.lblImagenAGuardar.Size = new System.Drawing.Size(113, 15);
            this.lblImagenAGuardar.TabIndex = 23;
            this.lblImagenAGuardar.Text = "Nombre Original";
            // 
            // lblImagenOriginal
            // 
            this.lblImagenOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblImagenOriginal.AutoSize = true;
            this.lblImagenOriginal.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenOriginal.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImagenOriginal.Location = new System.Drawing.Point(294, 43);
            this.lblImagenOriginal.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenOriginal.Name = "lblImagenOriginal";
            this.lblImagenOriginal.Size = new System.Drawing.Size(113, 15);
            this.lblImagenOriginal.TabIndex = 21;
            this.lblImagenOriginal.Text = "Nombre Original";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(3, 19);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(144, 15);
            this.label6.TabIndex = 22;
            this.label6.Text = "Nombre a Almacenar";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.picBoxProducto);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(428, 276);
            this.panel2.TabIndex = 24;
            // 
            // picBoxProducto
            // 
            this.picBoxProducto.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.picBoxProducto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picBoxProducto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picBoxProducto.Image = ((System.Drawing.Image)(resources.GetObject("picBoxProducto.Image")));
            this.picBoxProducto.Location = new System.Drawing.Point(0, 0);
            this.picBoxProducto.Margin = new System.Windows.Forms.Padding(2);
            this.picBoxProducto.Name = "picBoxProducto";
            this.picBoxProducto.Size = new System.Drawing.Size(428, 276);
            this.picBoxProducto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxProducto.TabIndex = 18;
            this.picBoxProducto.TabStop = false;
            // 
            // pnlBarraTareas
            // 
            this.pnlBarraTareas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.pnlBarraTareas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlBarraTareas.Controls.Add(this.pbBuscar);
            this.pnlBarraTareas.Controls.Add(this.txtConsultaProducto);
            this.pnlBarraTareas.Controls.Add(this.btnApagar);
            this.pnlBarraTareas.Controls.Add(this.btnExpandirMn);
            this.pnlBarraTareas.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBarraTareas.Location = new System.Drawing.Point(269, 0);
            this.pnlBarraTareas.Name = "pnlBarraTareas";
            this.pnlBarraTareas.Size = new System.Drawing.Size(939, 61);
            this.pnlBarraTareas.TabIndex = 21;
            // 
            // pbBuscar
            // 
            this.pbBuscar.Dock = System.Windows.Forms.DockStyle.Right;
            this.pbBuscar.Image = ((System.Drawing.Image)(resources.GetObject("pbBuscar.Image")));
            this.pbBuscar.Location = new System.Drawing.Point(817, 0);
            this.pbBuscar.Name = "pbBuscar";
            this.pbBuscar.Size = new System.Drawing.Size(65, 57);
            this.pbBuscar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbBuscar.TabIndex = 48;
            this.pbBuscar.TabStop = false;
            this.pbBuscar.Click += new System.EventHandler(this.pictureBox9_Click);
            // 
            // txtConsultaProducto
            // 
            this.txtConsultaProducto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConsultaProducto.Location = new System.Drawing.Point(109, 26);
            this.txtConsultaProducto.Name = "txtConsultaProducto";
            this.txtConsultaProducto.Size = new System.Drawing.Size(681, 20);
            this.txtConsultaProducto.TabIndex = 47;
            this.txtConsultaProducto.Enter += new System.EventHandler(this.txtConsultaProducto_Enter);
            this.txtConsultaProducto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtConsultaProducto_KeyPress);
            // 
            // btnApagar
            // 
            this.btnApagar.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnApagar.FlatAppearance.BorderSize = 0;
            this.btnApagar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnApagar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApagar.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApagar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnApagar.Image = ((System.Drawing.Image)(resources.GetObject("btnApagar.Image")));
            this.btnApagar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnApagar.Location = new System.Drawing.Point(882, 0);
            this.btnApagar.Name = "btnApagar";
            this.btnApagar.Size = new System.Drawing.Size(53, 57);
            this.btnApagar.TabIndex = 46;
            this.btnApagar.Text = "Salir";
            this.btnApagar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnApagar.UseVisualStyleBackColor = true;
            this.btnApagar.Click += new System.EventHandler(this.btnApagar_Click);
            // 
            // btnExpandirMn
            // 
            this.btnExpandirMn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnExpandirMn.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnExpandirMn.FlatAppearance.BorderSize = 0;
            this.btnExpandirMn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnExpandirMn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnExpandirMn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExpandirMn.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExpandirMn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnExpandirMn.Image = ((System.Drawing.Image)(resources.GetObject("btnExpandirMn.Image")));
            this.btnExpandirMn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExpandirMn.Location = new System.Drawing.Point(0, 0);
            this.btnExpandirMn.Name = "btnExpandirMn";
            this.btnExpandirMn.Size = new System.Drawing.Size(62, 57);
            this.btnExpandirMn.TabIndex = 45;
            this.btnExpandirMn.UseVisualStyleBackColor = false;
            this.btnExpandirMn.Click += new System.EventHandler(this.btnExpandirMn_Click);
            // 
            // PanelData
            // 
            this.PanelData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.PanelData.Controls.Add(this.dgProductos);
            this.PanelData.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelData.Location = new System.Drawing.Point(269, 533);
            this.PanelData.Name = "PanelData";
            this.PanelData.Size = new System.Drawing.Size(939, 191);
            this.PanelData.TabIndex = 20;
            // 
            // dgProductos
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.dgProductos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgProductos.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            this.dgProductos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgProductos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgProductos.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.HotTrack;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProductos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgProductos.ColumnHeadersHeight = 30;
            this.dgProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgProductos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgProductos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgProductos.EnableHeadersVisualStyles = false;
            this.dgProductos.GridColor = System.Drawing.Color.SteelBlue;
            this.dgProductos.Location = new System.Drawing.Point(0, 0);
            this.dgProductos.Name = "dgProductos";
            this.dgProductos.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProductos.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            this.dgProductos.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgProductos.Size = new System.Drawing.Size(939, 191);
            this.dgProductos.TabIndex = 0;
            this.dgProductos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgProductos_CellContentClick);
            // 
            // pnlNavbar
            // 
            this.pnlNavbar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.pnlNavbar.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlNavbar.Controls.Add(this.panelTimer);
            this.pnlNavbar.Controls.Add(this.panel4);
            this.pnlNavbar.Controls.Add(this.PnlInfo);
            this.pnlNavbar.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlNavbar.Location = new System.Drawing.Point(0, 0);
            this.pnlNavbar.Name = "pnlNavbar";
            this.pnlNavbar.Size = new System.Drawing.Size(269, 724);
            this.pnlNavbar.TabIndex = 20;
            // 
            // panelTimer
            // 
            this.panelTimer.Controls.Add(this.pictureBox1);
            this.panelTimer.Controls.Add(this.lblReloj);
            this.panelTimer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTimer.Location = new System.Drawing.Point(0, 662);
            this.panelTimer.Name = "panelTimer";
            this.panelTimer.Size = new System.Drawing.Size(265, 58);
            this.panelTimer.TabIndex = 43;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(54, 54);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // lblReloj
            // 
            this.lblReloj.Font = new System.Drawing.Font("Lucida Bright", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReloj.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblReloj.Location = new System.Drawing.Point(61, 8);
            this.lblReloj.Name = "lblReloj";
            this.lblReloj.Size = new System.Drawing.Size(164, 32);
            this.lblReloj.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.btnCompras);
            this.panel4.Controls.Add(this.button1);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.btnCajas);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.btnCreditos);
            this.panel4.Controls.Add(this.btnHome);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Location = new System.Drawing.Point(2, 291);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(261, 331);
            this.panel4.TabIndex = 42;
            // 
            // label9
            // 
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Location = new System.Drawing.Point(5, 137);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(250, 3);
            this.label9.TabIndex = 54;
            this.label9.Text = "label9";
            this.label9.UseWaitCursor = true;
            // 
            // btnCompras
            // 
            this.btnCompras.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnCompras.FlatAppearance.BorderSize = 2;
            this.btnCompras.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCompras.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnCompras.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCompras.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCompras.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCompras.Image = ((System.Drawing.Image)(resources.GetObject("btnCompras.Image")));
            this.btnCompras.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCompras.Location = new System.Drawing.Point(6, 89);
            this.btnCompras.Name = "btnCompras";
            this.btnCompras.Size = new System.Drawing.Size(249, 47);
            this.btnCompras.TabIndex = 53;
            this.btnCompras.Text = " Persona";
            this.btnCompras.UseVisualStyleBackColor = false;
            this.btnCompras.Click += new System.EventHandler(this.btnCompras_Click_1);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.button1.FlatAppearance.BorderSize = 2;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(6, 154);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(249, 47);
            this.button1.TabIndex = 52;
            this.button1.Text = "Clientes";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Location = new System.Drawing.Point(5, 203);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(250, 3);
            this.label8.TabIndex = 51;
            this.label8.Text = "label8";
            this.label8.UseWaitCursor = true;
            // 
            // btnCajas
            // 
            this.btnCajas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnCajas.FlatAppearance.BorderSize = 2;
            this.btnCajas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCajas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnCajas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCajas.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCajas.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCajas.Image = ((System.Drawing.Image)(resources.GetObject("btnCajas.Image")));
            this.btnCajas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCajas.Location = new System.Drawing.Point(5, 278);
            this.btnCajas.Name = "btnCajas";
            this.btnCajas.Size = new System.Drawing.Size(249, 47);
            this.btnCajas.TabIndex = 50;
            this.btnCajas.Text = "Cajas";
            this.btnCajas.UseVisualStyleBackColor = false;
            this.btnCajas.Click += new System.EventHandler(this.btnCajas_Click);
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Location = new System.Drawing.Point(2, 326);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(250, 3);
            this.label7.TabIndex = 49;
            this.label7.Text = "label7";
            this.label7.UseWaitCursor = true;
            // 
            // btnCreditos
            // 
            this.btnCreditos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnCreditos.FlatAppearance.BorderSize = 2;
            this.btnCreditos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCreditos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnCreditos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreditos.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreditos.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCreditos.Image = ((System.Drawing.Image)(resources.GetObject("btnCreditos.Image")));
            this.btnCreditos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCreditos.Location = new System.Drawing.Point(6, 216);
            this.btnCreditos.Name = "btnCreditos";
            this.btnCreditos.Size = new System.Drawing.Size(249, 47);
            this.btnCreditos.TabIndex = 43;
            this.btnCreditos.Text = "Créditos";
            this.btnCreditos.UseVisualStyleBackColor = false;
            this.btnCreditos.Click += new System.EventHandler(this.btnCreditos_Click);
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.btnHome.FlatAppearance.BorderSize = 2;
            this.btnHome.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnHome.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHome.Image = ((System.Drawing.Image)(resources.GetObject("btnHome.Image")));
            this.btnHome.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHome.Location = new System.Drawing.Point(7, 26);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(249, 47);
            this.btnHome.TabIndex = 40;
            this.btnHome.Text = "Home";
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // label18
            // 
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label18.Location = new System.Drawing.Point(7, 74);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(250, 3);
            this.label18.TabIndex = 38;
            this.label18.Text = "label18";
            this.label18.UseWaitCursor = true;
            // 
            // label20
            // 
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label20.Location = new System.Drawing.Point(6, 264);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(250, 3);
            this.label20.TabIndex = 37;
            this.label20.Text = "label20";
            this.label20.UseWaitCursor = true;
            // 
            // PnlInfo
            // 
            this.PnlInfo.Controls.Add(this.lblUserName);
            this.PnlInfo.Controls.Add(this.panel3);
            this.PnlInfo.Controls.Add(this.pictureBox14);
            this.PnlInfo.Controls.Add(this.lblCajaTitle);
            this.PnlInfo.Controls.Add(this.ipbLogo);
            this.PnlInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.PnlInfo.Location = new System.Drawing.Point(0, 0);
            this.PnlInfo.Name = "PnlInfo";
            this.PnlInfo.Size = new System.Drawing.Size(265, 291);
            this.PnlInfo.TabIndex = 41;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Lucida Bright", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblUserName.Location = new System.Drawing.Point(13, 246);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(238, 22);
            this.lblUserName.TabIndex = 50;
            this.lblUserName.Text = "NOMBRE DEL USUARIO ";
            this.lblUserName.UseWaitCursor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblNombreUser);
            this.panel3.Location = new System.Drawing.Point(268, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(600, 42);
            this.panel3.TabIndex = 48;
            // 
            // lblNombreUser
            // 
            this.lblNombreUser.AutoSize = true;
            this.lblNombreUser.Font = new System.Drawing.Font("Lucida Bright", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreUser.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblNombreUser.Location = new System.Drawing.Point(181, 70);
            this.lblNombreUser.Name = "lblNombreUser";
            this.lblNombreUser.Size = new System.Drawing.Size(238, 22);
            this.lblNombreUser.TabIndex = 50;
            this.lblNombreUser.Text = "NOMBRE DEL USUARIO ";
            this.lblNombreUser.UseWaitCursor = true;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(22)))), ((int)(((byte)(82)))));
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.Location = new System.Drawing.Point(10, 1);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(54, 58);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox14.TabIndex = 47;
            this.pictureBox14.TabStop = false;
            // 
            // lblCajaTitle
            // 
            this.lblCajaTitle.AutoSize = true;
            this.lblCajaTitle.Font = new System.Drawing.Font("Lucida Bright", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCajaTitle.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblCajaTitle.Location = new System.Drawing.Point(93, 12);
            this.lblCajaTitle.Name = "lblCajaTitle";
            this.lblCajaTitle.Size = new System.Drawing.Size(161, 33);
            this.lblCajaTitle.TabIndex = 46;
            this.lblCajaTitle.Text = "Productos";
            // 
            // ipbLogo
            // 
            this.ipbLogo.Image = ((System.Drawing.Image)(resources.GetObject("ipbLogo.Image")));
            this.ipbLogo.Location = new System.Drawing.Point(37, 65);
            this.ipbLogo.Name = "ipbLogo";
            this.ipbLogo.Size = new System.Drawing.Size(175, 168);
            this.ipbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ipbLogo.TabIndex = 43;
            this.ipbLogo.TabStop = false;
            this.ipbLogo.UseWaitCursor = true;
            // 
            // openFileDialogCargarImagen
            // 
            this.openFileDialogCargarImagen.FileName = "openFileDialog1";
            // 
            // FrmCatalogoProductos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1208, 724);
            this.Controls.Add(this.panelFormulario);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmCatalogoProductos";
            this.Text = "  e";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmCatalogoProductos_Load);
            this.panelFormulario.ResumeLayout(false);
            this.PnlContainerPrin.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panelFrm.ResumeLayout(false);
            this.panelFrm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.codBarraTry)).EndInit();
            this.panelImgProducto.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxProducto)).EndInit();
            this.pnlBarraTareas.ResumeLayout(false);
            this.pnlBarraTareas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBuscar)).EndInit();
            this.PanelData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgProductos)).EndInit();
            this.pnlNavbar.ResumeLayout(false);
            this.panelTimer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.PnlInfo.ResumeLayout(false);
            this.PnlInfo.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ipbLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelFormulario;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.OpenFileDialog openFileDialogCargarImagen;
        private System.Windows.Forms.Panel panelFrm;
        private System.Windows.Forms.CheckBox checkBoxEsPereceredo;
        private System.Windows.Forms.ComboBox comboUnidadDeMedida;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPrecio;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCodBarras;
        private System.Windows.Forms.Label lblCodBarras;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Panel panelImgProducto;
        private System.Windows.Forms.PictureBox picBoxProducto;
        private FontAwesome.Sharp.IconButton icoBtnCargarImagen;
        private System.Windows.Forms.Label lblImagenAGuardar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblImagenOriginal;
        private System.Windows.Forms.Panel pnlNavbar;
        private System.Windows.Forms.Panel panelTimer;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblReloj;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnCreditos;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel PnlInfo;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.Label lblCajaTitle;
        private System.Windows.Forms.PictureBox ipbLogo;
        private System.Windows.Forms.Panel pnlBarraTareas;
        private System.Windows.Forms.Button btnExpandirMn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnApagar;
        private System.Windows.Forms.Panel PanelData;
        private System.Windows.Forms.DataGridView dgProductos;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnEliminarclientes;
        private System.Windows.Forms.Button btnGuardarCliente;
        private System.Windows.Forms.Button btnModificarProducto;
        private System.Windows.Forms.Button btnAgregarProducto;
        private System.Windows.Forms.Button btnCajas;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox codBarraTry;
        private System.Windows.Forms.Panel PnlContainerPrin;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtEAN13;
        private System.Windows.Forms.PictureBox pbBuscar;
        private System.Windows.Forms.TextBox txtConsultaProducto;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblNombreUser;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnCompras;
    }
}