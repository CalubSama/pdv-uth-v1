﻿namespace pdv_uth_v1
{
    partial class FrmCreditos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCreditos));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pnlBarraTareas = new System.Windows.Forms.Panel();
            this.txtBusqueda = new System.Windows.Forms.TextBox();
            this.PicBuscar = new System.Windows.Forms.PictureBox();
            this.btnRetroceder = new System.Windows.Forms.Button();
            this.Cerrar = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgClientes = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.lblElejirCliente = new System.Windows.Forms.Label();
            this.dgCreditosClientes = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblInformacion = new System.Windows.Forms.Label();
            this.panelFormulario = new System.Windows.Forms.Panel();
            this.pnlAcomodar = new System.Windows.Forms.Panel();
            this.pnlCreditoInfo = new System.Windows.Forms.Panel();
            this.picUserImage = new System.Windows.Forms.PictureBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtApPat = new System.Windows.Forms.TextBox();
            this.txtApMat = new System.Windows.Forms.TextBox();
            this.txtCelular = new System.Windows.Forms.TextBox();
            this.txtCorreo = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtColonia = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.txtLocalidad = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.txtCurp = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.lblCalle = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.txtCalle = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.dtpFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.label34 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtMunicipio = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.PanelVenta = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAñadirVenta = new System.Windows.Forms.Button();
            this.txtSaldoParaVenta = new System.Windows.Forms.TextBox();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblfechaAper = new System.Windows.Forms.Label();
            this.txtSaldo = new System.Windows.Forms.TextBox();
            this.btnAñadirClientes = new System.Windows.Forms.Button();
            this.cbEstadoCuenta = new System.Windows.Forms.ComboBox();
            this.lblSaldo = new System.Windows.Forms.Label();
            this.dTFechaASaldar = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dTFechaApertura = new System.Windows.Forms.DateTimePicker();
            this.btnModificarCliente = new System.Windows.Forms.Button();
            this.btnGuardarCliente = new System.Windows.Forms.Button();
            this.panel5.SuspendLayout();
            this.pnlBarraTareas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBuscar)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgClientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgCreditosClientes)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panelFormulario.SuspendLayout();
            this.pnlAcomodar.SuspendLayout();
            this.pnlCreditoInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUserImage)).BeginInit();
            this.PanelVenta.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(21)))), ((int)(((byte)(45)))));
            this.panel5.Controls.Add(this.pnlBarraTareas);
            this.panel5.Controls.Add(this.btnRetroceder);
            this.panel5.Controls.Add(this.Cerrar);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1210, 65);
            this.panel5.TabIndex = 46;
            // 
            // pnlBarraTareas
            // 
            this.pnlBarraTareas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(21)))), ((int)(((byte)(45)))));
            this.pnlBarraTareas.Controls.Add(this.txtBusqueda);
            this.pnlBarraTareas.Controls.Add(this.PicBuscar);
            this.pnlBarraTareas.Location = new System.Drawing.Point(112, 3);
            this.pnlBarraTareas.Name = "pnlBarraTareas";
            this.pnlBarraTareas.Size = new System.Drawing.Size(693, 60);
            this.pnlBarraTareas.TabIndex = 49;
            // 
            // txtBusqueda
            // 
            this.txtBusqueda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBusqueda.Font = new System.Drawing.Font("Lucida Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBusqueda.Location = new System.Drawing.Point(11, 29);
            this.txtBusqueda.Name = "txtBusqueda";
            this.txtBusqueda.Size = new System.Drawing.Size(555, 23);
            this.txtBusqueda.TabIndex = 41;
            this.txtBusqueda.Tag = "Buscar Cliente";
            this.txtBusqueda.Text = "Buscar Cliente";
            this.txtBusqueda.Enter += new System.EventHandler(this.txtBusqueda_Enter);
            this.txtBusqueda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBusqueda_KeyPress);
            // 
            // PicBuscar
            // 
            this.PicBuscar.Dock = System.Windows.Forms.DockStyle.Right;
            this.PicBuscar.Image = ((System.Drawing.Image)(resources.GetObject("PicBuscar.Image")));
            this.PicBuscar.Location = new System.Drawing.Point(648, 0);
            this.PicBuscar.Name = "PicBuscar";
            this.PicBuscar.Size = new System.Drawing.Size(45, 60);
            this.PicBuscar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PicBuscar.TabIndex = 36;
            this.PicBuscar.TabStop = false;
            this.PicBuscar.Click += new System.EventHandler(this.PicBuscar_Click);
            // 
            // btnRetroceder
            // 
            this.btnRetroceder.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnRetroceder.FlatAppearance.BorderSize = 0;
            this.btnRetroceder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetroceder.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetroceder.Image = ((System.Drawing.Image)(resources.GetObject("btnRetroceder.Image")));
            this.btnRetroceder.Location = new System.Drawing.Point(0, 0);
            this.btnRetroceder.Name = "btnRetroceder";
            this.btnRetroceder.Size = new System.Drawing.Size(112, 65);
            this.btnRetroceder.TabIndex = 48;
            this.btnRetroceder.UseVisualStyleBackColor = true;
            this.btnRetroceder.Click += new System.EventHandler(this.btnRetroceder_Click);
            // 
            // Cerrar
            // 
            this.Cerrar.Dock = System.Windows.Forms.DockStyle.Right;
            this.Cerrar.FlatAppearance.BorderSize = 0;
            this.Cerrar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.Cerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cerrar.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cerrar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Cerrar.Image = ((System.Drawing.Image)(resources.GetObject("Cerrar.Image")));
            this.Cerrar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Cerrar.Location = new System.Drawing.Point(1157, 0);
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(53, 65);
            this.Cerrar.TabIndex = 47;
            this.Cerrar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Cerrar.UseVisualStyleBackColor = true;
            this.Cerrar.Click += new System.EventHandler(this.Cerrar_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            this.panel1.Controls.Add(this.dgClientes);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblElejirCliente);
            this.panel1.Controls.Add(this.dgCreditosClientes);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 59);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1210, 491);
            this.panel1.TabIndex = 47;
            // 
            // dgClientes
            // 
            this.dgClientes.AllowUserToOrderColumns = true;
            this.dgClientes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgClientes.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgClientes.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            this.dgClientes.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgClientes.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.HotTrack;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgClientes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgClientes.ColumnHeadersHeight = 30;
            this.dgClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgClientes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgClientes.EnableHeadersVisualStyles = false;
            this.dgClientes.GridColor = System.Drawing.Color.SteelBlue;
            this.dgClientes.ImeMode = System.Windows.Forms.ImeMode.On;
            this.dgClientes.Location = new System.Drawing.Point(3, 24);
            this.dgClientes.Name = "dgClientes";
            this.dgClientes.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgClientes.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            this.dgClientes.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgClientes.Size = new System.Drawing.Size(238, 463);
            this.dgClientes.TabIndex = 50;
            this.dgClientes.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgClientes_CellClick);
            this.dgClientes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgClientes_CellContentClick_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Bright", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Info;
            this.label1.Location = new System.Drawing.Point(373, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(276, 24);
            this.label1.TabIndex = 48;
            this.label1.Text = "Información de Créditos";
            // 
            // lblElejirCliente
            // 
            this.lblElejirCliente.AutoSize = true;
            this.lblElejirCliente.Font = new System.Drawing.Font("Lucida Bright", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblElejirCliente.ForeColor = System.Drawing.SystemColors.Info;
            this.lblElejirCliente.Location = new System.Drawing.Point(3, 0);
            this.lblElejirCliente.Name = "lblElejirCliente";
            this.lblElejirCliente.Size = new System.Drawing.Size(238, 24);
            this.lblElejirCliente.TabIndex = 51;
            this.lblElejirCliente.Text = "Seleccionar Cliente ▼";
            this.lblElejirCliente.Click += new System.EventHandler(this.lblElejirCliente_Click);
            // 
            // dgCreditosClientes
            // 
            this.dgCreditosClientes.AllowUserToOrderColumns = true;
            this.dgCreditosClientes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgCreditosClientes.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgCreditosClientes.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            this.dgCreditosClientes.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgCreditosClientes.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.HotTrack;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCreditosClientes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgCreditosClientes.ColumnHeadersHeight = 30;
            this.dgCreditosClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgCreditosClientes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgCreditosClientes.EnableHeadersVisualStyles = false;
            this.dgCreditosClientes.GridColor = System.Drawing.Color.SteelBlue;
            this.dgCreditosClientes.ImeMode = System.Windows.Forms.ImeMode.On;
            this.dgCreditosClientes.Location = new System.Drawing.Point(242, 25);
            this.dgCreditosClientes.Name = "dgCreditosClientes";
            this.dgCreditosClientes.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCreditosClientes.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.White;
            this.dgCreditosClientes.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgCreditosClientes.Size = new System.Drawing.Size(533, 462);
            this.dgCreditosClientes.TabIndex = 47;
            this.dgCreditosClientes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgCreditosClientes_CellContentClick);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(21)))), ((int)(((byte)(45)))));
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panelFormulario);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(772, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(438, 491);
            this.panel2.TabIndex = 45;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lblInformacion);
            this.panel4.Location = new System.Drawing.Point(6, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(427, 42);
            this.panel4.TabIndex = 49;
            // 
            // lblInformacion
            // 
            this.lblInformacion.AutoSize = true;
            this.lblInformacion.Font = new System.Drawing.Font("Lucida Bright", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInformacion.ForeColor = System.Drawing.SystemColors.Info;
            this.lblInformacion.Location = new System.Drawing.Point(17, 5);
            this.lblInformacion.Name = "lblInformacion";
            this.lblInformacion.Size = new System.Drawing.Size(410, 33);
            this.lblInformacion.TabIndex = 47;
            this.lblInformacion.Text = "                     Datos Clientes";
            // 
            // panelFormulario
            // 
            this.panelFormulario.Controls.Add(this.pnlAcomodar);
            this.panelFormulario.Controls.Add(this.label10);
            this.panelFormulario.Controls.Add(this.panel3);
            this.panelFormulario.Location = new System.Drawing.Point(2, 44);
            this.panelFormulario.Name = "panelFormulario";
            this.panelFormulario.Size = new System.Drawing.Size(434, 444);
            this.panelFormulario.TabIndex = 48;
            // 
            // pnlAcomodar
            // 
            this.pnlAcomodar.Controls.Add(this.pnlCreditoInfo);
            this.pnlAcomodar.Controls.Add(this.PanelVenta);
            this.pnlAcomodar.Controls.Add(this.label18);
            this.pnlAcomodar.Location = new System.Drawing.Point(173, 6);
            this.pnlAcomodar.Name = "pnlAcomodar";
            this.pnlAcomodar.Size = new System.Drawing.Size(257, 433);
            this.pnlAcomodar.TabIndex = 81;
            // 
            // pnlCreditoInfo
            // 
            this.pnlCreditoInfo.Controls.Add(this.picUserImage);
            this.pnlCreditoInfo.Controls.Add(this.txtNombre);
            this.pnlCreditoInfo.Controls.Add(this.txtApPat);
            this.pnlCreditoInfo.Controls.Add(this.txtApMat);
            this.pnlCreditoInfo.Controls.Add(this.txtCelular);
            this.pnlCreditoInfo.Controls.Add(this.txtCorreo);
            this.pnlCreditoInfo.Controls.Add(this.label27);
            this.pnlCreditoInfo.Controls.Add(this.txtColonia);
            this.pnlCreditoInfo.Controls.Add(this.label26);
            this.pnlCreditoInfo.Controls.Add(this.lblNombre);
            this.pnlCreditoInfo.Controls.Add(this.txtLocalidad);
            this.pnlCreditoInfo.Controls.Add(this.label38);
            this.pnlCreditoInfo.Controls.Add(this.txtCurp);
            this.pnlCreditoInfo.Controls.Add(this.label37);
            this.pnlCreditoInfo.Controls.Add(this.lblCalle);
            this.pnlCreditoInfo.Controls.Add(this.label36);
            this.pnlCreditoInfo.Controls.Add(this.txtCalle);
            this.pnlCreditoInfo.Controls.Add(this.label35);
            this.pnlCreditoInfo.Controls.Add(this.dtpFechaNacimiento);
            this.pnlCreditoInfo.Controls.Add(this.label34);
            this.pnlCreditoInfo.Controls.Add(this.label30);
            this.pnlCreditoInfo.Controls.Add(this.txtMunicipio);
            this.pnlCreditoInfo.Controls.Add(this.label32);
            this.pnlCreditoInfo.Controls.Add(this.txtTelefono);
            this.pnlCreditoInfo.Controls.Add(this.label33);
            this.pnlCreditoInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlCreditoInfo.Font = new System.Drawing.Font("Lucida Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlCreditoInfo.Location = new System.Drawing.Point(0, 3);
            this.pnlCreditoInfo.Name = "pnlCreditoInfo";
            this.pnlCreditoInfo.Size = new System.Drawing.Size(257, 430);
            this.pnlCreditoInfo.TabIndex = 86;
            // 
            // picUserImage
            // 
            this.picUserImage.Image = ((System.Drawing.Image)(resources.GetObject("picUserImage.Image")));
            this.picUserImage.Location = new System.Drawing.Point(95, 4);
            this.picUserImage.Name = "picUserImage";
            this.picUserImage.Size = new System.Drawing.Size(75, 71);
            this.picUserImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picUserImage.TabIndex = 48;
            this.picUserImage.TabStop = false;
            this.picUserImage.Click += new System.EventHandler(this.picUserImage_Click);
            // 
            // txtNombre
            // 
            this.txtNombre.Font = new System.Drawing.Font("Lucida Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.Location = new System.Drawing.Point(11, 78);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(108, 23);
            this.txtNombre.TabIndex = 0;
            // 
            // txtApPat
            // 
            this.txtApPat.Font = new System.Drawing.Font("Lucida Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApPat.Location = new System.Drawing.Point(11, 116);
            this.txtApPat.Name = "txtApPat";
            this.txtApPat.Size = new System.Drawing.Size(108, 23);
            this.txtApPat.TabIndex = 1;
            // 
            // txtApMat
            // 
            this.txtApMat.Font = new System.Drawing.Font("Lucida Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApMat.Location = new System.Drawing.Point(11, 154);
            this.txtApMat.Name = "txtApMat";
            this.txtApMat.Size = new System.Drawing.Size(108, 23);
            this.txtApMat.TabIndex = 2;
            // 
            // txtCelular
            // 
            this.txtCelular.Font = new System.Drawing.Font("Lucida Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCelular.Location = new System.Drawing.Point(11, 236);
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(108, 23);
            this.txtCelular.TabIndex = 4;
            // 
            // txtCorreo
            // 
            this.txtCorreo.Font = new System.Drawing.Font("Lucida Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCorreo.Location = new System.Drawing.Point(133, 275);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Size = new System.Drawing.Size(108, 23);
            this.txtCorreo.TabIndex = 6;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.GhostWhite;
            this.label27.Location = new System.Drawing.Point(200, 184);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(39, 14);
            this.label27.TabIndex = 48;
            this.label27.Text = "CURP";
            // 
            // txtColonia
            // 
            this.txtColonia.Font = new System.Drawing.Font("Lucida Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtColonia.Location = new System.Drawing.Point(132, 81);
            this.txtColonia.Name = "txtColonia";
            this.txtColonia.Size = new System.Drawing.Size(108, 23);
            this.txtColonia.TabIndex = 10;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.GhostWhite;
            this.label26.Location = new System.Drawing.Point(180, 105);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(60, 14);
            this.label26.TabIndex = 50;
            this.label26.Text = "Localidad";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.ForeColor = System.Drawing.Color.GhostWhite;
            this.lblNombre.Location = new System.Drawing.Point(11, 63);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(55, 14);
            this.lblNombre.TabIndex = 24;
            this.lblNombre.Text = "Nombre ";
            // 
            // txtLocalidad
            // 
            this.txtLocalidad.Font = new System.Drawing.Font("Lucida Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocalidad.Location = new System.Drawing.Point(132, 123);
            this.txtLocalidad.Name = "txtLocalidad";
            this.txtLocalidad.Size = new System.Drawing.Size(108, 23);
            this.txtLocalidad.TabIndex = 12;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.GhostWhite;
            this.label38.Location = new System.Drawing.Point(11, 101);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(99, 14);
            this.label38.TabIndex = 25;
            this.label38.Text = "Apellido Paterno";
            // 
            // txtCurp
            // 
            this.txtCurp.Font = new System.Drawing.Font("Lucida Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurp.Location = new System.Drawing.Point(131, 198);
            this.txtCurp.Name = "txtCurp";
            this.txtCurp.Size = new System.Drawing.Size(108, 23);
            this.txtCurp.TabIndex = 16;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.GhostWhite;
            this.label37.Location = new System.Drawing.Point(11, 140);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(102, 14);
            this.label37.TabIndex = 26;
            this.label37.Text = "Apellido Materno";
            // 
            // lblCalle
            // 
            this.lblCalle.AutoSize = true;
            this.lblCalle.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalle.ForeColor = System.Drawing.Color.GhostWhite;
            this.lblCalle.Location = new System.Drawing.Point(203, 220);
            this.lblCalle.Name = "lblCalle";
            this.lblCalle.Size = new System.Drawing.Size(36, 14);
            this.lblCalle.TabIndex = 44;
            this.lblCalle.Text = "Calle";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.GhostWhite;
            this.label36.Location = new System.Drawing.Point(11, 220);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(51, 14);
            this.label36.TabIndex = 27;
            this.label36.Text = "Celular ";
            // 
            // txtCalle
            // 
            this.txtCalle.Font = new System.Drawing.Font("Lucida Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCalle.Location = new System.Drawing.Point(131, 235);
            this.txtCalle.Name = "txtCalle";
            this.txtCalle.Size = new System.Drawing.Size(108, 23);
            this.txtCalle.TabIndex = 7;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.GhostWhite;
            this.label35.Location = new System.Drawing.Point(133, 258);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(112, 14);
            this.label35.TabIndex = 28;
            this.label35.Text = "Correo Electronico";
            // 
            // dtpFechaNacimiento
            // 
            this.dtpFechaNacimiento.Font = new System.Drawing.Font("Lucida Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaNacimiento.Location = new System.Drawing.Point(11, 196);
            this.dtpFechaNacimiento.Name = "dtpFechaNacimiento";
            this.dtpFechaNacimiento.Size = new System.Drawing.Size(108, 23);
            this.dtpFechaNacimiento.TabIndex = 3;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.GhostWhite;
            this.label34.Location = new System.Drawing.Point(190, 63);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(50, 14);
            this.label34.TabIndex = 29;
            this.label34.Text = "Colonia";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.GhostWhite;
            this.label30.Location = new System.Drawing.Point(11, 260);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(58, 14);
            this.label30.TabIndex = 39;
            this.label30.Text = "Telefono";
            // 
            // txtMunicipio
            // 
            this.txtMunicipio.Font = new System.Drawing.Font("Lucida Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMunicipio.Location = new System.Drawing.Point(132, 161);
            this.txtMunicipio.Name = "txtMunicipio";
            this.txtMunicipio.Size = new System.Drawing.Size(108, 23);
            this.txtMunicipio.TabIndex = 13;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.GhostWhite;
            this.label32.Location = new System.Drawing.Point(180, 146);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(60, 14);
            this.label32.TabIndex = 37;
            this.label32.Text = "Municipio";
            // 
            // txtTelefono
            // 
            this.txtTelefono.Font = new System.Drawing.Font("Lucida Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefono.Location = new System.Drawing.Point(11, 275);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(108, 23);
            this.txtTelefono.TabIndex = 5;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.GhostWhite;
            this.label33.Location = new System.Drawing.Point(8, 180);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(126, 14);
            this.label33.TabIndex = 36;
            this.label33.Text = "Fecha De Nacimiento";
            // 
            // PanelVenta
            // 
            this.PanelVenta.Controls.Add(this.label5);
            this.PanelVenta.Controls.Add(this.label2);
            this.PanelVenta.Controls.Add(this.btnAñadirVenta);
            this.PanelVenta.Controls.Add(this.txtSaldoParaVenta);
            this.PanelVenta.Controls.Add(this.txtTotal);
            this.PanelVenta.Controls.Add(this.label4);
            this.PanelVenta.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelVenta.Location = new System.Drawing.Point(0, 0);
            this.PanelVenta.Name = "PanelVenta";
            this.PanelVenta.Size = new System.Drawing.Size(257, 125);
            this.PanelVenta.TabIndex = 85;
            this.PanelVenta.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.GhostWhite;
            this.label5.Location = new System.Drawing.Point(54, 1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(155, 18);
            this.label5.TabIndex = 85;
            this.label5.Text = "Información Venta";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.GhostWhite;
            this.label2.Location = new System.Drawing.Point(169, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 15);
            this.label2.TabIndex = 82;
            this.label2.Text = "Saldo";
            // 
            // btnAñadirVenta
            // 
            this.btnAñadirVenta.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAñadirVenta.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(21)))), ((int)(((byte)(45)))));
            this.btnAñadirVenta.FlatAppearance.BorderSize = 2;
            this.btnAñadirVenta.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnAñadirVenta.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnAñadirVenta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAñadirVenta.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAñadirVenta.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAñadirVenta.Image = ((System.Drawing.Image)(resources.GetObject("btnAñadirVenta.Image")));
            this.btnAñadirVenta.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAñadirVenta.Location = new System.Drawing.Point(58, 75);
            this.btnAñadirVenta.Name = "btnAñadirVenta";
            this.btnAñadirVenta.Size = new System.Drawing.Size(141, 47);
            this.btnAñadirVenta.TabIndex = 78;
            this.btnAñadirVenta.Text = "Añadir Venta";
            this.btnAñadirVenta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAñadirVenta.UseVisualStyleBackColor = false;
            this.btnAñadirVenta.Click += new System.EventHandler(this.btnAñadirVenta_Click);
            // 
            // txtSaldoParaVenta
            // 
            this.txtSaldoParaVenta.Font = new System.Drawing.Font("Lucida Sans", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSaldoParaVenta.Location = new System.Drawing.Point(140, 39);
            this.txtSaldoParaVenta.Name = "txtSaldoParaVenta";
            this.txtSaldoParaVenta.Size = new System.Drawing.Size(108, 30);
            this.txtSaldoParaVenta.TabIndex = 81;
            this.txtSaldoParaVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtTotal
            // 
            this.txtTotal.Font = new System.Drawing.Font("Lucida Sans", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.Location = new System.Drawing.Point(13, 39);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(108, 30);
            this.txtTotal.TabIndex = 83;
            this.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.GhostWhite;
            this.label4.Location = new System.Drawing.Point(43, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 15);
            this.label4.TabIndex = 84;
            this.label4.Text = "Total";
            // 
            // label18
            // 
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label18.Location = new System.Drawing.Point(2, 125);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(258, 3);
            this.label18.TabIndex = 79;
            this.label18.Text = "label18";
            this.label18.UseWaitCursor = true;
            // 
            // label10
            // 
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Location = new System.Drawing.Point(174, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(258, 3);
            this.label10.TabIndex = 80;
            this.label10.Text = "label10";
            this.label10.UseWaitCursor = true;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.lblfechaAper);
            this.panel3.Controls.Add(this.txtSaldo);
            this.panel3.Controls.Add(this.btnAñadirClientes);
            this.panel3.Controls.Add(this.cbEstadoCuenta);
            this.panel3.Controls.Add(this.lblSaldo);
            this.panel3.Controls.Add(this.dTFechaASaldar);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.dTFechaApertura);
            this.panel3.Controls.Add(this.btnModificarCliente);
            this.panel3.Controls.Add(this.btnGuardarCliente);
            this.panel3.Location = new System.Drawing.Point(2, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(171, 443);
            this.panel3.TabIndex = 78;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // lblfechaAper
            // 
            this.lblfechaAper.AutoSize = true;
            this.lblfechaAper.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfechaAper.ForeColor = System.Drawing.Color.GhostWhite;
            this.lblfechaAper.Location = new System.Drawing.Point(9, 80);
            this.lblfechaAper.Name = "lblfechaAper";
            this.lblfechaAper.Size = new System.Drawing.Size(154, 18);
            this.lblfechaAper.TabIndex = 74;
            this.lblfechaAper.Text = "Fecha de apertura";
            // 
            // txtSaldo
            // 
            this.txtSaldo.Font = new System.Drawing.Font("Lucida Sans", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSaldo.Location = new System.Drawing.Point(11, 156);
            this.txtSaldo.Name = "txtSaldo";
            this.txtSaldo.Size = new System.Drawing.Size(149, 32);
            this.txtSaldo.TabIndex = 57;
            this.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnAñadirClientes
            // 
            this.btnAñadirClientes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAñadirClientes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(21)))), ((int)(((byte)(45)))));
            this.btnAñadirClientes.FlatAppearance.BorderSize = 2;
            this.btnAñadirClientes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnAñadirClientes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnAñadirClientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAñadirClientes.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAñadirClientes.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAñadirClientes.Image = ((System.Drawing.Image)(resources.GetObject("btnAñadirClientes.Image")));
            this.btnAñadirClientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAñadirClientes.Location = new System.Drawing.Point(26, 9);
            this.btnAñadirClientes.Name = "btnAñadirClientes";
            this.btnAñadirClientes.Size = new System.Drawing.Size(116, 47);
            this.btnAñadirClientes.TabIndex = 65;
            this.btnAñadirClientes.Text = "Añadir Crédito";
            this.btnAñadirClientes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAñadirClientes.UseVisualStyleBackColor = false;
            this.btnAñadirClientes.Click += new System.EventHandler(this.btnAñadirClientes_Click);
            // 
            // cbEstadoCuenta
            // 
            this.cbEstadoCuenta.Font = new System.Drawing.Font("Lucida Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEstadoCuenta.FormattingEnabled = true;
            this.cbEstadoCuenta.Items.AddRange(new object[] {
            "APERTURA",
            "OPERANDO",
            "CONGELADO",
            "SALDADO"});
            this.cbEstadoCuenta.Location = new System.Drawing.Point(33, 266);
            this.cbEstadoCuenta.Name = "cbEstadoCuenta";
            this.cbEstadoCuenta.Size = new System.Drawing.Size(114, 23);
            this.cbEstadoCuenta.TabIndex = 77;
            // 
            // lblSaldo
            // 
            this.lblSaldo.AutoSize = true;
            this.lblSaldo.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSaldo.ForeColor = System.Drawing.Color.GhostWhite;
            this.lblSaldo.Location = new System.Drawing.Point(58, 134);
            this.lblSaldo.Name = "lblSaldo";
            this.lblSaldo.Size = new System.Drawing.Size(51, 18);
            this.lblSaldo.TabIndex = 59;
            this.lblSaldo.Text = "Saldo";
            // 
            // dTFechaASaldar
            // 
            this.dTFechaASaldar.Font = new System.Drawing.Font("Lucida Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dTFechaASaldar.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dTFechaASaldar.Location = new System.Drawing.Point(34, 213);
            this.dTFechaASaldar.Name = "dTFechaASaldar";
            this.dTFechaASaldar.Size = new System.Drawing.Size(108, 23);
            this.dTFechaASaldar.TabIndex = 76;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.GhostWhite;
            this.label6.Location = new System.Drawing.Point(14, 242);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(147, 18);
            this.label6.TabIndex = 62;
            this.label6.Text = "Estado de cuenta";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.GhostWhite;
            this.label3.Location = new System.Drawing.Point(28, 191);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 18);
            this.label3.TabIndex = 75;
            this.label3.Text = "Fecha a saldar";
            // 
            // dTFechaApertura
            // 
            this.dTFechaApertura.Font = new System.Drawing.Font("Lucida Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dTFechaApertura.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dTFechaApertura.Location = new System.Drawing.Point(32, 108);
            this.dTFechaApertura.Name = "dTFechaApertura";
            this.dTFechaApertura.Size = new System.Drawing.Size(108, 23);
            this.dTFechaApertura.TabIndex = 73;
            // 
            // btnModificarCliente
            // 
            this.btnModificarCliente.FlatAppearance.BorderSize = 2;
            this.btnModificarCliente.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnModificarCliente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnModificarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModificarCliente.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificarCliente.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnModificarCliente.Image = ((System.Drawing.Image)(resources.GetObject("btnModificarCliente.Image")));
            this.btnModificarCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnModificarCliente.Location = new System.Drawing.Point(21, 373);
            this.btnModificarCliente.Name = "btnModificarCliente";
            this.btnModificarCliente.Size = new System.Drawing.Size(131, 57);
            this.btnModificarCliente.TabIndex = 68;
            this.btnModificarCliente.Text = "Modificar Crédito";
            this.btnModificarCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnModificarCliente.UseVisualStyleBackColor = true;
            this.btnModificarCliente.Click += new System.EventHandler(this.btnModificarCliente_Click);
            // 
            // btnGuardarCliente
            // 
            this.btnGuardarCliente.FlatAppearance.BorderSize = 2;
            this.btnGuardarCliente.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnGuardarCliente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnGuardarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardarCliente.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardarCliente.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnGuardarCliente.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardarCliente.Image")));
            this.btnGuardarCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardarCliente.Location = new System.Drawing.Point(21, 307);
            this.btnGuardarCliente.Name = "btnGuardarCliente";
            this.btnGuardarCliente.Size = new System.Drawing.Size(131, 57);
            this.btnGuardarCliente.TabIndex = 67;
            this.btnGuardarCliente.Text = "Guardar  Crédito";
            this.btnGuardarCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGuardarCliente.UseVisualStyleBackColor = true;
            this.btnGuardarCliente.Click += new System.EventHandler(this.btnGuardarCliente_Click);
            // 
            // FrmCreditos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1210, 550);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmCreditos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmCreditos";
            this.Load += new System.EventHandler(this.FrmCreditos_Load);
            this.panel5.ResumeLayout(false);
            this.pnlBarraTareas.ResumeLayout(false);
            this.pnlBarraTareas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBuscar)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgClientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgCreditosClientes)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panelFormulario.ResumeLayout(false);
            this.pnlAcomodar.ResumeLayout(false);
            this.pnlCreditoInfo.ResumeLayout(false);
            this.pnlCreditoInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUserImage)).EndInit();
            this.PanelVenta.ResumeLayout(false);
            this.PanelVenta.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnRetroceder;
        private System.Windows.Forms.Button Cerrar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlBarraTareas;
        private System.Windows.Forms.TextBox txtBusqueda;
        private System.Windows.Forms.PictureBox PicBuscar;
        private System.Windows.Forms.Label lblInformacion;
        private System.Windows.Forms.Panel panelFormulario;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtLocalidad;
        private System.Windows.Forms.TextBox txtCurp;
        private System.Windows.Forms.Label lblCalle;
        private System.Windows.Forms.TextBox txtCalle;
        private System.Windows.Forms.DateTimePicker dtpFechaNacimiento;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.TextBox txtMunicipio;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox txtColonia;
        private System.Windows.Forms.TextBox txtCorreo;
        private System.Windows.Forms.TextBox txtCelular;
        private System.Windows.Forms.TextBox txtApMat;
        private System.Windows.Forms.TextBox txtApPat;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblSaldo;
        private System.Windows.Forms.TextBox txtSaldo;
        private System.Windows.Forms.Button btnModificarCliente;
        private System.Windows.Forms.Button btnGuardarCliente;
        private System.Windows.Forms.Button btnAñadirClientes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgCreditosClientes;
        private System.Windows.Forms.DateTimePicker dTFechaASaldar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dTFechaApertura;
        private System.Windows.Forms.Label lblfechaAper;
        private System.Windows.Forms.ComboBox cbEstadoCuenta;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lblElejirCliente;
        private System.Windows.Forms.DataGridView dgClientes;
        private System.Windows.Forms.Panel PanelVenta;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAñadirVenta;
        private System.Windows.Forms.TextBox txtSaldoParaVenta;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel pnlCreditoInfo;
        private System.Windows.Forms.Panel pnlAcomodar;
        private System.Windows.Forms.PictureBox picUserImage;
    }
}