﻿using pdv_uth_v1.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace pdv_uth_v1
{
    public partial class FrmNotificaciones : Form
    {
        public FrmNotificaciones()
        {
            InitializeComponent();
        }
        //enum para la acción del cuadro
        public enum enmAction
        {
            wait,
            start,
            close
        }
        //enum paa clasificacion de los tipos de mensajes
        public enum enmType
        {
            Success,
            Warning,
            Error,
            Info
        }
        private FrmNotificaciones.enmAction action;
        //vars temporales
        private int x, y;
 
        //metodo
        public void notificacion(string msg, enmType type)
        {
            this.Opacity= 0.0;
            this.StartPosition = FormStartPosition.Manual;
            string fname;

            for (int i = 0; i < 10; i++)
            {
                fname = "alert" + i.ToString();
                FrmNotificaciones frm = (FrmNotificaciones)Application.OpenForms[fname];
                //posicionamiento
                if (frm == null)
                {
                    this.Name = fname;
                    this.x = Screen.PrimaryScreen.WorkingArea.Width - this.Width + 15;
                    this.y = Screen.PrimaryScreen.WorkingArea.Width - this.Height * i;
                    this.Location = new Point(this.x, this.y);
                    this.BringToFront();
                    break;
                }
            }
            this.x = Screen.PrimaryScreen.WorkingArea.Width - base.Width - 5;
            this.BringToFront();
            switch (type)
            {
                case enmType.Success:
                    this.pictureBox1.Image = Resources.ic_check_circle_white_48dp;
                    pnlNotificacion.BackColor = Color.ForestGreen;
                    pnlNotificacion.BringToFront();
                    break;
                case enmType.Error:
                    this.pictureBox1.Image = Resources.ic_sentiment_very_dissatisfied_white_48dp;
                    pnlNotificacion.BackColor = Color.DarkRed;
                    pnlNotificacion.BringToFront();
                    break;
                case enmType.Info:
                    this.pictureBox1.Image = Resources.ic_error_white_48dp;
                    pnlNotificacion.BackColor = Color.FromArgb(15, 57, 229);
                    pnlNotificacion.BringToFront();
                    break;
                case enmType.Warning:
                    this.pictureBox1.Image = Resources.ic_report_problem_white_48dp;
                    pnlNotificacion.BackColor = Color.Gold;
                    pnlNotificacion.BringToFront();
                    break;
            }
            this.lblMessage.Text = msg;
            
            this.Show();
            
            this.action = enmAction.start;
            
            this.timerClose.Interval = 1;
            
            timerClose.Start();
        }

        private void FrmNotificaciones_Load(object sender, EventArgs e)
        {
            this.Visible = false;
            this.BringToFront();
        }

        private void FrmNotificaciones_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timerClose_Tick(object sender, EventArgs e)
        {
            switch (this.action)
            {
                case enmAction.wait:
                    timerClose.Interval = 5000;
                    action = enmAction.close;
                    break;
                
                case enmAction.start:
                    timerClose.Interval = 1;
                    this.Opacity += 0.1;
                    if(this.x < this.Location.X)
                    {
                        this.Left--;
                    }
                    else
                    {
                        if(this.Opacity == 1.0)
                        {
                            action = enmAction.wait;
                        }
                    }
                    break;
               
                case enmAction.close:
                    timerClose.Interval = 1;
                    this.Opacity -= 0.1;
                    this.Left -= 3;
                    if (base.Opacity == 0.0)
                    {
                        base.Close();
                    }
                    break;
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            timerClose.Interval = 1;
            action = enmAction.close;
        }

    }
}
