﻿using Lib_pdv_uth_v1;
using Lib_pdv_uth_v1.clientes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace pdv_uth_v1
{
    public partial class FrmCatalogoCliente : Form
    {
        //instanciar timer
        private Timer ti;
        //vars para acciones CRUD
        Cliente cli = new Cliente();
        int idCliente = 0;

        public static int idParaPersona;
        //bandera boton guardar credito/cliente
        bool banGuardar = false;

        public FrmCatalogoCliente()
        {
            ti = new Timer();
            ti.Tick += new EventHandler(eventTimer);
            InitializeComponent();
            ti.Enabled = true;
        }
        private void eventTimer(object ob, EventArgs e)
        {
            DateTime today = DateTime.Now;
            lblReloj.Text = today.ToString("hh:mm:ss tt");

        }

        private void FrmCatalogoCliente_Load(object sender, EventArgs e)
        {
            if (FrmLogin.usuario.TipoUsuario.ToString() == "CAJERO")
            {
                label2.Visible = false;
                btnCajas.Visible = false;
            }
            else
            {
                label2.Visible = true;
                btnCajas.Visible = true;
            }
            btnPersonasAlternativas.Enabled = false;
            lblUserName.Text =  FrmLogin.personName;
            //cargar el formulario al que se le va enviar el id
            lblUserName.Text = FrmLogin.personName;
            FrmPersonaSecundaria Form2 = new FrmPersonaSecundaria();
            Form2.Activate();
            //Desabilitamos el formulario
            limpiarForm();
            //Mostrar los regristros del DataGrid
            mostrarRegistrosEnDG();
        }

        public void mostrarRegistrosEnDG()
        {
                dgClientes.DataSource = null;
                //borrar todos los ren del DG 
                dgClientes.Rows.Clear();
                //se crea lista de criterios de busqueda
                List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
                //se crea objeto de crioterio para busqueda
                CriteriosBusqueda criterio = new CriteriosBusqueda();
                //se asignan los datos del criterio del WHERE
                criterio.campo = " 1 ";
                criterio.operadorIntermedio = OperadorDeConsulta.IGUAL;
                criterio.valor = "1";
                criterio.operadorFinal = OperadorDeConsulta.NINGUNO;
                //se incluye el criterio en la lista de criterios
                criterios.Add(criterio);
                //se ejecuta la consulta y se asigna el resultado al DtaGrid
                dgClientes.DataSource = cli.consultar(criterios);
                //se refresca el dataGrid para mostrar los datos
                dgClientes.Refresh();
            }
        private void btnGuardarCliente_Click(object sender, EventArgs e)
        {
            if (banGuardar == false)
            {
                habilitarFrom();
                Cliente paraAlta = new Cliente();
                paraAlta.Nombre = txtNombre.Text;
                paraAlta.ApellidoPaterno = txtApPat.Text;
                paraAlta.ApellidoMaterno = txtApMat.Text;
                paraAlta.Celular = txtCelular.Text;
                paraAlta.Telefono = txtTelefono.Text;
                paraAlta.Correo = txtCorreo.Text;
                paraAlta.FechaNacimiento = dtpFechaNacimiento.Value;
                paraAlta.Domicilio.calle = txtCalle.Text;
                paraAlta.Domicilio.numero = txtNumCasa.Text;
                paraAlta.Domicilio.colonia = txtColonia.Text;
                paraAlta.Domicilio.seccionFraccionamiento = txtFraccionamiento.Text;
                paraAlta.Domicilio.codigoPostal = txtCP.Text;
                paraAlta.Domicilio.localidad = txtLocalidad.Text;
                paraAlta.Domicilio.municipio = txtMunicipio.Text;
                paraAlta.Domicilio.fotoComprobante = lblImagenAGuardar.Text;
                paraAlta.ComprobanteINE = lblImagenAGuardar2.Text;
                paraAlta.Curp = txtCurp.Text;
                paraAlta.CurpCompro = lblImagenAGuardar1.Text;
                if (paraAlta.alta())
                {
                    // obtener el dir de la app
                    string bin = System.IO.Path.GetDirectoryName(Application.StartupPath);
                    //ir a un dir arriba
                    string dir = System.IO.Path.GetDirectoryName(bin);
                    //agregar el path para guardar la imagen
                    dir += "\\Imagenes\\Clientes\\";
                    if (File.Exists(dir) == true)
                    {
                        //guardar la imagen
                        picComprobantes.Image.Save(dir + lblImagenAGuardar.Text);
                        pnlImgCURP.Image.Save(dir + lblImagenAGuardar2.Text);
                        PnlImgINE.Image.Save(dir + lblImagenAGuardar1.Text);
                    }
                    MessageBox.Show("registro correcto");
                    limpiarForm();
                    mostrarRegistrosEnDG();
                }
                else
                    MessageBox.Show("Errror al guardar cliente. " + Cliente.msgError);
            }
            else
            {
                FrmCreditos frm = new FrmCreditos();
                frm.ShowDialog();
                btnAñadirClientes.Text = "         Guardar      Crédito";
            }
        }


        private void btnEliminarclientes_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Realmente desea eliminar este cliente?", "Borrar cliente", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (cli.eliminar(idCliente))
                {
                    MessageBox.Show("Cliente eliminado ");

                }
                else MessageBox.Show("Error, Cliente no fue eliminado ");
            }
            btnGuardarCliente.Text = "         Guardar      Crédito";
            banGuardar = false;
            mostrarRegistrosEnDG();
        }

        private void btnApagar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Usted desea salir del programa?",
                                "Cerrando programa",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void dgClientes_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                idCliente = int.Parse(dgClientes.Rows[e.RowIndex].Cells[0].Value.ToString());
                txtNombre.Text = dgClientes.Rows[e.RowIndex].Cells[1].Value.ToString();
                txtApPat.Text = dgClientes.Rows[e.RowIndex].Cells[2].Value.ToString();
                txtApMat.Text = dgClientes.Rows[e.RowIndex].Cells[3].Value.ToString();
                dtpFechaNacimiento.Value = DateTime.Parse(dgClientes.Rows[e.RowIndex].Cells[4].Value.ToString());
                txtCorreo.Text = dgClientes.Rows[e.RowIndex].Cells[7].Value.ToString();
                txtCelular.Text = dgClientes.Rows[e.RowIndex].Cells[5].Value.ToString();
                txtTelefono.Text = dgClientes.Rows[e.RowIndex].Cells[6].Value.ToString();
                txtCalle.Text = dgClientes.Rows[e.RowIndex].Cells[8].Value.ToString();
                txtNumCasa.Text = dgClientes.Rows[e.RowIndex].Cells[9].Value.ToString();
                txtCP.Text = dgClientes.Rows[e.RowIndex].Cells[10].Value.ToString();
                txtColonia.Text = dgClientes.Rows[e.RowIndex].Cells[11].Value.ToString();
                txtFraccionamiento.Text = dgClientes.Rows[e.RowIndex].Cells[12].Value.ToString();
                txtLocalidad.Text = dgClientes.Rows[e.RowIndex].Cells[13].Value.ToString();
                txtMunicipio.Text = dgClientes.Rows[e.RowIndex].Cells[14].Value.ToString();
                lblImagenAGuardar.Text = dgClientes.Rows[e.RowIndex].Cells[15].Value.ToString();
                lblImagenAGuardar2.Text = dgClientes.Rows[e.RowIndex].Cells[16].Value.ToString();
                txtCurp.Text = dgClientes.Rows[e.RowIndex].Cells[17].Value.ToString();
                lblImagenAGuardar1.Text = dgClientes.Rows[e.RowIndex].Cells[18].Value.ToString();
            }
            btnPersonasAlternativas.Enabled = true;
            btnGuardarCliente.Text = "         Guardar      Crédito";
            banGuardar = true;
            idParaPersona = idCliente ;

        }
        private void btnModificarCliente_Click(object sender, EventArgs e)
        {
            //Datos Esta tomando el ultimo dato y lo sustituye en el sig add.
            habilitarFrom();
            Cliente paraModif = new Cliente();
            List<DatosParaActualizar> datos = new List<DatosParaActualizar>();

            datos.Add(new DatosParaActualizar("nombre", txtNombre.Text));
            datos.Add(new DatosParaActualizar("apellido_paterno", txtApPat.Text));
            datos.Add(new DatosParaActualizar("apellido_materno", txtApMat.Text));
            datos.Add(new DatosParaActualizar("fecha_de_nacimiento", dtpFechaNacimiento.Value.Year + "-" + dtpFechaNacimiento.Value.Month + "-" + dtpFechaNacimiento.Value.Day));
            datos.Add(new DatosParaActualizar("celular", txtCelular.Text));
            datos.Add(new DatosParaActualizar("telefono", txtTelefono.Text));
            datos.Add(new DatosParaActualizar("correo", txtCorreo.Text));
            datos.Add(new DatosParaActualizar("calle", txtCalle.Text));
            datos.Add(new DatosParaActualizar("numero_casa", txtNumCasa.Text));
            datos.Add(new DatosParaActualizar("codigo_postal", txtCP.Text));
            datos.Add(new DatosParaActualizar("colonia", txtColonia.Text));
            datos.Add(new DatosParaActualizar("fraccionamiento", txtFraccionamiento.Text));
            datos.Add(new DatosParaActualizar("localidad", txtLocalidad.Text));
            datos.Add(new DatosParaActualizar("municipio", txtMunicipio.Text));
            datos.Add(new DatosParaActualizar("img_comprobante_domicilio", lblImagenAGuardar.Text));
            datos.Add(new DatosParaActualizar("ine_comprobante", lblImagenAGuardar2.Text));
            datos.Add(new DatosParaActualizar("curp", txtCurp.Text));
            datos.Add(new DatosParaActualizar("curp_comprobante", lblImagenAGuardar1.Text));

            if (paraModif.modificar(datos, idCliente))
            {
                MessageBox.Show("cliente modificado");
            }
            else MessageBox.Show("Error cliente no modificado");
            mostrarRegistrosEnDG();

            banGuardar = false;
            btnGuardarCliente.Text = "         Guardar      Crédito";
        }


        private void btnExpandirMn_Click(object sender, EventArgs e)
        {
            if (pnlNavbar.Width == 269)
            {
                //Asignar el tamaño del menu
                pnlNavbar.Width = 69;
                //Quitar bordes al contraer menu
                btnHome.FlatAppearance.BorderSize = 0;
                btnProductos.FlatAppearance.BorderSize = 0;
                btnCreditos.FlatAppearance.BorderSize = 0;
                btnPersonaAlt.FlatAppearance.BorderSize = 0;
                btnCajas.FlatAppearance.BorderSize = 0;
                //ocultar imagen
                ipbLogo.Visible = false;
            }
            //activar bordes al expandir menu
            else
            {
                pnlNavbar.Width = 269;
                btnHome.FlatAppearance.BorderSize = 2;
                btnProductos.FlatAppearance.BorderSize = 2;
                btnCreditos.FlatAppearance.BorderSize = 2;
                btnPersonaAlt.FlatAppearance.BorderSize = 2;
                btnCajas.FlatAppearance.BorderSize = 2;
                ipbLogo.Visible = true;
            }
        }

        /*        private void abrirFormPanel(object formHijo)
                {
                    if (this.PanelData.Controls.Count>0)
                    {
                        this.PanelData.Controls.RemoveAt(0);
                        Form fh = formHijo as Form;
                        fh.TopLevel = false;
                        fh.Dock = DockStyle.Fill;
                        this.PanelData.Controls.Add(nfh);
                        this.PanelData.Tag = fh;
                        fh.Show();
                    }
                }*/

        //TODO: usar con show dialog
        private void btnPersonasAlternativas_Click(object sender, EventArgs e)
        {
            //frm.ShowDialog();  
            FrmPersonaSecundaria frm = new FrmPersonaSecundaria();
            frm.ShowDialog();
            btnGuardarCliente.Text = "         Guardar      Crédito";
            banGuardar = false;
        }
        //TODO CREAR METODO EN WINMOV 
        public void habilitarFrom()
        {
            //habilitar forms
            txtNombre.Enabled = txtApPat.Enabled = txtApMat.Enabled = dtpFechaNacimiento.Enabled = txtCorreo.Enabled =
            txtCelular.Enabled = txtTelefono.Enabled = txtCalle.Enabled = txtNumCasa.Enabled =
            txtCP.Enabled = txtColonia.Enabled = txtFraccionamiento.Enabled = txtLocalidad.Enabled = txtMunicipio.Enabled =
            txtComprobanteDomicilio.Enabled = txtComproINE.Enabled = txtCurp.Enabled = txtComprobanteCurp.Enabled = true;
            //change colors txt
            txtNombre.BackColor = txtApPat.BackColor = txtApMat.BackColor = txtCorreo.BackColor = txtCelular.BackColor =
            txtTelefono.BackColor = txtCalle.BackColor = txtNumCasa.BackColor = txtCP.BackColor = txtColonia.BackColor =
            txtFraccionamiento.BackColor = txtLocalidad.BackColor = txtMunicipio.BackColor = txtComprobanteDomicilio.BackColor =
            txtComproINE.BackColor = txtCurp.BackColor = txtComprobanteCurp.BackColor = Color.White;
        }
        public void limpiarForm()
        {
            //desabilitamos el form
            txtNombre.Enabled = txtApPat.Enabled = txtApMat.Enabled = dtpFechaNacimiento.Enabled = txtCorreo.Enabled =
        txtCelular.Enabled = txtTelefono.Enabled = txtCalle.Enabled = txtNumCasa.Enabled =
            txtCP.Enabled = txtColonia.Enabled = txtFraccionamiento.Enabled = txtLocalidad.Enabled = txtMunicipio.Enabled =
            txtComprobanteDomicilio.Enabled = txtComproINE.Enabled = txtCurp.Enabled = txtComprobanteCurp.Enabled = false;
            //limpiamos el form
            txtNombre.Text = txtApPat.Text = txtApMat.Text = txtCorreo.Text = txtCelular.Text =
            txtTelefono.Text = txtCalle.Text = txtNumCasa.Text = txtCP.Text = txtColonia.Text =
            txtFraccionamiento.Text = txtLocalidad.Text = txtMunicipio.Text = txtComprobanteDomicilio.Text =
            txtComproINE.Text = txtCurp.Text = txtComprobanteCurp.Text = "";
            // cambiar el color de fondo
            txtNombre.BackColor = txtApPat.BackColor = txtApMat.BackColor = txtCorreo.BackColor = txtCelular.BackColor =
            txtTelefono.BackColor = txtCalle.BackColor = txtNumCasa.BackColor = txtCP.BackColor = txtColonia.BackColor =
            txtFraccionamiento.BackColor     = txtLocalidad.BackColor = txtMunicipio.BackColor = txtComprobanteDomicilio.BackColor =
            txtComproINE.BackColor = txtCurp.BackColor = txtComprobanteCurp.BackColor = Color.DarkGray;

        }
        private void btnAñadirClientes_Click(object sender, EventArgs e)
        {
            habilitarFrom();
        }
        private void btnHome_Click(object sender, EventArgs e)
        {
            if (FrmLogin.usuario.TipoUsuario.ToString() == "CAJERO")
            {
                this.Hide();
                FrmCaja frm = new FrmCaja();
                frm.Show();

            }
            else
            {
                this.Hide();
                FrmMenuPpal frm = new FrmMenuPpal();
                frm.Show();

            }
        }
        private void btnVentas_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmVentas frm = new FrmVentas();
            frm.Show();
        }
        private void btnProductos_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmCatalogoProductos frm = new FrmCatalogoProductos();
            frm.Show();
        }
        private void btnCreditos_Click(object sender, EventArgs e)
        {            
            FrmCreditos frm = new FrmCreditos();
            frm.Show();

        }
        private void btnCompras_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmCompras frm = new FrmCompras();
            frm.Show();
        }

        private void btnCajas_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmCaja frm = new FrmCaja();
            frm.Show();

        }
        /*private void guardarImg(string nombreArchivo, PictureBox picBox)
        {
            //obtener el dir
            *//*string bin = System.IO.Path.GetDirectoryName(Application.StartupPath)
            string dir = System.IO*//*
        }*/


        //TODO Aplicar abstraccion en estas cosas de las imagenes
        //TODO METER EN WINMOV
        //Filtros de extenciones de imagenes
        public void tiposDeImagen()
        {
            openFileDialogComprobantes.Filter = "Image Files|*.png;";
            openFileDialogComprobantes.Filter = "Bitmap Image (.bmp)|*.bmp|Gif Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg|JPG Image (.jpg)|*.jpg|JFIF Image (.jfif)|*.jfif|Png Image (.png)|*.png|Tiff Image (.tiff)|*.tiff";
            openFileDialogComprobantes.Title = "Guardar Imagen Comprobante de Cliente";
            openFileDialogComprobantes.DefaultExt = ".png";
        }
        //guardar imagen
        private void btnImgCDom_Click(object sender, EventArgs e)
        {
            tiposDeImagen();
            //si se abre imagen, se toma archivo
            if (openFileDialogComprobantes.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = openFileDialogComprobantes.FileName;
                //se carga archivo por su nombre al picBox
                picComprobantes.Image = Image.FromFile(openFileDialogComprobantes.FileName);
                //se toma el nmbre original
                lblImagenOriginal.Text = openFileDialogComprobantes.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension = lblImagenOriginal.Text.Substring(lblImagenOriginal.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblImagenAGuardar.Text = string.Format(@"Dom_{0}." + txtNombre.Text + extension, DateTime.Now.Ticks);
            }
        }
        //guardar imagen
        private void btnImgComINE_Click(object sender, EventArgs e)
        {
            tiposDeImagen();
            //Var para asignar nombre

            //si se abre imagen, se toma archivo
            if (openFileDialogComprobantes.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = openFileDialogComprobantes.FileName;
                //se carga archivo por su nombre al picBox
                PnlImgINE.Image = Image.FromFile(openFileDialogComprobantes.FileName);
                //se toma el nmbre original
                lblImagenOriginal2.Text = openFileDialogComprobantes.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension1 = lblImagenOriginal2.Text.Substring(lblImagenOriginal2.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension1 = extension1.ToLower() == "peg" ? "jpeg" : extension1.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblImagenAGuardar2.Text = string.Format(txtNombre.Text + @"INE_{0}." + txtApPat.Text + extension1, DateTime.Now.Ticks);
            }
        }
        //guardar imagen
        private void btnImgCCurp_Click(object sender, EventArgs e)
        {
            tiposDeImagen();
            //si se abre imagen, se toma archivo
            if (openFileDialogComprobantes.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = openFileDialogComprobantes.FileName;
                //se carga archivo por su nombre al picBox
                pnlImgCURP.Image = Image.FromFile(openFileDialogComprobantes.FileName);
                //se toma el nmbre original
                lblImagenOriginal1.Text = openFileDialogComprobantes.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension2 = lblImagenOriginal1.Text.Substring(lblImagenOriginal1.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension2 = extension2.ToLower() == "peg" ? "jpeg" : extension2.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblImagenAGuardar1.Text = string.Format(@"CURP_{0}." + txtCurp.Text + extension2, DateTime.Now.Ticks);
            }
        }

        private void pnlMostarPicDom_MouseHover(object sender, EventArgs e)
        {
            //visibilidad en on y mostrar el componente
            picComprobantes.Visible = true;
            picComprobantes.Show();
            //Ocultar el datagrid
            dgClientes.Hide();
        }

        private void pnlMostarPicDom_MouseMove(object sender, MouseEventArgs e)
        {
            //visibilidad en on y mostrar el componente
            picComprobantes.Visible = true;
            picComprobantes.Show();
            //Ocultar el datagrid
            dgClientes.Hide();
        }

        private void pnlMostarPicDom_MouseLeave(object sender, EventArgs e)
        {
            txtComproINE.Text = lblImagenAGuardar.Text;
            //Ocultar las imagenes
            picComprobantes.Visible = false;
            picComprobantes.Hide();
            //mostrar el datagrid de nuevo
            dgClientes.Show();
        }

        private void pnlMostrarINE_MouseHover(object sender, EventArgs e)
        {
            PnlImgINE.Visible = true;
            PnlImgINE.Show();
            //mostrar el datagrid de nuevo
            dgClientes.Hide();
        }

        private void pnlMostrarINE_MouseLeave(object sender, EventArgs e)
        {
            txtComproINE.Text = lblImagenAGuardar1.Text;
            PnlImgINE.Visible = false;
            PnlImgINE.Hide();
            //mostrar el datagrid de nuevo
            dgClientes.Show();
        }

        private void pnlMostrarINE_MouseMove(object sender, MouseEventArgs e)
        {
            PnlImgINE.Visible = true;
            PnlImgINE.Show();
            //mostrar el datagrid de nuevo
            dgClientes.Hide();
        }
        private void pnlMostrarCURP_MouseHover(object sender, EventArgs e)
        {
            pnlImgCURP.Visible = true;
            pnlImgCURP.Show();
            //mostrar el datagrid de nuevo
            dgClientes.Hide();
        }

        private void pnlMostrarCURP_MouseLeave(object sender, EventArgs e)
        {
            txtComprobanteCurp.Text = lblImagenAGuardar2.Text;
            pnlImgCURP.Visible = false;
            pnlImgCURP.Hide();
            //mostrar el datagrid de nuevo
            dgClientes.Show();
        }

        private void pnlMostrarCURP_MouseMove(object sender, MouseEventArgs e)
        {
            pnlImgCURP.Visible = true;
            pnlImgCURP.Show();
            //mostrar el datagrid de nuevo
            dgClientes.Hide();
        }
        public void mostrarResConsulta()
        {

            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = "nombre ";
            criterio.operadorIntermedio = OperadorDeConsulta.LIKE;
            criterio.valor = "'%" + txtBusqueda.Text + "%'";
            criterio.operadorFinal = OperadorDeConsulta.OR;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            CriteriosBusqueda criterio2 = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio2.campo = "apellido_paterno";
            criterio2.operadorIntermedio = OperadorDeConsulta.LIKE;
            criterio2.valor = "'%" + txtBusqueda.Text + "%'";
            criterio2.operadorFinal = OperadorDeConsulta.OR;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio2);
            CriteriosBusqueda criterio3 = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio3.campo = "apellido_materno";
            criterio3.operadorIntermedio = OperadorDeConsulta.LIKE;
            criterio3.valor = "'%" + txtBusqueda.Text + "%'";
            criterio3.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio3);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dgClientes.DataSource = cli.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dgClientes.Update();
            dgClientes.Refresh();
        }
        private void PicBuscar_Click(object sender, EventArgs e)
        {
            if (txtBusqueda.Text == "")
                mostrarRegistrosEnDG();
            else
                mostrarResConsulta();

        }

        private void picComprobantes_Click(object sender, EventArgs e)
        {

        }

        private void txtBusqueda_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txtBusqueda.Text == "")
                    mostrarRegistrosEnDG();
                
                else
                    mostrarResConsulta();
            }
        }

        private void btnPersonaAlt_Click(object sender, EventArgs e)
        {
            FrmPersonaSecundaria frm = new FrmPersonaSecundaria();
            frm.ShowDialog();
        }
        /*Pasar dtos de un form  a otro
* private void labelCompartida_Click(object sender, EventArgs e)
{
labelCompartida.Text = FrmPersonaSecundaria.txtAGuardarDom;
}*/
    }
}
