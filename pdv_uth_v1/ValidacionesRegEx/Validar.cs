﻿using System.Text.RegularExpressions;
namespace ValidacionesRegEx
{
    public class Validar
    {
        /// <summary>
        /// Valida nombre personal que incluya mayusculas, acentos, Ñ, espacios
        /// </summary>
        /// <param name="cad">La cadena a validar</param>
        /// <returns>true si cumple, false si no cumple</returns>
        public static bool nombrePersonal(string cad)
        {
            return Regex.Match(cad, @"^[A-Z][a-zA-ZÀ-ÿ\u00f1\u00d1]+\s?([A-Z][a-zA-ZÀ-ÿ\u00f1\u00d1]+\s?)*$") != Match.Empty;
        }

        /// <summary>
        /// Valida nombrede cosas que incluya mayusculas, acentos, Ñ, espacios y nùmeros
        /// </summary>
        /// <param name="cad">La cadena a validar</param>
        /// <returns>true si cumple, false si no cumple</returns>
        public static bool nombreCosa(string cad)
        {
            return Regex.Match(cad, @"^[A-Z0-9][a-zA-ZÀ-ÿ\u00f1\u00d10-9]\s([a-zA-ZÀ-ÿ\u00f1\u00d10-9]+\s*)*$") != Match.Empty;
        }

        /// <summary>
        /// Valida correo electronico con dominio que puede incluir multiples extenciones como (.edu.mx)
        /// </summary>
        /// <param name="cad">La cadena a validar</param>
        /// <returns>true si cumple, false si no cumple</returns>
        public static bool correoElectronico(string cad)
        {
            return Regex.Match(cad, @"^[a-z][a-z0-9]{3,18}@([a-z][a-z0-9]{3,18}(\.[a-z]{3,4})+)$") != Match.Empty;
        }

        /// <summary>
        /// Valida nombre personal que incluya mayusculas, acentos, Ñ, espacios
        /// </summary>
        /// <param name="cad">La cadena a validar</param>
        /// <returns>true si cumple, false si no cumple</returns>
        public static bool codigoPostal(string cad)
        {
            return Regex.Match(cad, @"^83[0-9]{3}$") != Match.Empty;
        }

        /// <summary>
        /// Valida nombre personal que incluya mayusculas, acentos, Ñ, espacios
        /// </summary>
        /// <param name="cad">La cadena a validar</param>
        /// <returns>true si cumple, false si no cumple</returns>
        public static bool telefono(string cad)
        {
            //+52 6622001809
            return Regex.Match(cad, @"^[0-9]{2,3}[0-9]{7,8}$") != Match.Empty;
        }


    }
}
