-- MySQL dump 10.13  Distrib 8.0.19, for macos10.15 (x86_64)
--
-- Host: 127.0.0.1    Database: pdv_uth_bd_v2
-- ------------------------------------------------------
-- Server version	5.6.47-cll-lve

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cajas`
--

DROP TABLE IF EXISTS `cajas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cajas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cajas`
--

LOCK TABLES `cajas` WRITE;
/*!40000 ALTER TABLE `cajas` DISABLE KEYS */;
INSERT INTO `cajas` VALUES (1,'CAJA#1');
/*!40000 ALTER TABLE `cajas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido_paterno` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido_materno` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fecha_de_nacimiento` date NOT NULL,
  `celular` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `correo` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `calle` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `numero_casa` varchar(6) COLLATE utf8_spanish2_ci NOT NULL,
  `codigo_postal` varchar(5) COLLATE utf8_spanish2_ci NOT NULL,
  `colonia` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `fraccionamiento` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `localidad` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `municipio` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `img_comprobante_domicilio` text COLLATE utf8_spanish2_ci NOT NULL,
  `ine_comprobante` text COLLATE utf8_spanish2_ci NOT NULL,
  `curp` varchar(18) COLLATE utf8_spanish2_ci NOT NULL,
  `curp_comprobante` text COLLATE utf8_spanish2_ci,
  `imagen_cliente` text COLLATE utf8_spanish2_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (2,'Rogelio','Romero','Sanches','1996-05-11','1234567890',NULL,'chino@gmail.com','guasave','61','83280','emiliano zapata',NULL,'hillo','Hermosillo','qwerty.img','asdf.img','rosralgo','zxcv.img',NULL);
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creditos`
--

DROP TABLE IF EXISTS `creditos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `creditos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `saldo` double NOT NULL,
  `fecha_apertura` date NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `estado` enum('APERTURA','OPERANDO','CONGELADO','SALDADO') COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_saldado` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_creditos_a_clientes_idx` (`cliente_id`),
  CONSTRAINT `fk_creditos_a_clientes` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creditos`
--

LOCK TABLES `creditos` WRITE;
/*!40000 ALTER TABLE `creditos` DISABLE KEYS */;
/*!40000 ALTER TABLE `creditos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `establecimiento`
--

DROP TABLE IF EXISTS `establecimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `establecimiento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(40) COLLATE utf8_spanish2_ci NOT NULL,
  `hora_inicio` time NOT NULL,
  `hora_fin` time NOT NULL,
  `calle` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `numero_casa` varchar(6) COLLATE utf8_spanish2_ci NOT NULL,
  `codigo_postal` varchar(5) COLLATE utf8_spanish2_ci NOT NULL,
  `colonia` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `fraccionamiento` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `localidad` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `municipio` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `caja_1_id` int(11) NOT NULL,
  `caja_2_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_caja1_idx` (`caja_2_id`,`caja_1_id`),
  KEY `fk_caja1` (`caja_1_id`),
  CONSTRAINT `fk_caja1` FOREIGN KEY (`caja_1_id`) REFERENCES `cajas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `establecimiento`
--

LOCK TABLES `establecimiento` WRITE;
/*!40000 ALTER TABLE `establecimiento` DISABLE KEYS */;
INSERT INTO `establecimiento` VALUES (1,'Abarrotes PDV UTH','14:00:00','21:00:00','Blvd. de los Seris Final','0','83100','Parque Industrial','Parque Industrial','Hermosillo','Hermosillo',1,0);
/*!40000 ALTER TABLE `establecimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimientos_de_usuario_log`
--

DROP TABLE IF EXISTS `movimientos_de_usuario_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movimientos_de_usuario_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usuario_id` int(11) NOT NULL,
  `tipo_de_movimiento` enum('APERTURA-CAJA','CORTE-CAJA','RETIRO','ABRIR-CAJON','COBRAR-CREDITO','CREAR-USUARIO','ACTIVAR-CLIENTE','DESACTIVAR-CLIENTE','CREAR-PRODUCTO') COLLATE utf8_spanish2_ci NOT NULL,
  `observacion` text COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_log_a_usuarios_idx` (`usuario_id`),
  CONSTRAINT `fk_log_a_usuarios` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimientos_de_usuario_log`
--

LOCK TABLES `movimientos_de_usuario_log` WRITE;
/*!40000 ALTER TABLE `movimientos_de_usuario_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `movimientos_de_usuario_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operacion_cajas`
--

DROP TABLE IF EXISTS `operacion_cajas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `operacion_cajas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL,
  `caja_id` int(11) DEFAULT NULL,
  `fondo` double DEFAULT '500',
  `observacion` text,
  `tipo_operacion` enum('APERTURA','CORTE','RETIRO','ABRIR_CAJON','REIMPRIMIR_TICKET','CONFIG') CHARACTER SET utf8 DEFAULT NULL,
  `fecha_hr` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_usuario_id_idx` (`usuario_id`),
  KEY `fk_caja_id_idx` (`caja_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operacion_cajas`
--

LOCK TABLES `operacion_cajas` WRITE;
/*!40000 ALTER TABLE `operacion_cajas` DISABLE KEYS */;
INSERT INTO `operacion_cajas` VALUES (1,2,0,500,NULL,'APERTURA','2020-04-01'),(2,2,0,500,NULL,'APERTURA','2020-04-02');
/*!40000 ALTER TABLE `operacion_cajas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona_alternativas`
--

DROP TABLE IF EXISTS `persona_alternativas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `persona_alternativas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido_paterno` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido_materno` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fecha_de_nacimiento` date NOT NULL,
  `celular` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `correo` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `calle` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `numero_casa` varchar(6) COLLATE utf8_spanish2_ci NOT NULL,
  `codigo_postal` varchar(5) COLLATE utf8_spanish2_ci NOT NULL,
  `colonia` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `fraccionamiento` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `localidad` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `municipio` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `img_comprobante_domicilio` text COLLATE utf8_spanish2_ci NOT NULL,
  `ine_comprobante` text COLLATE utf8_spanish2_ci NOT NULL,
  `curp` varchar(18) COLLATE utf8_spanish2_ci NOT NULL,
  `curp_comprobante` text COLLATE utf8_spanish2_ci NOT NULL,
  `cliente_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_personas_alternativas_a_clientes_idx` (`cliente_id`),
  CONSTRAINT `fk_personas_alternativas_a_clientes` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona_alternativas`
--

LOCK TABLES `persona_alternativas` WRITE;
/*!40000 ALTER TABLE `persona_alternativas` DISABLE KEYS */;
/*!40000 ALTER TABLE `persona_alternativas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish2_ci,
  `precio` double NOT NULL,
  `codigo_barras` varchar(13) COLLATE utf8_spanish2_ci NOT NULL,
  `imagen_producto` text COLLATE utf8_spanish2_ci,
  `unidad_medida` enum('UNIDAD','LITRO','KILO','CAJA','PAQUETE') COLLATE utf8_spanish2_ci NOT NULL,
  `es_perecedero` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES (4,'Emoji producto','prodcuto de emoji verde',66.99,'7501212121211','prod_637210154431814251.jpeg','UNIDAD',_binary '\0'),(5,'Cloralex 1lt','Cloralex 1lt de knsdlfsjkhdf',88.21,'7501212121212','prod_637212787490747539.jpeg','UNIDAD',_binary '\0'),(10,'caja registradora','Caja registradora para punto de venta, de color amarillo',9000,'7508908908901','prod_637213308243392445.png','UNIDAD',_binary '\0'),(11,'Torundasde algodón','Bolsa con torundas de algodón para uso médico de 100gms',66.9,'7504545454545','prod_637213535294033903.png','UNIDAD',_binary ''),(12,'Ruffles 120gr','Bolsa ruffles 120gr. sabor queso',26,'1295482315483','Nombre Original','UNIDAD',_binary '');
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido_paterno` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido_materno` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fecha_de_nacimiento` date NOT NULL,
  `celular` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `correo` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `calle` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `numero_casa` varchar(6) COLLATE utf8_spanish2_ci NOT NULL,
  `codigo_postal` varchar(5) COLLATE utf8_spanish2_ci NOT NULL,
  `colonia` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `fraccionamiento` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `localidad` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `municipio` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `img_comprobante_domicilio` text COLLATE utf8_spanish2_ci NOT NULL,
  `ine_comprobante` text COLLATE utf8_spanish2_ci NOT NULL,
  `curp` varchar(18) COLLATE utf8_spanish2_ci NOT NULL,
  `curp_comprobante` text COLLATE utf8_spanish2_ci NOT NULL,
  `acta_nacimiento` text COLLATE utf8_spanish2_ci NOT NULL,
  `certificado_estudios` text COLLATE utf8_spanish2_ci NOT NULL,
  `numero_seguro_social` varchar(11) COLLATE utf8_spanish2_ci NOT NULL,
  `tipo_usuario` enum('ADMINISTRADOR','CAJERO') COLLATE utf8_spanish2_ci NOT NULL,
  `pwd` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'SUPER','ADMIN','ISTRADOR','1990-01-01','6621379254','66220000','admin@gmail.com','privada admin','22','83290','Admin','Admin','Hermosillo','Hermosillo','admin.jpg','admin.jpg','LORA790310HSRPML03','curp_admin.jpg','acta_admin.jpg','admin.jpg','00220022','ADMINISTRADOR','123123123'),(2,'CAJERO','CAJA','RO','1990-01-02','6622000000','662210000','cajero@gmail.com','caja','1','83000','Cajeros','Caja','Hermosillo','Hermosillo','cajeros.jpg','cajero.jpg','CAJE20202020202020','caja.jpg','acta_caja.jpg','caje.jpg','00220202','CAJERO','123123123');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ventas`
--

DROP TABLE IF EXISTS `ventas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ventas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `folio` int(11) unsigned zerofill NOT NULL,
  `caja_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `fecha_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `total_venta` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ventas_a_cajas_idx` (`caja_id`),
  CONSTRAINT `fk_ventas_a_cajas` FOREIGN KEY (`caja_id`) REFERENCES `cajas` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ventas`
--

LOCK TABLES `ventas` WRITE;
/*!40000 ALTER TABLE `ventas` DISABLE KEYS */;
INSERT INTO `ventas` VALUES (1,00000000001,1,1,'2020-03-26 16:57:17',0),(2,00000000002,1,1,'2020-03-26 06:00:00',0),(5,00000000003,1,1,'2020-03-26 06:00:00',88.48),(10,00000000004,1,2,'2020-04-01 06:00:00',200.7),(11,00000000005,1,2,'2020-04-02 06:00:00',36200.97);
/*!40000 ALTER TABLE `ventas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ventas_con_tarjetas`
--

DROP TABLE IF EXISTS `ventas_con_tarjetas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ventas_con_tarjetas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `venta_id` int(11) NOT NULL,
  `vigencia_mes` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `vigencia_anio` varchar(4) COLLATE utf8_spanish2_ci NOT NULL,
  `numero_autorizacion` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_ventas_con_tarjetas_a_ventas_idx` (`venta_id`),
  CONSTRAINT `fk_ventas_con_tarjetas_a_ventas` FOREIGN KEY (`venta_id`) REFERENCES `ventas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ventas_con_tarjetas`
--

LOCK TABLES `ventas_con_tarjetas` WRITE;
/*!40000 ALTER TABLE `ventas_con_tarjetas` DISABLE KEYS */;
/*!40000 ALTER TABLE `ventas_con_tarjetas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ventas_detalles`
--

DROP TABLE IF EXISTS `ventas_detalles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ventas_detalles` (
  `venta_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  `cantidad` float(11,3) NOT NULL,
  KEY `fk_ventas_detalles_ventas_idx` (`venta_id`),
  KEY `fk_ventas_detalles_productos_idx` (`producto_id`),
  CONSTRAINT `fk_ventas_detalles_productos` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ventas_detalles_ventas` FOREIGN KEY (`venta_id`) REFERENCES `ventas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ventas_detalles`
--

LOCK TABLES `ventas_detalles` WRITE;
/*!40000 ALTER TABLE `ventas_detalles` DISABLE KEYS */;
INSERT INTO `ventas_detalles` VALUES (10,11,3),(11,4,3),(11,10,4);
/*!40000 ALTER TABLE `ventas_detalles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-07 11:02:22
